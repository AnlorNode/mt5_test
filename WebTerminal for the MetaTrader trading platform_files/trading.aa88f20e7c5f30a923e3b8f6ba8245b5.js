var tradingWidgetForm = function () {
  function l ( n ) {
    n = n || window.event; Class( m, "active", !0 ); var d = b.height.value, e = b.width.value; /%/.test( d ) ? d = parseInt( d ) + "%" : ( d = parseInt( d ), d = isNaN( d ) || "" === d ? "100%" : d + "px" ); /%/.test( e ) ? e = parseInt( e ) + "%" : ( e = parseInt( e ), e = isNaN( e ) || "" === e ? "100%" : e + "px" ); var f = [], g = "", a = b.version.value; f.push( "version: " + a ); g += "version=" + a; var c = b.restrict_servers; a = b.servers; c = c.checked; a.disabled = !c; Class( a.parentNode.previousElementSibling, "disabled", c ); if ( c && ( a = a.value, a = a.split( "," ),
      0 < a.length && "" !== a[0] ) ) { g += "&amp;servers="; var l = "["; c = 0; for ( var k = a.length; c < k; c++ )l += '"' + a[c] + '"' + ( k - 1 === c ? "" : "," ), g += a[c] + ( k - 1 === c ? "" : "," ); f.push( "servers: " + ( l + "]" ) ) } a = b.login.value; "" !== a && ( f.push( 'login: "' + a + '"' ), g += "&amp;login=" + a ); a = b.trade_server.value; "" !== a && ( f.push( 'server: "' + a + '"' ), g += "&amp;trade_server=" + a ); b.demo_all_servers.checked && ( f.push( "demoAllServers: " + b.demo_all_servers.checked ), g += "&amp;demo_all_servers=1" ); b.demo_show_phone.checked && ( f.push( "demoAllowPhone: " + b.demo_show_phone.checked ),
        g += "&amp;demo_show_phone=1" ); a = b.utm_campaign.value; "" !== a && f.push( 'utmCampaign: "' + a + '"' ); a = b.utm_source.value; "" !== a && f.push( 'utmSource: "' + a + '"' ); f.push( 'startMode: "' + b.startup_mode.value + '"' ); g += "&amp;startup_mode=" + b.startup_mode.value; f.push( 'language: "' + b.lang.value + '"' ); g += "&amp;language=" + b.lang.value; f.push( 'colorScheme: "' + b.color_scheme.value + '"' ); g += "&amp;color_scheme=" + b.color_scheme.value; a = '<div id="webterminal" style="width:' + e + ";height:" + d + ';"></div>\r\n<script type="text/javascript" src="https://trade.mql5.com/trade/widget.js">\x3c/script>\r\n<script type="text/javascript">\r\n';
    a += '    new MetaTraderWebTerminal( "webterminal", {\r\n'; c = 0; for ( k = f.length; c < k; c++ )a += "        " + f[c] + ( k - 1 === c ? "" : "," ) + "\r\n"; a += "    } );\r\n"; a += "\x3c/script>\r\n"; p.innerText = a; h && ( h.setAttribute( "src", "https://trade.mql5.com/trade?" + g ), Class( h.parentNode, "trading-widget__preview_autosize", "100%" !== d ), h.style.height = d, h.style.width = e ); if ( n.preventDefault ) n.preventDefault(); else return !1
  } var b, m, h, p; return {
    Init: function () {
      b = document.getElementById( "tradingWidgetForm" ); p = document.getElementById( "codeArea" );
      if ( !p || !b ) return !1; Core.AddHandler( b, "input", l ); ( h = document.getElementById( "previewArea" ) ) && Class( h.parentNode, "trading-widget__preview_autosize" ); l(); m = document.getElementById( "btnCopy" ); if ( !m ) return !1; Core.AddHandler( m, "click", function () {
        var b = document.createElement( "textarea" ); b.value = p.innerText; b.style.position = "fixed"; b.style.left = "9999px"; document.body.appendChild( b ); b.focus(); if ( Core.IsIOS ) {
          var d = document.createRange(); d.selectNodeContents( b ); var e = window.getSelection(); e.removeAllRanges();
          e.addRange( d ); b.setSelectionRange( 0, 999999 )
        } else b.select(); document.execCommand( "copy" ); b.blur(); Class( m, "active" ); document.body.removeChild( b )
      } )
    }
  }
}();
