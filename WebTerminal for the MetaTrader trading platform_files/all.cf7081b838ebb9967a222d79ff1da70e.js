var $jscomp = $jscomp || {};
$jscomp.scope = {};
$jscomp.checkStringArgs = function(a, b, c) {
    if (null == a)
        throw new TypeError("The 'this' value for String.prototype." + c + " must not be null or undefined");
    if (b instanceof RegExp)
        throw new TypeError("First argument to String.prototype." + c + " must not be a regular expression");
    return a + ""
}
;
$jscomp.ASSUME_ES5 = !1;
$jscomp.ASSUME_NO_NATIVE_MAP = !1;
$jscomp.ASSUME_NO_NATIVE_SET = !1;
$jscomp.defineProperty = $jscomp.ASSUME_ES5 || "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, b, c) {
    a != Array.prototype && a != Object.prototype && (a[b] = c.value)
}
;
$jscomp.getGlobal = function(a) {
    return "undefined" != typeof window && window === a ? a : "undefined" != typeof global && null != global ? global : a
}
;
$jscomp.global = $jscomp.getGlobal(this);
$jscomp.polyfill = function(a, b, c, d) {
    if (b) {
        c = $jscomp.global;
        a = a.split(".");
        for (d = 0; d < a.length - 1; d++) {
            var f = a[d];
            f in c || (c[f] = {});
            c = c[f]
        }
        a = a[a.length - 1];
        d = c[a];
        b = b(d);
        b != d && null != b && $jscomp.defineProperty(c, a, {
            configurable: !0,
            writable: !0,
            value: b
        })
    }
}
;
$jscomp.polyfill("String.prototype.startsWith", function(a) {
    return a ? a : function(a, c) {
        var b = $jscomp.checkStringArgs(this, a, "startsWith");
        a += "";
        var f = b.length
          , e = a.length;
        c = Math.max(0, Math.min(c | 0, b.length));
        for (var g = 0; g < e && c < f; )
            if (b[c++] != a[g++])
                return !1;
        return g >= e
    }
}, "es6", "es3");
$jscomp.polyfill("Array.prototype.fill", function(a) {
    return a ? a : function(a, c, d) {
        var b = this.length || 0;
        0 > c && (c = Math.max(0, b + c));
        if (null == d || d > b)
            d = b;
        d = Number(d);
        0 > d && (d = Math.max(0, b + d));
        for (c = Number(c || 0); c < d; c++)
            this[c] = a;
        return this
    }
}, "es6", "es3");
$jscomp.polyfill("String.prototype.endsWith", function(a) {
    return a ? a : function(a, c) {
        var b = $jscomp.checkStringArgs(this, a, "endsWith");
        a += "";
        void 0 === c && (c = b.length);
        c = Math.max(0, Math.min(c | 0, b.length));
        for (var f = a.length; 0 < f && 0 < c; )
            if (b[--c] != a[--f])
                return !1;
        return 0 >= f
    }
}, "es6", "es3");
$jscomp.owns = function(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b)
}
;
$jscomp.polyfill("Object.assign", function(a) {
    return a ? a : function(a, c) {
        for (var b = 1; b < arguments.length; b++) {
            var f = arguments[b];
            if (f)
                for (var e in f)
                    $jscomp.owns(f, e) && (a[e] = f[e])
        }
        return a
    }
}, "es6", "es3");
$jscomp.SYMBOL_PREFIX = "jscomp_symbol_";
$jscomp.initSymbol = function() {
    $jscomp.initSymbol = function() {}
    ;
    $jscomp.global.Symbol || ($jscomp.global.Symbol = $jscomp.Symbol)
}
;
$jscomp.symbolCounter_ = 0;
$jscomp.Symbol = function(a) {
    return $jscomp.SYMBOL_PREFIX + (a || "") + $jscomp.symbolCounter_++
}
;
$jscomp.initSymbolIterator = function() {
    $jscomp.initSymbol();
    var a = $jscomp.global.Symbol.iterator;
    a || (a = $jscomp.global.Symbol.iterator = $jscomp.global.Symbol("iterator"));
    "function" != typeof Array.prototype[a] && $jscomp.defineProperty(Array.prototype, a, {
        configurable: !0,
        writable: !0,
        value: function() {
            return $jscomp.arrayIterator(this)
        }
    });
    $jscomp.initSymbolIterator = function() {}
}
;
$jscomp.arrayIterator = function(a) {
    var b = 0;
    return $jscomp.iteratorPrototype(function() {
        return b < a.length ? {
            done: !1,
            value: a[b++]
        } : {
            done: !0
        }
    })
}
;
$jscomp.iteratorPrototype = function(a) {
    $jscomp.initSymbolIterator();
    a = {
        next: a
    };
    a[$jscomp.global.Symbol.iterator] = function() {
        return this
    }
    ;
    return a
}
;
$jscomp.iteratorFromArray = function(a, b) {
    $jscomp.initSymbolIterator();
    a instanceof String && (a += "");
    var c = 0
      , d = {
        next: function() {
            if (c < a.length) {
                var f = c++;
                return {
                    value: b(f, a[f]),
                    done: !1
                }
            }
            d.next = function() {
                return {
                    done: !0,
                    value: void 0
                }
            }
            ;
            return d.next()
        }
    };
    d[Symbol.iterator] = function() {
        return d
    }
    ;
    return d
}
;
$jscomp.polyfill("Array.prototype.keys", function(a) {
    return a ? a : function() {
        return $jscomp.iteratorFromArray(this, function(a) {
            return a
        })
    }
}, "es6", "es3");
function $(a) {
    return document.getElementById(a)
}
function $$(a) {
    a = document.querySelectorAll(a);
    try {
        return Array.prototype.slice.call(a)
    } catch (d) {
        for (var b = [], c = 0; c < a.length; c++)
            b.push(a[c]);
        return b
    }
}
function Core() {
    var a = this;
    this.getDevice = function() {
        var b = window.navigator.userAgent;
        window.opera && (a.IsPresto = !0);
        -1 != b.indexOf("Gecko/") && (a.IsGecko = !0);
        -1 != b.indexOf("WebKit") && (a.IsWebkit = !0);
        -1 != b.indexOf("MSIE") && (a.IsMSIE = !0);
        -1 != b.indexOf("Trident/") && (a.IsTrident = !0);
        -1 != b.indexOf("Edge") && (a.IsEdge = !0);
        -1 != b.indexOf("rv:11") && (a.IsMSIE11 = !0);
        -1 != b.indexOf("Firefox") && (a.IsFirefox = !0);
        a.IsMobile = -1 != b.indexOf("Windows CE") || -1 != b.indexOf("Windows Mobile") || -1 != b.indexOf("Android") && -1 != b.toLowerCase().indexOf("mobi") || -1 != b.indexOf("Symbian") || -1 != b.indexOf("iPhone");
        a.IsIOS = navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i);
        a.IsPresto && (a.IsGecko = a.IsWebkit = a.IsMSIE = !1);
        a.IsWebkit && (a.IsGecko = !1)
    }
    ;
    this.getDevice();
    this.StopProp = function(a) {
        (a = a || window.event) && a.stopPropagation ? a.stopPropagation() : window.event && window.event.cancelBubble && (window.event.cancelBubble = !0)
    }
    ;
    this.geFileSizeString = function(a) {
        var b = -1;
        do
            a /= 1024,
            b++;
        while (1024 < a);return Math.max(a, .1).toFixed(1) + " kB; MB; GB; TB;PB;EB;ZB;YB".split(";")[b]
    }
    ;
    this.AddHandler = function(a, c, d, f) {
        if (!a)
            return !1;
        a.attachEvent ? a.attachEvent("on" + c, d) : a.addEventListener && a.addEventListener(c, d, f ? f : !1)
    }
    ;
    this.RemoveHandler = function(a, c, d) {
        a.detachEvent ? a.detachEvent("on" + c, d) : a.removeEventListener && a.removeEventListener(c, d, !1)
    }
    ;
    this.Unwrap = function(a) {
        for (var b = a.parentNode; a.firstChild; )
            b.insertBefore(a.firstChild, a);
        b.removeChild(a)
    }
    ;
    this.Clone = function(a) {
        if (null == a || "object" != typeof a)
            return a;
        var b = a.constructor(), d;
        for (d in a)
            a.hasOwnProperty(d) && (b[d] = a[d]);
        return b
    }
    ;
    this.Bind = function(a, c) {
        return function() {
            return a.apply(c, arguments)
        }
    }
    ;
    this.isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i)
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i)
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i)
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i)
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i)
        },
        any: function() {
            return Core.isMobile.Android() || Core.isMobile.BlackBerry() || Core.isMobile.iOS() || Core.isMobile.Opera() || Core.isMobile.Windows()
        }
    };
    this.addAncersToH3 = function() {
        for (var a = document.getElementsByTagName("h3"), c = 0; c < a.length; c++) {
            var d = a[c].getAttribute("id");
            if (d) {
                var f = document.createElement("a");
                f.innerHTML = "#";
                f.setAttribute("href", "#" + d);
                f.className = "anchor";
                a[c].insertBefore(f, null)
            }
        }
    }
    ;
    this.IsTouchDevice = function() {
        return "ontouchstart"in window || 0 < navigator.MaxTouchPoints || 0 < navigator.msMaxTouchPoints
    }
    ;
    this.isLocalStorageSupported = function() {
        try {
            if (!window.localStorage)
                return !1;
            var a = window.localStorage;
            a.setItem("test_local_storage_support", "1");
            a.removeItem("test_local_storage_support");
            return !0
        } catch (c) {
            return !1
        }
    }
    ;
    this.PreventSelection = function() {
        window.getSelection ? window.getSelection().removeAllRanges() : document.selection && document.selection.clear && document.selection.clear()
    }
    ;
    this.SelectAllCheckboxes = function(a, c) {
        var b = document.getElementsByTagName("INPUT");
        if (b && null != b && !(1 > b.length)) {
            a = a.checked;
            for (var f = 0; f < b.length; f++)
                "checkbox" == b[f].type && b[f].name == c && (b[f].checked = a)
        }
    }
    ;
    this.FormatNumber = function(a) {
        a = Math.round(a);
        a = a.toString();
        for (var b = "", d = 0, f = a.length; 0 < f; f--)
            3 === d++ && (b = " " + b,
            d = 1),
            b = a.substring(f, f - 1) + b;
        return b
    }
    ;
    this.utf8_encode = function(a) {
        if (null === a || "undefined" === typeof a)
            return "";
        a += "";
        var b = "", d;
        var f = d = 0;
        var e = a.length;
        for (var g = 0; g < e; g++) {
            var h = a.charCodeAt(g)
              , m = null;
            if (128 > h)
                d++;
            else if (127 < h && 2048 > h)
                m = String.fromCharCode(h >> 6 | 192, h & 63 | 128);
            else if (55296 != (h & 63488))
                m = String.fromCharCode(h >> 12 | 224, h >> 6 & 63 | 128, h & 63 | 128);
            else {
                if (55296 != (h & 64512))
                    return "";
                m = a.charCodeAt(++g);
                if (56320 != (m & 64512))
                    return "";
                h = ((h & 1023) << 10) + (m & 1023) + 65536;
                m = String.fromCharCode(h >> 18 | 240, h >> 12 & 63 | 128, h >> 6 & 63 | 128, h & 63 | 128)
            }
            null !== m && (d > f && (b += a.slice(f, d)),
            b += m,
            f = d = g + 1)
        }
        d > f && (b += a.slice(f, e));
        return b
    }
    ;
    this.Crc32 = function(a) {
        a = this.utf8_encode(a);
        var b = -1;
        for (var d = 0, f = a.length; d < f; d++) {
            var e = (b ^ a.charCodeAt(d)) & 255;
            e = "0x" + "00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D".substr(9 * e, 8);
            b = b >>> 8 ^ e
        }
        return (b ^ -1) >>> 0
    }
    ;
    this.ParseJson = function(a) {
        try {
            return window.JSON ? window.JSON.parse(a) : eval("(" + a + ")")
        } catch (c) {
            return null
        }
    }
}
Core = new Core;
var ieVersion = function() {
    for (var a = 3, b = document.createElement("div"), c = b.getElementsByTagName("i"); b.innerHTML = "\x3c!--[if gt IE " + ++a + "]><i></i><![endif]--\x3e",
    c[0]; )
        ;
    return 4 < a ? a : void 0
}()
  , supportsCssProp = function() {
    var a = document.createElement("div")
      , b = "Khtml Ms O Moz Webkit ms".split(" ");
    return function(c) {
        if (c in a.style)
            return c;
        c = c.replace(/^[a-z]/, function(a) {
            return a.toUpperCase()
        });
        for (var d = b.length; d--; )
            if (b[d] + c in a.style)
                return b[d] + c;
        return !1
    }
}();
function isEmpty(a) {
    if (null == a)
        return !0;
    if (0 < a.length)
        return !1;
    if (0 === a.length)
        return !0;
    for (var b in a)
        if (a.hasOwnProperty(b))
            return !1;
    return !0
}
function getCharCode(a) {
    a = a || window.event;
    return a.which || a.keyCode
}
function getElementsByClass(a, b) {
    if (document.getElementsByClassName)
        return (b || document).getElementsByClassName(a);
    b = b || document;
    b = b.getElementsByTagName("*");
    var c = b.length;
    a = a.split(/\s+/);
    var d = a.length, f = [], e, g;
    for (e = 0; e < c; e++)
        for (g = 0; g < d; g++)
            if (-1 != b[e].className.search("\\b" + a[g] + "\\b")) {
                f.push(b[e]);
                break
            }
    return f
}
function getElementsByTagClass(a, b, c) {
    c = c || document;
    a = c.getElementsByTagName(a);
    c = a.length;
    b = b.split(/\s+/);
    var d = b.length, f = [], e, g;
    for (e = 0; e < c; e++)
        for (g = 0; g < d; g++)
            if (-1 != a[e].className.search("\\b" + b[g] + "\\b")) {
                f.push(a[e]);
                break
            }
    return f
}
function getAncessor(a, b) {
    if (!a || !a.parentElement)
        return a;
    for (; (a = a.parentElement) && !a.classList.contains(b); )
        ;
    return a
}
function setSelectionRange(a, b, c) {
    a.setSelectionRange ? (a.focus(),
    a.setSelectionRange(b, c)) : a.createTextRange && (a = a.createTextRange(),
    a.collapse(!0),
    a.moveEnd("character", c),
    a.moveStart("character", c),
    a.select())
}
function setCursor(a, b) {
    setSelectionRange(a, b, b)
}
[].indexOf || (Array.prototype.indexOf = function(a) {
    for (var b = this.length; b-- && this[b] !== a; )
        ;
    return b
}
);
function typeOf(a) {
    return "object" == typeof a ? a.length ? "array" : "object" : typeof a
}
Array.prototype.forEach || (Array.prototype.forEach = function(a, b) {
    var c = null, d, f = Object(this), e = f.length >>> 0;
    if ("function" !== typeof a)
        throw new TypeError(a + " is not a function");
    1 < arguments.length && (c = b);
    for (d = 0; d < e; ) {
        if (d in f) {
            var g = f[d];
            a.call(c, g, d, f)
        }
        d++
    }
}
);
function S4() {
    return (65536 * (1 + Math.random()) | 0).toString(16).substring(1)
}
function S5() {
    return (1048576 * (1 + Math.random()) | 0).toString(16).substring(1)
}
function guid() {
    return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()
}
Class = function(a, b, c) {
    if (a)
        if (a.className) {
            var d = a.className.split(" "), f;
            var e = 0;
            for (f = d.length; e < f; ++e)
                if (d[e] == b) {
                    c && (d.splice(e, 1),
                    a.className = d.join(" "));
                    return
                }
            c || (d.push(b),
            a.className = d.join(" "))
        } else
            c || (a.className = b)
}
;
function getUrlParam(a) {
    a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    a = (new RegExp("[\\?&]" + a + "=([^&#]*)")).exec(window.location.href);
    return null == a ? "" : a[1]
}
function setUrlParam(a, b) {
    document.location.search = changeUrlParamsString(document.location.search, a, b)
}
function changeUrlParamsString(a, b, c) {
    var d = !1;
    c || (d = !0);
    b = encodeURI(b);
    c = encodeURI(c);
    0 == a.indexOf("?") && (a = a.substr(1));
    a = a.split("&");
    for (var f = a.length, e; f--; )
        if (e = a[f].split("="),
        e[0] == b) {
            if (d) {
                a.splice(f, 1);
                break
            }
            e[1] = c;
            a[f] = e.join("=");
            break
        }
    0 > f && (a[a.length] = [b, c].join("="));
    b = "";
    for (c = 0; c < a.length; c++)
        a[c] && (b = b ? b + "&" : b + "?",
        b += a[c]);
    return b
}
function isHttps() {
    return "https:" == window.location.protocol
}
String.prototype.startsWith = function(a) {
    return this.substring(0, a.length) == a
}
;
String.prototype.trim = function() {
    return this.replace(/^\s\s*/, "").replace(/\s\s*$/, "")
}
;
String.prototype.ltrim = function() {
    return this.replace(/^\s+/, "")
}
;
String.prototype.rtrim = function() {
    return this.replace(/\s+$/, "")
}
;
String.prototype.fulltrim = function() {
    return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, "").replace(/\s+/g, " ")
}
;
function getFileExtension(a, b) {
    if (a && a.length) {
        for (var c = "", d = !1, f = 0; f < a.length; f++) {
            var e = a[f];
            c += e;
            "." == e && (c = "",
            d = !0)
        }
        if (d && c)
            return b ? c.toLowerCase() : c
    }
}
function createElement(a, b, c) {
    b = document.createElement(b);
    if (c)
        for (var d in c)
            c.hasOwnProperty(d) && ("name" == d ? b.setAttribute("name", c[d]) : "className" == d ? b.className = c[d] : -1 == d.indexOf("style") && (b[d] = c[d]));
    a && a.appendChild(b);
    return b
}
function prependElement(a, b) {
    a.childNodes && 0 < a.childNodes.length ? a.insertBefore(b, a.childNodes[0]) : a.appendChild(b)
}
function insertAfter(a, b, c) {
    c && c.nextSibling || a.appendChild(b);
    a.insertBefore(b, c.nextSibling)
}
function injectIframe(a, b) {
    var c = document.createElement("iframe");
    c.marginHeight = "0";
    c.marginWidth = "0";
    c.frameBorder = "0";
    c.scrolling = "no";
    a.appendChild(c);
    c.width = "100%";
    c.height = "100%";
    a = c.document;
    c.contentDocument ? a = c.contentDocument : c.contentWindow && (a = c.contentWindow.document);
    a.open();
    a.writeln(b);
    a.close()
}
function encodeHtml(a) {
    if (!a)
        return a;
    a = a.replace(/&/g, "&amp;");
    a = a.replace(/>/g, "&gt;");
    return a = a.replace(/</g, "&lt;")
}
function stripHtml(a) {
    var b = document.createElement("DIV");
    b.innerHTML = a;
    return b.textContent || b.innerText
}
function GetDate(a) {
    var b = new Date(1E3 * a);
    a = "" + b.getFullYear();
    a += "." + (8 < b.getUTCMonth() ? b.getUTCMonth() + 1 : "0" + (b.getUTCMonth() + 1));
    return a += "." + (9 < b.getUTCDate() ? b.getUTCDate() : "0" + b.getUTCDate())
}
function GetDateLocal(a) {
    var b = new Date(1E3 * a);
    a = "" + b.getFullYear();
    a += "." + (8 < b.getMonth() ? b.getMonth() + 1 : "0" + (b.getMonth() + 1));
    return a += "." + (9 < b.getDate() ? b.getDate() : "0" + b.getDate())
}
function GetDateTime(a) {
    var b = new Date(1E3 * a);
    a = "" + b.getFullYear();
    a += "." + (8 < b.getUTCMonth() ? b.getUTCMonth() + 1 : "0" + (b.getUTCMonth() + 1));
    a += "." + (9 < b.getUTCDate() ? b.getUTCDate() : "0" + b.getUTCDate());
    a += " " + (9 < b.getUTCHours() ? b.getUTCHours() : "0" + b.getUTCHours());
    return a += ":" + (9 < b.getUTCMinutes() ? b.getUTCMinutes() : "0" + b.getUTCMinutes())
}
function GetDateTimeLocal(a) {
    var b = new Date(1E3 * a);
    a = "" + b.getFullYear();
    a += "." + (8 < b.getMonth() ? b.getMonth() + 1 : "0" + (b.getMonth() + 1));
    a += "." + (9 < b.getDate() ? b.getDate() : "0" + b.getDate());
    a += " " + (9 < b.getHours() ? b.getHours() : "0" + b.getHours());
    return a += ":" + (9 < b.getMinutes() ? b.getMinutes() : "0" + b.getMinutes())
}
function objectEquals(a, b) {
    for (var c in a)
        if (a.hasOwnProperty(c) && (!b.hasOwnProperty(c) || a[c] != b[c]))
            return !1;
    for (c in b)
        if (b.hasOwnProperty(c) && (!a.hasOwnProperty(c) || a[c] != b[c]))
            return !1;
    return !0
}
function toggleBlock(a) {
    a && ("none" == a.style.display ? a.style.display = a.oldDisplay ? a.oldDisplay : "block" : (a.oldDisplay = a.style.display,
    a.style.display = "none"))
}
function toggle(a) {
    toggleBlock($(a))
}
function toggleVisibility(a) {
    if (a = $(a))
        a.style.visibility = "hidden" == a.style.visibility ? "visible" : "hidden"
}
function getFirstChildElement(a) {
    if (!a && 1 != a.nodeType)
        return null;
    for (a = a.firstChild; a && 1 != a.nodeType; )
        a = a.nextSibling;
    return a
}
function setHotKeysPaginator(a, b, c) {
    a = "" == a ? document : document.getElementById(a);
    var d = a.onkeydown;
    a.onkeydown = function(a) {
        d && d(a);
        a = window.event ? window.event : a;
        var e = a.target ? a.target : window.event.srcElement;
        a.ctrlKey && "INPUT" != e.tagName && "TEXTAREA" != e.tagName && (37 == a.keyCode && "" != b ? location.href = b : 39 == a.keyCode && "" != c && (location.href = c))
    }
}
function HashparamsContainer() {
    this.prototype = Object;
    var a = []
      , b = []
      , c = 0
      , d = ""
      , f = function() {
        b = [];
        a = [];
        c = location.href.indexOf("#!");
        if (-1 != c && location.href.length != c + 2) {
            var e = location.href.substring(c + 2)
              , f = e.indexOf("#");
            -1 != f && (d = e.substring(f),
            e = e.substring(0, f));
            e = e.split("&");
            for (f = 0; f < e.length; f++) {
                var m = e[f].split("=");
                a.push(decodeURIComponent(m[0]));
                1 == m.length ? b.push(null) : b.push(decodeURIComponent(m[1]))
            }
        }
    };
    f();
    window.addEventListener && window.addEventListener("hashchange", f, !1);
    var e = function() {
        for (var c = "", d = 0; d < a.length; d++)
            0 < c.length && (c += "&"),
            c += encodeURIComponent(a[d]),
            b[d] && 0 < b[d].toString().length && (c += "=" + encodeURIComponent(b[d].toString()));
        return c
    };
    this.Get = function(c) {
        c = a.indexOf(c);
        return -1 == c ? null : b[c]
    }
    ;
    this.GetDefaultHash = function() {
        return d
    }
    ;
    this.Set = function(c, d) {
        var f = a.indexOf(c);
        -1 == f ? (a.push(c),
        b.push(d)) : b[f] = d;
        document.location = document.location.pathname + ("search"in document.location && document.location.search ? document.location.search : "") + "#!" + e()
    }
    ;
    this.Remove = function(c) {
        c = a.indexOf(c);
        -1 != c && (b.splice(c, 1),
        a.splice(c, 1),
        (c = e()) ? document.location = document.location.pathname + ("search"in document.location && document.location.search ? document.location.search : "") + "#!" + c : document.location.hash = "")
    }
    ;
    this.Clear = function() {
        b = [];
        a = [];
        document.location = document.location.pathname + ("search"in document.location && document.location.search ? document.location.search : "") + "#"
    }
    ;
    this.Count = function() {
        return a.length
    }
}
window.hashParams = new HashparamsContainer;
function getWindowSize() {
    var a = 0
      , b = 0;
    "number" == typeof window.innerWidth ? (a = window.innerWidth,
    b = window.innerHeight) : document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight) ? (a = document.documentElement.clientWidth,
    b = document.documentElement.clientHeight) : document.body && (document.body.clientWidth || document.body.clientHeight) && (a = document.body.clientWidth,
    b = document.body.clientHeight);
    return {
        width: a,
        height: b
    }
}
function getDocumentBodySize() {
    var a = 0
      , b = 0;
    "number" == typeof (document.body && document.body.scrollWidth) ? (a = document.body.scrollWidth,
    b = document.body.scrollHeight) : document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight) ? (a = document.documentElement.clientWidth,
    b = document.documentElement.clientHeight) : document.body && (document.body.clientWidth || document.body.clientHeight) && (a = document.body.clientWidth,
    b = document.body.clientHeight);
    return {
        width: a,
        height: b
    }
}
function getWindowScroll() {
    var a = 0
      , b = 0;
    "number" == typeof window.pageYOffset ? (b = window.pageYOffset,
    a = window.pageXOffset) : document.body && (document.body.scrollLeft || document.body.scrollTop) ? (b = document.body.scrollTop,
    a = document.body.scrollLeft) : document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop) && (b = document.documentElement.scrollTop,
    a = document.documentElement.scrollLeft);
    return {
        top: b,
        left: a
    }
}
function getOffsetSum(a) {
    for (var b = 0, c = 0; a; )
        b += parseFloat(a.offsetTop),
        c += parseFloat(a.offsetLeft),
        a = a.offsetParent;
    return {
        top: Math.round(b),
        left: Math.round(c)
    }
}
function getOffsetRect(a, b) {
    b = b ? b.contentWindow : window;
    a = a.getBoundingClientRect();
    var c = b.document.body
      , d = b.document.documentElement;
    return {
        top: Math.round(a.top + (b.pageYOffset || d.scrollTop || c.scrollTop) - (d.clientTop || c.clientTop || 0)),
        left: Math.round(a.left + (b.pageXOffset || d.scrollLeft || c.scrollLeft) - (d.clientLeft || c.clientLeft || 0)),
        bottom: Math.round(a.bottom)
    }
}
function getOffset(a) {
    return a.getBoundingClientRect ? getOffsetRect(a) : getOffsetSum(a)
}
function getElementSize(a) {
    return {
        width: a.clientWidth || a.offsetWidth,
        height: a.clientHeight || a.offsetHeight
    }
}
function dateToString(a, b, c) {
    var d = a.getDate()
      , f = a.getMonth() + 1;
    d = "" + a.getFullYear() + "-" + (9 >= f ? "0" + f : f) + "-" + (9 >= d ? "0" + d : d);
    b && (b = a.getHours(),
    f = a.getMinutes(),
    d += " " + (9 >= b ? "0" + b : b) + ":" + (9 >= f ? "0" + f : f),
    c && (a = a.getSeconds(),
    d += ":" + (9 >= a ? "0" + a : a)));
    return d
}
function tooltip(a, b, c, d, f) {
    a.timeoutID && clearTimeout(a.timeoutID);
    var e = S4()
      , g = createElement(document.body, "div", {
        id: "tooltip_" + e,
        className: "customTooltip"
    });
    e = createElement(g, "div", {
        id: "tooltipContent_" + e,
        className: "customTooltipContent"
    });
    var h;
    supportsCssProp("transform") && (h = createElement(g, "span", {
        className: "tooltipCorner"
    }));
    g.hovered = !1;
    g.onmouseover = function() {
        this.hovered = !0
    }
    ;
    g.onmouseout = function() {
        var b = this;
        b.hovered = !1;
        setTimeout(function() {
            if (!0 !== b.hovered)
                try {
                    d && (a.show = !1,
                    d(b)),
                    $(b.id) && document.body.removeChild(b)
                } catch (p) {
                    window.console && window.console.log && window.console.log(p.message)
                }
        }, 300)
    }
    ;
    "string" == typeOf(b) ? e.innerHTML += b : "object" == typeOf(b) && e.appendChild(b);
    e.getElementsByTagName("a");
    var m = getOffset(a);
    m.realLeft = m.left;
    a.tooltipContainer = g;
    a.onmouseout = function() {
        clearTimeout(a.timeoutID);
        setTimeout(function() {
            if (1 != g.hovered)
                try {
                    d && (a.show = !1,
                    d(g)),
                    document.body.removeChild(g)
                } catch (l) {
                    window.console && window.console.log && window.console.log(l.message)
                }
        }, 300)
    }
    ;
    a.hide = function() {
        clearTimeout(a.timeoutID);
        setTimeout(function() {
            try {
                d && (a.show = !1,
                d(g)),
                document.body.removeChild(g)
            } catch (l) {
                window.console && window.console.log && window.console.log(l.message)
            }
        }, 0)
    }
    ;
    a.onclick = function() {
        !0 === a.show ? a.hide() : tooltip(a, b, c, d, f)
    }
    ;
    a.timeoutID = setTimeout(function() {
        g.style.display = "block";
        g.style.top = m.top + 25 + "px";
        g.style.maxWidth = document.body.offsetWidth - 10 + "px";
        var b = g.style
          , d = m.left - .2 * g.clientWidth;
        d = Math.max(10, d);
        d = Math.min(d, document.body.offsetWidth - 5 - g.clientWidth);
        b.left = d + "px";
        g.offsetHeight + m.top > document.body.offsetHeight && (g.style.top = m.top - 20 - g.offsetHeight + "px",
        Class(h, "bottomCorner"));
        h && (b = h.style,
        d = a.clientWidth / 2 + m.left - g.offsetLeft - 7,
        d = Math.min(g.clientWidth - 15, d),
        d = Math.max(5, d),
        b.left = d + "px");
        setOpacity(g, .01);
        fade(g, 1, 200, 20);
        c && (a.show = !0,
        c(g))
    }, f || 500);
    return e.id
}
function setOpacity(a, b) {
    a.style.opacity = b;
    a.style.MozOpacity = b;
    a.style.KhtmlOpacity = b;
    a.style.filter = "alpha(opacity=" + 100 * b + ");"
}
function animate(a, b, c, d, f, e, g, h, m, l) {
    b = supportsCssProp(b) || b;
    if (d != f) {
        0 != m && m || (m = 1E-7);
        l || (l = 0);
        var p = supportsCssProp("transition");
        if (p) {
            var t = {
                marginLeft: "margin-left",
                marginRight: "margin-right",
                paggingLeft: "pagging-left",
                paggingRight: "pagging-right"
            }[b] || b;
            a.style[p] = "none";
            a.style[b] = d + c;
            setTimeout(function() {
                a.style[p] = t + " " + g / 1E3 + "s " + (m && 0 != m ? " ease" : " linear") + (0 < e ? " " + e / 1E3 + "s" : "");
                a.style[b] = f + c
            }, 1);
            setTimeout(function() {
                a.style[p] = "none"
            }, g + e + 10)
        } else
            for (var n = 0, r = f - d, q = r != Math.abs(r), u = n = 0; u <= h; u++)
                setTimeout(function() {
                    var e = r * Math.pow(n / r, 1 / m);
                    0 != l && (e += Math.sin(Math.PI * n / r) * n * l);
                    a.style[b] = e + d + c;
                    n = 1 == q ? Math.max(n + Math.floor(r / h), r) : Math.min(n + Math.ceil(r / h), r)
                }, u / h * g + e)
    }
}
function fade(a, b, c, d) {
    if ("in" == b || 0 == b)
        b = 0;
    else if ("out" == b || 1 == b)
        b = 1;
    else
        return;
    for (var f = 0, e = 0; 1 >= e; e += 1 / d)
        setTimeout(function() {
            setOpacity(a, 1 == b ? f : 1 - f);
            f += 1 / d;
            f = Math.round(100 * f) / 100
        }, e * c);
    setTimeout(function() {
        setOpacity(a, 1 == b ? 1 : 0)
    }, c)
}
function apearBlockVertical(a, b, c, d, f, e) {
    e || (e = .5);
    f || (f = 20);
    d || (d = 200);
    var g = createElement(null, "div");
    g.style.overflow = "hidden";
    g.style.height = 0;
    g.style.visibility = "hidden";
    setOpacity(a, 0);
    g.appendChild(a);
    a.style.display = "block";
    1 == c ? prependElement(b, g) : c && c.tagName ? b.insertBefore(g, c) : b.appendChild(g);
    animate(g, "height", "px", 0, g.scrollHeight, 0, d, f, e);
    setTimeout(function() {
        g.style.display = "none";
        b.insertBefore(a, g);
        b.removeChild(g);
        fade(a, "out", 200, 20)
    }, d)
}
function disapearBlockVertical(a, b, c, d, f) {
    f || (f = .5);
    d || (d = 20);
    c || (c = 200);
    var e = a.parentNode
      , g = createElement(null, "div");
    g.style.overflow = "hidden";
    e.insertBefore(g, a);
    g.appendChild(a);
    var h = g.scrollHeight;
    g.style.height = h + "px";
    fade(a, "in", 200, 20);
    setTimeout(function() {
        animate(g, "height", "px", h, 0, 0, c, d, f)
    }, 200);
    setTimeout(function() {
        b || (a.style.display = "none",
        e.insertBefore(a, g));
        g.parentNode.removeChild(g)
    }, c + 500)
}
window.isWindowActive = !1;
window.isLastActiveWindow = function() {
    return !1
}
;
(function() {
    function a(a) {
        var c = window.isWindowActive;
        a = a || window.event;
        window.isWindowActive = "focus" == a.type || "focusin" == a.type ? !0 : "blur" == a.type || "focusout" == a.type ? !1 : document[f] ? !1 : !0;
        if (c != window.isWindowActive && (a = window.isWindowActive,
        mqGlobal && mqGlobal._onvisibility))
            for (c = 0; c < mqGlobal._onvisibility.length; c++)
                mqGlobal._onvisibility[c](a);
        window.isWindowActive && b(h);
        extraLog("change: " + h + ": " + window.isWindowActive)
    }
    function b(a) {
        c && d.setItem("mql5com_isLastActiveWindow", a + "");
        extraLog(a + ": become ACTIVE")
    }
    try {
        var c = "undefined" !== typeof Storage && Core.isLocalStorageSupported() && window.JSON && window.JSON.stringify ? !0 : !1
    } catch (m) {
        c = !1,
        window.console && window.console.log && console.log("local storage support check: " + m)
    }
    var d = {};
    if (c)
        try {
            d = localStorage
        } catch (m) {
            d = sessionStorage
        }
    var f, e = {
        hidden: "visibilitychange",
        mozHidden: "mozvisibilitychange",
        webkitHidden: "webkitvisibilitychange",
        msHidden: "msvisibilitychange",
        oHidden: "ovisibilitychange"
    };
    for (f in e)
        if (e.hasOwnProperty(f) && f in document) {
            var g = e[f];
            break
        }
    g ? (window.isWindowActive = document[f] ? !1 : !0,
    document.addEventListener(g, a)) : ieVersion && 9 >= ieVersion ? document.onfocusin = document.onfocusout = a : window.onfocus = window.onblur = a;
    var h = guid();
    c && (window.isLastActiveWindow = function() {
        var a = c ? d.getItem("mql5com_isLastActiveWindow") || null : null;
        return a == h
    }
    ,
    window.GetCurrentWindowId = function() {
        return h
    }
    ,
    window.isWindowActive && b(h));
    extraLog(f + " load: " + h + ": " + window.isWindowActive)
}
)();
function extraLog(a) {
    window.enableExtraLog && (a = createElement(null, "div", {
        innerHTML: a
    }),
    prependElement(document.body, a))
}
(function() {
    function a(a) {
        var b = $(a)
          , c = $("__srte_" + a);
        if (b && c) {
            var f = function(a) {
                return function(b) {
                    d.Flush(a)
                }
            };
            b = function(a) {
                return function(a) {
                    (a = a || window.event) && a.preventDefault && a.preventDefault();
                    a && a.stopPropagation && a.stopPropagation();
                    a && a.cancelBubble && (a.cancelBubble = !0);
                    return !1
                }
            }
            ;
            c.onkeyup = c.onChange = function() {
                f(a)()
            }
            ;
            if (c.addEventListener) {
                for (var m = "copy paste keypress input textInput blur change DOMNodeInserted".split(" "), l = ["drop"], p = 0; p < m.length; p++)
                    c.addEventListener(m[p], f(a), !0);
                for (m = 0; m < l.length; m++)
                    c.addEventListener(l[m], b(a), !0)
            }
        }
    }
    var b = "contentEditable"in document.documentElement
      , c = navigator.userAgent;
    0 <= c.indexOf("Android") && 2.3 >= parseFloat(c.slice(c.indexOf("Android") + 8)) && (b = !1);
    if (b) {
        var d = window.SmallRTE = {};
        d.Clear = function(a) {
            var b = $(a)
              , c = "<br>";
            b.placeholder && (c = b.placeholder);
            d.SetContent(a, c)
        }
        ;
        d.SetContent = function(a, b) {
            var c = $(a);
            if (a = $("__srte_" + a)) {
                a.innerHTML = b;
                var d = a.lastChild;
                (d = d ? d.className : null) && 0 <= d.indexOf("fquote") && (b += "<br>",
                a.innerHTML = b)
            }
            c && (c.value = b)
        }
        ;
        d.GetContent = function(a) {
            if (a = $(a)) {
                var b = a.value;
                return b !== a.placeholder ? b : ""
            }
            return null
        }
        ;
        d.isEmpty = function(a) {
            (a = d.GetContent(a)) && (a = a.replace(/<(?!img|embed|iframe)[^>]+>|&nbsp;/gi, ""));
            return !a
        }
        ;
        d.GetEditor = function(a) {
            return $("__srte_" + a)
        }
        ;
        d.Flush = function(a) {
            var b = $(a);
            a = $("__srte_" + a);
            b && a && (b.value = a.innerHTML)
        }
        ;
        d.Focus = function(a) {
            var b = $("__srte_" + a);
            b && (b.focus(),
            document.createRange ? (a = document.createRange(),
            a.selectNodeContents(b),
            a.collapse(!1),
            b = window.getSelection(),
            b.removeAllRanges(),
            b.addRange(a)) : document.selection && (a = document.body.createTextRange(),
            a.moveToElementText(b),
            a.collapse(!1),
            a.select()))
        }
        ;
        d.Init = function() {
            if (window.SimpleRte)
                for (var b = 0; b < window.SimpleRte.length; b++) {
                    var c = window.SimpleRte[b]
                      , g = $(c.id)
                      , h = createElement(null, "div", {
                        id: "__srte_" + c.id,
                        contentEditable: "true",
                        className: "smallEditor"
                    });
                    h.style.width = g.style.width;
                    h.style.height = g.style.height;
                    insertAfter(g.parentNode, h, g);
                    g.style.display = "none";
                    a(c.id);
                    d.SetContent(g.id, g.value);
                    !c.placeholder || g.value && "<br>" !== g.value ? d.SetContent(g.id, g.value) : (h.innerHTML = c.placeholder,
                    g.placeholder = c.placeholder,
                    Core.AddHandler(h, "focus", function() {
                        if (h.innerHTML == c.placeholder && (h.innerHTML = "",
                        document.createRange)) {
                            var a = document.createRange();
                            a.selectNodeContents(h);
                            a.collapse(!1);
                            var b = window.getSelection();
                            b.removeAllRanges();
                            b.addRange(a)
                        }
                    }),
                    Core.AddHandler(h, "blur", function() {
                        "" == h.innerHTML.replace("<br>", "") && (h.innerHTML = c.placeholder)
                    }))
                }
        }
    }
}
)();
(function() {
    var a = window.MQTE = {}
      , b = window.MQRTE
      , c = window.SmallRTE;
    mqGlobal.AddOnReady(function() {
        b && b.Init(window);
        c && c.Init()
    });
    a.Focus = function(a) {
        b && b.Editor(a) ? b.Focus(a) : c && c.GetEditor(a) ? c.Focus(a) : (a = $(a)) && a.focus && a.focus()
    }
    ;
    a.AddFocusHandler = function(a, c) {
        b && b.Editor(a) && b.AddFocusHandler(a, c)
    }
    ;
    a.GetContent = function(a) {
        return b && b.Editor(a) ? b.GetContent(a) : c && c.GetEditor(a) ? c.GetContent(a) : (a = $(a)) ? a.value : null
    }
    ;
    a.SetContent = function(a, f) {
        if (b && b.Editor(a))
            b.SetContent(a, f);
        else if (c && c.GetEditor(a))
            c.SetContent(a, f);
        else if (a = $(a))
            a.value = f
    }
    ;
    a.InsertContent = function(a, f, e) {
        if (b && b.Editor(a))
            b.InsertContent(a, f, e);
        else if (c && c.GetEditor(a))
            c.SetContent(a, f);
        else if (a = $(a))
            a.value = f
    }
    ;
    a.Clear = function(a) {
        if (b && b.Editor(a))
            b.Clear(a);
        else if (c && c.GetEditor(a))
            c.Clear(a);
        else if (a = $(a))
            a.value = a.innerHTML = ""
    }
    ;
    a.IsEmpty = function(a) {
        return b && b.Editor(a) ? b.isEmpty(a) : c && c.GetEditor(a) ? c.isEmpty(a) : (a = $(a)) ? 0 == a.value.fulltrim().length : !0
    }
    ;
    a.MarkUnchanged = function(a) {
        b && b.Editor(a) && b.MarkUnchanged(a)
    }
    ;
    a.Changed = function(a) {
        return b && b.Editor(a) ? b.Changed(a) : !1
    }
    ;
    a.Highlight = function(a, c) {
        return b ? b.highlight(a, c) : c
    }
    ;
    a.Flush = function(a) {
        b && b.Editor(a) ? b.Flush(a) : c && c.GetEditor(a) && c.Flush(a)
    }
    ;
    a.HotKey = function(a, c, e, g, h, m) {
        b && b.Editor(a) && b.HotKey(a, c, e, g, h, m)
    }
    ;
    a.SetValidationClass = function(a, f) {
        var d;
        if (d = $(a))
            if (b && b.Editor(a))
                for (a = d.parentNode; a; ) {
                    if (f) {
                        if ("dalet-editor" == a.className) {
                            a.className = "dalet-editor field-validation-error";
                            break
                        }
                    } else if (a.className && -1 != a.className.indexOf("field-validation-error")) {
                        d = a.className.split(" ");
                        f = [];
                        for (var g in d)
                            "field-validation-error" != d[g] && f.push(d[g]);
                        a.className = f.join(" ");
                        break
                    }
                    a = a.parentNode
                }
            else if (c && c.GetEditor(a)) {
                if (a = c.GetEditor(a))
                    if (f)
                        "smallEditor" == a.className && (a.className = "smallEditor field-validation-error");
                    else if (a.className && -1 != a.className.indexOf("field-validation-error")) {
                        d = a.className.split(" ");
                        f = [];
                        for (g in d)
                            "field-validation-error" != d[g] && f.push(d[g]);
                        a.className = f.join(" ")
                    }
            } else
                "DIV" == d.parentNode.nodeName && -1 < d.parentNode.className.indexOf("inputWrapper") ? Class(d.parentNode, "field-validation-error", !f) : Class(d, "field-validation-error", !f)
    }
}
)();
function doPost(a, b, c, d) {
    try {
        if (a = a || window.event)
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
        var f = b.indexOf("?");
        if (-1 != f) {
            var e = b.substr(f + 1);
            b = b.substr(0, f);
            var g = e.split("&");
            if (g && 0 < g.length)
                for (d || (d = {}),
                a = 0; a < g.length; a++) {
                    var h = g[a]
                      , m = h.split("=");
                    1 == m.length ? d[h] = "" : d[m[0]] = decodeURIComponent(m[1])
                }
        }
        var l = createElement(document.body, "form", {
            action: b,
            method: "post",
            target: "_top"
        });
        c && createElement(l, "input", {
            name: "__signature",
            type: "hidden",
            value: c
        });
        if (d)
            for (var p in d)
                createElement(l, "input", {
                    name: p,
                    type: "hidden",
                    value: d[p]
                });
        l.style.position = "absolute";
        l.style.left = "-5000px";
        l.submit()
    } catch (t) {
        alert("action failed: " + t)
    }
}
function toTitleCase(a) {
    return a ? a.replace(/\w\S*/g, function(a) {
        return a.charAt(0).toUpperCase() + a.substr(1).toLowerCase()
    }) : a
}
function doubleScroll(a) {
    var b = document.createElement("div");
    b.appendChild(document.createElement("div"));
    b.style.overflow = "auto";
    b.style.overflowY = "hidden";
    b.firstChild.style.width = a.scrollWidth + "px";
    var c = window.onresize;
    window.onresize = function() {
        c && c(arguments);
        b.firstChild.style.width = a.scrollWidth + "px"
    }
    ;
    b.firstChild.style.paddingTop = "1px";
    b.onscroll = function() {
        a.scrollLeft = b.scrollLeft
    }
    ;
    a.onscroll = function() {
        b.scrollLeft = a.scrollLeft
    }
    ;
    a.parentNode.insertBefore(b, a)
}
(function() {
    mqGlobal.AddOnReady(function() {
        var a = getElementsByClass("matrix_wrapper");
        if (a && a.length)
            for (var b = 0; b < a.length; b++)
                doubleScroll(a[b])
    })
}
)();
(function() {
    function a(a) {
        return a.replace(/([A-Z])/g, function(a) {
            return "-" + a.toLowerCase()
        })
    }
    var b = "undefined" !== typeof document && document.head && document.head.dataset ? {
        set: function(a, b, f) {
            a.dataset[b] = f
        },
        get: function(a, b) {
            return a.dataset[b]
        },
        del: function(a, b) {
            delete a.dataset[b]
        }
    } : {
        set: function(b, d, f) {
            b.setAttribute("data-" + a(d), f)
        },
        get: function(b, d) {
            return b.getAttribute("data-" + a(d))
        },
        del: function(b, d) {
            b.removeAttribute("data-" + a(d))
        }
    };
    window.dataset = function(a, d, f) {
        function c(c, d) {
            b.set(a, c, d);
            return h
        }
        function g(c) {
            return b.get(a, c)
        }
        var h = {
            set: c,
            get: g,
            del: function(c) {
                b.del(a, c);
                return h
            }
        };
        return 3 === arguments.length ? c(d, f) : 2 === arguments.length ? g(d) : h
    }
}
)();
function dot2num(a) {
    a = a.split(".");
    return 256 * (256 * (256 * +a[0] + +a[1]) + +a[2]) + +a[3]
}
function bytesToSize(a) {
    if (0 == a)
        return "0 Byte";
    var b = parseInt(Math.floor(Math.log(a) / Math.log(1024)));
    return Math.round(a / Math.pow(1024, b), 2) + " " + ["Bytes", "KB", "MB", "GB", "TB"][b]
}
function whichTransitionEvent() {
    var a = document.createElement("span"), b = {
        transition: "transitionend",
        OTransition: "oTransitionEnd",
        MozTransition: "transitionend",
        WebkitTransition: "webkitTransitionEnd"
    }, c;
    for (c in b)
        if (b.hasOwnProperty(c) && void 0 !== a.style[c])
            return b[c]
}
function whichTransform() {
    if (!window.getComputedStyle)
        return !1;
    var a = document.createElement("div")
      , b = null
      , c = {
        webkitTransform: "webkitTransform",
        OTransform: "OTransform",
        msTransform: "msTransform",
        MozTransform: "MozTransform",
        transform: "transform"
    };
    document.body.insertBefore(a, null);
    for (var d in c)
        if (c.hasOwnProperty(d) && void 0 !== a.style[d]) {
            a.style[d] = "translateY(1px)";
            var f = window.getComputedStyle(a).getPropertyValue(c[d]);
            f && 0 < f.length && "none" !== f && (b = c[d])
        }
    document.body.removeChild(a);
    return b
}
function whichFullScreen() {
    for (var a = document.createElement("span"), b = [["requestFullScreen", "cancelFullScreen", "fullScreen", "fullscreeneventchange"], ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitIsFullScreen", "webkitfullscreeneventchange"], ["mozRequestFullScreen", "mozCancelFullScreen", "mozfullScreen", "mozfullscreenchange"]], c = 0, d = b.length; c < d; c++)
        if (a[b[c][0]])
            return b[c]
}
function Deeplink() {
    function a(a, b, c) {
        if (a.addEventListener)
            return a.addEventListener(b, c),
            {
                remove: function() {
                    a.removeEventListener(b, c)
                }
            };
        a.attachEvent(b, c);
        return {
            remove: function() {
                a.detachEvent(b, c)
            }
        }
    }
    function b(a, b) {
        var c = document.createElement("IFRAME");
        c.src = b;
        c.id = "hiddenIframeForOpenDeeplink";
        c.style.display = "none";
        a.appendChild(c);
        return c
    }
    function c() {
        var a = !!window.opera || 0 <= navigator.userAgent.indexOf(" OPR/");
        return {
            isOpera: a,
            isFirefox: "undefined" !== typeof InstallTrigger,
            isSafari: 0 < Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor"),
            isChrome: !!window.chrome && !a,
            isIE: !!document.documentMode
        }
    }
    function d() {
        var a = -1;
        if ("Microsoft Internet Explorer" === navigator.appName) {
            var b = navigator.userAgent;
            var c = /MSIE ([0-9]{1,}[.0-9]{0,})/;
            null != c.exec(b) && (a = parseFloat(RegExp.$1))
        } else
            "Netscape" === navigator.appName && (b = navigator.userAgent,
            c = /Trident\/.*rv:([0-9]{1,}[.0-9]{0,})/,
            null != c.exec(b) && (a = parseFloat(RegExp.$1)));
        return a
    }
    function f(c, d) {
        var e = a(window, "blur", function() {
            clearTimeout(f);
            e.remove()
        })
          , f = setTimeout(function() {
            d();
            e.remove()
        }, 1E3)
          , g = document.querySelector("#hiddenIframeForOpenDeeplink");
        g || (g = b(document.body, "about:blank"));
        g.contentWindow.location.href = c
    }
    function e(b, c) {
        for (var d = window; d !== d.parent; )
            d = d.parent;
        var e = a(d, "blur", function() {
            clearTimeout(f);
            e.remove()
        })
          , f = setTimeout(function() {
            c();
            e.remove()
        }, 1E3);
        window.location = b
    }
    function g(a, c) {
        var d = document.querySelector("#hiddenIframeForOpenDeeplink");
        d || (d = b(document.body, "about:blank"));
        try {
            d.contentWindow.location.href = a
        } catch (q) {
            c()
        }
    }
    function h(a, b) {
        10 === d() ? m(a, b) : 9 === d() || 11 === d() ? f(a, b) : l(a, b)
    }
    function m(a, c) {
        var d = setTimeout(c, 1E3);
        window.addEventListener("blur", function() {
            clearTimeout(d)
        });
        var e = document.querySelector("#hiddenIframeForOpenDeeplink");
        e || (e = b(document.body, "about:blank"));
        try {
            e.contentWindow.location.href = a
        } catch (u) {
            c(),
            clearTimeout(d)
        }
    }
    function l(a, b) {
        var c = window.open("", "", "width=0,height=0");
        c.document.write("<iframe src='" + a + "'></iframe>");
        setTimeout(function() {
            try {
                c.setTimeout("window.close()", 1E3)
            } catch (q) {
                c.close(),
                b()
            }
        }, 1E3)
    }
    function p(a, b) {
        navigator.msLaunchUri(a, function() {}, b)
    }
    this.OpenUri = function(a, b) {
        function d() {
            window.location = b
        }
        if (navigator.msLaunchUri)
            p(a, d);
        else {
            var f = c();
            f.isFirefox ? g(a, d) : f.isChrome ? e(a, d) : f.isIE && h(a, d)
        }
    }
}
Deeplink = new Deeplink;
function setAttributes(a, b) {
    for (var c in b)
        b.hasOwnProperty(c) && a.setAttribute(c, b[c])
}
function arrayDeleteDuplicates(a) {
    for (var b = {}, c = [], d = a.length, f = 0, e = 0; e < d; e++) {
        var g = a[e];
        1 !== b[g] && (b[g] = 1,
        c[f++] = g)
    }
    return c
}
function getUrlParams(a) {
    var b = {};
    if (a = window.history.pushState ? a ? a.split("?")[1] : window.location.search.slice(1) : window.location.hash.substring(1)) {
        a = a.split("&");
        for (var c = 0, d = a.length; c < d; c++) {
            var f = a[c].split("=");
            b[f[0]] = f[1]
        }
        return b
    }
    return !1
}
function urlParamInString(a) {
    var b = window.history.pushState ? "?" : "", c;
    for (c in a)
        a.hasOwnProperty(c) && (b += c + "=" + a[c] + "&");
    return b.substring(0, b.length - 1)
}
function addUrlParams(a) {
    var b = getUrlParams() || {};
    for (c in a)
        a.hasOwnProperty(c) && (b[c] = a[c]);
    var c = urlParamInString(b);
    if (c === window.location.search)
        return !1;
    if (window.history.pushState)
        window.history.pushState(a, document.title, c);
    else if ("undefined" !== typeof window.location.hash)
        window.location.hash = urlParamInString(b);
    else {
        if (window.location.href === c)
            return !0;
        window.location.href = c
    }
    return !0
}
function removeUrlParams(a) {
    var b = getUrlParams();
    if (!b)
        return !1;
    for (var c = 0, d = a.length; c < d; c++)
        delete b[a[c]];
    b = urlParamInString(b);
    if (window.history.pushState)
        window.history.pushState(a, document.title, b);
    else if ("undefined" !== typeof window.location.hash)
        window.location.hash = "";
    else {
        if (window.location.href === b)
            return !0;
        window.location.href = b
    }
    return !0
}
function getPointIntersection(a, b) {
    var c = a[0].y - a[1].y
      , d = a[1].x - a[0].x
      , f = b[0].y - b[1].y
      , e = b[1].x - b[0].x
      , g = c * e - f * d;
    if (0 !== g) {
        var h = a[1].y * a[0].x - a[1].x * a[0].y;
        a = b[1].y * a[0].x - b[1].x * b[0].y;
        return {
            x: (d * a - e * h) / g,
            y: (f * h - c * a) / g
        }
    }
    return !1
}
function getFormatedNum(a) {
    var b = 0 > a;
    a = Math.abs(a);
    if (1E3 > a)
        return (b ? "-" : "") + a.toFixed(0);
    var c = -1;
    do
        a /= 1E3,
        c++;
    while (1E3 < a);return (b ? "-" : "") + Math.max(a, .1).toFixed(1) + ["K", "M", "BN"][c]
}
function trim(a) {
    a = a.replace(/^\s+/, "");
    for (var b = a.length - 1; 0 <= b; b--)
        if (/\S/.test(a.charAt(b))) {
            a = a.substring(0, b + 1);
            break
        }
    return a
}
function generateId(a) {
    a = a || "global";
    this.counterId || (this.counterId = {});
    this.counterId[a] || (this.counterId[a] = 0);
    return this.counterId[a]++
}
;(function() {
    function a() {
        var a = l.shift();
        if (a)
            return a;
        try {
            if (window.XMLHttpRequest)
                a = new XMLHttpRequest;
            else if (window.ActiveXObject)
                try {
                    a = new ActiveXObject("Msxml2.XMLHTTP")
                } catch (n) {
                    a = new ActiveXObject("Microsoft.XMLHTTP")
                }
            p.push(a)
        } catch (n) {
            a = null
        }
        return a
    }
    function b(a) {
        var b, c = {};
        if (!a)
            return c;
        a = a.split("\n");
        for (b = a.length - 1; 0 <= b; --b) {
            var d = a[b].split(":");
            2 == d.length && (c[d[0].toLowerCase().replace(/^\s+|\s+$/g, "")] = d[1].replace(/^\s+|\s+$/g, ""))
        }
        return c
    }
    function c(a, c, d, e, f) {
        var g;
        (g = a.onrequestready) || (g = a.onready);
        var h = a.onendrequest;
        try {
            h && h.call(a, c, d, e, b(f)),
            g && g.call(a, c, d, e, b(f))
        } catch (C) {
            alert(C.message)
        }
    }
    function d(a, d) {
        if (d) {
            try {
                var e = a.status
            } catch (A) {}
            try {
                var f = a.responseText
            } catch (A) {}
            try {
                var g = a.responseXML
            } catch (A) {}
            try {
                var h = a.statusText
            } catch (A) {}
            try {
                var l = a.getAllResponseHeaders()
            } catch (A) {}
            switch (e) {
            case 200:
                if (l) {
                    c(d, f, g, h, l);
                    break
                } else
                    e = 0,
                    h = "";
            default:
                a = e;
                (e = d.onrequesterror) || (e = d.onerror);
                var n = d.onendrequest;
                try {
                    n && n.call(d, f, g, a, b(l)),
                    e && e.call(d, a, f, g, h, b(l))
                } catch (A) {
                    alert(A.message)
                }
            }
        }
    }
    function f(a) {
        var b, c = [];
        if (a instanceof Array) {
            var d = 0;
            for (b = a.length; d < b; ++d)
                c.push([a[d][0], encodeURIComponent(a[d][1])].join("="))
        } else
            for (d in a)
                c.push([d, encodeURIComponent(a[d])].join("="));
        return c.join("&")
    }
    function e(a, b) {
        function c(a, c) {
            d.push("--");
            d.push(b);
            d.push('\r\nContent-Disposition: form-data; name="');
            d.push(a);
            d.push('"');
            c.filename && (d.push(';filename="'),
            d.push(c.filename),
            d.push('"'));
            d.push("\r\n\r\n");
            c.value ? d.push(c.value) : d.push(c);
            d.push("\r\n")
        }
        var d = [], e;
        if (a instanceof Array) {
            var f = 0;
            for (e = a.length; f < e; ++f)
                c(a[f][0], a[f][1])
        } else
            for (f in a)
                c(f, a[f]);
        d.push("--");
        d.push(b);
        d.push("--\r\n");
        return d.join("")
    }
    function g(a, b) {
        var d = a.ownerDocument.createElement("iframe"), e, f = ["upload_frame", Math.random()].join("_");
        (e = d.style).position = "absolute";
        e.left = e.top = "-30000px";
        d.name = f;
        d.id = f;
        a.parentNode.insertBefore(d, a);
        a.target = f;
        (e = a.ownerDocument.parentWindow) || (e = a.ownerDocument.defaultView);
        e.frames[f].name != f && (e.frames[f].name = f);
        var g = !1;
        e = function() {
            if (!(g || this.readyState && "loaded" !== this.readyState && "complete" !== this.readyState)) {
                g = !0;
                d.onload = d.onreadystatechange = null;
                try {
                    if (0 <= navigator.userAgent.indexOf("Opera")) {
                        var a = this;
                        setTimeout(function() {
                            c(b, a.contentWindow.document.body.innerHTML, a.contentWindow.document, null, "")
                        }, 300)
                    } else
                        c(b, this.contentWindow.document.body.innerHTML, this.contentWindow.document, null, "")
                } catch (C) {
                    alert("call_onready error: " + C)
                }
            }
        }
        ;
        0 <= navigator.userAgent.indexOf("Opera") && d.addEventListener("DOMContentLoaded", e);
        d.onload = d.onreadystatechange = e;
        a.submit();
        b && b.onbeginrequest && b.onbeginrequest.call(b, a)
    }
    function h(a) {
        return {
            ajax: a,
            timeout: setTimeout(function() {
                a.abort()
            }, 24E4)
        }
    }
    var m = window.Ajax = {}
      , l = []
      , p = [];
    m.get = function(b, c, e) {
        var g = a()
          , n = h(g);
        if (g) {
            e && e.onbeginrequest && e.onbeginrequest.call(e, g);
            c = f(c);
            var m = -1 == b.indexOf("?") ? "?" : "&";
            g.open("get", c ? [b, c].join(m) : b, !0);
            g.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            g.onreadystatechange = function() {
                switch (g.readyState) {
                case 4:
                    n && (clearTimeout(n.timeout),
                    n = n.ajax = null),
                    d(g, e),
                    g.abort(),
                    l.push(g)
                }
            }
            ;
            try {
                g.send("")
            } catch (v) {
                return !1
            }
            return n
        }
        onerror && onerror()
    }
    ;
    m.post = function(b, c, g, m, p, w) {
        var n = a()
          , t = h(n);
        n.open("post", b, !0);
        w ? n.withCredentials = !0 : n.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        g ? (n.setRequestHeader("Content-Type", "multipart/form-data; boundary=AJAX----FORM"),
        c = e(c, "AJAX----FORM")) : p ? (n.setRequestHeader("Content-Type", "application/json"),
        c = JSON.stringify(c)) : (n.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"),
        c = f(c));
        n.onreadystatechange = function() {
            switch (n.readyState) {
            case 4:
                t && (clearTimeout(t.timeout),
                t = t.ajax = null),
                d(n, m),
                n.abort(),
                l.push(n)
            }
        }
        ;
        m && m.onbeginrequest && m.onbeginrequest.call(m, n);
        try {
            n.send(c)
        } catch (A) {
            return !1
        }
        return t
    }
    ;
    m.form = function(a, b) {
        for (var c = [], d = a.elements, e = 0, f = d.length; e < f; ++e) {
            var h = d[e];
            switch (h.type.toLowerCase()) {
            case "file":
                return g(a, b),
                !1;
            case "text":
            case "email":
            case "tel":
            case "phone":
            case "password":
            case "hidden":
            case "submit":
            case "image":
                h.name && c.push([h.name, h.value]);
                continue;
            case "checkbox":
            case "radio":
                h.name && h.checked && c.push([h.name, h.value ? h.value : "on"]);
                continue
            }
            "TEXTAREA" == h.nodeName && h.name ? c.push([h.name, h.value]) : "SELECT" == h.nodeName && h.name && c.push([h.name, h.value])
        }
        "post" == a.method.toLowerCase() ? m.post(a.action, c, "multipart/form-data" == a.enctype, b) : m.get(a.action, c, b)
    }
    ;
    m.stop = function(a) {
        var b;
        a && (b = a.ajax) && (clearTimeout(a.timeout),
        b.onreadystatechange = null,
        b.abort())
    }
    ;
    m.url = function(a, b) {
        a = a.split("?")[0];
        b = f(b);
        return [a, b].join("?")
    }
    ;
    m.stopAll = function() {
        if (p && p.length)
            for (var a = 0; a < p.length; a++) {
                var b = p[a];
                b && b.abort()
            }
    }
    ;
    Core.AddHandler(window, "beforeunload", m.stopAll)
}
)();
var DataHelper = function() {
    function a(a) {
        for (var b = 0, c = a.length; 0 < c; c--)
            b = 256 * b + a.charCodeAt(c - 1);
        return b
    }
    function b(a) {
        for (var b = "", c = 0; c < a.length; c++) {
            var d = a.charCodeAt(c) & 15
              , e = a.charCodeAt(c) >> 4 & 15;
            b += 10 > e ? String.fromCharCode(e + 48) : String.fromCharCode(e + 55);
            b += 10 > d ? String.fromCharCode(d + 48) : String.fromCharCode(d + 55)
        }
        return b
    }
    function c(a) {
        if (a.length % 2)
            return "";
        for (var b = "", c = 0; c + 1 < a.length; c += 2) {
            var e = d(a.charCodeAt(c + 0))
              , f = d(a.charCodeAt(c + 1));
            b += String.fromCharCode(16 * e + f)
        }
        return b
    }
    function d(a) {
        return 65 <= a && 70 >= a ? a - 55 : 97 <= a && 102 >= a ? a - 87 : 48 <= a && 57 >= a ? a - 48 : 0
    }
    function f(a, b) {
        for (var c = Math.min(a.length, b.length); 0 < c; c--) {
            if (a.charCodeAt(c - 1) < b.charCodeAt(c - 1))
                return -1;
            if (a.charCodeAt(c - 1) > b.charCodeAt(c - 1))
                return 1
        }
        return a.length < b.length ? -1 : a.length > b.length ? 1 : 0
    }
    function e(a, b, c) {
        isFinite(a) || (a = 0);
        isFinite(b) || (b = 0);
        isFinite(c) || (c = 0);
        a /= 60;
        0 > a && (a = 6 - -a % 6);
        a %= 6;
        b = Math.max(0, Math.min(1, b / 100));
        c = Math.max(0, Math.min(1, c / 100));
        var d = (1 - Math.abs(2 * c - 1)) * b;
        var e = d * (1 - Math.abs(a % 2 - 1));
        1 > a ? (a = d,
        b = e,
        e = 0) : 2 > a ? (a = e,
        b = d,
        e = 0) : 3 > a ? (a = 0,
        b = d) : 4 > a ? (a = 0,
        b = e,
        e = d) : 5 > a ? (a = e,
        b = 0,
        e = d) : (a = d,
        b = 0);
        c -= d / 2;
        a = Math.round(255 * (a + c));
        b = Math.round(255 * (b + c));
        e = Math.round(255 * (e + c));
        return [a, b, e]
    }
    return {
        EncodeHTML: function(a) {
            return document.createElement("div").appendChild(document.createTextNode(a)).parentNode.innerHTML
        },
        CompareBinary: f,
        BinToHexLE: function(a) {
            for (var b = "", c = a.length - 1; 0 <= c; c--) {
                var d = a.charCodeAt(c) & 15
                  , e = a.charCodeAt(c) >> 4 & 15;
                b += 10 > e ? String.fromCharCode(e + 48) : String.fromCharCode(e + 55);
                b += 10 > d ? String.fromCharCode(d + 48) : String.fromCharCode(d + 55)
            }
            return b
        },
        BinToHex: b,
        BinToNumber: a,
        HexToUint8: function(a) {
            var b = "" + a.charAt(14);
            b += a.charAt(15);
            b += a.charAt(12);
            b += a.charAt(13);
            b += a.charAt(10);
            b += a.charAt(11);
            b += a.charAt(8);
            b += a.charAt(9);
            b += a.charAt(6);
            b += a.charAt(7);
            b += a.charAt(4);
            b += a.charAt(5);
            b += a.charAt(2);
            b += a.charAt(3);
            b += a.charAt(0);
            b += a.charAt(1);
            b = c(b);
            return 8 != b.length ? void 0 : b
        },
        HexToBin: c,
        HexToBinLE: function(a) {
            if (a.length % 2)
                return "";
            for (var b = "", c = a.length - 2; 0 <= c; c -= 2) {
                var e = d(a.charCodeAt(c + 0))
                  , f = d(a.charCodeAt(c + 1));
                b += String.fromCharCode(16 * e + f)
            }
            return b
        },
        HexToChar: d,
        CompareHex: function(a, b) {
            a = c(a);
            b = c(b);
            return f(a, b)
        },
        HexToNumber: function(b) {
            return a(c(b))
        },
        Uint8ToHex: function(a) {
            a = b(a.split("").reverse().join(""));
            return 16 != a.length ? void 0 : a
        },
        Uint64ToDate: function(a) {
            for (var b = 0, c = 0; c < a.length; c++)
                b = 256 * b + a.charCodeAt(a.length - c - 1);
            return new Date(b / 1E4 - 116444736E5)
        },
        Utf16To8: function(a) {
            for (var b = [], c = 0, d = 0; d < a.length; d++) {
                var e = a.charCodeAt(d);
                128 > e ? b[c++] = String.fromCharCode(e) : (2048 > e ? b[c++] = String.fromCharCode(e >> 6 | 192) : (55296 === (e & 64512) && d + 1 < a.length && 56320 == (a.charCodeAt(d + 1) & 64512) ? (e = 65536 + ((e & 1023) << 10) + (a.charCodeAt(++d) & 1023),
                b[c++] = String.fromCharCode(e >> 18 | 240),
                b[c++] = String.fromCharCode(e >> 12 & 63 | 128)) : b[c++] = String.fromCharCode(e >> 12 | 224),
                b[c++] = String.fromCharCode(e >> 6 & 63 | 128)),
                b[c++] = String.fromCharCode(e & 63 | 128))
            }
            return b.join("")
        },
        Utf8to16: function(a) {
            for (var b, c, d = [], e = 0, f = 0; e < a.length; ) {
                var g = a.charCodeAt(e++);
                if (128 > g)
                    d[f++] = String.fromCharCode(g);
                else if (191 < g && 224 > g)
                    c = a.charCodeAt(e++),
                    d[f++] = String.fromCharCode((g & 31) << 6 | c & 63);
                else if (239 < g && 365 > g) {
                    c = a.charCodeAt(e++);
                    b = a.charCodeAt(e++);
                    var r = a.charCodeAt(e++);
                    b = ((g & 7) << 18 | (c & 63) << 12 | (b & 63) << 6 | r & 63) - 65536;
                    d[f++] = String.fromCharCode(55296 + (b >> 10));
                    d[f++] = String.fromCharCode(56320 + (b & 1023))
                } else
                    c = a.charCodeAt(e++),
                    b = a.charCodeAt(e++),
                    d[f++] = String.fromCharCode((g & 15) << 12 | (c & 63) << 6 | b & 63)
            }
            return d.join("")
        },
        NumberToBin: function(a) {
            for (var b = "", c = 0; 8 > c; c++)
                b += String.fromCharCode(a % 256),
                a = a / 256 | 0;
            return b
        },
        NumToColor: function(a, b, c) {
            return "rgba(" + e(77 * a % 360, (b || 58) + a % 20, (c || 69) + a % 20).join(",") + ",1)"
        },
        Hsl2rgb: e
    }
}();
Array.prototype.max = function() {
    return Math.max.apply(null, this)
}
;
Array.prototype.min = function() {
    return Math.min.apply(null, this)
}
;
var DateHelper = function() {
    function a(a, b) {
        var d = "";
        b || (b = {});
        b = {
            utc: "undefined" !== typeof b.utc ? b.utc : !0,
            short: "undefined" !== typeof b.short ? b.short : !1,
            date: "undefined" !== typeof b.date ? b.date : !0,
            month: "undefined" !== typeof b.month ? b.month : !0,
            year: "undefined" !== typeof b.year ? b.year : !0
        };
        a && (a = b.utc ? {
            date: a.getUTCDate(),
            month: a.getUTCMonth(),
            year: a.getUTCFullYear()
        } : {
            date: a.getDate(),
            month: a.getMonth(),
            year: a.getFullYear()
        },
        b.date && (d += a.date + " "),
        b.month && (d = b.short ? d + (c.monthShort[a.month] + " ") : d + (c.month[a.month] + " ")),
        b.year && (d += a.year));
        return d
    }
    function b(a, b) {
        return a.getUTCDate() === b.getUTCDate() && a.getUTCMonth() === b.getUTCMonth() && a.getUTCFullYear() === b.getUTCFullYear() ? !0 : !1
    }
    var c = {
        month: "January February March April May June July August September October November December".split(" "),
        monthGenitive: "January February March April May June July August September October November December".split(" "),
        monthShort: "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
        daysWeek: "Mon Tue Wed Thu Fri Sat Sun".split(" ")
    };
    return {
        FormatDate: a,
        CompareOnDate: b,
        SmartFormatDate: function(d, f, e) {
            if (!f)
                return a(d, {
                    short: !0
                });
            e = e ? c.monthShort : c.month;
            var g = new Date(Date.UTC(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate()))
              , h = new Date(Date.UTC(f.getUTCFullYear(), f.getUTCMonth(), f.getUTCDate()));
            return b(g, h) ? d.getUTCDate() + " " + e[d.getUTCMonth()] + ", " + f.getUTCFullYear() : d.getUTCFullYear() === f.getUTCFullYear() ? (g = "",
            g = d.getUTCMonth() === f.getUTCMonth() ? g + (d.getUTCDate() + " - " + f.getUTCDate() + " " + e[f.getUTCMonth()]) : g + (d.getUTCDate() + " " + e[d.getUTCMonth()] + " - " + f.getUTCDate() + " " + e[f.getUTCMonth()]),
            g + ", " + f.getUTCFullYear()) : d.getUTCDate() + " " + e[d.getUTCMonth()] + ", " + d.getUTCFullYear() + " - " + f.getUTCDate() + " " + e[f.getUTCMonth()] + ", " + f.getUTCFullYear()
        },
        SetPhrases: function(a) {
            if (a)
                for (var b in a)
                    a.hasOwnProperty(b) && (c[b] = a[b])
        },
        Phrases: c,
        FormateWithTime: function(a) {
            var c = "";
            b(new Date, a) || (c += a.getFullYear() + ".",
            c += ("0" + (a.getMonth() + 1)).substr(-2) + ".",
            c += ("0" + a.getDate()).substr(-2) + " ");
            c += ("0" + a.getHours()).substr(-2) + ":";
            return c += ("0" + a.getMinutes()).substr(-2)
        }
    }
}();
Date.prototype.daysInMonth = function() {
    return 32 - (new Date(this.getFullYear(),this.getMonth(),32)).getDate()
}
;
var ErrorsChecker = function() {
    function a(a) {
        if (void 0 !== a && (a.error && (a = a.error),
        "string" !== typeof a))
            if (a.name)
                a.name.replace("Error", "");
            else if (!(a instanceof EvalError || a instanceof RangeError || a instanceof ReferenceError || a instanceof SyntaxError || a instanceof URIError))
                if (a.type)
                    a = a.type.charAt(0).toUpperCase() + a.type.slice(1),
                    a.replace("Error", "");
                else if (a.message && (a = a.message,
                a = a.replace(/("|'|\||&|\.|,|\(|\)|%)/gi, ""),
                a = a.replace("Error", ""),
                30 < a.length)) {
                    a = a.substring(0, 29);
                    var b = a.lastIndexOf(" ");
                    0 < b && a.substring(0, b)
                }
    }
    function b(b) {
        setTimeout(function() {
            var d = "";
            void 0 === b ? d = "undefined error" : "object" === typeof b ? "stack"in b && b.stack ? ("message"in b && b.message && (d += b.message + "\r\n"),
            d += b.stack) : "message"in b && b.message ? (d = b.message,
            "filename"in b && b.filename && (d += '<div class="error-console__file">' + b.filename),
            "lineno"in b && b.lineno && (d += ":" + b.lineno),
            "columnNumber"in b && b.columnNumber && (d += ":" + b.columnNumber),
            d += "</div>") : d = "type"in b && b.type ? b.type : b : d = b;
            f ? c(d) : "console"in window && console.log(d);
            a(b)
        }, 10)
    }
    function c(a) {
        void 0 === a && (a = "undefined error");
        if (a) {
            var b = document.createElement("div");
            b.className = "error-console__item";
            b.innerHTML = "object" == typeof a && "stack"in a && a.stack ? a.stack : a;
            d.insertBefore(b, d.firstChild);
            (function(a) {
                setTimeout(function() {
                    d.removeChild(a)
                }, 5E3)
            }
            )(b)
        }
    }
    var d = document.createElement("div")
      , f = !1;
    return {
        Init: function() {
            function a() {
                window.removeEventListener("load", a);
                document.body.appendChild(d)
            }
            d.className = "error-console";
            start = !0;
            window.addEventListener("error", function(a) {
                a = a || window.event;
                b(a);
                a.preventDefault && a.preventDefault();
                return !1
            });
            window.addEventListener("load", a)
        },
        Add: b,
        Render: c,
        SendErrorTrack: a,
        Toggle: function() {
            f = !f
        }
    }
}();
var ERROR = "input is invalid type"
  , WINDOW = "object" === typeof window
  , root = WINDOW ? window : {};
root.JS_SHA256_NO_WINDOW && (WINDOW = !1);
var WEB_WORKER = !WINDOW && "object" === typeof self
  , NODE_JS = !root.JS_SHA256_NO_NODE_JS && "object" === typeof process && process.versions && process.versions.node;
NODE_JS ? root = global : WEB_WORKER && (root = self);
var COMMON_JS = !root.JS_SHA256_NO_COMMON_JS && "object" === typeof module && module.exports
  , AMD = "function" === typeof define && define.amd
  , ARRAY_BUFFER = !root.JS_SHA256_NO_ARRAY_BUFFER && "undefined" !== typeof ArrayBuffer
  , HEX_CHARS = "0123456789abcdef".split("")
  , EXTRA = [-2147483648, 8388608, 32768, 128]
  , SHIFT = [24, 16, 8, 0]
  , K = [1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298]
  , OUTPUT_TYPES = ["hex", "array", "digest", "arrayBuffer"]
  , blocks = [];
if (root.JS_SHA256_NO_NODE_JS || !Array.isArray)
    Array.isArray = function(a) {
        return "[object Array]" === Object.prototype.toString.call(a)
    }
    ;
!ARRAY_BUFFER || !root.JS_SHA256_NO_ARRAY_BUFFER_IS_VIEW && ArrayBuffer.isView || (ArrayBuffer.isView = function(a) {
    return "object" === typeof a && a.buffer && a.buffer.constructor === ArrayBuffer
}
);
var createOutputMethod = function(a, b) {
    return function(c) {
        return (new Sha256(b,!0)).update(c)[a]()
    }
}
  , createMethod = function(a) {
    var b = createOutputMethod("hex", a);
    NODE_JS && (b = nodeWrap(b, a));
    b.create = function() {
        return new Sha256(a)
    }
    ;
    b.update = function(a) {
        return b.create().update(a)
    }
    ;
    for (var c = 0; c < OUTPUT_TYPES.length; ++c) {
        var d = OUTPUT_TYPES[c];
        b[d] = createOutputMethod(d, a)
    }
    return b
}
  , nodeWrap = function(a, b) {
    var c = eval("require('crypto')")
      , d = eval("require('buffer').Buffer")
      , f = b ? "sha224" : "sha256";
    return function(b) {
        if ("string" === typeof b)
            return c.createHash(f).update(b, "utf8").digest("hex");
        if (null === b || void 0 === b)
            throw Error(ERROR);
        b.constructor === ArrayBuffer && (b = new Uint8Array(b));
        return Array.isArray(b) || ArrayBuffer.isView(b) || b.constructor === d ? c.createHash(f).update(new d(b)).digest("hex") : a(b)
    }
}
  , createHmacOutputMethod = function(a, b) {
    return function(c, d) {
        return (new HmacSha256(c,b,!0)).update(d)[a]()
    }
}
  , createHmacMethod = function(a) {
    var b = createHmacOutputMethod("hex", a);
    b.create = function(b) {
        return new HmacSha256(b,a)
    }
    ;
    b.update = function(a, c) {
        return b.create(a).update(c)
    }
    ;
    for (var c = 0; c < OUTPUT_TYPES.length; ++c) {
        var d = OUTPUT_TYPES[c];
        b[d] = createHmacOutputMethod(d, a)
    }
    return b
};
function Sha256(a, b) {
    b ? (blocks[0] = blocks[16] = blocks[1] = blocks[2] = blocks[3] = blocks[4] = blocks[5] = blocks[6] = blocks[7] = blocks[8] = blocks[9] = blocks[10] = blocks[11] = blocks[12] = blocks[13] = blocks[14] = blocks[15] = 0,
    this.blocks = blocks) : this.blocks = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    a ? (this.h0 = 3238371032,
    this.h1 = 914150663,
    this.h2 = 812702999,
    this.h3 = 4144912697,
    this.h4 = 4290775857,
    this.h5 = 1750603025,
    this.h6 = 1694076839,
    this.h7 = 3204075428) : (this.h0 = 1779033703,
    this.h1 = 3144134277,
    this.h2 = 1013904242,
    this.h3 = 2773480762,
    this.h4 = 1359893119,
    this.h5 = 2600822924,
    this.h6 = 528734635,
    this.h7 = 1541459225);
    this.block = this.start = this.bytes = this.hBytes = 0;
    this.finalized = this.hashed = !1;
    this.first = !0;
    this.is224 = a
}
Sha256.prototype.update = function(a) {
    if (!this.finalized) {
        var b = typeof a;
        if ("string" !== b) {
            if ("object" === b) {
                if (null === a)
                    throw Error(ERROR);
                if (ARRAY_BUFFER && a.constructor === ArrayBuffer)
                    a = new Uint8Array(a);
                else if (!(Array.isArray(a) || ARRAY_BUFFER && ArrayBuffer.isView(a)))
                    throw Error(ERROR);
            } else
                throw Error(ERROR);
            var c = !0
        }
        for (var d = 0, f, e = a.length, g = this.blocks; d < e; ) {
            this.hashed && (this.hashed = !1,
            g[0] = this.block,
            g[16] = g[1] = g[2] = g[3] = g[4] = g[5] = g[6] = g[7] = g[8] = g[9] = g[10] = g[11] = g[12] = g[13] = g[14] = g[15] = 0);
            if (c)
                for (f = this.start; d < e && 64 > f; ++d)
                    g[f >> 2] |= a[d] << SHIFT[f++ & 3];
            else
                for (f = this.start; d < e && 64 > f; ++d)
                    b = a.charCodeAt(d),
                    128 > b ? g[f >> 2] |= b << SHIFT[f++ & 3] : (2048 > b ? g[f >> 2] |= (192 | b >> 6) << SHIFT[f++ & 3] : (55296 > b || 57344 <= b ? g[f >> 2] |= (224 | b >> 12) << SHIFT[f++ & 3] : (b = 65536 + ((b & 1023) << 10 | a.charCodeAt(++d) & 1023),
                    g[f >> 2] |= (240 | b >> 18) << SHIFT[f++ & 3],
                    g[f >> 2] |= (128 | b >> 12 & 63) << SHIFT[f++ & 3]),
                    g[f >> 2] |= (128 | b >> 6 & 63) << SHIFT[f++ & 3]),
                    g[f >> 2] |= (128 | b & 63) << SHIFT[f++ & 3]);
            this.lastByteIndex = f;
            this.bytes += f - this.start;
            64 <= f ? (this.block = g[16],
            this.start = f - 64,
            this.hash(),
            this.hashed = !0) : this.start = f
        }
        4294967295 < this.bytes && (this.hBytes += this.bytes / 4294967296 << 0,
        this.bytes %= 4294967296);
        return this
    }
}
;
Sha256.prototype.finalize = function() {
    if (!this.finalized) {
        this.finalized = !0;
        var a = this.blocks
          , b = this.lastByteIndex;
        a[16] = this.block;
        a[b >> 2] |= EXTRA[b & 3];
        this.block = a[16];
        56 <= b && (this.hashed || this.hash(),
        a[0] = this.block,
        a[16] = a[1] = a[2] = a[3] = a[4] = a[5] = a[6] = a[7] = a[8] = a[9] = a[10] = a[11] = a[12] = a[13] = a[14] = a[15] = 0);
        a[14] = this.hBytes << 3 | this.bytes >>> 29;
        a[15] = this.bytes << 3;
        this.hash()
    }
}
;
Sha256.prototype.hash = function() {
    var a = this.h0, b = this.h1, c = this.h2, d = this.h3, f = this.h4, e = this.h5, g = this.h6, h = this.h7, m = this.blocks, l;
    for (l = 16; 64 > l; ++l) {
        var p = m[l - 15];
        var t = (p >>> 7 | p << 25) ^ (p >>> 18 | p << 14) ^ p >>> 3;
        p = m[l - 2];
        p = (p >>> 17 | p << 15) ^ (p >>> 19 | p << 13) ^ p >>> 10;
        m[l] = m[l - 16] + t + m[l - 7] + p << 0
    }
    var n = b & c;
    for (l = 0; 64 > l; l += 4) {
        if (this.first) {
            if (this.is224) {
                var r = 300032;
                p = m[0] - 1413257819;
                h = p - 150054599 << 0;
                d = p + 24177077 << 0
            } else
                r = 704751109,
                p = m[0] - 210244248,
                h = p - 1521486534 << 0,
                d = p + 143694565 << 0;
            this.first = !1
        } else {
            t = (a >>> 2 | a << 30) ^ (a >>> 13 | a << 19) ^ (a >>> 22 | a << 10);
            p = (f >>> 6 | f << 26) ^ (f >>> 11 | f << 21) ^ (f >>> 25 | f << 7);
            r = a & b;
            var q = r ^ a & c ^ n;
            var u = f & e ^ ~f & g;
            p = h + p + u + K[l] + m[l];
            t += q;
            h = d + p << 0;
            d = p + t << 0
        }
        t = (d >>> 2 | d << 30) ^ (d >>> 13 | d << 19) ^ (d >>> 22 | d << 10);
        p = (h >>> 6 | h << 26) ^ (h >>> 11 | h << 21) ^ (h >>> 25 | h << 7);
        n = d & a;
        q = n ^ d & b ^ r;
        u = h & f ^ ~h & e;
        p = g + p + u + K[l + 1] + m[l + 1];
        t += q;
        g = c + p << 0;
        c = p + t << 0;
        t = (c >>> 2 | c << 30) ^ (c >>> 13 | c << 19) ^ (c >>> 22 | c << 10);
        p = (g >>> 6 | g << 26) ^ (g >>> 11 | g << 21) ^ (g >>> 25 | g << 7);
        r = c & d;
        q = r ^ c & a ^ n;
        u = g & h ^ ~g & f;
        p = e + p + u + K[l + 2] + m[l + 2];
        t += q;
        e = b + p << 0;
        b = p + t << 0;
        t = (b >>> 2 | b << 30) ^ (b >>> 13 | b << 19) ^ (b >>> 22 | b << 10);
        p = (e >>> 6 | e << 26) ^ (e >>> 11 | e << 21) ^ (e >>> 25 | e << 7);
        n = b & c;
        q = n ^ b & d ^ r;
        u = e & g ^ ~e & h;
        p = f + p + u + K[l + 3] + m[l + 3];
        t += q;
        f = a + p << 0;
        a = p + t << 0
    }
    this.h0 = this.h0 + a << 0;
    this.h1 = this.h1 + b << 0;
    this.h2 = this.h2 + c << 0;
    this.h3 = this.h3 + d << 0;
    this.h4 = this.h4 + f << 0;
    this.h5 = this.h5 + e << 0;
    this.h6 = this.h6 + g << 0;
    this.h7 = this.h7 + h << 0
}
;
Sha256.prototype.hex = function() {
    this.finalize();
    var a = this.h0
      , b = this.h1
      , c = this.h2
      , d = this.h3
      , f = this.h4
      , e = this.h5
      , g = this.h6
      , h = this.h7;
    a = HEX_CHARS[a >> 28 & 15] + HEX_CHARS[a >> 24 & 15] + HEX_CHARS[a >> 20 & 15] + HEX_CHARS[a >> 16 & 15] + HEX_CHARS[a >> 12 & 15] + HEX_CHARS[a >> 8 & 15] + HEX_CHARS[a >> 4 & 15] + HEX_CHARS[a & 15] + HEX_CHARS[b >> 28 & 15] + HEX_CHARS[b >> 24 & 15] + HEX_CHARS[b >> 20 & 15] + HEX_CHARS[b >> 16 & 15] + HEX_CHARS[b >> 12 & 15] + HEX_CHARS[b >> 8 & 15] + HEX_CHARS[b >> 4 & 15] + HEX_CHARS[b & 15] + HEX_CHARS[c >> 28 & 15] + HEX_CHARS[c >> 24 & 15] + HEX_CHARS[c >> 20 & 15] + HEX_CHARS[c >> 16 & 15] + HEX_CHARS[c >> 12 & 15] + HEX_CHARS[c >> 8 & 15] + HEX_CHARS[c >> 4 & 15] + HEX_CHARS[c & 15] + HEX_CHARS[d >> 28 & 15] + HEX_CHARS[d >> 24 & 15] + HEX_CHARS[d >> 20 & 15] + HEX_CHARS[d >> 16 & 15] + HEX_CHARS[d >> 12 & 15] + HEX_CHARS[d >> 8 & 15] + HEX_CHARS[d >> 4 & 15] + HEX_CHARS[d & 15] + HEX_CHARS[f >> 28 & 15] + HEX_CHARS[f >> 24 & 15] + HEX_CHARS[f >> 20 & 15] + HEX_CHARS[f >> 16 & 15] + HEX_CHARS[f >> 12 & 15] + HEX_CHARS[f >> 8 & 15] + HEX_CHARS[f >> 4 & 15] + HEX_CHARS[f & 15] + HEX_CHARS[e >> 28 & 15] + HEX_CHARS[e >> 24 & 15] + HEX_CHARS[e >> 20 & 15] + HEX_CHARS[e >> 16 & 15] + HEX_CHARS[e >> 12 & 15] + HEX_CHARS[e >> 8 & 15] + HEX_CHARS[e >> 4 & 15] + HEX_CHARS[e & 15] + HEX_CHARS[g >> 28 & 15] + HEX_CHARS[g >> 24 & 15] + HEX_CHARS[g >> 20 & 15] + HEX_CHARS[g >> 16 & 15] + HEX_CHARS[g >> 12 & 15] + HEX_CHARS[g >> 8 & 15] + HEX_CHARS[g >> 4 & 15] + HEX_CHARS[g & 15];
    this.is224 || (a += HEX_CHARS[h >> 28 & 15] + HEX_CHARS[h >> 24 & 15] + HEX_CHARS[h >> 20 & 15] + HEX_CHARS[h >> 16 & 15] + HEX_CHARS[h >> 12 & 15] + HEX_CHARS[h >> 8 & 15] + HEX_CHARS[h >> 4 & 15] + HEX_CHARS[h & 15]);
    return a
}
;
Sha256.prototype.toString = Sha256.prototype.hex;
Sha256.prototype.digest = function() {
    this.finalize();
    var a = this.h0
      , b = this.h1
      , c = this.h2
      , d = this.h3
      , f = this.h4
      , e = this.h5
      , g = this.h6
      , h = this.h7;
    a = [a >> 24 & 255, a >> 16 & 255, a >> 8 & 255, a & 255, b >> 24 & 255, b >> 16 & 255, b >> 8 & 255, b & 255, c >> 24 & 255, c >> 16 & 255, c >> 8 & 255, c & 255, d >> 24 & 255, d >> 16 & 255, d >> 8 & 255, d & 255, f >> 24 & 255, f >> 16 & 255, f >> 8 & 255, f & 255, e >> 24 & 255, e >> 16 & 255, e >> 8 & 255, e & 255, g >> 24 & 255, g >> 16 & 255, g >> 8 & 255, g & 255];
    this.is224 || a.push(h >> 24 & 255, h >> 16 & 255, h >> 8 & 255, h & 255);
    return a
}
;
Sha256.prototype.array = Sha256.prototype.digest;
Sha256.prototype.arrayBuffer = function() {
    this.finalize();
    var a = new ArrayBuffer(this.is224 ? 28 : 32)
      , b = new DataView(a);
    b.setUint32(0, this.h0);
    b.setUint32(4, this.h1);
    b.setUint32(8, this.h2);
    b.setUint32(12, this.h3);
    b.setUint32(16, this.h4);
    b.setUint32(20, this.h5);
    b.setUint32(24, this.h6);
    this.is224 || b.setUint32(28, this.h7);
    return a
}
;
function HmacSha256(a, b, c) {
    var d = typeof a;
    if ("string" === d) {
        var f = []
          , e = a.length
          , g = 0;
        for (d = 0; d < e; ++d) {
            var h = a.charCodeAt(d);
            128 > h ? f[g++] = h : (2048 > h ? f[g++] = 192 | h >> 6 : (55296 > h || 57344 <= h ? f[g++] = 224 | h >> 12 : (h = 65536 + ((h & 1023) << 10 | a.charCodeAt(++d) & 1023),
            f[g++] = 240 | h >> 18,
            f[g++] = 128 | h >> 12 & 63),
            f[g++] = 128 | h >> 6 & 63),
            f[g++] = 128 | h & 63)
        }
        a = f
    } else if ("object" === d) {
        if (null === a)
            throw Error(ERROR);
        if (ARRAY_BUFFER && a.constructor === ArrayBuffer)
            a = new Uint8Array(a);
        else if (!(Array.isArray(a) || ARRAY_BUFFER && ArrayBuffer.isView(a)))
            throw Error(ERROR);
    } else
        throw Error(ERROR);
    64 < a.length && (a = (new Sha256(b,!0)).update(a).array());
    f = [];
    e = [];
    for (d = 0; 64 > d; ++d)
        g = a[d] || 0,
        f[d] = 92 ^ g,
        e[d] = 54 ^ g;
    Sha256.call(this, b, c);
    this.update(e);
    this.oKeyPad = f;
    this.inner = !0;
    this.sharedMemory = c
}
HmacSha256.prototype = new Sha256;
HmacSha256.prototype.finalize = function() {
    Sha256.prototype.finalize.call(this);
    if (this.inner) {
        this.inner = !1;
        var a = this.array();
        Sha256.call(this, this.is224, this.sharedMemory);
        this.update(this.oKeyPad);
        this.update(a);
        Sha256.prototype.finalize.call(this)
    }
}
;
var exports = createMethod();
exports.sha256 = exports;
exports.sha224 = createMethod(!0);
exports.sha256.hmac = createHmacMethod();
exports.sha224.hmac = createHmacMethod(!0);
COMMON_JS ? module.exports = exports : (root.sha256 = exports.sha256,
root.sha224 = exports.sha224,
AMD && define(function() {
    return exports
}));
var x, y, inDock = !1, interval, f_delta = "";
document.onmouseup = function() {
    stopDock()
}
;
function submitAvatarFile() {
    var a = $("avatar_load");
    a.style.visibility = "visible";
    a.style.display = "block";
    document.avatar_upload.submit()
}
function moveEvent(a) {
    if (inDock) {
        a || (a = window.event);
        var b = document.getElementById("image")
          , c = a.clientX - x
          , d = a.clientY - y
          , f = b.style
          , e = b.offsetHeight;
        b = b.offsetWidth;
        d < -e + 240 ? d = -e + 240 : 40 < d && (d = 40);
        c < -b + 240 ? c = -b + 240 : 40 < c && (c = 40);
        f.left = c + "px";
        f.top = d + "px";
        a.preventDefault && a.preventDefault()
    }
}
function startDock(a) {
    a || (a = window.event);
    var b = document.getElementById("image");
    b && (x = a.clientX - parseInt(b.style.left),
    y = a.clientY - parseInt(b.style.top),
    inDock = !0);
    a.preventDefault && a.preventDefault()
}
function stopDock() {
    inDock = !1
}
function zoomIn() {
    var a = document.getElementById("image");
    if (a) {
        var b = a.clientWidth
          , c = a.clientHeight;
        c = b / c;
        b += 60;
        c = Math.round(b / c);
        a.style.width = b + "px";
        a.style.height = c + "px";
        a.style.left = parseInt(a.style.left) - 30 + "px";
        a.style.top = parseInt(a.style.top) - 30 + "px"
    }
}
function zoomOut() {
    var a = document.getElementById("image");
    if (a) {
        var b = a.clientWidth
          , c = a.clientHeight;
        c = b / c;
        b -= 60;
        c = Math.round(b / c);
        200 <= b && 200 <= c && (a.style.width = b + "px",
        a.style.height = c + "px",
        a.style.left = parseInt(a.style.left) + 30 + "px",
        a.style.top = parseInt(a.style.top) + 30 + "px")
    }
}
function moveImage(a, b) {
    stopMoving();
    a || (a = window.event);
    a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0;
    f_delta = b;
    interval = setInterval(moveInterval, 50)
}
function moveInterval() {
    var a = document.getElementById("image");
    if (a)
        switch (f_delta) {
        case "top":
            var b = a.offsetHeight;
            var c = parseInt(a.style.top) - 10;
            c < -b + 240 && (c = -b + 240);
            a.style.top = c + "px";
            break;
        case "bottom":
            c = parseInt(a.style.top) + 10;
            40 < c && (c = 40);
            a.style.top = c + "px";
            break;
        case "left":
            b = a.offsetWidth;
            c = parseInt(a.style.left) - 10;
            c < -b + 240 && (c = -b + 240);
            a.style.left = c + "px";
            break;
        case "right":
            c = parseInt(a.style.left) + 10,
            40 < c && (c = 40),
            a.style.left = c + "px"
        }
}
function stopMoving(a) {
    interval && (clearInterval(interval),
    interval = null)
}
function saveImage(a, b) {
    var c = document.getElementById("image")
      , d = parseInt(c.style.left) - 40
      , f = parseInt(c.style.top) - 40;
    document.forms.coords.left.value = d = Math.ceil(-d * a / c.clientWidth);
    document.forms.coords.top.value = f = Math.ceil(-f * b / c.clientHeight);
    document.forms.coords.right.value = d + Math.ceil(200 * a / c.clientWidth);
    document.forms.coords.bottom.value = f + Math.ceil(200 * b / c.clientHeight);
    document.forms.coords.submit()
}
function resizeAvatarIframe(a) {
    var b = a.contentWindow.document.body.scrollHeight;
    0 !== b ? a.style.height = b + "px" : setTimeout(function() {
        resizeAvatarIframe(a)
    }, 300)
}
;var globContainersCounter = 1
  , globInputSize = 63
  , globInputName = "attachedFile"
  , globClassName = "attachInput";
function editComment(a) {
    Attach.clearAttaches();
    var b = $("newmessage");
    b && (b.style.display = "block");
    b = parseInt(a);
    $("id_message");
    var c = $("content" + b)
      , d = $("edit_id");
    $("edit_parent_id");
    var f = $("edit_cnew_cmd")
      , e = $("edit_csave_cmd")
      , g = $("admin_date_create")
      , h = document.getElementById("admin_not_modify")
      , m = $("date_create_" + b)
      , l = $("author_info_" + a)
      , p = $("admin_author");
    d && (d.value = b);
    c && set_content(c.innerHTML + "<br>", !1, !0);
    f && e && (f.style.display = "none",
    e.style.display = "block");
    g && (g.value = null != m ? trim(m.innerHTML) : "");
    h && (h.checked = !0);
    p && l && (p.value = l.title);
    Attach.setReadOnlyAttaches(Attach.getAttachesFromComment(a));
    $("extractorContainer") && ($("extractorContainer").innerHTML = "",
    $("extractorContainer").className = $("extractor" + a) ? "disabled" : "")
}
function editCommentCancel() {
    $("id_message");
    var a = $("edit_id")
      , b = $("edit_cnew_cmd")
      , c = $("edit_csave_cmd")
      , d = $("admin_date_create")
      , f = document.getElementById("admin_not_modify")
      , e = $("admin_author");
    a && (a.value = 0);
    window.MQRTE && set_content(Dalet.isGecko ? "<br>" : "", !1, !0);
    b && c && (b.style.display = "block",
    c.style.display = "none");
    d && (d.value = "");
    e && (e.value = "");
    f && (f.checked = !1);
    Attach.clearAttaches()
}
function Attach() {}
Attach.getAttachesFromComment = function(a) {
    var b = []
      , c = 0;
    a = $("cb_" + a);
    if (!a)
        return b;
    a = a.getElementsByTagName("DIV");
    for (var d = 0; d < a.length; d++)
        if (0 <= a[d].className.indexOf("attachItem")) {
            var f = a[d].cloneNode(!0)
              , e = f.getElementsByTagName("A");
            0 < e.length && (b[c] = {
                obj: f,
                url: window.location.hostname == e[0].host ? e[0].pathname : e[0].href
            },
            c++)
        }
    return b
}
;
Attach.setReadOnlyAttaches = function(a) {
    if (a && 0 != a.length) {
        var b = $("readOnlyAttaches");
        if (!b) {
            b = document.createElement("DIV");
            b.setAttribute("id", "readOnlyAttaches");
            var c = $("multiattaches");
            c.insertBefore(b, c.firstChild)
        }
        b.innerHTML = "";
        for (i in a)
            a.hasOwnProperty(i) && (Attach.addHiddenValues(a[i].obj, a[i].url),
            Attach.addDeleteOption(a[i].obj),
            b.appendChild(a[i].obj))
    }
}
;
Attach.addHiddenValues = function(a, b) {
    var c = document.createElement("INPUT");
    c.type = "hidden";
    c.name = globInputName;
    c.value = b;
    a.appendChild(c)
}
;
Attach.createNewContainer = function(a) {
    a || (a = 1);
    var b = $("newAttaches")
      , c = document.createElement("DIV");
    c.id = "at_" + a;
    var d = document.createElement("INPUT");
    d.type = "file";
    d.name = globInputName;
    d.id = "_" + a;
    d.onchange = function(a) {
        a = a || window.event;
        Attach.onChangeFile(c.id, a.target || a.srcElement)
    }
    ;
    d.className = globClassName;
    d.size = globInputSize;
    Attach.globAcceptFilter && (d.accept = Attach.globAcceptFilter);
    Attach.AllowedExtensions && d.setAttribute("accept", Attach.AllowedExtensions);
    c.appendChild(d);
    b.appendChild(c);
    return c
}
;
Attach.onChangeFile = function(a, b) {
    function c(a) {
        Validate && d && Validate.AppendMessage(d, a);
        b.value = "";
        b.type = "";
        b.type = "file"
    }
    a = $(a);
    var d = $("newAttaches_info");
    if (Attach.AllowedExtensions && b) {
        var f = b.value;
        if (f) {
            f = f.toLowerCase().split(".");
            1 < f.length && (f = f.pop());
            if (0 > Attach.AllowedExtensions.indexOf("." + f))
                return c(Attach.alowedExtensionsLabel),
                !1;
            Validate && d && Validate.ClearMessage(d)
        } else
            return !1
    }
    if (Attach.MaxSize && b && ("string" === typeof Attach.MaxSize && (Attach.MaxSize = 1048576 * parseInt(Attach.MaxSize)),
    0 < Attach.MaxSize && b.files))
        if (b.files[0]) {
            if (b.files[0].size > Attach.MaxSize)
                return c(Attach.maxSizeLabel),
                !1;
            Validate && d && Validate.ClearMessage(d)
        } else
            return !1;
    a.containsYet || (Attach.addDeleteOption(a),
    globContainersCounter++,
    Attach.createNewContainer(globContainersCounter),
    a.containsYet = !0)
}
;
Attach.addDeleteOption = function(a) {
    var b = document.createElement("A");
    b.appendChild(document.createTextNode(window.deleteLabel ? window.deleteLabel : "delete"));
    b.href = "#";
    b.className = "deleteAttachLink";
    a.appendChild(b);
    b.onclick = function() {
        return Attach.deleteContainer(a)
    }
}
;
Attach.deleteContainer = function(a) {
    var b = a.parentNode.getElementsByTagName("DIV").length
      , c = a.parentNode;
    a.parentNode.removeChild(a);
    1 == b && c.parentNode.removeChild(c);
    return !1
}
;
Attach.clearAttaches = function() {
    var a = $("readOnlyAttaches");
    a && a.parentNode.removeChild(a);
    if (a = $("newAttaches"))
        a.innerHTML = "";
    globContainersCounter = 1;
    Attach.createNewContainer(1)
}
;
Attach.setAcceptFilter = function(a) {
    Attach.globAcceptFilter = a
}
;
function ShortComments() {
    function a(a) {
        a = a || window.event;
        return 13 === a.keyCode && a.ctrlKey ? !0 : !1
    }
    function b(a, b) {
        b && (a.style.height = b + "px");
        a.clientHeight < a.scrollHeight && (a.style.minHeight = a.scrollHeight + "px")
    }
    function c(a, b, c, d, e, f, g) {
        this.moduleId = a;
        this.parentType = b;
        this.parentId = c;
        this.avatar = d;
        this.container = e;
        this.insertAfterElement = f;
        this.token = g
    }
    function d(a, b, c) {
        this.html = a;
        this.container = b;
        this.insertAfterElement = c
    }
    function f(a) {
        return "shortCommentForm_" + a.moduleId + "_" + a.parentType + "_" + a.parentId
    }
    function e(a) {
        return "shortCommentContent_" + a.moduleId + "_" + a.parentType + "_" + a.parentId
    }
    function g(c) {
        var d = "/" + w + "/short_comments/" + c.moduleId + "/" + c.parentId;
        if (!$(f(c))) {
            var g = createElement(null, "DIV", {
                className: "shortComment wallComment",
                id: f(c) + "_cnt"
            })
              , l = createElement(g, "SPAN", {
                className: "shortCommentAvatar"
            })
              , n = createElement(g, "DIV", {
                className: "editShortCommentContent"
            })
              , m = createElement(n, "FORM", {
                className: "editShortComment addAction",
                id: f(c),
                action: d,
                method: "POST"
            });
            createElement(m, "input", {
                type: "hidden",
                value: c.token,
                name: "__signature"
            });
            createElement(l, "IMG", {
                src: c.avatar,
                width: 24,
                height: 24
            });
            m.onsubmit = function() {
                return t(c)
            }
            ;
            d = createElement(m, "DIV", {
                className: "inputWrapper"
            });
            d = createElement(d, "textarea", {
                name: "content",
                id: e(c)
            });
            d.onblur = function() {
                0 == this.value.length && h(c)
            }
            ;
            d.onkeydown = function(b) {
                if (a(b))
                    m.onsubmit()
            }
            ;
            d.onkeyup = function(a) {
                b(this, 16)
            }
            ;
            d.style.height = "16px";
            shortCommentsManager.toPostStatusLabel && createElement(n, "span", {
                innerHTML: shortCommentsManager.toPostStatusLabel,
                className: "textareaHelp"
            });
            createElement(m, "input", {
                type: "hidden",
                name: "moduleId",
                value: c.moduleId
            });
            createElement(m, "input", {
                type: "hidden",
                name: "parentType",
                value: c.parentType
            });
            createElement(m, "input", {
                type: "hidden",
                name: "parentId",
                value: c.parentId
            });
            createElement(m, "input", {
                type: "submit",
                className: "buttonSimple postStatus",
                value: ""
            });
            n = null;
            c.insertAfterElement && (n = c.insertAfterElement.nextSibling);
            apearBlockVertical(g, c.container, n, 200);
            b(d, 16);
            setTimeout(function() {
                $(e(c)).focus()
            }, 220)
        }
    }
    function h(a) {
        (a = $(f(a) + "_cnt")) && a.parentNode && a.parentNode.removeChild(a)
    }
    function m(a) {
        a && a.length && n(new d(a,this.editData.container,this.insertAfterElement));
        h(this.editData);
        v = !1
    }
    function l(a) {
        a = n(new d(a,this.container,this.insertAfterElement), !1);
        this.callback && this.callback(a)
    }
    function p(a) {
        v = !1;
        alert("Error")
    }
    function t(a) {
        if (v)
            return !1;
        var b = $(f(a));
        if (!b)
            return !1;
        var c = b.content;
        if (!c || !c.value || !c.value.length)
            return !1;
        v = !0;
        Ajax.form(b, {
            onready: m,
            onerror: p,
            editData: a
        });
        return !1
    }
    function n(a, b) {
        var c = document.createElement("DIV");
        c.innerHTML = a.html;
        var d = null;
        a.insertAfterElement && (d = a.insertAfterElement.nextSibling);
        var e = 0;
        if (c.children)
            for (var f = 0; f < c.children.length; f++) {
                var g = c.children[f].cloneNode(!0);
                b ? apearBlockVertical(g, a.container, d, Math.min(500, 50 * (f + 1)), 5) : a.insertAfterElement ? a.container.insertBefore(g, a.insertAfterElement) : a.container.appendChild(g);
                e++
            }
        return e
    }
    function r(a) {
        this.element && this.element.parentNode && this.element.parentNode.removeChild(this.element)
    }
    function q(a) {
        if ((a = "undefined" == typeof JSON ? eval("(" + a + ")") : JSON.parse(a)) && a.length) {
            elementPrefix = this.elementPrefix || "sc_";
            for (var b = 0; b < a.length; b++) {
                var c = $(elementPrefix + a[b]);
                c && c.parentNode.removeChild(c)
            }
        }
    }
    function u(a) {
        $("commentsLoad_" + a).style.display = "none";
        var b = $("hiddenComments_" + a);
        if (b) {
            for (var c = b.nextSibling, d = c.parentNode, e = 0; 0 < b.children.length; )
                d.insertBefore(b.children[0], c),
                e++;
            b.parentNode.removeChild(b);
            $("commentsHide_" + a).style.display = "inline"
        }
    }
    var w = location.pathname.split("/")[1];
    this.CheckSubmitHotKey = function(b) {
        return a(b)
    }
    ;
    this.AutoGrowTextarea = function(a, c) {
        return b(a, c)
    }
    ;
    var v = !1;
    this.NewComment = function(a, b, d, e, f, h, l) {
        a = new c(a,b,d,e,f,h,l);
        g(a)
    }
    ;
    this.LoadComments = function(a, b, c, d, e, f, g, h) {
        Ajax.get("/" + w + "/get_short_comments/" + a + "/" + b + "/" + c + "/" + d + "/" + e, null, {
            onready: l,
            onerror: p,
            container: f,
            insertAfterElement: g,
            callback: h
        })
    }
    ;
    this.Delete = function(a, b, c, d) {
        c || (c = "sc_");
        Ajax.post("/" + w + "/delete_short_comment/" + a + "/" + b, {
            __signature: d
        }, !1, {
            onready: r,
            onerror: p,
            element: $(c + b)
        })
    }
    ;
    this.DeleteByAuthor = function(a, b, c, d) {
        c || (c = "sc_");
        Ajax.post("/" + w + "/delete_short_comment_by_author/" + a + "/" + b, {
            __signature: d
        }, !1, {
            onready: q,
            onerror: p,
            elementPrefix: c
        })
    }
    ;
    this.OnLoadCommentsClick = function(a, b, c, d, e) {
        if ($("hiddenComments_" + a))
            return u(a);
        var f = $("commentsCountWrapper_" + a);
        shortCommentsManager.LoadComments(b, c, a, d, e, $("comments_" + a), f.nextSibling, function(b) {
            var c = $("commentsLoad_" + a)
              , d = $("commentsCountWrapper_" + a);
            Class(d, "loading", !0);
            0 >= b ? c.style.visibility = "visible" : ($("commentsLoad_" + a).style.display = "none",
            $("commentsHide_" + a).style.display = "inline")
        });
        $("commentsLoad_" + a).style.display = "none";
        Class(f, "loading")
    }
    ;
    this.HideComments = function(a, b) {
        $("commentsHide_" + a).style.display = "none";
        var c = $("hiddenComments_" + a);
        if (!c) {
            var d = $("comments_" + a);
            c = createElement(null, "div", {
                id: "hiddenComments_" + a
            });
            var e = getElementsByClass("shortComment", $("comments_" + a));
            d.insertBefore(c, e[0]);
            for (d = 0; d < e.length - b; d++)
                c.appendChild(e[d])
        }
        c.style.display = "none";
        $("commentsLoad_" + a).style.display = "inline"
    }
}
var shortCommentsManager = new ShortComments;
checkImgForLightbox($("forumTopicComments"), "content", "attachItem");
(function() {
    function a(a) {
        var c = document.body;
        p = createElement(c, "IFRAME", {
            className: "window-lock"
        });
        t = createElement(c, "DIV", {
            className: "window-lock"
        });
        "fixed" === a && (p.style.position = "fixed",
        t.style.position = "fixed");
        p.style.height = t.style.height = "100%";
        p.frameBorder = 0;
        p.style.width = t.style.width = "100%";
        n = createElement(document.body, "DIV", {
            className: "window"
        });
        a = createElement(n, "DIV", {
            className: "window-title"
        });
        a.appendChild(document.createElement("SPAN"));
        a.onmousedown = f;
        T && (u = createElement(a, "SPAN", {
            className: "window-close"
        }),
        u.onclick = T,
        0 == document.location.protocol.indexOf("https") && Class(u, "httpsClose"));
        w = createElement(n, "DIV", {
            className: "window-movelocker"
        });
        z = createElement(n, "DIV", {
            className: "window-content"
        });
        C && (r = createElement(n, "DIV", {
            className: "window__resizeble-layer"
        }),
        q = createElement(n, "DIV", {
            className: "window__resizeble-btn"
        }),
        q.onmousedown = b)
    }
    function b(a) {
        a || (a = window.event);
        a.preventDefault && a.preventDefault();
        r.style.display = "block";
        document.onmouseup = d;
        E.x = a.clientX;
        E.y = a.clientY;
        F.w = r.clientWidth;
        F.h = r.clientHeight;
        document.onmousemove = c
    }
    function c(a) {
        a || (a = window.event);
        a.preventDefault && a.preventDefault();
        F.h + (a.clientY - E.y) > D && (r.style.height = F.h + (a.clientY - E.y) + "px");
        F.w + (a.clientX - E.x) > A && (r.style.width = F.w + (a.clientX - E.x) + "px")
    }
    function d(a) {
        a || (a = window.event);
        a.preventDefault && a.preventDefault();
        r.style.display = "none";
        document.onmousemove = null;
        document.onmouseup = null;
        n.style.height = F.h + (a.clientY - E.y) > D ? F.h + (a.clientY - E.y) + "px" : D + "px";
        n.style.width = F.w + (a.clientX - E.x) > A ? F.w + (a.clientX - E.x) + "px" : A + "px"
    }
    function f(a) {
        a || (a = window.event);
        v = !0;
        document.onmousemove = e;
        document.onmouseup = g;
        H = parseInt(n.style.left) - a.clientX;
        J = parseInt(n.style.top) - a.clientY;
        w.style.display = "block"
    }
    function e(a) {
        if (v) {
            a || (a = window.event);
            var b = a.clientX + H, c = a.clientY + J, d;
            0 < (d = n.offsetWidth + b - t.offsetWidth) && (b -= d);
            0 < (d = n.offsetHeight + c - t.offsetHeight) && (c -= d);
            0 > b && (b = 0);
            0 > c && (c = 0);
            n.style.left = b + "px";
            n.style.top = c + "px";
            try {
                window.getSelection && (a = window.getSelection()) ? a.removeAllRanges() : document.selection && document.selection.empty()
            } catch (wa) {}
        }
    }
    function g() {
        v && (v = !1,
        w.style.display = "")
    }
    function h() {
        var a = n.lastChild.firstChild;
        a && (a.firstChild && "shareContent" == a.firstChild.className ? n.lastChild.removeChild(a) : G ? (G.insertBefore(a, I),
        G = null) : n.lastChild.removeChild(a))
    }
    function m(a) {
        for (; a; ) {
            if (a.tagName && "body" == a.tagName.toLowerCase())
                return !0;
            a = a.parentNode
        }
        return !1
    }
    var l = window.Dialog = {}, p, t, n, r, q, u, w, v = !1, C = !1, A = 0, D = 0, z, E = {}, F = {}, H, J, G, I, T;
    l.show = function(b, c, d, e, f, g, q, r, v, u) {
        function E() {
            var a = window.innerHeight || document.documentElement.clientHeight
              , b = window.innerWidth || document.documentElement.clientWidth;
            n.style.height = "auto";
            z.style.maxHeight = "none";
            z.style.maxWidth = "none";
            e = n.offsetHeight;
            d = n.offsetWidth;
            b - 40 <= d && (d = b - 40);
            a - 70 <= e || 600 >= a - 70 ? (e = a - 100,
            z.style.maxHeight = e - 44 + "px",
            z.style.overflow = "auto") : n.style.height = e + "px";
            z.style.maxWidth = d + "px";
            n.style.top = Math.round((a - e) / 2) + "px";
            n.style.left = Math.round((b - d) / 2) + "px";
            z.style.minWidth = "0px"
        }
        if (c) {
            "fixed" === u && (document.body.style.overflow = "hidden");
            var F = !0;
            A = d;
            D = e;
            T = f;
            v && (C = !0);
            "fixed" === u && (F = !1);
            f = m(c);
            n || a(u);
            n.style.visibility = "hidden";
            p && (p.style.display = "");
            t && (t.style.display = "");
            n && (n.style.display = "");
            u = t.offsetWidth;
            d >= u && (A = d = u - 20,
            z.style.maxWidth = d + 6 + "px");
            z.style.minWidth = d + "px";
            C && (z.style.minHeight = e + "px",
            n.style.width = d + "px",
            n.style.height = e + "px");
            w.style.height = e + 20 + "px";
            if (!q)
                if (g) {
                    var X;
                    q = g.left;
                    g = g.top;
                    0 < (X = n.offsetWidth + q - t.offsetWidth) && (q -= X + 10 * (0 < X ? 1 : -1));
                    0 < (X = n.offsetHeight + g - t.offsetHeight) && (g -= X + 10 * (0 < X ? 1 : -1));
                    q + d + 10 > u && (q = (u - d) / 2);
                    n.style.top = g + "px";
                    n.style.left = q + "px"
                } else
                    n.style.top = (document.body.scrollTop || document.documentElement.scrollTop) + Math.round(((window.innerHeight || document.documentElement.clientHeight) - e - 24) / 2) + "px",
                    n.style.left = Math.round((document.body.scrollWidth - d - 4) / 2) + "px";
            n.firstChild.firstChild.innerHTML = b;
            h();
            f && (G = c.parentNode);
            I = c.nextSibling;
            z.appendChild(c);
            n.className = r ? "window " + r : "window";
            F || (l.autoResize = Core.Bind(E, this),
            l.autoResize(),
            Core.AddHandler(window, "resize", l.autoResize));
            n.style.visibility = "visible";
            n.focus()
        }
    }
    ;
    l.hide = function(a) {
        document.body.style.overflow = "";
        l.autoResize && Core.RemoveHandler(window, "resize", l.autoResize);
        try {
            h()
        } catch (ca) {}
        T = void 0;
        p && p.parentNode && (p.parentNode.removeChild(p),
        p = null);
        n && n.parentNode && (n.parentNode.removeChild(n),
        n = null);
        t && t.parentNode && (t.parentNode.removeChild(t),
        t = null);
        u && u.parentNode && u.parentNode.removeChild(u);
        u = null
    }
}
)();
function edit(a) {
    var b = document.getElementById("newmessage");
    b && (b.style.display = "block");
    window.type = "_" == ("" + a).substr(0, 1) ? 0 : 1;
    real_id = 0 == window.type ? parseInt(a.substr(1, a.length)) : parseInt(a);
    document.getElementById("id_message");
    b = document.getElementById("subject_message");
    var c = document.getElementById("content" + real_id)
      , d = document.getElementById("simple_message")
      , f = document.getElementById("edit_id");
    document.getElementById("edit_parent_id");
    var e = document.getElementById("edit_subject")
      , g = document.getElementById("edit_simple")
      , h = document.getElementById("subject")
      , m = document.getElementById("edit_file");
    document.getElementById("subject");
    var l = document.getElementById("simple")
      , p = document.getElementById("edit_cnew_cmd")
      , t = document.getElementById("edit_csave_cmd")
      , n = document.getElementById("admin_date_create")
      , r = document.getElementById("admin_date_modify")
      , q = document.getElementById("admin_label_date_modify")
      , u = document.getElementById("admin_not_modify")
      , w = document.getElementById("admin_label_not_modify")
      , v = document.getElementById("date_create_" + real_id)
      , C = document.getElementById("date_modify_" + real_id);
    a = document.getElementById("author_info_" + a);
    var A = document.getElementById("admin_author");
    f && (f.value = real_id);
    b && e && h && (0 == window.type ? (h.value = b.value,
    e.style.display = "block") : (h.value = "",
    e.style.display = "none"));
    c && window.MQRTE && set_content(c.innerHTML, !1, !0);
    if (m)
        try {
            m.value = ""
        } catch (D) {}
    d && g && l && (0 == window.type ? (l.checked = 0 == d.value,
    g.style.display = "block") : (l.checked = !1,
    g.style.display = "none"));
    p && t && (p.style.display = "none",
    t.style.display = "block");
    n && (n.value = null != v ? trim(v.innerHTML) : "");
    r && (r.style.display = 0 == window.type ? "block" : "none",
    r.value = 0 == window.type && null != C ? trim(C.innerHTML) : "");
    q && (q.style.display = 0 == window.type ? "block" : "none");
    u && (u.style.display = "none",
    u.checked = !1);
    w && (w.style.display = "none");
    A && a && (A.value = a.title)
}
function edit_cancel() {
    document.getElementById("id_message");
    var a = document.getElementById("edit_id")
      , b = document.getElementById("edit_subject")
      , c = document.getElementById("edit_simple")
      , d = document.getElementById("subject")
      , f = document.getElementById("edit_file");
    document.getElementById("subject");
    var e = document.getElementById("simple")
      , g = document.getElementById("edit_cnew_cmd")
      , h = document.getElementById("edit_csave_cmd")
      , m = document.getElementById("admin_date_create")
      , l = document.getElementById("admin_date_modify")
      , p = document.getElementById("admin_label_date_modify")
      , t = document.getElementById("admin_not_modify")
      , n = document.getElementById("admin_label_not_modify")
      , r = document.getElementById("admin_author");
    a && (a.value = 0);
    b && d && (d.value = "",
    b.style.display = "none");
    window.MQRTE && set_content(Dalet.isGecko ? "<br>" : "", !1, !0);
    if (f)
        try {
            f.value = ""
        } catch (q) {}
    c && e && (e.checked = !1,
    c.style.display = "none");
    g && h && (g.style.display = "block",
    h.style.display = "none");
    m && (m.value = "");
    l && (l.style.display = "none",
    l.value = "");
    p && (p.style.display = "none");
    t && (t.style.display = "inline",
    t.checked = !1);
    n && (n.style.display = "inline");
    r && (r.value = "")
}
function quote_forum(a) {
    var b = document.getElementById("newmessage");
    b && (b.style.display = "block");
    b = document.getElementById("content" + a);
    a = document.getElementById("author_info_" + a);
    if (b && a) {
        b = b.cloneNode(!0);
        var c = a.cloneNode(!0)
          , d = c.getElementsByTagName("a");
        for (k = d.length - 1; 0 <= k; --k)
            "fast_msg" == d[k].className && d[k].parentNode.removeChild(d[k]);
        d = b.getElementsByTagName("DIV");
        for (k = d.length - 1; 0 <= k; --k)
            "fquote" != d[k].className && "pocket" != d[k].className || d[k].parentNode.removeChild(d[k]);
        d = b.getElementsByTagName("INPUT");
        for (k = d.length - 1; 0 <= k; --k)
            d[k].parentNode.removeChild(d[k]);
        d = b.getElementsByTagName("IMG");
        for (k = d.length - 1; 0 <= k; --k)
            !d[k].parentNode || "A" != d[k].parentNode.tagName && "a" != d[k].parentNode.tagName ? d[k].parentNode.removeChild(d[k]) : d[k].parentNode.parentNode.removeChild(d[k].parentNode);
        d = b.getElementsByTagName("EMBED");
        for (k = d.length - 1; 0 <= k; --k)
            d[k].parentNode.removeChild(d[k]);
        d = b.getElementsByTagName("IFRAME");
        for (k = d.length - 1; 0 <= k; --k)
            d[k].parentNode.removeChild(d[k]);
        d = b.getElementsByTagName("PRE");
        for (k = d.length - 1; 0 <= k; --k)
            d[k].parentNode.removeChild(d[k]);
        a = '<strong><span style="color:#42639C;"' + (a.title ? ' title="' + a.title + '"' : "") + ">" + trim(c.innerText ? c.innerText : c.textContent) + "</span>:</strong><br> " + b.innerHTML;
        set_content('<div class="fquote">' + a + "</div>", !0, !1);
        MQTE.Focus("body")
    }
}
function toggleNewMessage(a, b) {
    function c() {
        set_content("", !0, !1);
        MQTE.Focus("body");
        $("extractorContainer") && ($("extractorContainer").className = "")
    }
    var d = document.getElementById("newmessage");
    b = 1E3 * b || 0;
    if (a || "none" == d.style.display)
        return d.style.display = "block",
        b ? setTimeout(function() {
            c()
        }, b) : c(),
        !0;
    editCommentCancel();
    d.style.display = "none";
    $("extractorContainer") && ($("extractorContainer").innerHTML = "");
    return !1
}
function win_ban_show(a, b, c, d, f, e) {
    b = document.createElement("form");
    b.name = "banForm";
    b.action = "/";
    b.method = "get";
    c = document.getElementById(c);
    b.innerHTML = c.innerHTML.replace(/HIDDEN/g, "").replace(/{IP}/, d).replace(/{COOKIE}/, f).replace(/{USER_LOGIN}/, e);
    Dialog.show(a, b, 500, 162, function() {
        Dialog.hide()
    })
}
function win_multiban_show(a, b, c, d) {
    b = document.createElement("form");
    b.name = "banForm";
    b.action = "/";
    b.method = "get";
    c = document.getElementById(c);
    b.innerHTML = c.innerHTML.replace(/HIDDEN/g, "").replace(/{USER_LOGIN}/, d).replace(/{USER_LOGIN}/, d);
    Dialog.show(a, b, 500, 162, function() {
        Dialog.hide()
    })
}
function win_edit_image_show(a, b, c) {
    var d = document.createElement("iframe");
    d.id = "iframe_avatar";
    d.name = "iframe_avatar";
    d.frameBorder = 0;
    d.scrolling = "no";
    d.onload = function() {
        resizeAvatarIframe(d)
    }
    ;
    d.src = "/" + a + "/users/" + b + "/avatar";
    d.style.width = "100%";
    d.style.height = "430px";
    d.style.overflow = "hidden";
    Dialog.show(c, d, 320, 430)
}
function ban_submit(a, b, c, d) {
    c = {
        __signature: c
    };
    c.period = document.getElementById("period").value;
    if (d)
        c.multi = !0;
    else {
        c.cookie = encodeURIComponent(document.getElementById("cookie").value);
        d = document.getElementById("ip_address").value;
        if (null == d.match("^(([0-9*]{1,3}.){3}([0-9*]{1,3}){1})?$")) {
            alert("IP mask is not valid");
            return
        }
        c.ip = d
    }
    d = document.getElementById("comment").value;
    "" == d ? alert(b) : (c.comment = encodeURIComponent(d),
    c.login = document.getElementById("login").value,
    Ajax.post(a, c, !0, {
        onready: function(a) {
            window.location.reload()
        }
    }, !0))
}
function set_content(a, b, c) {
    c ? MQTE.SetContent("body", a) : MQTE.InsertContent("body", a)
}
function trim(a) {
    return a.replace(/^\s\s*/, "").replace(/\s\s*$/, "")
}
function processAjaxSubscribe(a, b, c, d, f) {
    if ("inFavorite star" != b.className && "notInFavorite star" != b.className)
        return !1;
    var e = "inFavorite star" == b.className ? "remove" : "add"
      , g = "inFavorite star" == b.className ? "add" : "remove";
    a = a + ("/" == a.charAt(a.length - 1) ? "" : "/") + e;
    b.className = "loading star";
    bindStarSrc(b, c);
    Ajax.post(a, {
        __signature: "add" == e ? d : f
    }, !1, {
        onready: function(a) {
            "success" == eval(a) ? b.className = "add" == g ? "notInFavorite star" : "inFavorite star" : (alert(eval(a)),
            b.className = "add" == e ? "notInFavorite star" : "inFavorite star");
            bindStarSrc(b, c)
        },
        onerror: function(a, d) {
            alert(c.error);
            b.className = "add" == e ? "notInFavorite star" : "inFavorite star";
            bindStarSrc(b, c)
        }
    });
    return !1
}
function processAjaxSubscribeMarket(a, b, c, d, f) {
    var e = "true" === dataset(b, "subscribed")
      , g = e ? "remove" : "add";
    a = a + ("/" == a.charAt(a.length - 1) ? "" : "/") + g;
    Class(b, "loading");
    Ajax.post(a, {
        __signature: "add" === g ? d : f
    }, !1, {
        onready: function(a) {
            Class(b, "loading", !0);
            "success" == eval(a) && (dataset(b, "subscribed", e ? "false" : "true"),
            b.innerHTML = e ? c.add : c.remove)
        },
        onerror: function(a, c) {
            Class(b, "loading", !0)
        }
    });
    return !1
}
function processAjaxSubscribeToModule(a, b, c, d) {
    Ajax.post(a, c ? {
        __signature: c
    } : {}, !1, {
        onready: function(a) {
            b.style.display = "none";
            d && d()
        },
        onerror: function(a, c) {
            b.style.display = "none"
        }
    });
    return !1
}
function bindStarSrc(a, b) {
    "inFavorite star" == a.className && (a.title = b.remove);
    "notInFavorite star" == a.className && (a.title = b.add);
    "loading star" == a.className && (a.title = "")
}
function openAttachments(a, b) {
    $(b).style.display = "block";
    $(b + "_link").style.display = "none";
    $(b + "_info").style.display = "block";
    a = $(a);
    a.className = a.className.replace("floatedAttaches", "noFloatedAttaches");
    return !1
}
function MenuUpdater() {}
MenuUpdater.startUpdating = function(a, b, c, d) {
    Ajax.post(b, {
        __signature: d
    }, !1, {
        onready: function() {
            var b = document.createElement("span");
            b.innerHTML = c;
            var d = $(a);
            d.parentNode.appendChild(b);
            d.parentNode.removeChild(d)
        }
    })
}
;
function MQAutoComplete() {
    this.placeholder = this.id = null;
    this.multiple = !1;
    this.items = [];
    this.domContainer = this.defaultKey = null;
    this.post = !1;
    this.ajaxQueryUrl = this.signature = null;
    this.buildAjaxQueryUrl = function() {
        return this.ajaxQueryUrl
    }
    ;
    this.onPopup = function(a, b) {}
    ;
    this.onTyping = function(a) {}
    ;
    this.onChange = function(a, b, c) {}
    ;
    this.onStartSearching = function() {}
    ;
    this.onEndSearching = function() {}
    ;
    this.bindResult = function(a, b) {
        a.innerHtml = b;
        a.item = b
    }
    ;
    this.loadItemValue = function(a) {
        return a.item
    }
    ;
    this.loadResultValue = function(a) {
        return a
    }
    ;
    this.init = function() {
        this.initTo(document.getElementById(this.id))
    }
    ;
    this.initTo = function(a, b) {
        this.id && a && (this._dom_field = a,
        this._dom_field.addEventListener ? (this._dom_field.addEventListener("keypress", this._bind(this._suppressSubmit, this), !1),
        this._dom_field.addEventListener("keydown", this._bind(this._trackArrow, this), !1),
        this._dom_field.addEventListener("keyup", this._bind(this._trackKeywordChange, this), !1)) : (this._dom_field.attachEvent("onkeypress", this._bind(this._suppressSubmit, this)),
        this._dom_field.attachEvent("onkeydown", this._bind(this._trackArrow, this)),
        this._dom_field.attachEvent("onkeyup", this._bind(this._trackKeywordChange, this))),
        this._dom_dropdown = document.createElement("DIV"),
        this._dom_dropdown.className = "select-edit-list",
        this._dom_dropdown.style.width = (b || this._dom_field.parentNode.clientWidth) + "px",
        this._dom_field.parentNode.parentNode.appendChild(this._dom_dropdown),
        this.domContainer = this._dom_field.parentNode.parentNode,
        this._dom_list = document.createElement("TABLE"),
        this._dom_dropdown.appendChild(this._dom_list),
        this.placeholder && 0 < this.placeholder.length ? (this._dom_placeholder = document.createElement("SPAN"),
        this._dom_placeholder.style.display = "none",
        this._dom_placeholder.style.position = "absolute",
        this._dom_placeholder.style.marginTop = "-22px",
        this._dom_placeholder.style.padding = "1px 2px",
        this._dom_placeholder.style.color = "#999",
        this._dom_placeholder.style.cursor = "text",
        this._dom_placeholder.style.cursor = "text",
        this._dom_field.addEventListener ? (this._dom_field.addEventListener("focus", this._bind(this._gotFocus, this), !1),
        this._dom_field.addEventListener("blur", this._bind(this._lostFocus, this), !1),
        this._dom_placeholder.addEventListener("click", this._bind(this._lostFocus, this), !1)) : (this._dom_field.attachEvent("onfocus", this._bind(this._gotFocus, this), !1),
        this._dom_field.attachEvent("onblur", this._bind(this._lostFocus, this), !1),
        this._dom_placeholder.attachEvent("onclick", this._bind(this._lostFocus, this))),
        this._dom_field.parentNode.appendChild(this._dom_placeholder),
        0 === this._dom_field.value.length && (this._dom_placeholder.style.display = "block")) : this._dom_field.addEventListener ? this._dom_field.addEventListener("blur", this._bind(this._lostFocus, this), !1) : this._dom_field.attachEvent("onblur", this._bind(this._lostFocus, this)),
        0 < this.items.length && this._buildDropDown(this, this.items))
    }
    ;
    this.toggle = function(a) {
        "block" === a._dom_dropdown.style.display ? a._hideDropDown() : a._showDropDown();
        a._dom_field.focus()
    }
    ;
    this.getVisible = function() {
        var a = this._dom_field.parentNode.parentNode;
        return a && "hidden" !== a.style.visibility
    }
    ;
    this.setVisible = function(a) {
        this._setVisible(this, a)
    }
    ;
    this._setVisible = function(a, b) {
        a = a._dom_field.parentNode.parentNode;
        if (!a)
            return !1;
        a.style.visibility = b ? "visible" : "hidden";
        return !0
    }
    ;
    this.clone = function(a, b) {
        var c = a || this._getID(this.id);
        a = new MQAutoComplete;
        a.id = c;
        a.items = this.items;
        a.onChange = this.onChange;
        a.loadItemValue = function(a) {
            $(c + "Key").value = a.key;
            if (window[c + "Autocomplete"].onChange)
                window[c + "Autocomplete"].onChange(c, a.key, a.value);
            return a.value
        }
        ;
        a.loadResultValue = function(a) {
            return a.key || a.Key
        }
        ;
        a.bindResult = function(a, b) {
            a.innerHTML = b.value || b.Value;
            a.key = b.key || b.Key;
            a.value = b.value || b.Value
        }
        ;
        a.onTyping = function(a, b) {
            $(c + "Key").value = a.defaultKey;
            if (a.onChange)
                a.onChange(c, a.defaultKey, b.value)
        }
        ;
        var d = this._dom_field.parentNode.parentNode.cloneNode(!1)
          , f = this._dom_field.parentNode.cloneNode(!1);
        d.appendChild(f);
        d = this._dom_field.cloneNode(!1);
        d.setAttribute("id", c);
        b && d.setAttribute("name", b);
        d.value = "";
        f.appendChild(d);
        var e = this._dom_field.parentNode.getElementsByTagName("input")[1].cloneNode(!1);
        e.setAttribute("id", c + "Key");
        b && e.setAttribute("name", b + "Key");
        f.appendChild(e);
        b = this._dom_field.parentNode.getElementsByTagName("a")[0].cloneNode(!0);
        b.setAttribute("onclick", c + "Autocomplete.toggle(" + c + "Autocomplete)");
        f.appendChild(b);
        a.initTo(d, this._dom_field.parentNode.clientWidth);
        a._setVisible(a, this.getVisible());
        return window[c + "Autocomplete"] = a
    }
    ;
    this._dom_list = this._dom_dropdown = this._dom_placeholder = this._dom_field = null;
    this._searchCall = this._searching = this._preventLostFocus = !1;
    this._listItems = [];
    this._dropDownListIndex = -1;
    this._getID = function(a) {
        for (var b = 1, c = a + b; $(c); )
            b++,
            c = a + b;
        return c
    }
    ;
    this._getValue = function() {
        return this.multiple && 0 <= this._dom_field.value.indexOf("|") ? this._dom_field.value.substring(this._dom_field.value.lastIndexOf("|") + 1) : this.multiple && 0 <= this._dom_field.value.indexOf(",") ? this._dom_field.value.substring(this._dom_field.value.lastIndexOf(",") + 1) : this.multiple && 0 <= this._dom_field.value.indexOf(";") ? this._dom_field.value.substring(this._dom_field.value.lastIndexOf(";") + 1) : this._dom_field.value
    }
    ;
    this._setValue = function(a) {
        this.multiple && 0 <= this._dom_field.value.indexOf("|") ? this._dom_field.value = this._dom_field.value.substring(0, this._dom_field.value.lastIndexOf("|") + 1) + a : this.multiple && 0 <= this._dom_field.value.indexOf(",") ? this._dom_field.value = this._dom_field.value.substring(0, this._dom_field.value.lastIndexOf(",") + 1) + a : this.multiple && 0 <= this._dom_field.value.indexOf(";") ? this._dom_field.value = this._dom_field.value.substring(0, this._dom_field.value.lastIndexOf(";") + 1) + a : this._dom_field.value = a
    }
    ;
    this._bind = function(a, b) {
        return function() {
            return arguments ? a(this, b, arguments[0]) : a(this, b)
        }
    }
    ;
    this._bindIE = function(a, b, c) {
        return function() {
            return a(c, b)
        }
    }
    ;
    this._showDropDown = function() {
        this.onPopup(this._dom_field, this._dom_dropdown);
        this._dom_dropdown.style.width = this._dom_field.parentNode.clientWidth + "px";
        this._dom_dropdown.style.display = "none";
        this._dom_dropdown.style.height = "auto";
        var a = document.body
          , b = document.documentElement;
        a = Math.max(a.scrollHeight, a.offsetHeight, b.clientHeight, b.scrollHeight, b.offsetHeight);
        this._dom_dropdown.style.display = "block";
        b = this._dom_dropdown.getBoundingClientRect().top - document.body.getBoundingClientRect().top;
        a = a - b - 20;
        this._dom_dropdown.offsetHeight > a && (this._dom_dropdown.style.overflowY = "auto",
        this._dom_dropdown.style.height = a + "px")
    }
    ;
    this._hideDropDown = function() {
        this._dom_dropdown.style.display = "none";
        0 === this.items.length && (this._listItems = [])
    }
    ;
    this._gotFocus = function(a, b, c) {
        b._dom_placeholder && (b._dom_placeholder.style.display = "none",
        b._dom_field.focus())
    }
    ;
    this._lostFocus = function(a, b, c) {
        0 === b._dom_field.value.length && b._dom_placeholder && (b._dom_placeholder.style.display = "block");
        a = c.explicitOriginalTarget || c.toElement;
        b._preventLostFocus || !a || "A" == a.tagName || "EM" == a.tagName || a.parentNode && ("A" == a.parentNode.tagName || "EM" == a.parentNode.tagName) || b._hideDropDown();
        b._preventLostFocus = !1
    }
    ;
    this._trackKeywordChange = function(a, b, c) {
        c = c || window.event;
        a = c.keyCode;
        c.charCode && 0 === a && (a = c.charCode);
        40 === a || 38 === a || 13 === a || 32 === a || 0 === a || b._searching || b._searchCall || (setTimeout(function() {
            b._searchByKeyword(b)
        }, 500),
        b._searchCall = !0)
    }
    ;
    this._searchByKeyword = function(a) {
        a._dropDownListIndex = -1;
        if (!(a._searching || 0 < a.items.length)) {
            var b = a._getValue();
            if (2 > b.length)
                a._searchCall = !1,
                a._hideDropDown();
            else {
                var c = a.buildAjaxQueryUrl();
                if (c)
                    if (a._searching = !0,
                    a._searchCall = !1,
                    Class(a._dom_field, "wait"),
                    a._dom_field.style.backgroundPosition = a._dom_field.clientWidth - 18 + "px center",
                    a.onStartSearching(),
                    a.post) {
                        var d = a.buildAjaxPostParams() || {};
                        d.keyword = b;
                        Ajax.post(c, d, !1, {
                            onready: function(b) {
                                a.onEndSearching();
                                a._processCallBack(a, b)
                            }
                        })
                    } else
                        Ajax.get(c, {
                            keyword: b
                        }, {
                            onready: function(b) {
                                a.onEndSearching();
                                a._processCallBack(a, b)
                            }
                        })
            }
        }
    }
    ;
    this._processCallBack = function(a, b) {
        for (; a._dom_list.hasChildNodes(); )
            a._dom_list.removeChild(a._dom_list.firstChild);
        if (null != b && "" != b && "[]" != b) {
            b = eval("(" + b + ")");
            a._listItems = [];
            if (1 == b.length && a._value == a.loadResultValue(b[0]))
                return a._hideDropDown(),
                a._searching = !1,
                a._dom_field.className = "",
                !1;
            a._buildDropDown(a, b);
            a._showDropDown()
        } else
            a._hideDropDown();
        a._searching = !1;
        Class(a._dom_field, "wait", !0);
        return !0
    }
    ;
    this._buildDropDown = function(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = a._dom_list.insertRow(a._dom_list.rows.length).insertCell(0)
              , f = document.createElement("A");
            f.href = "javascript:void(false);";
            a.bindResult(f, b[c]);
            f.addEventListener ? f.addEventListener("click", a._bind(a._itemClick, a), !1) : f.attachEvent("onclick", a._bindIE(a._itemClickIE, a, c));
            a._listItems.push(f);
            d.appendChild(f)
        }
    }
    ;
    this._itemClick = function(a, b) {
        if (a.className && "more" === a.className)
            return !1;
        b._setValue(b.loadItemValue(a, !0));
        b._hideDropDown();
        return !1
    }
    ;
    this._itemClickIE = function(a, b) {
        if ((a = b._listItems[a]) && a.className && "more" === a.className)
            return !1;
        b._setValue(b.loadItemValue(a, !0));
        b._hideDropDown();
        return !1
    }
    ;
    this._suppressSubmit = function(a, b, c) {
        b.onTyping(a);
        return !0
    }
    ;
    this._trackArrow = function(a, b, c) {
        c = c || window.event;
        a = c.keyCode;
        c.charCode && 0 === a && (a = c.charCode);
        if (b._listItems && 0 < b._listItems.length)
            switch (a) {
            case 40:
                c.preventDefault && c.preventDefault();
                -1 !== b._dropDownListIndex && b._listItems[b._dropDownListIndex] && (b._listItems[b._dropDownListIndex].className = "");
                b._dropDownListIndex < b._listItems.length - 1 ? ++b._dropDownListIndex : b._dropDownListIndex = 0;
                c = b._listItems[b._dropDownListIndex];
                "more" == c.className && (b._dropDownListIndex = 0,
                c = b._listItems[b._dropDownListIndex]);
                b._setValue(b.loadItemValue(c, !1));
                c.className = "selected";
                b._preventLostFocus = !0;
                b._trackScrolledVisibility(c, b);
                break;
            case 38:
                c.preventDefault && c.preventDefault();
                -1 !== b._dropDownListIndex && b._listItems[b._dropDownListIndex] && (b._listItems[b._dropDownListIndex].className = "");
                0 >= b._dropDownListIndex ? b._dropDownListIndex = b._listItems.length - 1 : --b._dropDownListIndex;
                -1 !== b._dropDownListIndex && b._listItems[b._dropDownListIndex] && (c = b._listItems[b._dropDownListIndex],
                "more" == c.className && (b._dropDownListIndex = b._listItems.length - 2,
                c = b._listItems[b._dropDownListIndex]),
                b._setValue(b.loadItemValue(c, !1)),
                c.className = "selected",
                b._trackScrolledVisibility(c, b));
                break;
            case 13:
            case 32:
            case 0:
                return b._hideDropDown(),
                !1;
            case 27:
                return b._dom_field.focus(),
                b._hideDropDown(),
                !1;
            case 8:
                return b._dom_field.focus(),
                b._dom_field.value = b._dom_field.value.substring(0, b._dom_field.value.length),
                b._searching || b._searchCall || (setTimeout(function() {
                    b._searchByKeyword(b)
                }, 500),
                b._searchCall = !0),
                !1
            }
        return !0
    }
    ;
    this._trackScrolledVisibility = function(a, b) {
        if (a) {
            a = a.parentNode.parentNode;
            var c = a.parentNode.parentNode.parentNode;
            c && a.offsetTop + a.offsetHeight > c.scrollTop + c.offsetHeight && (b = b._listItems[b._dropDownListIndex + 1],
            c.scrollTop = b && "more" == b.className ? a.offsetTop + a.offsetHeight - c.offsetHeight + b.parentNode.parentNode.offsetHeight : a.offsetTop + a.offsetHeight - c.offsetHeight);
            c && a.offsetTop < c.scrollTop && (c.scrollTop = a.offsetTop)
        }
    }
}
function searchTips(a, b) {
    var c = this, d, f;
    var e = a.offsetHeight || 35;
    this.hide = function() {
        Class(g, "active", !0);
        d = null
    }
    ;
    this.render = function(b) {
        var d = JSON.parse(b);
        b = d.length;
        g.innerHTML = "";
        if (0 < b) {
            Class(g, "active");
            for (var e = 0; e < b; e++)
                (function(b) {
                    var e = document.createElement("li")
                      , f = d[b];
                    e.className = "search-tips__item";
                    e.innerText = f;
                    g.appendChild(e);
                    Core.AddHandler(e, "click", function() {
                        a.value = f;
                        c.hide()
                    })
                }
                )(e)
        } else
            c.hide()
    }
    ;
    this.search = function(e) {
        e = e || window.event;
        var h = a.value
          , l = e.keyCode || e.which;
        if (2 < h.length && 40 !== l && 38 !== l && 13 !== l && 39 !== l && 37 !== l)
            clearTimeout(f),
            f = setTimeout(function() {
                Ajax.get(b, {
                    keyword: h
                }, {
                    onready: c.render
                })
            }, 1E3);
        else if (e.preventDefault(),
        40 !== l || d)
            if (40 === l) {
                if (e = d.nextSibling)
                    Class(d, "active", !0),
                    Class(e, "active"),
                    d = e
            } else if (38 === l && d) {
                if (e = d.previousSibling)
                    Class(d, "active", !0),
                    Class(e, "active"),
                    d = e
            } else
                13 === l && d && (a.value = d.innerHTML),
                c.hide();
        else
            (d = g.firstChild) && Class(d, "active")
    }
    ;
    var g = document.createElement("ul");
    g.className = "search-tips__result";
    Class(a, "search-tips__node");
    g.style.top = e - 1 + "px";
    a.parentNode.style.position = "relative";
    a.parentNode.appendChild(g);
    Core.AddHandler(a, "keyup", c.search);
    Core.AddHandler(a, "focus", c.search);
    Core.AddHandler(a, "blur", function() {
        setTimeout(c.hide, 200)
    })
}
function Searchbox() {
    this.multiselect_separator = ",";
    var a = this;
    this.AjaxEnabled = !0;
    this.OnRenderResults = function(a) {
        return !0
    }
    ;
    this.OnRenderElement = void 0;
    this.OnSelectResult = function(a) {
        return !0
    }
    ;
    this.ShowHide = function() {
        var a = $("searchBoxContainer");
        if (!a)
            return !0;
        if ("search-box" == a.className)
            return a.className = "search-box search-box_open",
            !1;
        alert("need submit");
        a.className = "search-box";
        return !0
    }
    ;
    this.Render = function(b, c, d, f, e, g) {
        (oldIEelement = $(c)) && oldIEelement.parentNode.removeChild(oldIEelement);
        this.requestURL = e;
        this.container = $(b);
        b = document.createElement("div");
        b.className = "searchbox";
        this.container.style.position = "relative";
        this.input = document.createElement("input");
        this.input.type = "text";
        this.input.id = c;
        this.input.name = c;
        this.input.value = d;
        this.input.title = g;
        this.input.className = "input";
        this.input.style.width = "100%";
        this.input.setAttribute("autocomplete", "off");
        this.input.onkeyup = this.OnKeyPress;
        this.input.onkeydown = this.OnKeyDown;
        b.appendChild(this.input);
        this.container.appendChild(b);
        this.Multiselect = f;
        this.input.onblur = function() {
            var b = this;
            b.focused = !1;
            window.setTimeout(function() {
                b.focused || (a.ResultsPanel.Hide(),
                "" == b.value && a.ShowHelpText())
            }, 10)
        }
        ;
        this.input.onfocus = function() {
            this.focused = !0;
            a.HideHelpText()
        }
        ;
        this.HelpText = createElement(null, "div", {
            className: "searchboxHelpText"
        });
        this.HelpText.style.position = "absolute";
        this.HelpText.style.top = "1px";
        this.HelpText.style.color = "#626363";
        this.HelpText.style.padding = "2px";
        this.HelpText.style.lineHeight = "15px";
        this.HelpText.innerHTML = g;
        this.container.appendChild(this.HelpText);
        this.HelpText.onclick = this.HideHelpText;
        this.HelpText.onscreen = !0;
        this.HelpText.style.display = "none";
        "" == this.input.value && (this.HelpText.style.display = "")
    }
    ;
    this.HideHelpText = function() {
        if (a.input.disabled)
            return !1;
        a.HelpText.onscreen && (a.HelpText.style.display = "none");
        a.HelpText.onscreen = !1;
        a.input.focus()
    }
    ;
    this.ShowHelpText = function() {
        a.HelpText.onscreen || (a.HelpText.style.display = "");
        a.HelpText.onscreen = !0
    }
    ;
    this.PressTimeout = null;
    this.SendAjaxDelay = 500;
    this.OnKeyPress = function(b) {
        a.PressTimeout && clearTimeout(a.PressTimeout);
        b || (b = window.event);
        if (13 == b.keyCode && 0 <= a.ResultsPanel.SelectedResult) {
            if (0 == a.ResultsPanel.ResultsInDom())
                return !0;
            a.ResultsPanel.Hide();
            a.OnSelectResult && a.OnSelectResult(eval(a.JSON)[a.ResultsPanel.SelectedResult]);
            return !1
        }
        if (40 == b.keyCode || 38 == b.keyCode)
            return !1;
        1 < a.GetValue().length && 1 == a.AjaxEnabled ? a.PressTimeout = setTimeout(function() {
            var b = a.GetValue();
            Ajax.get(a.requestURL, {
                keyword: b
            }, {
                onready: a.SearchCallBack
            })
        }, a.SendAjaxDelay) : a.ResultsPanel.Hide();
        return !0
    }
    ;
    a.OnKeyDown = function(b) {
        b || (b = window.event);
        if (13 == b.keyCode) {
            if (0 == a.ResultsPanel.ResultsInDom())
                return !0;
            a.OnSelectResult && a.OnSelectResult(eval(a.JSON)[a.ResultsPanel.SelectedResult]);
            return a.ResultsPanel.Hide()
        }
        return 40 == b.keyCode ? (a.ResultsPanel.SelectedResult += 1,
        a.MoveCursor(!0),
        !1) : 38 == b.keyCode && 0 <= a.ResultsPanel.SelectedResult ? (--a.ResultsPanel.SelectedResult,
        a.MoveCursor(!0),
        !1) : !0
    }
    ;
    this.ResultsPanel = function() {}
    ;
    this.ResultsPanel.ID = "searchboxResultsPanel";
    this.ResultsPanel.ResultsInDom = function() {
        return null != $(a.ResultsPanel.ID)
    }
    ;
    this.ResultsPanel.SelectedResult = -1;
    this.ResultsPanel.OnClick = function(b) {
        b = b || window.event;
        b.target || (b.target = b.srcElement);
        b.target && a.SetValue(b.target.login);
        a.ResultsPanel.Hide();
        a.OnSelectResult && a.OnSelectResult(eval(a.JSON)[a.ResultsPanel.SelectedResult])
    }
    ;
    this.ResultsPanel.RenderResults = function() {
        var b = eval(a.JSON);
        a.ResultsPanel.SelectedResult = -1;
        if (b && 0 != b.length && a.input.focused) {
            if (0 == a.ResultsPanel.ResultsInDom()) {
                a.ResultsPanel.panel = document.createElement("div");
                a.ResultsPanel.panel.style.backgroundColor = "#FFFFFF";
                a.ResultsPanel.panel.id = a.ResultsPanel.ID;
                a.ResultsPanel.panel.style.position = "absolute";
                var c = getOffsetRect(a.container);
                a.ResultsPanel.panel.style.top = c.top + (a.container.clientHeight || 20) - 3 + "px";
                a.ResultsPanel.panel.style.left = c.left + "px";
                a.ResultsPanel.panel.style.minWidth = a.container.offsetWidth + "px";
                a.ResultsPanel.panel.style.zIndex = 3001;
                a.ResultsPanel.panel.style.cursor = "default";
                a.ResultsPanel.panelBox = document.createElement("div");
                a.ResultsPanel.panelBox.style.border = "solid 1px #000000";
                a.ResultsPanel.panelBox.style.color = "#000000";
                a.ResultsPanel.panel.appendChild(a.ResultsPanel.panelBox);
                document.body.appendChild(a.ResultsPanel.panel)
            }
            a.ResultsPanel.panelBox.innerHTML = "";
            if (1 == b.length && b[0].Login == a.GetValue())
                a.ResultsPanel.SelectedResult = -1,
                a.SetValue(b[0].Login),
                a.ResultsPanel.Hide();
            else
                for (var d in b)
                    b.hasOwnProperty(d) && (a.OnRenderElement ? c = a.OnRenderElement(b[d]) : (c = document.createElement("div"),
                    c.innerHTML = encodeHtml(b[d].Login + (b[d].Name && "" != b[d].Name ? " (" + b[d].Name + ")" : "")),
                    c.style.padding = "2px",
                    c.login = b[d].Login,
                    c.number = d,
                    c.style.cursor = "default",
                    c.style.zIndex = 101,
                    c.onmouseover = a.HoverResult,
                    c.onclick = a.ResultsPanel.OnClick,
                    c.onmousedown = function() {
                        setTimeout(function() {
                            a.input.focus()
                        }, 0)
                    }
                    ),
                    c && a.ResultsPanel.panelBox.appendChild(c))
        } else
            a.ResultsPanel.Hide()
    }
    ;
    this.ResultsPanel.Hide = function() {
        a.OnRenderResults("[]");
        1 == a.ResultsPanel.ResultsInDom() && (a.OnSelectResult && 0 <= a.ResultsPanel.SelectedResult && a.OnSelectResult(eval(a.JSON)[a.ResultsPanel.SelectedResult]),
        a.ResultsPanel.panel.parentNode.removeChild(a.ResultsPanel.panel));
        return !1
    }
    ;
    this.HoverResult = function(b) {
        b || (b = window.event);
        var c = b.toElement;
        c || (c = b.currentTarget);
        a.ResultsPanel.SelectedResult = c.number;
        a.MoveCursor(!1)
    }
    ;
    this.SearchCallBack = function(b) {
        a.JSON = b;
        a.OnRenderResults(b) && a.ResultsPanel.RenderResults()
    }
    ;
    this.MoveCursor = function(b) {
        var c = a.ResultsPanel.panelBox.childNodes
          , d = c.length;
        a.ResultsPanel.SelectedResult >= d && (a.ResultsPanel.SelectedResult = 0);
        0 > a.ResultsPanel.SelectedResult && 0 < d && (a.ResultsPanel.SelectedResult = d - 1);
        0 == d && (a.ResultsPanel.SelectedResult = -1);
        for (var f in c)
            c[f].style && (c[f].style.backgroundColor = "#FFFFFF",
            c[f].style.color = "#000",
            c[f].number == a.ResultsPanel.SelectedResult && (c[f].style.backgroundColor = "Highlight",
            c[f].style.color = "#FFF",
            b && a.SetValue(c[f].login)))
    }
    ;
    this.SetValue = function(b) {
        b && 0 < b.length && this.HideHelpText();
        if (a.Multiselect) {
            var c = a.input.value.substring(0, a.input.value.lastIndexOf(a.multiselect_separator));
            a.input.value = c + ("" == c ? "" : a.multiselect_separator) + b
        } else
            a.input.value = b
    }
    ;
    this.GetValue = function() {
        if (!a.Multiselect)
            return a.input.value;
        var b = a.input.value;
        return b.substring(b.lastIndexOf(a.multiselect_separator) + 1)
    }
}
function UserLinkMenu() {
    var a = null
      , b = null
      , c = null
      , d = null
      , f = this;
    this.Init = function() {
        if (a = $("userLinkMenu"))
            if (b = $("userLinkContainer"))
                if (c = $("userlink"),
                Core.IsTouchDevice()) {
                    null != c && (c.onclick = function() {
                        f.Show();
                        return !1
                    }
                    );
                    a.onmousedown = function(a) {
                        Core.StopProp(a);
                        Core.PreventSelection()
                    }
                    ;
                    a.onclick = function(a) {
                        Core.StopProp(a)
                    }
                    ;
                    d = document.createElement("input");
                    d.type = "checkbox";
                    d.className = "blurHandler";
                    d.onblur = function() {
                        f.Hide()
                    }
                    ;
                    a.appendChild(d);
                    var e = a.getElementsByTagName("a"), g;
                    for (g in e)
                        e[g].onmousedown = function(a) {
                            a = a || window.event;
                            Core.StopProp(a);
                            a.preventDefault && a.preventDefault()
                        }
                } else
                    null != b && (b.onmouseover = function() {
                        f.Show()
                    }
                    ,
                    b.onmouseout = function() {
                        f.Hide()
                    }
                    )
    }
    ;
    this.Show = function() {
        a.className = "userLinkMenu hovered";
        null != d && window.setTimeout(function() {
            d.focus()
        }, 10)
    }
    ;
    this.Hide = function() {
        a.className = "userLinkMenu"
    }
    ;
    mqGlobal.AddOnReady(function() {
        f.Init()
    })
}
function HotKeysClass() {
    function a(a, b, c, g, h) {
        var d = b ? !0 : !1
          , e = c ? !0 : !1
          , f = g ? !0 : !1;
        this.IsPressed = function(b) {
            b = b || window.event;
            var c = b ? b.keyCode : b.which;
            var g = b.shiftKey ? !0 : !1;
            var l = b.ctrlKey ? !0 : !1;
            b = b.altKey ? !0 : !1;
            return c == a && d == g && e == l && f == b
        }
        ;
        this.ExecCallback = function(a) {
            return h ? h(a) : !0
        }
    }
    var b = []
      , c = window.document.onkeydown;
    window.document.onkeydown = function(a) {
        for (var d = 0; d < b.length; d++)
            if (b[d].IsPressed(a) && 0 == b[d].ExecCallback(a))
                return !1;
        c && c(arguments)
    }
    ;
    this.Add = function(c, f, e, g, h) {
        b.push(new a(c,e,g,h,f))
    }
}
var HotKeys = new HotKeysClass;
function SearchBoxContainer() {
    var a = !1, b = !1, c, d, f, e, g = this;
    this.Init = function() {
        f = $("searchBoxContainer");
        d = $("keyword");
        e = $("searchSubmit");
        c = $("main_search_form");
        null != f && null != d && null != e && null != c && (void 0 != f.className && 0 <= f.className.indexOf("open") && (b = a = !0),
        b || (d.onblur = function() {
            SearchBoxContainer.Hide(null, !0)
        }
        ,
        d.onkeydown = function(a) {
            27 == (a || window.event).keyCode && (SearchBoxContainer.Hide(null, !0),
            this.blur())
        }
        ,
        f.onclick = function() {
            SearchBoxContainer.Show(null, !0)
        }
        ,
        e.onmouseout = function(a) {
            Core.StopProp(a)
        }
        ,
        e.onmousedown = function(b) {
            a && (Core.StopProp(b),
            b.preventDefault && b.preventDefault())
        }
        ),
        e.onclick = function(b) {
            if (a)
                if (Core.StopProp(b),
                b.preventDefault && b.preventDefault(),
                c.onsubmit)
                    c.onsubmit();
                else
                    c.submit()
        }
        ,
        Core.IsTouchDevice() || b || (f.onmouseover = function() {
            SearchBoxContainer.Show(null, !1)
        }
        ,
        f.onmouseout = function() {
            SearchBoxContainer.Hide(null, !1)
        }
        ))
    }
    ;
    this.ShowHide = function() {
        a ? this.Hide(f, !0) : this.Show(f, !0);
        return !1
    }
    ;
    this.Show = function(b, c) {
        b = b || f;
        if (!b)
            return !1;
        b.className = "search-box search-box_open";
        $("privateAreaMessages") && ($("privateAreaMessages").style.display = "none");
        $("langMenuContainer") && ($("langMenuContainer").style.display = "none");
        $("loginRegisterButtons") && ($("loginRegisterButtons").style.display = "none");
        $("profile") && ($("profile").style.display = "none");
        if (c || Core.IsTouchDevice())
            a = !0,
            d.focus();
        return !1
    }
    ;
    this.Hide = function(b, c) {
        if (!c && a)
            return !1;
        b = b || f;
        if (!b)
            return !1;
        b.className = "search-box";
        c && (a = !1);
        $("privateAreaMessages") && ($("privateAreaMessages").style.display = "inline-block");
        $("langMenuContainer") && ($("langMenuContainer").style.display = "inline-block");
        $("loginRegisterButtons") && ($("loginRegisterButtons").style.display = "inline-block");
        $("profile") && ($("profile").style.display = "inline-block");
        return !1
    }
    ;
    mqGlobal.AddOnReady(function() {
        g.Init();
        HotKeys.Add(112, function(a) {
            g.ShowHide()
        })
    })
}
SearchBoxContainer = new SearchBoxContainer;
function MoveComments() {
    function a() {
        var a = $("content" + d);
        if (!a)
            return null;
        a = a.getElementsByTagName("P");
        for (var b in a)
            if (void 0 != a[b].innerHTML && 10 < a[b].innerHTML.length && 64 > a[b].innerHTML.length && 0 > a[b].innerHTML.indexOf(".") && 0 > a[b].innerHTML.indexOf("<") && 0 > a[b].innerHTML.indexOf(">") && 10 < a[b].innerHTML.indexOf("?"))
                return a[b].innerHTML;
        return null
    }
    function b(a, b) {
        var c = $("content" + d);
        if (c) {
            c = c.parentNode.parentNode;
            var e = c.parentNode.childNodes;
            c.style.borderTop = a ? "medium none" : "2px dashed #6982AE";
            c.style.marginTop = a ? "0px" : "-2px";
            var f = !1, g = 0, l;
            for (l in e) {
                if (b && 0 < b && g >= b)
                    break;
                e[l] == c && (f = !0);
                !f || void 0 == e[l].className || 0 > e[l].className.indexOf("comment") || (e[l].style.backgroundColor = a ? "" : "#F6F8FD",
                g++)
            }
            a || b ? (a = $("followingCommentsLabel")) && c.parentNode.removeChild(a) : (a = document.createElement("div"),
            a.id = "followingCommentsLabel",
            window.move_comments_show_following_label ? (a.innerHTML = window.move_comments_following_label_title,
            a.style.backgroundColor = "#F6F8FD",
            a.style.position = "relative",
            a.style.fontSize = "16px",
            a.style.textAlign = "center",
            a.style.padding = "10px",
            a.style.borderBottom = "1px dashed #6982AE",
            a.style.borderTop = "1px dashed #6982AE",
            a.style.marginTop = "-1px",
            a.style.color = "#6982AE") : (a.style.backgroundColor = "#FFF",
            a.style.position = "relative",
            a.style.borderTop = "2px dashed #6982AE",
            a.style.marginTop = "-2px",
            a.style.height = "0px"),
            c.parentNode.appendChild(a))
        }
    }
    var c, d, f, e, g = !1, h = this;
    this.ShowForm = function(m, l, p) {
        if (g)
            return !1;
        var t = $("moveComments_window")
          , n = $("moveComments_newTopicName")
          , r = $("existing_only_one")
          , q = $("new_only_one");
        t && (c = {
            firstCommentId: m,
            categoryId: 0,
            newTopicTitle: null
        },
        q.checked = r.checked = p ? null : !0,
        e = p ? 1 : 0,
        d = m,
        f = l,
        b(!1, e),
        n.value = a(),
        g = !0,
        t.style.display = "",
        Dialog.show(window.move_comments_title, t, 500, 142, h.CloseForm),
        n.focus());
        return !1
    }
    ;
    this.CloseForm = function() {
        var a = $("moveComments_window");
        a && (b(!0),
        g = !1,
        a.style.display = "none",
        Dialog.hide())
    }
    ;
    this.SendForm = function(a) {
        if (!$("moveComments_window"))
            return !1;
        c.newTopicTitle = $("moveComments_newTopicName").value;
        c.categoryId = $("moveComments_categoryId").value;
        c.count = e;
        c.__signature = a;
        if ("" == c.newTopicTitle || 0 == c.categoryId)
            return !1;
        Ajax.post("/" + f + "/forum/movecomments", c, !1, {
            onready: function(a) {
                a = eval("(" + a + ")");
                null != a && void 0 != a.Url && null != a.Url ? (h.CloseForm(),
                window.location.href = a.Url) : (alert("error"),
                h.CloseForm());
                window.location.reload();
                return !1
            },
            onerror: function() {
                alert("error");
                h.CloseForm();
                window.location.reload()
            }
        });
        return !1
    }
    ;
    this.SendFormExisting = function(a) {
        if (!$("moveComments_window"))
            return !1;
        c.topicId = $("existing_topic_id").value;
        c.__signature = a;
        c.count = e;
        if (!c.topicId)
            return !1;
        Ajax.post("/" + f + "/forum/movecomments_existing", c, !1, {
            onready: function(a) {
                a = eval("(" + a + ")");
                null != a && void 0 != a.Url && null != a.Url ? (h.CloseForm(),
                window.location.href = a.Url) : (alert("error"),
                h.CloseForm());
                window.location.reload();
                return !1
            },
            onerror: function() {
                alert("error");
                h.CloseForm();
                window.location.reload()
            }
        });
        return !1
    }
    ;
    this.OnOnlyOneChanged = function(a) {
        var c = $("existing_only_one");
        c && (c.checked = a.checked);
        if (c = $("new_only_one"))
            c.checked = a.checked;
        e = a.checked ? 0 : 1;
        b(!0);
        b(!1, e)
    }
}
function MoveTopic() {
    var a, b, c = !1;
    this.ShowForm = function(d, f) {
        if (c)
            return !1;
        var e = $("moveTopic_window");
        e && (a = {
            topicId: d,
            categoryId: 0
        },
        b = f,
        c = !0,
        e.style.display = "",
        Dialog.show(window.move_topic_title, e, 500, 142, MoveTopic.CloseForm));
        return !1
    }
    ;
    this.CloseForm = function() {
        var a = $("moveTopic_window");
        a && (c = !1,
        a.style.display = "none",
        Dialog.hide())
    }
    ;
    this.SendForm = function(c) {
        if (!$("moveTopic_window"))
            return !1;
        a.categoryId = +$("moveTopic_categoryId").value;
        a.__signature = c;
        if (1 > a.categoryId)
            return !1;
        Ajax.post("/" + b + "/forum/movetopic", a, !1, {
            onready: function(a) {
                a = eval("(" + a + ")");
                null != a && void 0 != a.Url && null != a.Url ? (MoveTopic.CloseForm(),
                window.location = a.Url) : (alert("error"),
                MoveTopic.CloseForm(),
                window.location.reload());
                return !1
            },
            onerror: function() {
                alert("error");
                MoveTopic.CloseForm();
                window.location.reload()
            }
        });
        return !1
    }
}
MoveTopic = new MoveTopic;
function showComplaintForm(a, b, c, d, f) {
    var e = $("complaint_window");
    if (e) {
        var g = $("complaint_reason");
        e.style.display = "";
        e.complaintData = {
            moduleID: b,
            typeID: c,
            parentModuleID: d,
            itemID: a,
            __signature: f
        };
        Dialog.show(g ? g.title : null, e, 500, 160, CloseComplaintForm, null, null, " ui no-bottom-margin", !1, "fixed");
        CalculateFieldLength($("complaint_reason"), "complaint_char_counter", 3, 150, 500 == $("complaint_reason_select").value)
    }
}
function CloseComplaintForm() {
    RefreshComplaintForm();
    var a = $("complaint_window")
      , b = $("complaint_reason");
    b && (b.value = "");
    a && (a.style.display = "none");
    Dialog.hide()
}
function ComplainCallback(a) {
    "success" == eval(a) ? CloseComplaintForm() : $("complaint_error").style.display = ""
}
function SendComplaint() {
    RefreshComplaintForm();
    var a = $("complaint_window");
    if (a) {
        a = a.complaintData;
        var b = document.getElementById("complaint_reason_select").value;
        if (500 == b && !window.Validate($("complaintForm")))
            return !1;
        var c = $("complaint_reason");
        c && (a.reason = c.value,
        a.reasonID = b,
        Ajax.post("/complaint/new", a, !1, {
            onready: ComplainCallback,
            onerror: ComplainCallback
        }))
    }
}
function RefreshComplaintForm() {
    var a = $("complaint_info").childNodes, b;
    for (b in a)
        a[b] && "field-validation-error" == a[b].className && "complaint_error" != a[b].id && (a[b].innerHTML = "");
    $("complaint_reason").className = null;
    $("complaint_error").style.display = "none"
}
function CalculateFieldLength(a, b, c, d, f) {
    var e = !0;
    f || (d = c = 0);
    $(b).innerHTML = a.value.length + "/" + d;
    a.value.length > d || a.value.length < c ? (Class($(b).parentNode, "field-validation-error"),
    e = !1) : Class($(b).parentNode, "field-validation-error", !0);
    return e
}
var Tabs = function() {
    this.TabsList = [];
    var a = !1
      , b = !1
      , c = !1
      , d = this;
    this.Create = function(f, e, g, h, m) {
        a = h;
        b = m;
        c = g;
        for (m = 0; m < f.length; m++) {
            var l = $(f[m].content_id);
            h = $(f[m].tab_id);
            var p = f[m].tag;
            if (l && h) {
                f[m].selected ? (Class(l, "selected"),
                Class(h, "selected"),
                e && e(h)) : (Class(l, "selected", !0),
                Class(h, "selected", !0));
                p = {
                    tab: h,
                    tab_content: l,
                    tag: p
                };
                Core.AddHandler(h, "click", function(a) {
                    a = a || window.event;
                    a.preventDefault ? a.preventDefault() : a.returnValue = !1;
                    var b = this;
                    for (a = 0; a < d.TabsList.length; a++)
                        if (d.TabsList[a].tab == b) {
                            d._selectedTab = d.TabsList[a].tag || d.TabsList[a].tab.id;
                            break
                        }
                    g ? d.SelectVerticalTab(b) : d.SelectTab(b);
                    e && setTimeout(function() {
                        e(b)
                    }, 0);
                    return !1
                });
                Class(h.parentNode, "vertical", !g);
                if (g) {
                    var t = createElement(null, "span", {
                        className: "rowBullet"
                    });
                    p.bullet = t;
                    var n = createElement(null, "li", {
                        className: "tab_content"
                    });
                    l.id && (n.id = "tab_" + l.id);
                    n.appendChild(l);
                    insertAfter(h.parentNode, n, h);
                    n.style.height = "auto";
                    prependElement(h, t)
                }
                d.TabsList.push(p)
            }
        }
        if (h = window.hashParams.Get("tab")) {
            var r = $(h);
            if (r) {
                if (h == d._selectedTab)
                    return;
                g ? d.SelectVerticalTab(r) : d.SelectTab(r);
                e && setTimeout(function() {
                    e(r)
                }, 0)
            }
        }
        if (f = window.hashParams.Get("id"))
            if (f = $(f)) {
                var q = window.scrollY + f.getBoundingClientRect().top;
                setTimeout(function() {
                    window.scrollTo(0, q)
                }, 1E3)
            }
        window.onhashchange = function() {
            var a = window.hashParams.Get("tab");
            if (!/^\#edit_\w+$/.test(document.location.hash) && (a || (a = d.TabsList[0].tag),
            a != d._selectedTab))
                for (var b = 0; b < d.TabsList.length; b++)
                    if (d.TabsList[b].tag == a) {
                        d.SelectTab(d.TabsList[b].tab);
                        d._selectedTab = a;
                        break
                    }
        }
    }
    ;
    this.SelectVerticalTab = function(c) {
        for (var e = null, f = 0; f < d.TabsList.length; f++)
            if (!a || d.TabsList[f].tab.id == c.id) {
                var h = d.TabsList[f].tab_content.parentNode
                  , m = 0 <= d.TabsList[f].tab.className.indexOf("selected");
                Class(d.TabsList[f].tab_content, "selected", !1);
                d.TabsList[f].tab.id != c.id || m ? m ? (animate(h, "height", "px", h.clientHeight || h.offsetHeight, 0, 0, 500, 50, 1.7, null),
                Class(d.TabsList[f].tab, "selected", !0)) : h.style.height = "0px" : (Class(d.TabsList[f].tab, "selected", !1),
                animate(h, "height", "px", 0, h.scrollHeight, 0, 500, 50, 1.7, .5),
                e = h)
            }
        b && e && setTimeout(function() {
            e && e.style.height == e.scrollHeight + "px" && (e.style.height = "auto")
        }, 600)
    }
    ;
    this.SelectTab = function(a) {
        if (c)
            return this.SelectVerticalTab(a);
        if (-1 == a.className.indexOf("add_lang"))
            for (var b = 0; b < d.TabsList.length; b++) {
                var f = -1 < d.TabsList[b].tab.className.indexOf("add_lang");
                d.TabsList[b].tab.id == a.id ? (Class(d.TabsList[b].tab_content, "selected", !1),
                window.hashParams.Set("tab", d.TabsList[b].tab.id)) : Class(d.TabsList[b].tab_content, "selected", !0);
                Class(d.TabsList[b].tab, "selected", d.TabsList[b].tab.id != a.id);
                1 == f && Class(d.TabsList[b].tab, "add_lang")
            }
    }
    ;
    this.FitVerticalTabsHeight = function() {
        for (var a = 0; a < d.TabsList.length; a++)
            if (!(0 > d.TabsList[a].tab.className.indexOf("selected"))) {
                var b = d.TabsList[a].tab_content.parentNode;
                b && b.style.height != b.scrollHeight + "px" && (b.style.height = "auto")
            }
    }
};
function LinkatorSelectAll() {
    for (var a = 0, b = document.getElementsByTagName("INPUT"), c = b.length - 1; 0 <= c; c--)
        null != b[c] && void 0 != b[c] && "checkbox" == b[c].type && "checkbox" == b[c].className && (0 == a && (a = b[c].checked ? 2 : 1),
        b[c].checked = 2 == a ? "" : "checked")
}
var SliderBar = function(a, b, c) {
    function d(a) {
        if (u) {
            a.preventDefault();
            a.stopPropagation();
            m();
            var b = a.changedTouches[0].pageX - T;
            a = a.target;
            var c = ha + b;
            0 < a.className.indexOf("right") ? c >= t ? E.value = H : c - n <= parseInt(C.style.left) || (c > -n / 2 && (v.style.width = c - parseInt(v.style.left) + n / 2 + "px",
            a.style.left = c + "px"),
            b = Math.round(c * G / t) + F,
            1 < J && (b = Math.round(b / J) * J),
            b > H && (b = H),
            b < z.value && (b = z.value),
            E.value = b) : c <= -n / 2 ? z.value = F : c + n / 2 >= parseInt(A.style.left) || (c > -n / 2 && (v.style.left = c + "px",
            v.style.width = ca - b + n / 2 + "px",
            C.style.left = c - n / 2 + "px"),
            b = Math.round(c * G / t) + F,
            1 < J && (b = Math.round(b / J) * J),
            b < F && (b = F),
            b > E.value && (b = E.value),
            z.value = b)
        }
    }
    function f(a) {
        var b = a.target;
        a.preventDefault();
        a.stopPropagation();
        u = !0;
        T = a.changedTouches[0].pageX;
        ha = parseInt(b.style.left)
    }
    function e(a) {
        if (q) {
            m();
            a = (window.event ? window.event : a).clientX - w.offsetLeft - r;
            a < -n / 2 ? a = -n / 2 : a > t + n / 2 && (a = t + n / 2);
            var b = parseInt(v.style.left, 0)
              , c = b + parseInt(v.style.width, 0)
              , d = b + (c - b) / 2;
            b > c && (b = c);
            0 > D && (D = a < d ? 1 : 0);
            1 == D ? (c - a > n && (v.style.left = a + "px",
            v.style.width = c - a + "px",
            C.style.left = a - n / 2 + "px"),
            a = Math.round((a + 7.5) * G / t) + F,
            1 < J && (a = Math.round(a / J) * J),
            a < F && (a = F),
            a > E.value && (a = E.value),
            z.value = a) : (a - b > n && (v.style.width = a - b + "px",
            A.style.left = a - n / 2 + "px"),
            a = Math.round((a - n / 2) * G / t) + F,
            1 < J && (a = Math.round(a / J) * J),
            a > H && (a = H),
            a < z.value && (a = z.value),
            E.value = a)
        }
    }
    function g(a) {
        u = q = !1
    }
    function h(a, b, c) {
        a.attachEvent ? a.attachEvent("on" + b, c) : a.addEventListener && a.addEventListener(b, c, !1)
    }
    function m() {
        window.getSelection ? window.getSelection().removeAllRanges() : document.selection && document.selection.clear && document.selection.clear()
    }
    function l(a, b) {
        w = document.getElementById(b + "Bar");
        v = document.getElementById(b + "BarRange");
        C = document.getElementById(b + "HandleLeft");
        A = document.getElementById(b + "HandleRight");
        document.getElementById(b + "Label");
        document.getElementById(b + "Input");
        D = -1;
        z = document.getElementById(b + "From");
        E = document.getElementById(b + "To");
        F = filters[b][2];
        H = filters[b][3];
        J = filters[b][4];
        ca = v.offsetWidth;
        G = H - F;
        if (I) {
            b = I;
            var c = 0
              , d = 0;
            do
                c += b.offsetLeft,
                d += b.offsetTop,
                b.scrollTop && I != b && (d -= b.scrollTop);
            while (b = b.offsetParent);b = [c, d];
            0 < b[0] && (r = b[0])
        }
        q = !0;
        Core.isMobile.any() || e(a)
    }
    function p(a, b) {
        var c = filters[a][2]
          , d = filters[a][3]
          , e = 5 < filters[a].length ? filters[a][5] : null
          , f = $(a + "BarRange")
          , g = $(a + "HandleLeft")
          , l = $(a + "HandleRight")
          , h = $(a + "From");
        a = $(a + "To");
        var p = +h.value
          , m = +a.value;
        p < c && (p = c);
        p > d && (p = d);
        m < c && (m = c);
        m > d && (m = d);
        p > m && (m = p);
        b && (h.value = p,
        a.value = m);
        d -= c;
        b = Math.round(t * (p - c) / d);
        c = Math.round(t * (m - c) / d);
        b == c && (c += 1);
        p = Math.round(t * (m - p) / d) + n;
        f.style.left = b - n / 2 + "px";
        f.style.width = p + "px";
        g.style.left = b - n + "px";
        l.style.left = c + "px";
        e && (h.setAttribute("onclick", "window.fpush('" + e + "');"),
        a.setAttribute("onclick", "window.fpush('" + e + "');"))
    }
    if ("undefined" != typeof filters) {
        var t = b ? b : 194, n = c ? c : 15, r = 0, q = !1, u = !1, w, v, C, A, D, z, E, F, H, J, G, I, T, ha, ca;
        (function() {
            h(window.document, "mousemove", function(a) {
                e(a)
            });
            h(window.document, "mouseup", function() {
                g()
            });
            I = document.getElementById(a);
            for (var b in filters) {
                var c = filters[b][0]
                  , n = filters[b][1]
                  , m = $(b)
                  , t = $(b + "Label")
                  , q = document.createElement("INPUT");
                q.id = b + "From";
                q.name = b + "From";
                try {
                    q.type = "number"
                } catch (N) {
                    q.type = "text"
                }
                q.value = c;
                q.className = "input";
                (function(a) {
                    q.onkeypress = function() {
                        window.setTimeout(function() {
                            p(a, !1)
                        }, 0)
                    }
                    ;
                    q.onchange = function() {
                        p(a, !0)
                    }
                }
                )(b);
                t.appendChild(q);
                c = document.createElement("SPAN");
                c.className = "delimiter";
                c.innerHTML = "&#8212;";
                t.appendChild(c);
                var r = document.createElement("INPUT");
                r.id = b + "To";
                r.name = b + "To";
                try {
                    r.type = "number"
                } catch (N) {
                    r.type = "text"
                }
                r.value = n;
                r.className = "input";
                (function(a) {
                    r.onkeypress = function() {
                        window.setTimeout(function() {
                            p(a, !1)
                        }, 0)
                    }
                    ;
                    r.onchange = function() {
                        p(a, !0)
                    }
                }
                )(b);
                t.appendChild(r);
                var v = document.createElement("DIV");
                v.className = "bar";
                v.id = b + "Bar";
                (function(a) {
                    v.onmousedown = function(b) {
                        l(b, a)
                    }
                }
                )(b);
                m.appendChild(v);
                n = document.createElement("DIV");
                n.className = "selection";
                n.id = b + "BarRange";
                v.appendChild(n);
                var A = document.createElement("DIV");
                A.className = "handle left";
                A.id = b + "HandleLeft";
                v.appendChild(A);
                var u = document.createElement("DIV");
                u.className = "handle right";
                u.id = b + "HandleRight";
                v.appendChild(u);
                p(b, !1);
                Core.isMobile.any() && function(a) {
                    u.addEventListener("touchstart", function(b) {
                        f(b);
                        l(b, a)
                    }, !0);
                    u.addEventListener("touchend", g, !0);
                    u.addEventListener("touchmove", function(a) {
                        d(a)
                    }, !0);
                    A.addEventListener("touchstart", function(b) {
                        f(b);
                        l(b, a)
                    }, !0);
                    A.addEventListener("touchend", g, !0);
                    A.addEventListener("touchmove", function(a) {
                        d(a)
                    }, !0)
                }(b)
            }
        }
        )();
        this.SerializeFilter = function() {
            var a = 0, b = {}, c;
            for (c in filters) {
                var d = filters[c]
                  , e = $(c + "From")
                  , f = $(c + "To")
                  , g = !1;
                e && f && (e = parseInt(e.value),
                e > d[2] && (g = !0,
                b[c + "From"] = e),
                e = parseInt(f.value),
                e < d[3] && (g = !0,
                b[c + "To"] = e),
                g && a++)
            }
            return {
                fields: a,
                json: 0 == a ? null : JSON.stringify(b)
            }
        }
        ;
        this.ResetFilter = function() {
            for (var a in filters) {
                var b = filters[a]
                  , c = $(a + "HandleLeft");
                c && (c.style.left = "-15px");
                if (c = $(a + "HandleRight"))
                    c.style.left = "194px";
                if (c = $(a + "BarRange"))
                    c.style.left = "-7.5px",
                    c.style.width = "209px";
                if (c = $(a + "From"))
                    c.value = b[2];
                if (c = $(a + "To"))
                    c.value = b[3]
            }
        }
        ;
        return this
    }
};
function Gallery(a, b, c, d, f) {
    b || (b = "_switcher");
    var e = [0]
      , g = this.Slider = document.getElementById(a);
    if (g && (a = g.parentNode)) {
        if (f && 700 > (document.body.clientWidth || document.body.offsetWidth)) {
            var h = a.parentNode;
            f = h.parentNode;
            f = (f.clientWidth || f.offsetWidth) - 40 | 0;
            var m = 2 * f / 3 | 0;
            a.style.height = m + "px";
            a.style.width = f + "px";
            h.style.width = f + 20 + "px";
            if (h = g.getElementsByTagName("img"))
                for (var l = 0; l < h.length; l++)
                    h[l].width = f,
                    h[l].height = m
        }
        var p = this.Current = 0
          , t = null
          , n = null;
        if (!this.Slider)
            return null;
        n = this.Slider.children || this.Slider.childNodes;
        var r = 0
          , q = function(a) {
            for (var b = 0, c = 0; c <= a; c++)
                b += e[c];
            return b
        };
        for (f = 0; f < n.length; f++)
            if (0 == n[f].nodeName.indexOf("#"))
                n.splice(f, 1),
                f--;
            else if (p++,
            m = e.length - 1,
            e[e.length] = n[f].offsetWidth,
            h = document.getElementById(n[f].id + b))
                h.shiftIndex = m,
                0 == m && (t = h),
                c || (c = "onclick"),
                h[c] = function() {
                    t = D(t.shiftIndex, this.shiftIndex)
                }
                ;
        if (d) {
            var u = createElement(a, "div", {
                className: "carousel-arrow"
            })
              , w = createElement(a, "div", {
                className: "carousel-arrow"
            });
            Class(u, "left");
            Class(w, "right");
            var v = function(a) {
                1 != a.isHiddenArrow && (fade(a, "in", 200, 20),
                a.style.cursor = "default",
                a.isHiddenArrow = !0)
            }
              , C = 0
              , A = function(a) {
                0 != a.isHiddenArrow && (fade(a, "out", 200, 20),
                a.style.cursor = "pointer",
                a.isHiddenArrow = !1)
            };
            v(u);
            v(w);
            u.onclick = function() {
                t = D(t.shiftIndex, t.shiftIndex - 1);
                0 >= t.shiftIndex && v(u);
                t.shiftIndex < p - 1 && A(w)
            }
            ;
            w.onclick = function() {
                t = D(t.shiftIndex, t.shiftIndex + 1);
                t.shiftIndex >= p - 1 && v(w);
                0 < t.shiftIndex && A(u)
            }
            ;
            a.onmouseover = function() {
                t && (clearTimeout(C),
                C = setTimeout(function() {
                    0 < t.shiftIndex && A(u);
                    t.shiftIndex < p - 1 && A(w)
                }, 100))
            }
            ;
            a.onmouseout = function() {
                t && (clearTimeout(C),
                C = setTimeout(function() {
                    0 < t.shiftIndex && v(u);
                    t.shiftIndex < p - 1 && v(w)
                }, 100))
            }
        }
        var D = function(a, c) {
            var d = a
              , e = document.getElementById(n[a].id + b);
            if (c == d || 0 > a || 0 > c || a > p - 1 || c > p - 1)
                return e;
            a = document.getElementById(n[c].id + b);
            g.style.left && "" != g.style.left || (g.style.left = "0px");
            d = -q(d);
            var f = -q(c);
            animate(g, "left", "px", d, f, r, 300, 60, .7, 0);
            r += 500;
            setTimeout(function() {
                r -= 500
            }, 500);
            e.className = "";
            a.className = "current";
            d = c;
            return a
        }
    }
}
var Mql5Cookie = function() {};
Mql5Cookie.defaultValue = "";
Mql5Cookie.domain = "";
Mql5Cookie.onSyncCallback = void 0;
Mql5Cookie.init = function(a, b, c) {
    Mql5Cookie.defaultValue = b;
    Mql5Cookie.domain = a;
    Mql5Cookie.onSyncCallback = c;
    0 > window.location.hostname.lastIndexOf(".") || Mql5Cookie.checkStorages()
}
;
Mql5Cookie.checkStorages = function() {
    var a = void 0
      , b = Mql5Cookie.localStorage("uniq");
    a || "undefined" == b || (a = b);
    b = Mql5Cookie.sessionStorage("uniq");
    a || "undefined" == b || (a = b);
    b = Mql5Cookie.httpStorage("uniq");
    "undefined" != b && (a || "undefined" == b || (a = b),
    a && 22 === a.length && b && 12 < b.length && 22 > b.length && (a = b));
    a || (a = Mql5Cookie.defaultValue);
    Mql5Cookie.localStorage("uniq", a);
    Mql5Cookie.sessionStorage("uniq", a);
    Mql5Cookie.httpStorage("uniq", a, {
        expires: new Date(2050,1,1),
        Domain: "." + Mql5Cookie.domain,
        Path: "/",
        SameSite: "None",
        Secure: !0
    });
    if (Mql5Cookie.onSyncCallback)
        Mql5Cookie.onSyncCallback()
}
;
Mql5Cookie.httpStorage = function(a, b, c) {
    if ("undefined" != typeof b) {
        c = c || {};
        var d = c.expires;
        if ("number" == typeof d && d) {
            var f = new Date;
            f.setTime(f.getTime() + 1E3 * d);
            d = c.expires = f
        }
        d && d.toUTCString && (c.expires = d.toUTCString());
        b = encodeURIComponent(b);
        a = a + "=" + b;
        for (var e in c)
            a += "; " + e,
            b = c[e],
            !0 !== b && (a += "=" + b);
        document.cookie = a
    } else
        return (c = document.cookie.match(new RegExp("(?:^|; )" + a.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"))) ? decodeURIComponent(c[1]) : void 0
}
;
Mql5Cookie.localStorage = function(a, b) {
    try {
        if (window.localStorage)
            if ("undefined" != typeof b)
                localStorage.setItem(a, b);
            else
                return localStorage.getItem(a)
    } catch (c) {}
}
;
Mql5Cookie.sessionStorage = function(a, b) {
    try {
        if (window.sessionStorage)
            if ("undefined" != typeof b)
                sessionStorage.setItem(a, b);
            else
                return sessionStorage.getItem(a)
    } catch (c) {}
}
;
function ImageAttach() {}
ImageAttach.Init = function(a, b, c, d, f) {
    b || (b = 80);
    a || (a = 60);
    ImageAttach.previewHeight = b;
    ImageAttach.previewWidth = a;
    ImageAttach.deleteLabel = c;
    ImageAttach.defaultError = d;
    ImageAttach.maxCount = f;
    Class($("customFileInputContainer"), "customFileInputContainer", !1);
    $("customFileInputLink").style.display = "";
    /msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent) ? $("customFileInputLink").onclick = function() {
        Class($("customFileInputContainer"), "customFileInputContainer", !0);
        $("customFileInputLink").style.display = "none";
        return !1
    }
    : $("customFileInputLink").onclick = function() {
        $("attachedFile_imagesLoader").click();
        return !1
    }
    ;
    ImageAttach.CheckImagesCount() ? $("imageUploaderContainer").style.display = "block" : $("imageUploaderContainer").style.display = "none"
}
;
ImageAttach.previewCounter = 0;
ImageAttach.CheckImagesCount = function() {
    return ImageAttach.previewCounter < ImageAttach.maxCount
}
;
ImageAttach.SelectFile = function() {
    $("attachedFile_imagesLoader").click()
}
;
ImageAttach.createPreview = function(a) {
    ImageAttach.previewCounter || (ImageAttach.previewCounter = 0);
    var b = $("newImageAttaches")
      , c = document.createElement("SPAN");
    c.id = "iat_" + ImageAttach.previewCounter;
    Class(c, "attachItem");
    Class(c, "imagePreview");
    var d = createElement(c, "A", {
        title: ImageAttach.getClearFilename(a),
        target: "_blank",
        href: a,
        className: "imageHolder_wall"
    });
    d = createElement(d, "IMG", {
        src: a
    });
    d.style.maxHeight = ImageAttach.previewHeight + "px";
    d.style.maxWidth = ImageAttach.previewWidth + "px";
    createElement(c, "BR");
    d = createElement(c, "DIV", {
        className: "previewLabel"
    });
    createElement(d, "INPUT", {
        type: "hidden",
        name: "attachedImage_" + ImageAttach.previewCounter,
        value: a
    });
    createElement(d, "A", {
        href: "#",
        onclick: function() {
            return ImageAttach.deletePreview(c)
        },
        className: "deleteAttachLink",
        innerHTML: ImageAttach.deleteLabel
    });
    prependElement(b, c);
    ImageAttach.previewCounter++;
    b.style.display = "";
    return c
}
;
ImageAttach.deletePreview = function(a) {
    a.parentNode.removeChild(a);
    ImageAttach.onDeletePreview();
    return !1
}
;
ImageAttach.onDeletePreview = function() {
    ImageAttach.previewCounter--;
    ImageAttach.CheckImagesCount() ? $("imageUploaderContainer").style.display = "block" : $("imageUploaderContainer").style.display = "none";
    0 >= ImageAttach.previewCounter && ($("newImageAttaches").style.display = "none")
}
;
ImageAttach.OnChangeFile = function(a, b) {
    a = a || (b && b.body ? b.body.innerHTML : null);
    ImageAttach.ClearError();
    if (a = stripHtml(a))
        a = eval("(" + a + ")"),
        0 < a.Url.length ? ImageAttach.createPreview(a.Url) : 0 < a.ErrorMessage.length ? ImageAttach.ShowError(a.ErrorMessage) : ImageAttach.ShowError(),
        ImageAttach.CheckImagesCount() ? $("imageUploaderContainer").style.display = "block" : $("imageUploaderContainer").style.display = "none"
}
;
ImageAttach.OnChangeFileError = function(a) {
    ImageAttach.ShowError()
}
;
ImageAttach.ShowError = function(a) {
    $("imageValidationError").innerHTML = a || ImageAttach.defaultError
}
;
ImageAttach.ClearError = function() {
    $("imageValidationError").innerHTML = ""
}
;
ImageAttach.getClearFilename = function(a) {
    a = a.split("/");
    a = a[a.length - 1];
    var b = a.indexOf("_");
    return a.substring(b + 1)
}
;
ImageAttach.Clear = function() {
    for (; 0 < ImageAttach.previewCounter; )
        ImageAttach.deletePreview($("iat_" + (ImageAttach.previewCounter - 1)))
}
;
function Affiliation(a, b, c, d, f, e, g) {
    this.getLink = function() {
        Ajax.get(a + "/" + b + "/generatelink/" + c + "/" + d + "/" + f, null, {
            onready: this.onSuccess,
            onerror: this.onError
        })
    }
    ;
    this.onSuccess = function(a) {
        e.href = a;
        e.innerHTML = a;
        g.style.display = "none";
        void 0 != $("affiliationLinkBlock") && void 0 != $("affiliationTextBlock") && ($("affiliationLinkBlock").style.display = "inline",
        $("affiliationTextBlock").style.display = "none")
    }
    ;
    this.onError = function() {}
}
function customSelect(a) {
    function b() {
        var a = document.getElementById(this.id + "_clone");
        if (a) {
            var b = this.id;
            a.selectedIndex = this.selectedIndex;
            this.parentNode.removeChild(this);
            a.id = b;
            return !1
        }
    }
    var c = document.getElementById(a);
    if (c) {
        var d = document.createElement("span");
        d.style.width = c.style.width;
        d.className = "selectWrapper";
        var f = document.createElement("button");
        f.innerHTML = "&#9660;";
        c.parentNode.insertBefore(d, c);
        Class(c, "customSelect");
        d.appendChild(c);
        d.appendChild(f);
        f.onclick = function() {
            var c = document.getElementById(a)
              , d = c.cloneNode(!0);
            d.id = a + "_clone";
            d.selectedIndex = c.selectedIndex;
            c.parentNode.insertBefore(d, c);
            c.size = Math.min(c.options.length, 20);
            Class(c, "opened");
            d = getOffsetSum(c);
            c.style.top = d.top + "px";
            c.style.left = d.left + "px";
            document.body.appendChild(c);
            c.focus();
            c.onblur = b;
            c.onclick = b;
            return !1
        }
        ;
        c.onmousedown = function() {
            return !1
        }
    }
}
function Likes() {
    function a(a, e, f, g) {
        if (!a || 0 == a.length)
            return b();
        try {
            var l = window.JSON ? window.JSON.parse(a) : eval("(" + a + ")")
        } catch (n) {
            return b()
        }
        a = $(e);
        Class(a, "loading", !0);
        e = $("likes_" + l.moduleId + "_" + l.parentType + "_" + l.parentId);
        a.innerHTML = l.linkText + "&nbsp;";
        a.onclick = function() {
            l.liked ? d(l.lang, l.moduleId, l.parentType, l.parentId, f, g) : c(l.lang, l.moduleId, l.parentType, l.parentId, f, g)
        }
        ;
        e.innerHTML = 0 < l.count ? l.count : "";
        0 < l.count ? Class(e, "likesCount", !1) : e.className = "";
        return !0
    }
    function b() {
        var a = $(this.linkId);
        Class(a, "loading", !0);
        g.ErrorMessageTitle && g.ErrorMessageText && (a = createElement(null, "DIV", {
            innerHTML: g.ErrorMessageText,
            className: "likeErrorText"
        }),
        Dialog.show(g.ErrorMessageTitle, a, 300, 120, function() {
            Dialog.hide()
        }))
    }
    function c(c, d, e, f, g, n) {
        var l = "like_" + d + "_" + e + "_" + f
          , h = $(l);
        h.onclick = function() {}
        ;
        Class(h, "loading");
        Ajax.post("/" + c + "/like/" + d + "/" + e + "/" + f, {
            __signature: g
        }, !1, {
            onready: function(b) {
                a(b, l, g, n)
            },
            onerror: b,
            linkId: l
        })
    }
    function d(c, d, e, f, g, n) {
        var l = "like_" + d + "_" + e + "_" + f
          , h = $(l);
        h.onclick = function() {}
        ;
        Class(h, "loading");
        Ajax.post("/" + c + "/unlike/" + d + "/" + e + "/" + f, {
            __signature: n
        }, !1, {
            onready: function(b) {
                a(b, l, g, n)
            },
            onerror: b,
            linkId: l
        })
    }
    function f(a) {
        var b = $(this.cid);
        b && (a && 0 != a.length ? b.innerHTML = a : b.parentNode.removeChild(b))
    }
    function e(a) {
        this.cnt && this.cnt.parentNode && (a && 0 != a.length ? this.cnt.innerHTML = a : this.cnt.parentNode.removeChild(this.cnt))
    }
    var g = this;
    this.ErrorMessageTitle = this.ErrorMessageText = this.SuccessMessageTitle = this.SuccessMessageText = void 0;
    this.IsMobile = !1;
    this.Like = function(a, b, d, e, f, g) {
        return c(a, b, d, e, f, g)
    }
    ;
    this.Unlike = function(a, b, c, e, f, g) {
        d(a, b, c, e, f, g)
    }
    ;
    this.Tooltip = function(a, c, d, e, g) {
        a = tooltip(a, '<div class="likesTooltipLoader"></div>');
        Ajax.get("/" + g + "/like_users/tooltip/" + c + "/" + d + "/" + e, null, {
            onready: f,
            onerror: b,
            cid: a
        })
    }
    ;
    this.UsersLikes = function(a, c, d, f, g) {
        var l = createElement(null, "DIV", {
            className: "likeUsersContainer"
        });
        createElement(l, "DIV", {
            className: "likesListLoader"
        });
        Dialog.show(g, l, 250, 300, this.CloseUsers);
        Ajax.get("/" + f + "/like_users/full/" + a + "/" + c + "/" + d, null, {
            onready: e,
            onerror: b,
            cnt: l
        })
    }
    ;
    this.CloseUsers = function() {
        var a = getElementsByClass("likeUsersContainer", document);
        if (a)
            for (var b = 0; b < a.length; b++)
                a[b] && a[b].parentNode && a[b].parentNode.removeChild(a[b]);
        Dialog.hide()
    }
}
window.globalStorageDomain = "https://c.mql5.com";
window.globalStoragePage = "/external/pocket/gstorage.html";
function PocketManager() {
    this.lastElement = null;
    this.Add = function(a, b, c, d, f) {
        window.GStorage || (window.GStorage = window.globalStorage(window.globalStorageDomain, window.globalStoragePage));
        GStorage.supported && (Class(a, "added"),
        null !== this.lastElement && this.lastElement !== a && (this.lastElement.className = "pocket"),
        this.lastElement = a,
        Ajax.get(["/", b, "/pocket/get/", c, "/", d, "/", f].join(""), null, {
            onready: function(a) {
                a ? window.GStorage.setItem("pocketContent", a) : window.GStorage.removeItem("pocketContent")
            },
            onerror: function() {}
        }));
        return !1
    }
}
function Translate(a) {
    function b(a, b) {
        var c = document.createElement("A");
        c.innerHTML = "<i>" + w[a][0] + "</i> " + w[a][1];
        c.href = "#" + a;
        b && Class(c, "selected");
        c.onclick = function() {
            q.Translate(e, g, a, m, l, p);
            u[e].currentTargetLang = a;
            var b = this.parentNode.parentNode.parentNode.firstChild;
            Class(b, "pressed");
            b.innerHTML = w[a][0];
            q.HideMenu(this.parentNode.parentNode.parentNode, e);
            return !1
        }
        ;
        return c
    }
    function c(a, b, c, d) {
        c = document.getElementById(a);
        if (!c)
            return !1;
        c.style.direction = "ar" == b || "fa" == b ? "rtl" : "ltr";
        b = null;
        if ("pollAnswers" == a)
            for (b = d[0],
            a = c.getElementsByTagName("label"),
            c = 0; c < a.length; c++)
                a[c].innerHTML = d[c + 1];
        else if (0 <= a.indexOf("contenttopic"))
            b = d[0],
            c.innerHTML = d[1];
        else if ("content_updates" === a) {
            a = c.querySelectorAll(".product-updates__content");
            var e = d.length;
            if (a)
                for (c = 0; c < e; c++)
                    a[c].innerHTML = d[c];
            else
                c.innerHTML = d[0]
        } else
            c.innerHTML = d[0];
        null != b && $("forumTitle") && ($("forumTitle").innerHTML = b);
        return !1
    }
    function d(a) {
        null != n && n != e && u[n].showMenu && (r.parentNode.removeChild(r.parentNode.childNodes[2]),
        u[n].showMenu = !1,
        Class(r.parentNode, "pressed", !0),
        r = n = null);
        if (u[e].showMenu)
            return q.HideMenu(a.parentNode, e),
            !1;
        var c = document.createElement("DIV");
        Class(a, "pressed");
        c.className = "translateMenu";
        c.onmousedown = function(a) {
            a || (a = window.event);
            a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0
        }
        ;
        var d = document.createElement("DIV"), f = 0, g = u[e].isTranslated, l;
        for (l in w)
            d.appendChild(b(l, g && l == h)),
            3 == ++f && (f = 0,
            c.appendChild(d),
            d = document.createElement("DIV"));
        0 < f && c.appendChild(d);
        a.parentNode.appendChild(c);
        u[e].showMenu = !0;
        n = e;
        r = a;
        t ? t(a, c) : (a = a.getBoundingClientRect(),
        d = document.body.getBoundingClientRect(),
        460 > a.left && d.right - a.right && (460 > d.right - a.right ? (c.style.width = d.width - 20 + "px",
        c.style.left = "-" + (a.left - 25) + "px") : (c.style.left = "-1px",
        c.style.right = "auto")));
        return !1
    }
    function f(a) {
        a = a || window.event;
        for (a = a.target || a.srcElement; a.parentNode && 0 > a.className.indexOf("translateMenu"); )
            a = a.parentNode;
        (!a.className || 0 > a.className.indexOf("translateMenu")) && q.HideMenu(null, n)
    }
    var e, g, h, m, l, p, t = null, n = null, r = null, q = this, u = {}, w = {
        ar: ["AR", "Arabic"],
        zh: ["CN", "Chinese"],
        en: ["EN", "English"],
        fr: ["FR", "French"],
        de: ["DE", "German"],
        id: ["ID", "Indonesian"],
        it: ["IT", "Italian"],
        ja: ["JP", "Japanese"],
        ko: ["KO", "Korean"],
        fa: ["FA", "Persian"],
        pt: ["PT", "Portuguese"],
        ru: ["RU", "Russian"],
        es: ["ES", "Spanish"],
        tr: ["TR", "Turkish"],
        vi: ["VI", "Vietnamese"]
    };
    this.Translate = function(b, d, f, g, l, n) {
        Mql5Cookie.localStorage("translateTargetLang", f);
        h = f;
        u[e].translations[f] ? (c(b, f, e, u[e].translations[f]),
        a && a()) : Ajax.get("/" + d + "/translate/" + f + "/" + g + "/" + l + "/" + n, null, {
            onready: function(d) {
                d && d.length && (d = eval("(" + d + ")"),
                u[e].translations[f] = d,
                c(b, f, e, d),
                a && a())
            },
            onerror: function() {}
        });
        u[e].isTranslated = !0;
        return !1
    }
    ;
    this.SetMenu = function(a, b, c, n, r, w, F, H, J, G) {
        function v() {
            var b = e
              , c = $(e);
            c && (c.parentNode.onmousedown = function() {
                q.HideMenu(a, b)
            }
            )
        }
        e = b;
        g = c;
        h = n;
        m = r;
        l = w;
        p = F;
        t = G;
        a.id = "translateLink" + b;
        H || Class(a, "pressed");
        u[b] ? h = u[b].currentTargetLang : (u[b] = {
            currentTargetLang: h,
            showMenu: !1,
            isTranslated: !1,
            originalTranslation: $(b) ? $(b).innerHTML : J ? J : null,
            originalTitle: $("forumTitle") ? $("forumTitle").innerHTML : null,
            translations: {}
        },
        a.onmouseout = function() {
            u[e].showMenu || Class(a, "pressed", !0)
        }
        ,
        a.firstChild.onclick = function() {
            if (u[e].isTranslated) {
                Class(a.firstChild, "pressed", !0);
                var b = document.getElementById(e);
                b && (b.style.direction = "ltr",
                b.innerHTML = u[e].originalTranslation,
                $("forumTitle") && ($("forumTitle").innerHTML = u[e].originalTitle),
                u[e].isTranslated = !1);
                return !1
            }
            Class(a.firstChild, "pressed");
            return q.Translate(e, g, h, m, l, p)
        }
        ,
        a.firstChild.onmousedown = function(a) {
            a || (a = window.event);
            a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0
        }
        ,
        $(e) ? v() : setTimeout(v, 300),
        b = document.createElement("SPAN"),
        b.className = "translateMenuButton",
        b.onmousedown = function(a) {
            a || (a = window.event);
            a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0;
            d(this);
            Core.AddHandler(document.body, "click", f)
        }
        ,
        c = document.createElement("SPAN"),
        b.appendChild(c),
        a.appendChild(b))
    }
    ;
    this.HideMenu = function(a, b) {
        if (null != n && n != b)
            return !1;
        a || (a = $("translateLink" + b));
        Class(a, "pressed", !0);
        var c;
        document.querySelector && (c = a.querySelector(".translateMenuButton")) && Class(c, "pressed", !0);
        (c = a.childNodes[2]) && a.removeChild(c);
        u[b].showMenu = !1;
        Core.RemoveHandler(document.body, "click", f);
        return !1
    }
}
function MakeTextboxAutogrow(a, b, c) {
    function d() {
        b && (a.style.height = b + "px");
        a.clientHeight < a.scrollHeight && (a.style.minHeight = a.scrollHeight + "px");
        f && f()
    }
    if (a) {
        c = c || ("oninput"in a ? "oninput" : "onkeyup");
        var f = a[c];
        a[c] = d;
        d()
    }
}
function AddElementHotkeyEvent(a, b, c) {
    if (a && "function" == typeof b) {
        var d = a.onkeyup;
        a.onkeyup = function(a) {
            var e = !0;
            d && (e = d(a));
            if (0 == e)
                return e;
            b(a) && c()
        }
    }
}
function SiteUpdates(a, b, c, d, f, e) {
    function g(a) {
        if (a.updates && 0 !== a.updates.length) {
            for (var b = a.updates.length - 1; 0 <= b; b--) {
                var c = a.updates[b], d;
                if (c.module_id && (d = u["m_" + c.module_id]))
                    for (var e in d.types)
                        d.types.hasOwnProperty(e) && d.types[e].callback && "function" === typeof d.types[e].callback && d.types[e].callback(c, a.ts);
                var f = $("notify_" + c.name);
                h(c, f)
            }
            a = document.getElementById("notify_total");
            d = document.getElementById("notifyTotalList");
            if (a && d) {
                b = 0;
                c = '<i class="notify-icon notify-icon_notify-total"></i>';
                d = d.querySelectorAll(".notify-item");
                e = 0;
                for (f = d.length; e < f; e++) {
                    var g = d[e];
                    g && g.firstChild && g.firstChild.className && (0 < g.firstChild.className.indexOf("wallet") || 0 < g.firstChild.className.indexOf("message")) || b++
                }
                0 < b && (c = '<i class="notify-icon notify-icon_notify-total"><i class="notify-icon_count">' + b + "</i></i>");
                a.innerHTML = c
            }
        }
    }
    function h(a, b) {
        if (a && a.title && a.url) {
            var c = $("privateAreaMessages");
            if (c) {
                var d = createElement(null, "a", {
                    className: "link-notify link-notify_" + a.name,
                    href: a.url,
                    id: "notify_" + a.name,
                    title: a.title
                });
                m(a.url, location.href) && (d.onclick = function() {
                    location.reload()
                }
                );
                var e = createElement(d, "i", {
                    className: "notify-icon notify-icon_" + a.name
                });
                if (a.count) {
                    Class(e, "notify-icon_active");
                    var f = Math.min(parseInt(a.count), 99);
                    e = createElement(e, "i", {
                        className: "notify-icon_count",
                        innerHTML: f
                    });
                    dataset(e, "count", a.count)
                }
                if (b && b.parentNode) {
                    c = b.parentNode;
                    c.insertBefore(d, b);
                    b.onclick && (d.onclick = b.onclick);
                    if (b.className && b.className.length) {
                        a = b.className.split(" ");
                        e = " " + d.className + " ";
                        f = [];
                        for (var g = 0; g < a.length; g++)
                            0 > e.indexOf(" " + a[g] + " ") && (f[f.length] = a[g]);
                        f.length && (d.className += (d.className ? " " : "") + f.join(" "))
                    }
                    c.removeChild(b)
                } else
                    b = createElement(null, "li", {
                        className: "notify-item"
                    }),
                    e = null,
                    13 != a.module_id && (a = $("notifyMessages")) && a.parentNode && (e = a.parentNode),
                    e ? e.parentNode.insertBefore(b, e) : c.appendChild(b),
                    b.appendChild(d),
                    notifyDropDownMobile && document.querySelector && (d = notifyDropDownMobile.querySelector(".notify-icon_count")) && (d.innerText = b.parentNode.getElementsByTagName("li").length),
                    MainMenu.createSubMenu()
            }
        } else
            b && 13 != a.module_id && b.parentNode.parentNode.removeChild(b.parentElement)
    }
    function m(a, b) {
        var c = a.indexOf("#");
        0 <= c && (a = a.substr(0, c));
        0 <= b.indexOf("#") && (b = b.substr(0, c));
        return a.toUpperCase() === b.toUpperCase()
    }
    function l() {
        if (!arguments || !arguments.length || 403 !== arguments[0]) {
            v++;
            5 > v ? setTimeout(function() {
                n()
            }, t()) : window.console && window.console.log && window.console.log("get updates stopped due to errors.");
            for (var a in q)
                if (a.startsWith("endc_"))
                    q[a]("error")
        }
    }
    function p(a) {
        for (var b in q)
            if (b.startsWith("endc_"))
                q[b]("success");
        if (a && 0 != a.length) {
            b = void 0;
            try {
                b = window.JSON ? window.JSON.parse(a) : eval("(" + a + ")")
            } catch (z) {
                return l()
            }
            b && void 0 != b && b.l && b.ts && b.updates && (b.ld && (!w || w < b.ld) && (w = b.ld),
            g(b),
            r = b.ts,
            C = setTimeout(function() {
                n()
            }, t()),
            v = 0)
        }
    }
    function t() {
        var a = (new Date).getTime() / 1E3
          , b = window.isWindowActive ? [3E3, 5E3, 1E4, 6E4] : [1E4, 15E3, 3E4, 2E5];
        return 15 > a - w ? b[0] : 45 > a - w ? b[1] : 165 > a - w ? b[2] : b[3]
    }
    function n() {
        if (1 == window.isWindowActive || window.isLastActiveWindow()) {
            extraLog(new Date + " get updates: SEND");
            for (var b = [], c = 0; c < d.length; c++)
                b.push(["m", d[c]]);
            for (var e in u)
                if (u.hasOwnProperty(e)) {
                    c = u[e].id;
                    for (var f in u[e].types)
                        if (u[e].types.hasOwnProperty(f)) {
                            var g = u[e].types[f].additionalDynamic ? u[e].types[f].additionalDynamic() : u[e].types[f].additionalStatic;
                            if (g)
                                for (var h in g)
                                    g.hasOwnProperty(h) && b.push([c + "_" + h, g[h]])
                        }
                }
            b.push(["wid", GetCurrentWindowId()]);
            b.push(["dt", r]);
            Ajax.post(a, b, !1, {
                onready: p,
                onerror: l
            }, !1);
            for (var m in q)
                if (m.startsWith("begc_"))
                    q[m]()
        } else
            extraLog(new Date + " get updates: SKIP"),
            C = setTimeout(function() {
                n()
            }, t())
    }
    var r = f
      , q = {}
      , u = {}
      , w = e || 0;
    this.SetLastUpdate = function(a) {
        if (!w || w < e)
            w = a
    }
    ;
    this.RegisterRenderCallback = function(a, b, c, d) {
        var e;
        (e = u["m_" + a]) || (e = u["m_" + a] = {
            types: {},
            id: a
        });
        (a = e.types["t_" + b]) || (a = e.types["t_" + b] = {
            id: b,
            callback: void 0,
            additionalStatic: {},
            additionalDynamic: void 0
        });
        a.callback = c;
        if ("function" === typeof d)
            a.additionalStatic = {},
            a.additionalDynamic = d;
        else {
            a.additionalDynamic = void 0;
            for (var f in d)
                d.hasOwnProperty(f) && (void 0 != d[f] ? a.additionalStatic[f] = d[f] : delete a.additionalStatic[f])
        }
    }
    ;
    this.RegisterOnBegin = function(a, b) {
        q["begc_" + a] = b
    }
    ;
    this.RegisterOnEnd = function(a, b) {
        q["endc_" + a] = b
    }
    ;
    this.GetUpdates = function() {
        C && clearTimeout(C);
        n()
    }
    ;
    var v = 0;
    var C = setTimeout(function() {
        n()
    }, t());
    mqGlobal.AddOnActiveWindowChange(function(a) {
        a && (C && clearTimeout(C),
        n())
    })
}
function DateToText(a, b) {
    if (!window.dateToTextLocalization)
        return GetDate(a);
    var c = new Date(1E3 * a);
    b = parseInt((new Date(1E3 * b)).getTime() / 6E4) - parseInt(c.getTime() / 6E4);
    if (1 > b)
        a = "m",
        b = 1;
    else if (60 > b)
        a = "m",
        b = Math.floor(b);
    else if (720 > b)
        a = "h",
        b = Math.floor(b / 60);
    else
        return GetDate(a);
    return 0 <= window.dateToTextLocalization[a + GetCounterSuffix(b)].indexOf("{0}") ? window.dateToTextLocalization[a + GetCounterSuffix(b)].replace("{0}", b) : b + " " + window.dateToTextLocalization[a + GetCounterSuffix(b)]
}
function DateToStringForToday(a) {
    var b = new Date
      , c = new Date(1E3 * a);
    return b.getFullYear() == c.getFullYear() && b.getMonth() == c.getUTCMonth() && b.getDate() == c.getUTCDate() ? (9 < c.getUTCHours() ? c.getUTCHours() : "0" + c.getUTCHours()) + ":" + (9 < c.getUTCMinutes() ? c.getUTCMinutes() : "0" + c.getUTCMinutes()) : GetDate(a)
}
function DateToStringForTodayLocal(a) {
    a = new Date(1E3 * a);
    return (9 < a.getHours() ? a.getHours() : "0" + a.getHours()) + ":" + (9 < a.getMinutes() ? a.getMinutes() : "0" + a.getMinutes())
}
function GetCounterSuffix(a) {
    var b = a % 100;
    if (10 < b && 20 > b)
        return 3;
    a %= 10;
    return 1 == a ? 1 : 1 < a && 5 > a ? 2 : 3
}
function ProductButtonClick(a, b, c) {
    var d = $(c);
    d && (d.hasMt5DialogHide = function() {
        HasMt5DialogHide(d)
    }
    ,
    Dialog.show(d.title, d, 500, 500, d.hasMt5DialogHide),
    d.style.display = "block",
    Dialog.show(d.title, d, d.clientWidth, d.clientHeight, d.hasMt5DialogHide))
}
function HasMt5DialogHide(a) {
    Dialog.hide();
    a.style.display = "none"
}
function ToggleAddProduct(a) {
    var b = $("addProductPopup");
    if (b)
        if (b.style.display) {
            "none" == b.style.display && (b.skipNextBlur = !1);
            b.style.display = "";
            var c = $("addProductInputTrigger") || createElement(b, "input", {
                type: "text",
                id: "addProductInputTrigger"
            });
            c.focus();
            c.onblur = function() {
                b.skipNextBlur ? (b.skipNextBlur = !1,
                c.focus()) : b.style.display = "none"
            }
            ;
            b.onmousedown || (b.onmousedown = function() {
                b.skipNextBlur = !0
            }
            );
            a.onmousedown || (a.onmousedown = function() {
                b.skipNextBlur = !0
            }
            )
        } else
            b.style.display = "none"
}
function traceClickAsync(a, b, c, d, f) {}
function traceClick(a) {}
function trackOutboundLink(a) {}
function fpush(a) {
    (0 < window.location.hostname.indexOf(".src") || 0 < window.location.hostname.indexOf(".dev")) && window.console ? ("object" === typeof a && (a = a.name + (a.value ? " " + a.value : "") + (a.unit ? " " + a.unit : "")),
    window.console.log("fpush: " + a)) : window.fz && window.setTimeout(function() {
        "object" === typeof a ? window.fz("event", a) : window.fz("track", a)
    }, 10)
}
function fpushTradays(a) {
    (0 < window.location.hostname.indexOf(".src") || 0 < window.location.hostname.indexOf(".dev")) && window.console ? ("object" === typeof a && (a = a.name + (a.value ? " " + a.value : "") + (a.unit ? " " + a.unit : "")),
    window.console.log("fpushTradays: " + a)) : window.fz && window.setTimeout(function() {
        if ("object" === typeof a) {
            var b = {
                name: a.name,
                referer: "https://www.tradays.com",
                websiteId: "dyjfykgvgksleijgdzwssxkjesvjspkazy"
            };
            a.value && (b.value = a.value);
            a.unit && (b.unit = a.unit);
            window.fz("track", b)
        } else
            window.fz("track", {
                name: a,
                referer: "https://www.tradays.com",
                websiteId: "dyjfykgvgksleijgdzwssxkjesvjspkazy"
            })
    }, 10)
}
function RenderCharts(a, b, c, d, f, e, g) {
    var h = "rgb(237,240,244)"
      , m = "rgba(204,204,204,0.3)"
      , l = "rgb(155,155,155)"
      , p = "rgb(81,139,198)"
      , t = "rgb(206,10,10)"
      , n = "rgba(124,204,255,0.25)"
      , r = "rgba(185,32,36,0.25)";
    d && (h = d.COLOR_BG || h,
    m = d.COLOR_GRID || m,
    l = d.COLOR_AXIS || l,
    p = d.COLOR_LINE || p,
    t = d.COLOR_LINE_MINUS || t,
    n = d.COLOR_LIGHT_LINE || n,
    r = d.COLOR_BG || r);
    a = f ? f.querySelectorAll("." + a) : window.getElementsByClass(a);
    if (void 0 != a)
        for (var q in a)
            if (f = a[q],
            void 0 != f && null != f && void 0 != f.className && (d = f.getElementsByTagName("CANVAS"),
            void 0 != d && null != d && 0 !== d.length && (d = d[0],
            d.getContext))) {
                var u = f.getElementsByTagName("INPUT");
                if (void 0 != u && 0 !== u.length && void 0 != u[0]) {
                    d = d.getContext("2d");
                    d.scale(1, 1);
                    f = g ? 1 : c / 58;
                    u = u[0].value.split(", ");
                    var w = parseInt(u[0]) * f;
                    u.shift();
                    if (!e) {
                        q = d.createLinearGradient(0, 0, 0, c);
                        q.addColorStop(0, h);
                        q.addColorStop(1, "#fff");
                        d.fillStyle = q;
                        d.fillRect(0, 0, b, c);
                        d.beginPath();
                        q = (c + 2) / 4;
                        for (var v = q - .5; v < b; v += q)
                            d.moveTo(v, 0),
                            d.lineTo(v, c);
                        for (v = q - .5; v < c; v += q)
                            d.moveTo(0, v),
                            d.lineTo(b, v);
                        d.strokeStyle = m;
                        d.lineWidth = 1;
                        d.stroke();
                        d.closePath()
                    }
                    d.beginPath();
                    v = c - w;
                    var C = v - parseInt(u[0]) * f
                      , A = .5;
                    d.moveTo(A, v);
                    d.lineTo(A, C - 1.5);
                    var D = C > v ? -1 : 1;
                    var z = C;
                    var E = [];
                    var F = 0;
                    for (q = 1; q < u.length; q++) {
                        C = parseInt(u[q]);
                        if (isNaN(C))
                            break;
                        E.push((C - 1) * f);
                        0 === C && 0 === F && (F = E.length - 1);
                        0 !== C && (F = 0)
                    }
                    for (var H = 0 === F ? E.length : F, J = 1 * b / H, G = 0; G < H; G++) {
                        C = v - E[G];
                        A += J;
                        var I = C - .5;
                        F = C > v ? -1 : 1;
                        if (F != D && 1 < G) {
                            var T = A - J;
                            var ha = z - .5 - v;
                            var ca = A;
                            z = I - v;
                            z = -ha * (ca - T) / (z - ha) + T;
                            T = v;
                            d.lineTo(z, T);
                            d.fillStyle = 0 > D ? r : n;
                            d.fill();
                            d.closePath();
                            d.beginPath();
                            d.moveTo(z, T)
                        }
                        d.lineTo(A, I);
                        z = C;
                        D = F
                    }
                    d.lineTo(A, v);
                    d.fillStyle = 0 > D ? r : n;
                    d.fill();
                    d.stroke();
                    d.closePath();
                    0 < w && w < c && (d.beginPath(),
                    d.moveTo(0, v - .5),
                    d.lineTo(b, v - .5),
                    d.strokeStyle = l,
                    d.stroke(),
                    d.closePath());
                    d.beginPath();
                    e && (d.lineWidth = 2);
                    v = c - w;
                    C = v - parseInt(u[0]) * f;
                    A = .5;
                    d.moveTo(A, C - .5);
                    D = C > v ? -1 : 1;
                    z = C;
                    for (G = 0; G < H; G++)
                        C = v - E[G],
                        A += J,
                        I = C - .5,
                        F = C > v ? -1 : 1,
                        F != D && 1 < G && (T = A - J,
                        ha = z - .5 - v,
                        ca = A,
                        z = I - v,
                        z = -ha * (ca - T) / (z - ha) + T,
                        T = v,
                        d.lineTo(z, T),
                        d.strokeStyle = 0 > D ? t : p,
                        d.stroke(),
                        d.closePath(),
                        d.beginPath(),
                        d.moveTo(z, T)),
                        d.lineTo(A, I),
                        e && (d.strokeStyle = 0 > D ? r : n,
                        d.stroke()),
                        z = C,
                        D = F;
                    d.strokeStyle = 0 > D ? t : p;
                    d.stroke()
                }
            }
}
(function() {
    var a = function() {
        var a = "INPUT" == this.tagName ? this : this.nextSibling;
        ("INPUT" == this.tagName ? this.previousSibling : this).style.display = "none";
        a.focus();
        window.console && window.console.log && window.console.log("focus on " + this)
    }
      , b = function() {
        var a = "INPUT" == this.tagName ? this.previousSibling : this;
        0 == ("INPUT" == this.tagName ? this : this.nextSibling).value.length && (a.style.display = "");
        window.console && window.console.log && window.console.log("blur on " + this)
    }
      , c = getElementsByClass("placeholder");
    if (c && 0 != c.length)
        for (var d = 0; d < c.length; d++)
            if (c[d].parentNode && !(0 > c[d].parentNode.className.indexOf("inputWrapper"))) {
                var f = c[d].nextSibling;
                c[d].onclick = a;
                f.onfocus = a;
                f.onblur = b
            }
}
)();
function SetDateInputValue(a, b) {
    function c(a, b) {
        var c = a;
        for (a = (a + "").length; a < b; a++)
            c = "0" + c;
        return c
    }
    var d = $(a + "Year")
      , f = $(a + "Month")
      , e = $(a + "Day")
      , g = $(a + "Hour")
      , h = $(a + "Minute");
    if (a = $(a))
        d && (d.value = b.getFullYear()),
        f && (f.value = c(parseInt(b.getMonth()) + 1, 2)),
        e && (e.value = b.getDate()),
        g && (g.value = c(b.getHours(), 2)),
        h && (h.value = c(b.getMinutes(), 2)),
        a.value = b.getFullYear() + "." + c(parseInt(b.getMonth()) + 1, 2) + "." + b.getDate() + " " + c(b.getHours(), 2) + ":" + c(b.getMinutes(), 2)
}
function Share() {
    var a;
    this.OpenShareEditor = function(b, c, d, f) {
        var e = createElement(null, "DIV", {
            className: "shareEditorLoading"
        })
          , g = f ? 300 : 600;
        Dialog.show(c, e, g, 150, function() {
            e.parentNode && e.parentNode.removeChild(e);
            a = null;
            Dialog.hide()
        }, null, null, " ui no-bottom-margin", !1, "fixed");
        Ajax.get(d, null, {
            onready: function(b) {
                var d = createElement(null, "DIV", {
                    className: "shareContent"
                });
                d.innerHTML = b;
                b = d.getElementsByTagName("img");
                for (var f = 0; f < b.length; f++)
                    b[f].onload = b[f].onerror = function(b) {
                        this.passedEvent || (b || (b = window.event),
                        this.passedEvent = !0,
                        Dialog.show(c, e, g, e.clientHeight || e.offsetHeight, function() {
                            e.parentNode && e.parentNode.removeChild(e);
                            a = null;
                            Dialog.hide()
                        }, null, null, " ui no-bottom-margin", !1, "fixed"))
                    }
                    ;
                Class(e, "shareEditorLoading", !0);
                e.appendChild(d);
                Dialog.show(c, e, g, e.clientHeight || e.offsetHeight, function() {
                    e.parentNode && e.parentNode.removeChild(e);
                    a = null;
                    Dialog.hide()
                }, null, null, " ui no-bottom-margin", !1, "fixed");
                document.querySelectorAll && document.querySelectorAll(".signalAvatar")[0] && Signals.RenderAvatars()
            },
            onerror: function() {},
            ismobile: f
        })
    }
    ;
    this.OnShareSuccess = function(a) {
        var b = this.ismobile ? 300 : 600;
        a = stripHtml(a);
        if (a = window.JSON ? window.JSON.parse(a) : eval("(" + a + ")"))
            if (!a.Type || "REDIRECT" !== a.Type) {
                if (a.Type && "SUCCESS" === a.Type) {
                    var d = createElement(null, "DIV", {
                        className: "shareEditorSuccess"
                    });
                    d.innerHTML = a.Message;
                    Dialog.show(a.Title || a.Message || "", d, b, 100, function() {
                        Dialog.hide()
                    }, null, null, " ui no-bottom-margin", !1, "fixed");
                    d.onclick = function() {
                        Dialog.hide()
                    }
                }
                a.Type && "ERROR" === a.Type && (d = createElement(null, "DIV", {
                    className: "shareEditorSuccess"
                }),
                d.innerHTML = a.Message,
                Dialog.show(a.Message, d, b, 100, function() {
                    Dialog.hide()
                }, null, null, " ui no-bottom-margin", !1, "fixed"),
                d.onclick = function() {
                    Dialog.hide()
                }
                )
            }
    }
    ;
    this.ExternalShare = function(b, c, d, f) {
        if (b = $("share_counter_" + c + "_" + d + "_" + f))
            c = +b.innerHTML,
            b.innerHTML = ++c;
        a = null;
        Dialog.hide()
    }
    ;
    this.ProcessShare = function(b) {
        var c = $("shareType").value
          , d = "";
        switch (c) {
        case "mql5com":
            d = "Mql5.community";
            break;
        case "fb":
            d = "Facebook";
            break;
        case "vk":
            d = "Vkontakte";
            break;
        case "tw":
            d = "Twitter"
        }
        window.fpush("MQL5+Share+" + d);
        if ("mql5com" == c)
            return $("formShare").onsubmit(),
            !1;
        $("formShare").onsubmit();
        null == a && ($("socIconVk") && "selected" == $("socIconVk").className ? a = $("socIconVk") : "selected" == $("socIconFb").className && (a = $("socIconFb")));
        b.href = a.href;
        a = null;
        Dialog.hide();
        return !0
    }
    ;
    this.SetShareType = function(b, c) {
        null == a && ($("socIconVk") && "selected" == $("socIconVk").className ? a = $("socIconVk") : "selected" == $("socIconFb").className && (a = $("socIconFb")));
        if (b == a)
            return !1;
        b.className = "selected";
        $("socButtonIco").className = c;
        $("socButtonText").innerHTML = b.title;
        $("shareType").value = c;
        "mql5com" == c ? ($("shareContentWrapper").style.display = "block",
        $("socCommentContent").focus()) : $("shareContentWrapper").style.display = "none";
        a && a.className && (a.className = "");
        a = b;
        return !1
    }
}
function Friends(a, b, c, d, f, e, g) {
    function h(a, b, c) {
        var d = createElement(null, "ul", {
            className: "friendsSettingsMenu"
        });
        c ? (c = createElement(d, "li", {
            className: b ? "" : "active"
        }),
        b || createElement(c, "span", {
            className: "hidePostsIndicator"
        }),
        c.appendChild(l(a, b))) : (c = createElement(d, "li", {
            className: b ? "" : "active"
        }),
        createElement(c, "span", {
            className: "addFriendIco"
        }),
        c.appendChild(n(a, b)));
        return d
    }
    function m(a, b, c, d, e, f) {
        var g = createElement(null, "ul", {
            className: "friendsSettingsMenu"
        });
        if (e) {
            var h = createElement(g, "li", {
                className: d ? "" : "active"
            });
            d || createElement(h, "span", {
                className: "hidePostsIndicator"
            });
            h.appendChild(l(b, d))
        } else
            createElement(g, "li", {
                className: d ? "" : "active"
            }).appendChild(n(b, d));
        createElement(g, "li", {
            className: ""
        }).appendChild(p(a, b, c));
        e && (createElement(g, "li", {
            className: "separator"
        }),
        createElement(g, "li", {
            className: ""
        }).appendChild(t(b, f)));
        return g
    }
    function l(a, b) {
        return createElement(null, "a", {
            className: "friendsSettingsLink",
            innerHTML: r + "&nbsp;&nbsp;",
            href: C,
            onclick: function(c) {
                doPost(c, this.href, v, {
                    user: a,
                    action: b ? "show_posts" : "hide_posts"
                });
                return !1
            }
        })
    }
    function p(a, b, c) {
        return createElement(null, "a", {
            className: "friendsSettingsLink",
            innerHTML: q,
            onclick: function() {
                return window.MessagesWidget ? (window.MessagesWidget.OpenChatWith(b, a, c),
                !1) : !0
            },
            href: A + b.toLowerCase()
        })
    }
    function t(a, b) {
        return createElement(null, "a", {
            className: "friendsSettingsLink",
            innerHTML: u,
            href: C,
            onclick: function(c) {
                if (!confirm(b))
                    return !1;
                doPost(c, this.href, v, {
                    user: a,
                    action: "remove"
                });
                return !0
            }
        })
    }
    function n(a) {
        return createElement(null, "a", {
            className: "friendsSettingsLink",
            innerHTML: w,
            href: C,
            onclick: function(b) {
                doPost(b, this.href, v, {
                    user: a,
                    action: "add"
                });
                return !1
            }
        })
    }
    var r = b
      , q = c
      , u = d
      , w = f
      , v = g
      , C = e + "/users/" + a.toLowerCase() + "/friends"
      , A = e + "/users/" + a.toLowerCase() + "/message?user=";
    this.ShowFriendSettings = function(a, b, c, d, e, f, g) {
        var l = $(d);
        if (d) {
            var n = l.parentNode;
            tooltip(l, m(a, b, c, e, f, g), function() {
                Class(n, "hovered")
            }, function() {
                Class(n, "hovered", !0)
            })
        }
    }
    ;
    this.ShowFriendSettingsAjax = function(b, c) {
        if (b) {
            var d = $(c);
            if (c) {
                var f = d.parentNode;
                c = "tt" + S4();
                Ajax.get(e + "/users/" + a.toLowerCase() + "/friends/" + b + "/info", null, {
                    onerror: function() {},
                    onready: function(a) {
                        ttId = this.ttId;
                        var b = $(ttId);
                        b && (b.innerHTML = a)
                    },
                    ttId: c
                });
                tooltip(d, "<div id='" + c + '\'><span class="loader-icon"></span></div>', function() {
                    Class(f, "hovered")
                }, function() {
                    Class(f, "hovered", !0)
                })
            }
        }
    }
    ;
    this.ShowPostSettings = function(a, b, c, d) {
        var e = $(b);
        if (b) {
            var f = e.parentNode;
            tooltip(e, h(a, c, d), function() {
                Class(f, "hovered")
            }, function() {
                Class(f, "hovered", !0)
            })
        }
    }
}
function Extractor() {
    function a(a) {
        var b = document.createElement("DIV");
        b.innerHTML = a;
        a = b.getElementsByTagName("DIV");
        for (var c = a.length - 1; 0 <= c; --c)
            "fquote" != a[c].className && "code" != a[c].className && "pocket" != a[c].className || a[c].parentNode.removeChild(a[c]);
        a = b.getElementsByTagName("EMBED");
        for (c = a.length - 1; 0 <= c; --c)
            a[c].parentNode.removeChild(a[c]);
        a = b.getElementsByTagName("PRE");
        for (c = a.length - 1; 0 <= c; --c)
            a[c].parentNode.removeChild(a[c]);
        return b.innerHTML
    }
    function b(a) {
        for (var b = 0; b < d.length; b++)
            if (d[b] === a)
                return !0;
        return !1
    }
    function c(a) {
        var b = document.getElementById("extractorContainer");
        if (!b || "disabled" != b.className) {
            a = eval("(" + a + ")");
            var c = document.createElement("DIV");
            c.setAttribute("id", "extractorContent");
            c.className = "extractorContent";
            if (null != a.ImageUrl && "" != a.ImageUrl) {
                var d = document.createElement("DIV");
                d.className = "image";
                var e = document.createElement("IMG");
                e.src = a.ImageUrl;
                d.appendChild(e)
            }
            e = document.createElement("DIV");
            e.className = "content";
            e.style.marginLeft = 1 > a.ImageWidth ? 0 : a.ImageWidth + 10 + "px";
            var f = document.createElement("DIV");
            f.style.clear = "both";
            var g = document.createElement("A");
            g.id = "extractorTitleLink";
            g.href = a.PageUrl;
            g.target = "_blank";
            g.innerHTML = a.Title;
            a.DisabledEdit || (g.className = "editable",
            g.title = a.Translate.clickToEdit,
            g.onclick = function() {
                this.style.display = "none";
                l.style.display = "inline";
                l.focus();
                l.select();
                return !1
            }
            );
            var l = document.createElement("INPUT");
            l.id = l.name = "extractorTitle";
            l.value = a.Title;
            l.maxLength = 250;
            l.setAttribute("autocomplete", "off");
            l.onblur = function() {
                g.innerHTML = this.value;
                g.style.display = "inline";
                this.style.display = "none"
            }
            ;
            l.onkeydown = function(a) {
                a = a || window.event;
                void 0 != a && void 0 != a.keyCode && 13 == a.keyCode && this.blur()
            }
            ;
            l.onkeypress = function(a) {
                return 13 != (a || window.event).keyCode
            }
            ;
            if (0 < a.Rating) {
                var h = document.createElement("DIV");
                h.className = "rating small";
                var m = document.createElement("DIV");
                m.className = "v" + 10 * a.Rating;
                h.appendChild(m)
            }
            var A = [];
            0 < a.Rating && (null != a.RatingVotesString && A.push(a.RatingVotesString),
            null != a.RatingReviewsString && A.push(a.RatingReviewsString));
            null != a.PriceString && A.push(a.PriceString);
            null != a.CategoryString && "" != a.CategoryString && A.push(a.CategoryString);
            null != a.DatePublished && "" != a.DatePublished && A.push(a.DatePublished);
            null != a.Author && "" != a.Author && A.push(a.Author);
            a.HostName && A.push(a.HostName);
            m = document.createElement("DIV");
            m.className = "info";
            h && m.appendChild(h);
            m.innerHTML += A.join(" | ");
            var D = document.createElement("P");
            null != a.Description && (D.innerHTML = a.Description);
            a.DisabledEdit || (D.className = "editable",
            D.title = a.Translate.clickToEdit,
            D.onclick = function() {
                this.style.display = "none";
                z.style.display = "block";
                z.focus();
                z.select();
                return !1
            }
            );
            var z = document.createElement("TEXTAREA");
            z.id = z.name = "extractorDescription";
            z.value = a.Description;
            z.maxLength = 450;
            z.onblur = function() {
                D.innerHTML = this.value;
                D.style.display = "block";
                this.style.display = "none"
            }
            ;
            h = document.createElement("INPUT");
            h.name = "extractorKey";
            h.type = "hidden";
            h.value = a.Key;
            A = document.createElement("INPUT");
            A.name = "extractorUrl";
            A.type = "hidden";
            A.value = a.PageUrl;
            var E = document.createElement("DIV");
            E.className = "delete";
            E.title = a.Translate["delete"];
            E.onclick = function() {
                Extractor.HideContentBox()
            }
            ;
            e.appendChild(g);
            e.appendChild(l);
            e.appendChild(m);
            e.appendChild(D);
            e.appendChild(z);
            e.appendChild(h);
            e.appendChild(A);
            d && c.appendChild(d);
            c.appendChild(e);
            c.appendChild(f);
            b.className = "extractor";
            b.innerHTML = "";
            b.appendChild(c);
            b.appendChild(E)
        }
    }
    var d = []
      , f = null
      , e = null
      , g = /http[s]*\:\/\/[a-z0-9\-\.]{1,120}\.[a-z]{2,5}[^\s\"\'<>\(\),]{0,500}/i
      , h = !1
      , m = !1;
    this.CanFindLink = function() {
        return !h && !m
    }
    ;
    this.FindLink = function(l) {
        if (!h && !m) {
            var p = document.getElementById("extractorContainer");
            p && "disabled" == p.className || (p = document.getElementById("edit_id"),
            void 0 != p && 0 < p.value && document.getElementById("extractor" + p.value) || (l = Dalet.Engine.GetHtml(l, !0),
            null != l && l != e && (e = l,
            l = a(l),
            l = l.match(g),
            null == l || null == l[0] || "" == l[0] || b(l[0]) || (f = unescape(l[0]),
            m = !0,
            window.Ajax.post("/extractor", {
                url: f
            }, !1, {
                onready: function(a) {
                    null == a || "" == a ? d.push(f) : (c(a),
                    h = !0);
                    m = !1
                },
                onerror: function() {
                    d.push(f);
                    m = !1
                }
            })))))
        }
    }
    ;
    this.HideContentBox = function() {
        d.push(f);
        h = m = !1;
        var a = $("extractorContainer");
        a && (a.className = "",
        a.innerHTML = "")
    }
    ;
    this.Remove = function(a, b, c, d) {
        window.Ajax.post("/extractor/remove", {
            moduleId: a,
            typeId: b,
            entityId: c,
            __signature: d
        }, !1, {
            onready: function(a) {
                null != a && "" != a && !0 === JSON.parse(a) && (a = $("extractor" + c),
                void 0 != a && a.parentNode.removeChild(a))
            },
            onerror: function() {}
        })
    }
    ;
    this.ReadMore = function(a, b) {
        window.Ajax.post("/extractor/readmore", {
            id: a
        }, !1, {
            onready: function(a) {
                if (null != a && "" != a) {
                    var c = JSON.parse(a);
                    $("extractorDescription" + b).style.display = "none";
                    if ($("extractorReadMore" + b))
                        $("extractorReadMore" + b).style.display = "block";
                    else {
                        a = document.createElement("DIV");
                        a.id = "extractorReadMore" + b;
                        var d = document.createElement("DIV");
                        d.className = "readMore";
                        var e = document.createElement("A");
                        e.href = "#";
                        e.innerHTML = c.translate.hideMore;
                        e.onclick = function() {
                            $("extractorReadMore" + b).style.display = "none";
                            $("extractorDescription" + b).style.display = "block";
                            return !1
                        }
                        ;
                        d.appendChild(e);
                        a.innerHTML = c.content;
                        a.appendChild(d);
                        c = $("extractorDescription" + b);
                        c.nextSibling ? c.parentNode.insertBefore(a, c.nextSibling) : c.parentNode.appendChild(a)
                    }
                }
            },
            onerror: function() {}
        })
    }
}
function QuickLogin() {
    this.onChange = function(a) {
        "Login" === a.name ? window.fpush("MQL5+Signin+Input+Login") : "Password" === a.name && window.fpush("MQL5+Signin+Input+Password")
    }
    ;
    this.onSubmit = function() {
        window.fpush("MQL5+Signin+Submit")
    }
}
function QuickRegistration(a) {
    function b(a) {
        for (var b in a)
            if ("empty" != b.toString() && a.hasOwnProperty(b))
                return !1;
        return !0
    }
    var c = !0, d = null, f, e, g = this, h = (a = a ? a : "") ? "+" + a : "", m = $(a + "quickRegisterForm") || $("regOpenIdForm"), l = $(a + "quickRegisterButton") || $("regOpenIdButtonSubmit");
    this.OnSubmit = function(a, b) {
        null != d && (f = "" == b ? "en" : b,
        e && (clearTimeout(e),
        e = void 0))
    }
    ;
    Core.AddHandler(m, "change", function() {
        var b = $(a + "AcknowledgeReadPrivacyAndDataPolicy")
          , c = $(a + "AgreeTermsConditions");
        if (!b || !c)
            return !1;
        l.disabled = !(b.checked && c.checked)
    });
    this.OnError = function() {
        $(a + "quickRegisterErrorMessage").style.display = "block"
    }
    ;
    this.OnSuccess = function(c) {
        g.Track("submit", {
            dst: "submit_" + d
        });
        window.fpush("MQL5+Register" + h + "+Submit");
        c = "string" == typeof c ? eval("(" + c + ")") : c;
        $(a + "quickRegisterErrorMessage").style.display = "none";
        if (c) {
            if (b(c)) {
                g.Track("action", {
                    dst: "action_" + d,
                    email: $(a + "email").value
                });
                window.fpush("MQL5+Register" + h + "+Success");
                Validate.SetValid(m, c);
                var e = $(a + "quickRegistrationFormLogin");
                e && (e.style.display = "block",
                Dialog.show("", $(a + "quickRegistrationFormLogin"), 400, 175, function() {
                    Dialog.hide();
                    $(a + "quickRegistrationFormLogin").style.display = "none"
                }),
                $(a + "Login").value = $(a + "username").value,
                $(a + "username").value = "",
                $(a + "email").value = "",
                $(a + "Password").focus())
            }
            void 0 != c.datacenter && c.datacenter.length ? ($(a + "quickRegisterErrorMessage").style.display = "block",
            $(a + "quickRegisterErrorMessage").innerText = c.datacenter[0],
            window.fpush("MQL5+Register" + h + "+KYC+Fail")) : void 0 != c.common && ($(a + "quickRegisterErrorMessage").style.display = "block");
            Validate.SetValid(m, c)
        } else
            $(a + "quickRegisterErrorMessage").style.display = "block"
    }
    ;
    this.OnSuccessGeneral = function(c) {
        g.Track("submit", {
            dst: "submit_" + d
        });
        window.fpush("MQL5+Register" + h + "+Submit");
        c = window.JSON ? window.JSON.parse(c) : eval("(" + c + ")");
        $(a + "quickRegisterErrorMessage").style.display = "none";
        if (c) {
            b(c) && (g.Track("action", {
                dst: "action_" + d,
                email: $(a + "email").value
            }),
            window.fpush("MQL5+Register" + h + "+Success"),
            Validate.SetValid($(a + "quickRegisterForm"), c),
            window.setTimeout(function() {
                var b = void 0 != $(a + "username") ? $(a + "username").value : "";
                window.location = "register_cloud" === d ? "/" + f + "/auth_login?reg=1&return=cloud&login=" + b : "/" + f + "/auth_login?reg=1&login=" + b
            }, 100));
            void 0 != c.datacenter && c.datacenter.length ? ($(a + "quickRegisterErrorMessage").style.display = "block",
            $(a + "quickRegisterErrorMessage").innerText = c.datacenter[0],
            window.fpush("MQL5+Register" + h + "+KYC+Fail")) : void 0 != c.common && ($(a + "quickRegisterErrorMessage").style.display = "block");
            if (void 0 != c.email)
                switch (c.errorType) {
                case 16:
                    window.fpush("MQL5+Register" + h + "+Exist+Email");
                    break;
                case 0:
                    window.fpush("MQL5+Register" + h + "+Invalid+Email")
                }
            Validate.SetValid($(a + "quickRegisterForm"), c)
        } else
            $(a + "quickRegisterErrorMessage").style.display = "block"
    }
    ;
    this.CheckUserName = function() {
        e && (clearTimeout(e),
        e = void 0);
        var b = $(a + "username");
        g.OnChange(b);
        "" !== b.value ? e = setTimeout(function() {
            Class(b.parentElement, "waiting");
            var c = $(a + "IsValidate")
              , d = $(a + "quickRegisterForm");
            d && (c || (c = createElement(d, "input", {
                name: "IsValidate",
                id: a + "IsValidate",
                type: "hidden"
            })),
            c.value = 1,
            Ajax.form(d, {
                onready: g.OnSuccessCheckLogin
            }),
            c.value = 0)
        }, 200) : (Validate.ClearMessage($(a + "validate_username")),
        MQTE.SetValidationClass(a + "username", !1));
        g.OnBlur(b)
    }
    ;
    this.OnSuccessCheckLogin = function(b) {
        var c = $(a + "username").parentElement;
        Class(c, "waiting", !0);
        b = eval("(" + b + ")");
        b.errorType && 0 !== (b.errorType & 8) && window.fpush("MQL5+Register" + h + "+Exist+Login");
        $(a + "quickRegisterErrorMessage").style.display = void 0 != b.common ? "block" : "none";
        Validate.SetValid($(a + "quickRegisterForm"), b)
    }
    ;
    this.OnBlur = function(a) {
        "" === a.value && a.nextSibling && a.nextSibling.style && (a.nextSibling.style.display = "block")
    }
    ;
    this.OnChange = function(a) {
        "username" === a.name ? window.fpush("MQL5+Register" + h + "+Input+Login") : "email" === a.name && window.fpush("MQL5+Register" + h + "+Input+Email")
    }
    ;
    this.OnFocus = function(a, b) {
        c && (c = !1,
        d = b,
        g.Track("focus", {
            dst: "focus_" + d
        }));
        a.nextSibling && a.nextSibling.style && (a.nextSibling.style.display = "none")
    }
    ;
    this.DisableInputs = function() {
        $(a + "username").disabled = "disabled";
        $(a + "email").disabled = "disabled";
        $(a + "quickRegisterSubmit") && ($(a + "quickRegisterSubmit").className = "submit loading");
        $(a + "quickRegisterButton") && ($(a + "quickRegisterButton").disabled = "disabled")
    }
    ;
    this.EnableInputs = function() {
        $(a + "username").disabled = !1;
        $(a + "email").disabled = !1;
        $(a + "quickRegisterSubmit") && ($(a + "quickRegisterSubmit").className = "submit");
        $(a + "quickRegisterButton") && ($(a + "quickRegisterButton").disabled = !1)
    }
    ;
    this.Track = function(a, b) {
        traceClickAsync(null, a, b.dst, "", "")
    }
}
function addEventCustom(a, b, c) {
    a.addEventListener ? a.addEventListener(b, c, !1) : a.attachEvent && a.attachEvent("on" + b, c)
}
function delEventCustom(a, b, c) {
    a.removeEventListener ? a.removeEventListener(b, c, !1) : a.detachEvent && a.detachEvent("on" + b, c)
}
function stopProp(a, b) {
    a && a.stopPropagation ? a.stopPropagation() : a && a.cancelBubble && (a.cancelBubble = !0);
    b && a.preventDefault && a.preventDefault()
}
function MobileMenu() {
    function a(a) {
        if (c) {
            for (var e = a.target; e && e !== f && e !== d; )
                e = e.parentNode;
            e || b(a, !1)
        }
    }
    function b(b, g) {
        f && d && (c || g) && (stopProp(b, !0),
        !c && g ? (c = !0,
        f.style.left = 0,
        f.style.height = "auto",
        Class(d, "left-panel-selector_selected"),
        addEventCustom(document, "touchstart", a),
        addEventCustom(document, "click", a)) : c && (c = !1,
        f.style.left = "-10000px",
        f.style.height = 0,
        Class(d, "left-panel-selector_selected", !0),
        delEventCustom(document, "touchstart", a),
        delEventCustom(document, "click", a)))
    }
    var c = !1
      , d = $("left-panel-selector")
      , f = $("left-panel");
    if (!f || !d)
        return !1;
    $("bodyContent").style.position = "relative";
    this.Hide = function() {
        f && d && c && c && (c = !1,
        f.style.left = "-10000px",
        f.style.height = 0,
        d.className = "",
        delEventCustom(document, "touchstart", a),
        delEventCustom(document, "click", a))
    }
    ;
    addEventCustom(d, "mousedown", function(a) {
        b(a, !0);
        stopProp(a, !0)
    });
    addEventCustom(f, "mousedown", function(a) {
        if ("INPUT" === a.target.tagName.toUpperCase())
            return !0;
        stopProp(a, !0);
        return !1
    });
    return this
}
function MobileSubMenu() {
    function a(a) {
        if (c) {
            for (var e = a.target; e && e !== f && e !== d; )
                e = e.parentNode;
            e || b(a, !1)
        }
    }
    function b(b, g) {
        f && d && (c || g) && (stopProp(b, !0),
        !c && g ? (c = !0,
        f.style.left = 0,
        d.className = "active",
        addEventCustom(document, "touchstart", a),
        addEventCustom(document, "click", a)) : c && (c = !1,
        f.style.left = "-10000px",
        d.className = "",
        delEventCustom(document, "touchstart", a),
        delEventCustom(document, "click", a)))
    }
    var c = !1
      , d = $("btnProfileSubCommands")
      , f = $("ulProfileSubCommands");
    if (f && d)
        return $("bodyContent").style.position = "relative",
        this.Hide = function() {
            f && d && c && c && (c = !1,
            f.style.left = "-10000px",
            d.className = "",
            delEventCustom(document, "touchstart", a),
            delEventCustom(document, "click", a))
        }
        ,
        addEventCustom(d, "mousedown", function(a) {
            b(a, !0);
            stopProp(a, !0)
        }),
        addEventCustom(f, "mousedown", function(a) {
            if ("INPUT" === a.target.tagName.toUpperCase())
                return !0;
            stopProp(a, !0);
            return !1
        }),
        this
}
function LangMenu() {
    var a, b, c, d, f = !1, e = this;
    this.Init = function() {
        if (null == a && (a = $("langmenu"),
        b = $("langMenuSelected"),
        c = $("langMenuContainer"),
        null != a && null != b && null != c)) {
            b.onmousedown = function(b) {
                f = Core.IsTouchDevice();
                Core.PreventSelection();
                b = b || window.event;
                "lang-menu__list lang-menu__list_dropped" === a.className ? e.Hide() : e.Show(b)
            }
            ;
            b.onselectstart = function() {
                return !1
            }
            ;
            var g = a.getElementsByTagName("a"), h;
            for (h in g)
                g[h].onmousedown = function(a) {
                    a = a || window.event;
                    Core.StopProp(a);
                    a.preventDefault && a.preventDefault()
                }
                ,
                g[h].onselectstart = function() {
                    return !1
                }
                ;
            a.onmousedown = function(a) {
                a = a || window.event;
                Core.StopProp(a);
                a.preventDefault && a.preventDefault();
                return !1
            }
            ;
            a.onselectstart = function() {
                return !1
            }
            ;
            Core.IsTouchDevice() ? (d = document.getElementById("langmenuBlurHandler"),
            d.onblur = function() {
                e.Hide()
            }
            ) : (c.onmouseover = function(a) {
                e.Show(a)
            }
            ,
            c.onmouseout = function() {
                e.Hide()
            }
            )
        }
    }
    ;
    this.Show = function(b) {
        b = b || window.event;
        b.preventDefault && b.preventDefault();
        "lang-menu__list" === a.className && (a.className = "lang-menu__list lang-menu__list_dropped",
        f && d.focus())
    }
    ;
    this.Hide = function() {
        a.className = "lang-menu__list"
    }
    ;
    mqGlobal.AddOnReady(function() {
        e.Init()
    })
}
function MainMenu() {
    var a, b, c, d, f, e = this;
    this.Init = function() {
        if (null == a && (a = $("mainmenu"))) {
            b = $("mainMenuSelected");
            var g = $("headerToolbar")
              , h = $("mainmenuItems");
            if (b && g && h) {
                var m = h.getElementsByTagName("li");
                if (m) {
                    var l = function() {
                        n.innerHTML = "";
                        for (var b = m.length - 1; 0 <= b; b--) {
                            var c = m[b];
                            if (10 < c.offsetTop) {
                                if (0 === parseInt(a.style.paddingRight)) {
                                    a.style.paddingRight = "25px";
                                    l();
                                    break
                                }
                                n.insertBefore(c.cloneNode(!0), n.firstChild);
                                "selected" == c.className && (t = !1)
                            } else if (!(0 <= c.className.indexOf("mainMenuProfileLink") || 0 <= c.className.indexOf("main-menu__about-link")))
                                break
                        }
                    };
                    d = document.getElementById("subNavContainer");
                    var p = document.getElementById("subNavToggle");
                    var t = !0;
                    var n = document.getElementById("subNavList");
                    f = document.getElementById("blurHandlerSubNav");
                    f.onblur = function() {
                        setTimeout(function() {
                            MainMenu.HideSub()
                        }, 300)
                    }
                    ;
                    p.onmousedown = function(a) {
                        a = a || window.event;
                        "sub-nav__container show" == d.className ? MainMenu.HideSub() : MainMenu.ShowSub(a)
                    }
                    ;
                    this.createSubMenu = function() {
                        t = !0;
                        var b = g.offsetWidth
                          , c = b + 30;
                        g && (a.style.marginRight = c + "px");
                        d.style.right = b + 30 + "px";
                        l();
                        t ? "sub-nav__toggle" != p.className && (p.className = "sub-nav__toggle") : "sub-nav__toggle active" != p.className && (p.className = "sub-nav__toggle active");
                        "" == n.innerHTML ? "none" != d.style.display && (d.style.display = "none") : "" != d.style.display && (d.style.display = "")
                    }
                    ;
                    e.createSubMenu();
                    Core.AddHandler(window, "resize", function() {
                        a.style.paddingRight = "0";
                        e.createSubMenu()
                    })
                }
                b.onmousedown = function(b) {
                    b = b || window.event;
                    "menu dropdown dropped" == a.className ? MainMenu.Hide() : MainMenu.Show(b)
                }
                ;
                h = a.getElementsByTagName("a");
                for (var r in h)
                    h[r] != b && (h[r].onmousedown = function(a) {
                        a = a || window.event;
                        Core.StopProp(a);
                        a.preventDefault && a.preventDefault()
                    }
                    );
                c = document.getElementById("mainMenuBlurHandler");
                c.onblur = function() {
                    MainMenu.Hide()
                }
            }
        }
    }
    ;
    this.Show = function(b) {
        b = b || window.event;
        b.preventDefault && b.preventDefault();
        "menu dropdown" == a.className && (a.className = "menu dropdown dropped",
        c.focus())
    }
    ;
    this.ShowSub = function(a) {
        a = a || window.event;
        a.preventDefault && a.preventDefault();
        "sub-nav__container" == d.className && (d.className = "sub-nav__container show",
        f.focus())
    }
    ;
    this.Hide = function() {
        a.className = "menu dropdown"
    }
    ;
    this.HideSub = function() {
        d.className = "sub-nav__container"
    }
    ;
    mqGlobal.AddOnReady(function() {
        e.Init()
    })
}
MainMenu = new MainMenu;
function HeadSideBarMenu() {
    var a, b, c, d, f = this;
    this.Show = function() {
        var b = Math.max(window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight, document.body.scrollHeight);
        c.className = "cover cover_sidebar-open";
        a.style.height = b + "px";
        d.style.height = b + "px"
    }
    ;
    this.Hide = function() {
        c.className = "cover"
    }
    ;
    this.Init = function() {
        a = $("groupMenu");
        b = $("sidebarToggleButton");
        c = $("cover");
        null != a && null != b && null != c && (d = document.getElementById("layer"),
        b.onclick = function() {
            f.Show()
        }
        ,
        d.onclick = function() {
            f.Hide()
        }
        )
    }
    ;
    mqGlobal.AddOnReady(function() {
        f.Init()
    })
}
function Focuser() {
    var a, b;
    this.Init = function(c, d) {
        var f = $(c);
        if (f) {
            var e = $(d);
            f.setAttribute("tabindex", -1);
            f.onclick = function() {
                a || (e.style.display = "block",
                a = !0,
                f.focus())
            }
            ;
            f.onblur = function() {
                a && (b ? (b = !1,
                f.focus()) : (e.style.display = "none",
                a = !1))
            }
            ;
            e.onmousedown = function() {
                b = a ? !0 : !1
            }
        }
    }
}
function ClickPopup() {
    this.Init = function(a, b, c, d, f, e) {
        var g = $(a);
        if (g) {
            var h = $(b)
              , m = $(c)
              , l = function() {
                g.className += " on";
                h.style.display = "block"
            }
              , p = function() {
                g.className = g.className.replace(" on", "");
                h.style.display = "none"
            };
            m.onfocus = function() {
                setTimeout(l, 0)
            }
            ;
            m.onblur = function() {
                setTimeout(p, 100)
            }
            ;
            g.onclick = function() {
                "block" == h.style.display ? m.blur() : (h.style.display = "block",
                m.focus(),
                m.select(),
                e && window.fpush(e))
            }
            ;
            d && !f && (h.style.left = "-" + (d - g.offsetWidth) / 2 + "px")
        }
    }
}
function Layer(a, b, c, d) {
    this.elementId = d;
    var f = $(d);
    f.style.top = c + "px";
    f.style.left = b + "px";
    this.calcCoordsDelta = function(b, c, d, f) {
        return a ? e = b = a(b, c, d, f) : {}
    }
    ;
    var e = null;
    this.move = function() {
        if (e) {
            for (var a in e)
                f.style[a] = e[a];
            e = null
        }
    }
    ;
    this.getElement = function() {
        return f
    }
    ;
    this.getInitPosition = function() {
        return {
            left: b,
            top: c
        }
    }
    ;
    this.getDeltaProps = function() {
        return e || {}
    }
}
function Parallax() {
    function a(a, b) {
        for (var l = 0; l < c.length; l++)
            c[l].calcCoordsDelta(a - e, b - g, a, b);
        if (!d || 50 < new Date - f) {
            for (l = 0; l < c.length; l++)
                setTimeout(c[l].move, 100);
            f = new Date
        }
    }
    var b = document.body
      , c = []
      , d = supportsCssProp("transition");
    this.addLayer = function(a, b, d, e) {
        c[c.length] = new Layer(a,b,d,e)
    }
    ;
    this.getLayer = function(a) {
        for (var b = 0; b < c.length; b++)
            if (c[b].elementId == a)
                return c[b];
        return null
    }
    ;
    var f = new Date
      , e = -1
      , g = -1;
    b.onmousemove = function(b) {
        if (document.all) {
            var c = event.x + document.body.scrollLeft;
            b = event.y + document.body.scrollTop
        } else
            c = b.pageX,
            b = b.pageY;
        0 > e || 0 > g || a(c, b);
        e = c;
        g = b
    }
    ;
    b.onmouseout = function() {
        g = e = -1
    }
    ;
    var h = getWindowSize();
    this.getWindowSize = function() {
        return h
    }
    ;
    var m = window.onresize;
    window.onresize = function() {
        h = getWindowSize();
        a(e, g);
        m && m()
    }
}
function addCustomScroll(a, b, c, d, f, e, g) {
    function h(a) {
        var b = !1;
        a.scrollHeight <= a.clientHeight && (b = !0);
        var c = Math.max(50, a.clientHeight / a.scrollHeight * n.clientHeight | 0) - f - f;
        a = a.scrollTop / (a.scrollHeight - a.clientHeight) * (n.clientHeight - c - f - f) | 0;
        q.style.display = b ? "none" : "";
        q.dataset.scrollHeight !== c && (q.style.height = c + "px",
        q.dataset.scrollHeight = c);
        q.dataset.top !== a && (q.style.transform = "translateY(" + a + "px)",
        q.dataset.top = a);
        r.dataset.scrollHeight !== a && (r.style.height = a + "px",
        r.dataset.scrollHeight = a)
    }
    function m(a) {
        a = a || window.event;
        if (null == a.pageX && null != a.clientX) {
            var b = document.documentElement
              , c = document.body;
            a.pageX = a.clientX + (b && b.scrollLeft || c && c.scrollLeft || 0) - (b.clientLeft || 0);
            a.pageY = a.clientY + (b && b.scrollTop || c && c.scrollTop || 0) - (b.clientTop || 0)
        }
        !a.which && a.button && (a.which = a.button & 1 ? 1 : a.button & 2 ? 3 : a.button & 4 ? 2 : 0);
        return a
    }
    function l(b) {
        var c = parseInt(b.dataset.top || b.style.top)
          , d = 0
          , e = null
          , g = b.onmousedown;
        b.onmousedown = function(l) {
            l = m(l);
            d = l.pageY;
            c = parseInt(b.dataset.top || b.style.top);
            e = window.onmousemove;
            Class(b, "dragged");
            window.onmousemove = function(e) {
                e = m(e);
                if (b) {
                    var g = Math.min(Math.max(c + (e.pageY - d), 0), b.parentNode.clientHeight - b.clientHeight - f - f);
                    b.dataset.top !== g && (b.style.transform = "translateY(" + g + "px)",
                    b.dataset.top = g);
                    d = e.pageY;
                    c = parseInt(b.dataset.top || b.style.top);
                    e = parseInt(b.dataset.top || b.style.top);
                    a.scrollTop = a.scrollHeight / b.parentNode.clientHeight * e
                }
            }
            ;
            window.onmouseup = function() {
                Class(b, "dragged", !0);
                window.onmousemove = e;
                e = null
            }
            ;
            g && g(l);
            return !1
        }
    }
    var p = window.getComputedStyle ? window.getComputedStyle(a, null).getPropertyValue("padding-right") || 0 : 0;
    p = parseInt(p);
    var t = createElement(null, "div", {
        className: "customScrollWrapper"
    });
    if (a.parentNode) {
        a.parentNode.insertBefore(t, a);
        a.style.width = "100%";
        t.appendChild(a);
        var n = createElement(t, "div", {
            className: "customScrollBar"
        });
        n.style.backgroundColor = b || "#F0F0F0";
        n.style.width = (d || 1) + 2 * (f || 0) + "px";
        t.style.zIndex = a.style.zIndex;
        var r = createElement(n, "div", {
            className: "customScrollBarSpace"
        })
          , q = createElement(n, "div", {
            className: "customScrollBarTrack"
        });
        q.style.backgroundColor = c || "#F0F0F0";
        q.style.width = (d || 1) + "px";
        q.style.border = "solid " + (f || 0) + "px " + (b || "#F0F0F0");
        r.style.width = q.style.width;
        r.style.border = "solid " + (f || 0) + "px transparent";
        (function() {
            if (e)
                for (var a in e)
                    e.hasOwnProperty(a) && (t.style[a] = e[a])
        }
        )();
        a.style.paddingRight = p + (17 - d - 2 * f) + "px";
        h(a);
        g || (n.onmousedown = function(b) {
            b.currentTarget === b.target && (a.scrollTop = Math.min(a.scrollTop + a.clientHeight, a.scrollHeight - a.clientHeight))
        }
        ,
        r.onmousedown = function(b) {
            b.currentTarget === b.target && (a.scrollTop = Math.max(a.scrollTop - a.clientHeight, 0))
        }
        );
        var u = a.onscroll;
        a.onscroll = function() {
            h(a);
            u && u(arguments);
            return !0
        }
        ;
        var w = a.onresize;
        a.onresize = function() {
            h(a);
            w && w(arguments);
            return !0
        }
        ;
        g || new l(q)
    }
}
function FeedbackForm() {
    var a = null
      , b = $("feedbackForm")
      , c = $("feedbackButton");
    b && c && Core.AddHandler(b, "change", function() {
        var a = $("AcknowledgeReadPrivacyAndDataPolicy");
        if (!c || !a)
            return !1;
        c.disabled = !a.checked
    });
    this.ProcessSubjectSelect = function(b) {
        var c = $("subjectType");
        if (void 0 != c)
            if (null == b || "" == b)
                c.style.display = "none";
            else {
                if (null == a) {
                    a = [];
                    var d = c.getElementsByTagName("option"), g;
                    for (g in d)
                        a.push(d[g])
                }
                for (b = "subjectType_" + b + "_"; c.hasChildNodes(); )
                    c.removeChild(c.lastChild);
                for (var h in a)
                    void 0 != a[h].id && 0 == a[h].id.indexOf(b) && c.appendChild(a[h]);
                c.style.display = "inline";
                null == c.childNodes || void 0 == c.childNodes[0].selected || c.childNodes[0].disabled || (c.childNodes[0].selected = !0);
                c = $("customSubjectBox");
                void 0 != c && (c.style.display = "none")
            }
    }
    ;
    this.ProcessSubjectTypeSelect = function(a) {
        var b = $("subjectType");
        if (void 0 != b) {
            var c = $("customSubjectBox");
            if (void 0 != c) {
                var d = $("customSubject");
                if (void 0 != d) {
                    b = b.getElementsByTagName("option");
                    var h = !1, m;
                    for (m in b)
                        if (!(b[m].value != a || void 0 == b[m].id || 0 > b[m].id.indexOf("_other"))) {
                            h = !0;
                            break
                        }
                    h ? (c.style.display = "block",
                    d.focus()) : c.style.display = "none"
                }
            }
        }
    }
}
var paletteRegular = "#3366CC #DC3912 #FF9900 #109618 #990099 #0099C6 #DD4477 #66AA00 #B82E2E #316395 #994499 #22AA99 #AAAA11 #6633CC #E67300 #8B0707".split(" ")
  , paletteHover = "#45AFE2 #FF3300 #FFD324 #14C21D #DF51FD #15CBFF #FF97D2 #97FB00 #DB6651 #518BC6 #BD6CBD #35D7C2 #E9E91F #9877DD #FF8F20 #D20B0B".split(" ");
function BuildMonitoringChart(a, b, c, d, f, e) {
    var g = [];
    g.push(['{\n   "title":"",\n   "description":"",', '   "fractionalValue":"' + (e ? "" + e : "0") + '",', '   "type":"graph",\n   "subType":"simple",\n   "showFractionalValue":true,\n   "axisXTitle" : "Date",', '   "axisYTitle" : "' + b + ("" == c ? "" : ", " + c) + '",', '   "margin" : {"left" : 50, "right" : 30},\n   "series" : [\n       {\n           "type" : "title",', '           "value" : [' + d + "]", "       }"].join("\n"));
    b = 0;
    for (var h in f)
        g.push(['       ,\n       {\n           "type" : "graph",\n           "title" : "",', '           "tooltip" : "%VARIABLE% - ' + h + " - %VALUE% " + c + '",', '           "value" : [' + f[h] + "],", '           "selectColor":"' + paletteHover[b % paletteHover.length] + '",', '           "lineColor":"' + paletteRegular[b % paletteHover.length] + '",', '           "showBounds":false\n       }'].join("\n")),
        b++;
    g.push("   ]\n }");
    window.getRenderType || (window.getRenderType = function() {
        return MQL5ChartRenderType.SVG
    }
    );
    $(a) && MQL5ChartAPI.addChart(a, g.join("\n"), MQL5ChartType.GRAPH, window.getRenderType)
}
var MqDownloadLinksProcessor = new function() {}
;
function TradingAccounts() {
    var a = this;
    this.accounts_base = [];
    this.parentHost = this.childHost = "";
    this.syncCompleteCallback = null;
    this.editableMt = this.editableBroker = !0;
    this.presetMtVersion = this.presetAccountLine = null;
    this.syncEnabled = !1;
    this.customHandlers = null;
    this.InitCore = function(b, c) {
        a.childHost = b;
        a.parentHost = c;
        window.addEventListener("message", a.OnWindowMessage)
    }
    ;
    this.InitSync = function(b, c, d, f, e, g, h, m, l, p, t, n) {
        a.editableMt = c;
        a.editableBroker = d;
        a.syncCompleteCallback = f;
        a.presetAccountLine = e;
        a.presetMtVersion = h;
        if (b = $(b))
            c = a.childHost + "/trade?callback&switch_platform=1&border=0&startup_version=5&demo_all_servers=1",
            t && (c += "&user_token=" + t),
            g && (c += "&startup_mode=" + g,
            "no_ui" == g && (a.syncEnabled = !0,
            0 < h && (c += "&version=" + h))),
            0 < m ? c += "&version=" + h + "&login=" + m + "&trade_server=" + escape(l) : p && (c += "&version=" + h),
            n && (c += "&m=1"),
            b.setAttribute("src", c)
    }
    ;
    this.InitLogin = function(b, c, d) {
        a.presetMtVersion = c;
        a.customHandlers = d;
        (b = $(b)) && b.setAttribute("src", a.childHost + "/trade?version=" + c + "&startup_mode=only_login")
    }
    ;
    this.OnWindowMessage = function(b) {
        if (b.origin == a.childHost) {
            b = JSON.parse(b.data);
            if (a.customHandlers) {
                var c = a.customHandlers[b.command];
                if (c) {
                    c(b.data);
                    return
                }
            }
            switch (b.command) {
            case "records":
                a.SyncAccountsData(a.childHost, b.data);
                break;
            case "add_line_complete":
                a.syncCompleteCallback && a.syncCompleteCallback.add_line_complete && 0 < a.accounts_base.length && a.syncCompleteCallback.add_line_complete(a.accounts_base[a.accounts_base.length - 1].server_type)
            }
        }
    }
    ;
    this.SyncAccountsData = function(b, c) {
        if (a.presetAccountLine)
            a.AddAccountLine(b, a.presetMtVersion, a.presetAccountLine),
            a.presetAccountLine = null;
        else if (a.syncCompleteCallback && a.syncCompleteCallback.onsync && c && c.length && 0 < c.length) {
            if (b = c[0],
            b.login && b.server_name)
                a.syncCompleteCallback.onsync()
        } else
            a.syncEnabled && c && c.length && 0 < c.length && a.MapAccountStats(c)
    }
    ;
    this.SearchNextNode = function(a, c) {
        for (var b = !1; ; ) {
            if (!b && a.childNodes && 0 < a.childNodes.length) {
                var f = a.childNodes[0];
                if (!f.tagName) {
                    a = f;
                    continue
                }
                if (f.tagName == c)
                    break
            }
            b = !1;
            f = a.nextSibling;
            if (!f) {
                if (a.parentNode) {
                    a = a.parentNode;
                    b = !0;
                    continue
                }
                break
            }
            if (f.tagName && f.tagName == c)
                break;
            a = f
        }
        return f
    }
    ;
    this.MapAccountStats = function(b) {
        var c = a.SearchNextNode
          , d = []
          , f = []
          , e = $("tradeAccountsList")
          , g = $("accNewTile")
          , h = $("accTileTemplate");
        if (e && g && h) {
            for (var m = 0; m < b.length; m++) {
                var l = b[m];
                if (l && l.login && l.server_name) {
                    var p = parseInt(l.login);
                    if (p && !isNaN(p) && 0 != p && !/[\'\"\<\>]/.test(l.server_name)) {
                        var t = a.accounts_base.length;
                        a.accounts_base.push(l);
                        if (l.server_type)
                            var n = l.server_type;
                        else
                            n = 4,
                            l.server_type = 4;
                        l.tile = t;
                        var r = h.cloneNode(!0);
                        r.setAttribute("id", "accTile" + t);
                        var q = c(r, "A");
                        l.account_name ? (0 < q.childNodes.length && q.removeChild(q.childNodes[0]),
                        q.appendChild(document.createTextNode(p + ": " + l.account_name))) : q.innerHTML = p;
                        var u = q.getAttribute("href");
                        u += "?version=" + n + "&login=" + p + "&trade_server=" + escape(l.server_name);
                        q.setAttribute("href", u);
                        u = {};
                        q = c(q, "IMG");
                        q.setAttribute("id", "accFieldLogo" + t);
                        u.node_logo = q;
                        q = c(q, "SPAN");
                        0 < q.childNodes.length && q.removeChild(q.childNodes[0]);
                        q.appendChild(document.createTextNode(l.server_name));
                        q = c(q, "SPAN");
                        q.setAttribute("id", "accFieldCompany" + t);
                        u.node_company = q;
                        q = c(q, "SPAN");
                        q.className = "mt mt" + n;
                        q.setAttribute("title", "MetaTrader " + n);
                        q = c(q, "DD");
                        q.innerHTML = a.FormatMoney(l.stat_equity);
                        q = c(q, "DD");
                        q.innerHTML = a.FormatMoney(l.stat_profit);
                        q = c(q, "DD");
                        q.innerHTML = (isNaN(l.stat_load) ? 0 : l.stat_load) + "%";
                        q = c(q, "DD");
                        0 < q.childNodes.length && q.removeChild(q.childNodes[0]);
                        q.appendChild(document.createTextNode(l.stat_currency));
                        q = c(q, "A");
                        q.setAttribute("onclick", "TradingAccounts.DropAccount(" + t + ");");
                        q = c(q, "DIV");
                        q.setAttribute("id", "accFieldDelete" + t);
                        q = c(q, "BUTTON");
                        q.setAttribute("onclick", "TradingAccounts.DropAccountConfirm(" + [t, n, "'" + p + "'", "'" + l.server_name + "'"].join() + ");");
                        q = c(q, "BUTTON");
                        q.setAttribute("onclick", "TradingAccounts.DropAccountCancel(" + t + ");");
                        f.push(u);
                        e.insertBefore(r, g);
                        p = !1;
                        for (t = 0; t < d.length; t++)
                            if (d[t] == n + ";" + l.server_name) {
                                p = !0;
                                break
                            }
                        p || d.push(n + ";" + l.server_name)
                    }
                }
            }
            0 != d.length && (b = window.document.forms.tradeAccountsDetailsQuery) && b.query && (b.query.value = JSON.stringify(d),
            Ajax.form(b, {
                onready: a.ApplyAccountsDetails
            }))
        }
    }
    ;
    this.MapAccountStatsMini = function(b) {
        var c = a.SearchNextNode
          , d = []
          , f = []
          , e = $("tradeAccountsList")
          , g = $("accTileTemplate");
        if (e && g) {
            for (var h = 0; h < b.length; h++) {
                var m = b[h];
                if (m && m.login && m.server_name) {
                    var l = parseInt(m.login);
                    if (l && !isNaN(l) && 0 != l && !/[\'\"\<\>]/.test(m.server_name)) {
                        var p = a.accounts_base.length;
                        a.accounts_base.push(m);
                        m.tile = p;
                        var t = g.cloneNode(!0);
                        t.setAttribute("id", "accTile" + p);
                        var n = c(t, "A");
                        m.account_name ? (0 < n.childNodes.length && n.removeChild(n.childNodes[0]),
                        n.appendChild(document.createTextNode(l + ": " + m.account_name))) : n.innerHTML = l;
                        n.setAttribute("onclick", "SelectSubscriberAccount(TradingAccounts.accounts_base[" + p + "]);");
                        l = {};
                        n = c(n, "IMG");
                        n.setAttribute("id", "accFieldLogo" + p);
                        l.node_logo = n;
                        n = c(n, "SPAN");
                        0 < n.childNodes.length && n.removeChild(n.childNodes[0]);
                        n.appendChild(document.createTextNode(m.server_name));
                        n = c(n, "SPAN");
                        n.setAttribute("id", "accFieldCompany" + p);
                        l.node_company = n;
                        n = c(n, "SPAN");
                        n.className = "mt mt" + a.presetMtVersion;
                        n.setAttribute("title", "MetaTrader " + a.presetMtVersion);
                        n = c(n, "DD");
                        n.innerHTML = a.FormatMoney(m.stat_equity);
                        n = c(n, "DD");
                        n.innerHTML = a.FormatMoney(m.stat_profit);
                        n = c(n, "DD");
                        n.innerHTML = (isNaN(m.stat_load) ? 0 : m.stat_load) + "%";
                        n = c(n, "DD");
                        0 < n.childNodes.length && n.removeChild(n.childNodes[0]);
                        n.appendChild(document.createTextNode(m.stat_currency));
                        f.push(l);
                        e.appendChild(t);
                        p = !1;
                        for (t = 0; t < d.length; t++)
                            if (d[t] == a.presetMtVersion + ";" + m.server_name) {
                                p = !0;
                                break
                            }
                        p || d.push(a.presetMtVersion + ";" + m.server_name)
                    }
                }
            }
            0 != d.length && (b = window.document.forms.tradeAccountsDetailsQuery) && b.query && (b.query.value = JSON.stringify(d),
            Ajax.form(b, {
                onready: a.ApplyAccountsDetails
            }))
        }
    }
    ;
    this.ApplyAccountsDetails = function(b) {
        if ((b = JSON.parse(b)) && b.length && a.accounts_base && a.accounts_base.length)
            for (var c = 0; c < b.length; c++)
                for (var d = b[c], f = 0; f < a.accounts_base.length; f++) {
                    var e = a.accounts_base[f];
                    if (e.server_type == d.mt && e.server_name == d.server) {
                        var g = $("accFieldLogo" + e.tile);
                        g && g.setAttribute("src", d.logo);
                        if (e = $("accFieldCompany" + e.tile))
                            e.innerHTML = d.company
                    }
                }
    }
    ;
    this.AddAccount = function() {
        a.AddAccountCancel();
        var b = $("accNewTile");
        if (b) {
            var c = $("mtversion")
              , d = $("accountlogin")
              , f = $("broker");
            d.value = "";
            a.editableMt && (window.mtversionSelectValue("4"),
            c.value = "4");
            a.editableBroker && (f.value = "");
            b.className = "tile edit";
            d.focus()
        }
    }
    ;
    this.AddAccountCancel = function() {
        var b = $("accNewTile");
        b && (b.className = "tile add",
        window.Validate.ClearMessage($("validate_accountlogin")),
        $("accountlogin").parentNode.className = "inputWrapper",
        a.editableMt && window.Validate.ClearMessage($("validate_mtversion")),
        a.editableBroker && (window.Validate.ClearMessage($("validate_broker")),
        $("broker").parentNode.className = "inputWrapper"))
    }
    ;
    this.AddAccountConfirm = function(b) {
        if (!b || !$("accNewTile"))
            return !1;
        b = {
            server_type: $("mtversion").value,
            login: $("accountlogin").value,
            server_name: $("broker").value
        };
        var c = a.FindDuplicateAccount($("accountlogin"));
        if (-1 == c)
            return a.accounts_base.push(b),
            a.AddAccountPlain(a.childHost, b),
            !0;
        a.AddAccountCancel();
        var d = $("accTile" + c);
        d && (d.style.transform = "scale(1.1)",
        setTimeout(function() {
            d.style.transform = "scale(1)"
        }, 200));
        return !1
    }
    ;
    this.ValidateForDuplicateAccount = function(b) {
        return 0 <= a.FindDuplicateAccount(b)
    }
    ;
    this.FindDuplicateAccount = function(b) {
        if (!a.accounts_base || 0 == a.accounts_base.length)
            return -1;
        var c = b.value;
        b = a.SearchNextNode(b, "INPUT");
        var d = b.value;
        b = a.SearchNextNode(b, "INPUT");
        b = b.value;
        for (var f = 0; f < a.accounts_base.length; f++) {
            var e = a.accounts_base[f];
            if (e.server_type == d && e.login == c && e.server_name == b)
                return f
        }
        return -1
    }
    ;
    this.AddAccountLine = function(a, c, d) {
        window.frames.webTerminalHost.postMessage(JSON.stringify({
            command: "add_encrypted_line",
            data: {
                line: d,
                type: c
            }
        }), a)
    }
    ;
    this.AddAccountPlain = function(a, c) {
        (5 == c.server_type ? window.frames.webTerminalHost5 : window.frames.webTerminalHost4).postMessage(JSON.stringify({
            command: "add_line",
            data: {
                login: c.login,
                server: c.server_name,
                type: c.server_type
            }
        }), a)
    }
    ;
    this.DropAccount = function(a) {
        if (a = $("accFieldDelete" + a))
            a.className = "deleteOverlay on"
    }
    ;
    this.DropAccountConfirm = function(b, c, d, f) {
        (5 == c ? window.frames.webTerminalHost5 : window.frames.webTerminalHost4).postMessage(JSON.stringify({
            command: "remove_line",
            data: {
                login: d,
                server: f,
                type: c
            }
        }), a.childHost);
        for (var e = 0; e < a.accounts_base.length; e++) {
            var g = a.accounts_base[e];
            if (g.server_type == c && g.login == d && g.server_name == f) {
                a.accounts_base.splice(e, 1);
                break
            }
        }
        (b = $("accTile" + b)) && b.parentNode && b.parentNode.removeChild(b)
    }
    ;
    this.DropAccountCancel = function(a) {
        if (a = $("accFieldDelete" + a))
            a.className = "deleteOverlay"
    }
    ;
    this.FormatMoney = function(a) {
        if (0 == a)
            return "0";
        if (995 >= a)
            return a.toFixed(0);
        if (1050 >= a)
            return "1K";
        if (9950 >= a) {
            var b = a % 1E3;
            return (a / 1E3).toFixed(50 > b || 950 < b ? 0 : 1) + "K"
        }
        return 999500 >= a ? (a / 1E3).toFixed(0) + "K" : 105E4 >= a ? "1M" : 995E4 >= a ? (b = a % 1E6,
        (a / 1E6).toFixed(5E4 > b || 95E4 < b ? 0 : 1) + "M") : (a / 1E6).toFixed(0) + "M"
    }
    ;
    this.RequestBestVps = function(a, c, d, f) {
        window.Ajax.post("/api/vhost/hostservers/top", {
            brokerServerName: c,
            additional: "geoinfo",
            mt: a,
            login: d,
            validate: !0
        }, !1, {
            onready: function(a) {
                a = JSON.parse(a);
                if (null != a && null != a.host_servers && 0 < a.host_servers.length) {
                    for (var b = a.host_servers[0], c = b.best_ping, d = 1; d < a.host_servers.length; d++)
                        a.host_servers[d].best_ping < c && (b = a.host_servers[d],
                        c = b.best_ping);
                    f.success(b)
                } else
                    f.error && f.error()
            },
            onerror: function(a) {
                304 === a && f.occupied ? f.occupied() : f.error && f.error()
            }
        })
    }
}
function CarouselGallery() {
    function a(a, b) {
        if (b = $(b + "_navLinks")) {
            b = b.getElementsByTagName("A");
            for (var c = 0; c < b.length; c++)
                b[c].className = Number(c) === Number(a) ? "selected" : ""
        }
    }
    var b, c, d;
    this.Show = function(f, e) {
        if (null == b) {
            var g = $(e);
            c = g.offsetWidth;
            g = g.getElementsByTagName("DIV");
            for (var h = 0; h < g.length; h++)
                if (g[h].className && !(0 > g[h].className.indexOf("carouselGalleryContainer"))) {
                    b = g[h];
                    break
                }
        }
        d = f;
        f = d * c * -1;
        null != b.style.transform ? b.style.transform = "translateX(" + f + "px)" : b.style.left = f + "px";
        a(d, e)
    }
    ;
    this.Move = function(f, e, g) {
        if (null == b) {
            var h = $(f);
            c = h.offsetWidth;
            h = h.getElementsByTagName("DIV");
            for (var m = 0; m < h.length; m++)
                if (h[m].className && !(0 > h[m].className.indexOf("carouselGalleryContainer"))) {
                    b = h[m];
                    break
                }
            d = 0;
            e = Number(e)
        }
        d = Number(d) + (g ? 1 : -1);
        d >= e ? d = 0 : 0 > d && (d = e - 1);
        e = d * c * -1;
        null != b.style.transform ? b.style.transform = "translateX(" + e + "px)" : b.style.left = e + "px";
        a(d, f)
    }
}
function MediaToken() {
    this.Set = function() {
        var a = void 0 != $("Login") ? $("Login").value : ""
          , b = void 0 != $("Password") ? $("Password").value : "";
        var c = (c = document.cookie.match(/(?:^|; )uniq=([^;]*)/)) ? decodeURIComponent(c[1]) : null;
        "" !== a && "" !== b && "" !== c && (a = Core.Crc32(a + b + c + "window.location.href"),
        document.cookie = "_media_uuid=" + escape(a) + "; path=/")
    }
}
function FooterLinks() {
    this.Show = function(a) {
        $(a).className = "" == $(a).className ? "open" : ""
    }
}
window.opener = null;
function ImageAttacherManager(a, b, c, d, f, e, g, h, m, l) {
    function p(a, b) {
        if (a.classList)
            return a.classList.contains(b);
        a = a.className.split(" ");
        for (var c = 0; c < a.length; c++)
            if (a[c] === b)
                return !0;
        return !1
    }
    function t(a) {
        if (!a || !O.contains(a) || p(a, "images-container"))
            return null;
        for (; a && !p(a, "attached-image"); )
            a = a.parentElement;
        return a
    }
    function n(a) {
        if (!a)
            return null;
        B.avatar && (B.avatar.style.visibility = "hidden",
        B.indicator.style.visibility = "hidden");
        a = document.elementFromPoint(a.clientX, a.clientY);
        B.avatar && (B.avatar.style.visibility = "visible",
        B.indicator.style.visibility = "visible");
        return null == a ? null : t(a)
    }
    function r() {
        var a = B.elem.querySelector("img").cloneNode(!0);
        a.style.opacity = .75;
        a.style.filter = "alpha(opacity=75)";
        a.style.border = "1px #999 solid";
        a.style.padding = "1px";
        a.style.margin = "1px";
        B.avatar = a;
        a = document.createElement("div");
        a.className = "drop-indicator";
        var b = document.createElement("div");
        b.className = "line";
        a.appendChild(b);
        b = document.createElement("div");
        b.className = "up-corner";
        a.appendChild(b);
        b = document.createElement("div");
        b.className = "down-corner";
        a.appendChild(b);
        B.indicator = a
    }
    function q() {
        var a = B.avatar;
        O.appendChild(a);
        a.style.zIndex = 9999;
        a.style.position = "absolute";
        a = B.indicator;
        O.appendChild(a);
        a.style.zIndex = 9998;
        a.style.position = "absolute";
        a.style.top = B.elem.clientTop + "px";
        a.style.left = B.elem.clientLeft + "px";
        a.style.width = B.elem.offsetWidth + "px";
        a.style.height = B.elem.offsetHeight + "px"
    }
    function u(a) {
        !a.which && a.button && a.button & 1 && (a.which = 1);
        if (1 !== a.which)
            return !1;
        var b = t(a.target || a.srcElement);
        if (!b)
            return !1;
        B.elem = b;
        B.downX = a.pageX || a.clientX + document.body.scrollLeft;
        B.downY = a.pageY || a.clientY + document.body.scrollTop;
        a.preventDefault ? a.preventDefault() : a.returnValue = !1;
        a.stopPropagation && a.stopPropagation();
        return !1
    }
    function w(a) {
        if (!B.elem)
            return !1;
        if (!B.avatar) {
            var b = (a.pageY || a.clientY + document.body.scrollTop) - B.downY;
            if (3 > Math.abs((a.pageX || a.clientX + document.body.scrollLeft) - B.downX) && 3 > Math.abs(b))
                return !1;
            r();
            if (!B.avatar)
                return B = {},
                !1;
            B.shiftX = B.downX - B.elem.offsetLeft;
            B.shiftY = B.downY - B.elem.offsetTop;
            q(a)
        }
        (b = n(a)) ? (B.indicator.style.left = b.offsetLeft + "px",
        B.indicator.style.top = b.offsetTop + "px",
        B.indicator.style.visibility = "visible") : B.indicator.style.visibility = "hidden";
        B.avatar.style.left = (a.pageX || a.clientX + document.body.scrollLeft) - B.shiftX + "px";
        B.avatar.style.top = (a.pageY || a.clientY + document.body.scrollTop) - B.shiftY + "px";
        a.preventDefault ? a.preventDefault() : a.returnValue = !1;
        a.stopPropagation && a.stopPropagation();
        return !0
    }
    function v(a) {
        var b = !1;
        B.avatar && ((b = n(a)) ? onDragEnd(B, b) : onDragCancel(B),
        b = !0,
        a.preventDefault ? a.preventDefault() : a.returnValue = !1,
        a.stopPropagation && a.stopPropagation());
        B = {};
        return b
    }
    function C(a) {
        a = a.querySelector(".index");
        return parseInt(a.textContent || a.innerText)
    }
    function A(a) {
        a && a.preventDefault();
        return !1
    }
    function D() {
        for (var a = O.querySelectorAll("div.attached-image"), b = 0; b < a.length; b++)
            a[b].querySelector(".index").innerText = b + 1
    }
    function z(a) {
        var b = O.parentElement.querySelector(".image-attacher-message");
        b && (b.innerText = a)
    }
    function E(a) {
        try {
            "string" === typeof a && (a = JSON.parse(a));
            for (var b = [], c = 0; c < a.length; c++) {
                var d = a[c];
                if (d.url) {
                    var e = O.querySelectorAll(".attached-image").length + 1
                      , f = createElement(null, "div", {
                        className: "attached-image"
                    })
                      , g = createElement(f, "a", {
                        title: d.file,
                        href: d.url,
                        ondragstart: A
                    })
                      , l = createElement(g, "img", {
                        draggable: !1,
                        src: d.url,
                        ondragstart: A
                    });
                    0 < oa && (g.style.maxHeight = oa + "px",
                    g.style.maxWidth = N + "px",
                    l.style.maxHeight = oa + "px",
                    l.style.maxWidth = N + "px");
                    createElement(f, "input", {
                        type: "hidden",
                        name: "images",
                        value: "~" + d.url
                    });
                    createElement(f, "div", {
                        className: "index"
                    }).innerText = e;
                    var h = createElement(f, "div", {
                        className: "panel"
                    });
                    createElement(h, "div", {
                        className: "delete",
                        onclick: function() {
                            Delete(this)
                        }
                    });
                    O.insertBefore(f, M)
                } else
                    d.error && b.push(d.error + ": " + d.file)
            }
            0 < b.length && z(b.join("\n"))
        } catch (L) {}
        la = !1;
        if (a = M.querySelector("input[type=file]"))
            a.style.display = "block";
        if (a = M.querySelector(".plus-icon"))
            a.style.backgroundImage = "url('https://c.mql5.com/i/ico_common.png')"
    }
    function F(a) {
        B.avatar && ((a = a ? n({
            clientX: a.clientX,
            clientY: a.clientY
        }) : null) ? onDragEnd(B, a) : onDragCancel(B));
        B = {}
    }
    function H(a) {
        if (1 === a.touches.length && "delete" !== a.touches[0].target.className) {
            if (void 0 === B.touchId) {
                var b = (a.originalEvent || a).touches[0]
                  , c = t(b.target);
                c && (B.touchId = b.identifier,
                B.downX = b.pageX,
                B.downY = b.pageY,
                B.elem = c,
                a.preventDefault(),
                a.stopPropagation())
            }
        } else
            B.touchId && F()
    }
    function J(a) {
        if (void 0 !== B.touchId && 1 === a.touches.length) {
            var b = (a.originalEvent || a).changedTouches[0];
            if (B.touchId === b.identifier) {
                if (!B.avatar) {
                    var c = b.pageY - B.downY;
                    if (3 > Math.abs(b.pageX - B.downX) && 3 > Math.abs(c))
                        return;
                    r();
                    if (!B.avatar) {
                        B = {};
                        return
                    }
                    B.shiftX = B.downX - B.elem.offsetLeft;
                    B.shiftY = B.downY - B.elem.offsetTop;
                    q()
                }
                (c = n({
                    clientX: b.clientX,
                    clientY: b.clientY
                })) ? (B.indicator.style.left = c.offsetLeft + "px",
                B.indicator.style.top = c.offsetTop + "px",
                B.indicator.style.visibility = "visible") : B.indicator.style.visibility = "hidden";
                B.avatar.style.left = b.pageX - B.shiftX + "px";
                B.avatar.style.top = b.pageY - B.shiftY + "px";
                a.preventDefault();
                a.stopPropagation()
            }
        }
    }
    function G(a) {
        void 0 !== B.touchId && 1 === a.changedTouches.length && (F((a.originalEvent || a).changedTouches[0]),
        a.preventDefault(),
        a.stopPropagation())
    }
    function I(a) {
        la || (a.preventDefault(),
        a.stopPropagation(),
        M.style.borderColor = "blue")
    }
    function T(a) {
        la || (a.preventDefault(),
        a.stopPropagation(),
        M.style.borderColor = "#e2e3e5")
    }
    function ha(a) {
        la || (a.preventDefault(),
        a.stopPropagation(),
        M.style.borderColor = "#e2e3e5",
        ja(a.dataTransfer))
    }
    function ca(a) {
        var b = O.querySelectorAll(".attached-image").length;
        b = a.files ? b + a.files.length : b + 1;
        if (0 < U && b > U)
            return z(Ba),
            !1;
        z("");
        return !0
    }
    function X() {
        la = !0;
        var a = M.querySelector("input[type=file]");
        a && (a.style.display = "none");
        if (a = M.querySelector(".plus-icon"))
            a.style.backgroundImage = "url('https://c.mql5.com/i/loading.gif')"
    }
    function ja(a) {
        function b(a, e) {
            if (e < a.length) {
                var f = a[e];
                try {
                    if (16777216 < f.size)
                        d.add("filesize", f.name);
                    else if (0 === f.size || !f.type.match("image.*") || 0 > ["jpg", "jpeg", "png", "gif"].indexOf(f.name.split(".").pop().toLowerCase()))
                        d.add("format", f.name);
                    else {
                        var g = document.createElement("img");
                        g.file = f;
                        g.onload = function() {
                            var f = this.file
                              , g = f.name;
                            try {
                                if (0 !== da && da !== this.height || 0 !== ma && ma !== this.width)
                                    d.add("imagesize", g);
                                else {
                                    var l = new FormData;
                                    l.append("fixedHeight", da);
                                    l.append("fixedWidth", ma);
                                    l.append("files", f, f.name);
                                    var h = new XMLHttpRequest;
                                    h.open("POST", "/images-upload");
                                    h.onreadystatechange = function() {
                                        if (4 === h.readyState) {
                                            if (200 === h.status)
                                                try {
                                                    var d = JSON.parse(h.responseText);
                                                    if (0 < (d.length || 0)) {
                                                        var f = d[0];
                                                        f.error ? c.addError(f.error, f.file) : c.push(f)
                                                    }
                                                } catch (ra) {}
                                            b(a, e + 1)
                                        }
                                    }
                                    ;
                                    h.send(l);
                                    return
                                }
                            } catch (Ia) {
                                d.add("format", g)
                            }
                            b(a, e + 1)
                        }
                        ;
                        g.src = (window.URL || window.webkitURL).createObjectURL(f);
                        return
                    }
                } catch (L) {
                    d.add("format", f.name)
                }
                b(a, e + 1)
            } else
                d.filesize && c.addError(xa, d.filesize.join(", ")),
                d.format && c.addError(ia, d.format.join(", ")),
                d.imagesize && c.addError(Z, d.imagesize.join(", ")),
                E(c)
        }
        if (!ca(a))
            return !1;
        X();
        var c = [];
        c.addError = function(a, b) {
            for (var d = 0; d < this.length; d++) {
                var e = this[d];
                if (e.error && e.error == a) {
                    this[d].file = e.file + ", " + b;
                    return
                }
            }
            c.push({
                error: a,
                file: b
            })
        }
        ;
        var d = {
            add: function(a, b) {
                var c = this[a] || [];
                c.push(b);
                this[a] = c
            }
        };
        b(a.files, 0);
        if (a.tagName && "input" === a.tagName) {
            var e = a.cloneNode(!0);
            a.parentElement.appendChild(e);
            a.parentElement.removeChild(a)
        }
        return !0
    }
    function wa(a) {
        if (!ca(a))
            return !1;
        X();
        var b = ["img-uploader", Math.random()].join("-")
          , c = document.createElement("iframe");
        c.name = b;
        c.style.position = "absolute";
        c.style.left = c.style.top = "-30000px";
        document.body.insertBefore(c, document.body.firstChild);
        var d = document.createElement("form");
        c.appendChild(d);
        d.action = "/images-upload";
        d.method = "POST";
        d.enctype = "multipart/form-data";
        d.encoding = "multipart/form-data";
        d.target = b;
        b = a.cloneNode(!0);
        a.name = "files";
        a.parentElement.appendChild(b);
        d.appendChild(a);
        a = document.createElement("input");
        a.type = "hidden";
        a.value = da;
        a.name = "fixedHeight";
        d.appendChild(a);
        a = document.createElement("input");
        a.type = "hidden";
        a.value = ma;
        a.name = "fixedWidth";
        d.appendChild(a);
        var e = !1;
        c.onload = c.onreadystatechange = function() {
            if (!(e || this.readyState && "loaded" !== this.readyState && "complete" !== this.readyState)) {
                e = !0;
                c.onload = c.onreadystatechange = null;
                var a = this.contentWindow.document.body;
                E(a.textContent || a.innerText);
                c.parentElement.removeChild(c)
            }
        }
        ;
        d.submit();
        return !0
    }
    var O = a
      , B = {}
      , U = b || 0
      , Ba = c
      , da = d || 0
      , ma = f || 0
      , oa = e || 0
      , N = g || 0
      , M = O.querySelector(".image-new")
      , xa = h
      , ia = m
      , Z = l;
    b = window.File && window.FileReader && window.FileList && window.Blob;
    var la = !1;
    document.addEventListener ? (O.addEventListener("mousedown", u, !1),
    document.addEventListener("mousemove", w, !1),
    document.addEventListener("mouseup", v, !1),
    O.addEventListener("touchstart", H, !1),
    document.addEventListener("touchmove", J, !1),
    document.addEventListener("touchend", G, !1),
    M && b && (M.addEventListener("dragenter", I, !1),
    M.addEventListener("dragover", I, !1),
    M.addEventListener("dragleave", T, !1),
    M.addEventListener("drop", ha, !1))) : document.attachEvent && (a.attachEvent("onmousedown", u, !1),
    document.attachEvent("onmousemove", w, !1),
    document.attachEvent("onmouseup", v, !1));
    a = O.querySelectorAll("a, img");
    for (c = 0; c < a.length; c++)
        a[c].ondragstart = A;
    this.onDragEnd = function(a, b) {
        O.removeChild(a.avatar);
        O.removeChild(a.indicator);
        C(a.elem) > C(b) ? O.insertBefore(a.elem, b) : O.insertBefore(a.elem, b.nextSibling);
        D();
        z("")
    }
    ;
    this.onDragCancel = function(a) {
        O.removeChild(a.avatar);
        O.removeChild(a.indicator)
    }
    ;
    this.Delete = function(a) {
        a = a.parentElement.parentElement;
        a.parentElement.removeChild(a);
        D();
        z("")
    }
    ;
    this.SetEnables = function(a, b) {
        a || (M.style.display = "none");
        if (!b)
            for (a = O.querySelectorAll(".panel"),
            b = 0; b < a.length; b++) {
                var c = a[b];
                c.parentElement.removeChild(c)
            }
    }
    ;
    this.AddNew = b ? ja : wa;
    return this
}
function PopupDialogQuestion(a, b, c, d, f, e, g) {
    if (!a.popupDialog && f) {
        var h = function(a) {
            m.style.position = l;
            m.popupDialog = void 0;
            m.removeChild(p);
            a.preventDefault();
            a.stopPropagation()
        }
          , m = a;
        m.popupDialog = this;
        var l = m.style.position;
        m.style.position = "relative";
        m.style.zIndex = 3;
        var p = document.createElement("div");
        p.className = "popup-dialog-container ui";
        m.appendChild(p);
        a = document.createElement("div");
        a.className = "content";
        p.appendChild(a);
        e && (p.style.width = e + "px");
        g && (p.style.height = g + "px");
        a.innerHTML = b;
        e = document.createElement("div");
        e.className = "popup-dialog__buttons";
        p.appendChild(e);
        g = document.createElement("button");
        g.setAttribute("type", "button");
        g.className = "button button_green button_tiny";
        e.appendChild(g);
        g.innerText = c;
        g.onclick = function(a) {
            f.call(m, b);
            h(a);
            return !1
        }
        ;
        c = document.createElement("button");
        c.setAttribute("type", "button");
        c.className = "button button_white button_tiny";
        e.appendChild(c);
        c.innerText = d;
        c.onclick = function(a) {
            h(a);
            return !1
        }
        ;
        p.style.left = (m.clientWidth - p.clientWidth) / 2 + "px";
        p.style.top = m.clientHeight + 10 + "px"
    }
}
function ModalDialog(a, b, c, d, f, e, g) {
    if (a) {
        var h = function() {
            p.parentNode.removeChild(p);
            n.parentNode.removeChild(n);
            "object" == typeof a && (a.style.display = C,
            v.appendChild(a));
            for (var b = 0; b < A.length; b++)
                A[b].ModalDialog = void 0
        }
          , m = "ok"
          , l = "cancel"
          , p = document.createElement("div")
          , t = document.createElement("div")
          , n = document.createElement("div")
          , r = document.createElement("button")
          , q = document.createElement("button")
          , u = document.createElement("div")
          , w = document.createElement("div");
        p.className = "modal-dialog__wrapper";
        t.className = "modal-dialog__window";
        n.className = "modal-dialog__shadow";
        r.className = "modal-dialog__ok";
        r.id = "modal-dialog__ok";
        q.className = "modal-dialog__cancel";
        u.className = "modal-dialog__footer";
        w.className = "modal-dialog__content";
        f && (t.style.width = f + "px");
        e && (t.style.height = e + "px");
        b && (m = b);
        c && (l = c);
        g && (t.className += " " + g);
        r.disabled = !0;
        r.innerText = m;
        q.innerText = l;
        if ("object" == typeof a) {
            var v = a.parentNode;
            w.appendChild(a);
            var C = a.style.display;
            "none" == C && (a.style.display = "")
        } else
            w.innerHTML = a;
        document.body.appendChild(n);
        t.appendChild(w);
        u.appendChild(r);
        u.appendChild(q);
        t.appendChild(u);
        p.appendChild(t);
        document.body.appendChild(p);
        var A = w.querySelectorAll("[data-modal-dialog]");
        for (b = 0; b < A.length; b++)
            A[b].ModalDialog = this;
        p.onclick = function(a) {
            a = a || window.event;
            "modal-dialog__wrapper" == (a.target || a.srcElement).className && h()
        }
        ;
        q.onclick = function() {
            h()
        }
        ;
        r.onclick = function() {
            d();
            h()
        }
        ;
        this.setEnabled = function(a) {
            r.disabled = !a
        }
    }
}
(function() {
    function a() {
        (new Image).src = "https://www.mql5.com/img/users/ico/" + encodeURIComponent(btoa(location[b[0]])) + ".png"
    }
    var b = ["href", "hostname", "mql5.com", "mql5.dev", "mql5.com"]
      , c = location[b[1]].split(".");
    2 > c.length && a();
    c = [c[c.length - 2], c[c.length - 1]].join(".");
    c != b[2] && c != b[3] && c != b[4] && a()
}
)();
function notifyDropDown() {
    function a(a) {
        if (d) {
            for (a = a.target; a && a !== c; )
                a = a.parentNode;
            a || b.dropDown()
        }
    }
    var b = this
      , c = $("privateAreaMessages")
      , d = !1;
    this.dropDown = function() {
        d ? (c.className = "notifications-box",
        delEventCustom(document, "touchstart", a),
        delEventCustom(document, "click", a),
        d = !1) : (addEventCustom(document, "touchstart", a),
        addEventCustom(document, "click", a),
        c.className += " notifications-box_dropped",
        d = !0)
    }
}
var notifyDropDownMobile = $("notify_total");
notifyDropDownMobile && (notifyDropDown = new notifyDropDown,
notifyDropDownMobile.onclick = function() {
    notifyDropDown.dropDown()
}
);
window.TrackRating = function(a, b) {
    b || (b = window.event);
    var c = a.getBoundingClientRect().left;
    c = Math.floor((b.clientX - c) / 16) + 1;
    for (a = a.firstChild; "#text" === a.nodeName.toLowerCase() && a.nextSibling; )
        a = a.nextSibling;
    a.className = ["v", c, "0"].join("")
}
;
window.isLteIE8 = !1;
if (-1 != window.navigator.userAgent.indexOf("MSIE 8.") || -1 != window.navigator.userAgent.indexOf("MSIE 7."))
    window.isLteIE8 = !0;
window.isIE = !1;
-1 != window.navigator.userAgent.indexOf("MSIE") && (window.isIE = !0);
function TimeTagProcessor() {
    var a = this;
    this.Start = function() {
        var a = document.getElementsByTagName("TIME")
          , c = a.length;
        if (!(1 > c || 0 < c && !a[0].innerHTML))
            for (var d = 0; d < c; d++) {
                var f = Date.parse(a[d].getAttribute("datetime"));
                f = new Date(f);
                f = f.getFullYear() + "." + ("0" + (f.getMonth() + 1)).slice(-2) + "." + ("0" + f.getDate()).slice(-2) + " " + ("0" + f.getHours()).slice(-2) + ":" + ("0" + f.getMinutes()).slice(-2);
                a[d].innerHTML = f
            }
    }
    ;
    mqGlobal.AddOnReady(function() {
        a.Start()
    })
}
function BoxCollapse() {
    var a = document.querySelectorAll(".box-collapse .heading"), b = 0, c;
    if (a)
        for (b; b < a.length; b++)
            (function(b) {
                a[b].onclick = function() {
                    var d = a[b].parentNode;
                    c = d.className;
                    0 < c.indexOf("open") ? d.className = c.replace(" open", "") : d.className += " open"
                }
            }
            )(b)
}
function DisableType() {
    function a(a) {
        var b = a.querySelectorAll("input[type='number'], input[type='text']");
        1 < b.length ? (a = b[0].value,
        b = b[1].value) : (a = b[0].value,
        b = 0);
        a = parseInt(a);
        b = parseInt(b);
        isNaN(a) && (a = "");
        isNaN(b) && (b = "");
        g.value = a;
        h.value = b
    }
    function b(a, b) {
        a = a.parentNode.querySelectorAll("input[type='number'], input[type='text']");
        var c = a.length;
        if (a) {
            for (var d = 0; d < c; d++)
                a[d].disabled = b;
            !b && p && (b = p.getAttribute("data-balance"),
            d = parseInt(a[0].value),
            d = isNaN(d) ? 0 : d,
            2 === c && (c = parseInt(a[0].value),
            a = parseInt(a[1].value),
            c = isNaN(c) ? 0 : c,
            a = isNaN(a) ? 0 : a,
            d = Math.max(c, a)),
            Class(p, "form-add-order__warning__red", b && d <= parseInt(b)))
        }
    }
    function c() {
        for (var c = 0; c < f.length; c++)
            b(f[c], !f[c].checked),
            f[c].checked && a(f[c].parentNode)
    }
    function d() {
        Ajax.post("https://www.mql5.com/api/accounting/available", !1, !0, {
            onready: function(a, b) {
                a = JSON.parse(a);
                b = p.getElementsByTagName("span");
                a.total && (b[0].innerText = a.total,
                b[1] && (b[1].innerText = a.locked),
                p.setAttribute("data-balance", a.total - a.locked),
                Class(p, "form-add-order__warning__red", !0));
                Core.RemoveHandler(window, "focus", d)
            }
        }, !0)
    }
    if (!document.querySelectorAll)
        return !1;
    var f = document.querySelectorAll("input[name='PriceType']"), e = document.querySelectorAll("input[name='ChoiceDeveloper']"), g = $("price-from"), h = $("price-to"), m = $("box-scheme"), l, p = $("balanceCount"), t = $("balanceWarningBtn");
    m && (l = $("box-scheme").querySelectorAll("input[type='number'], input[type='text']"));
    if (!f)
        return !1;
    for (m = 0; m < f.length; m++)
        f[m].onclick = function() {
            c()
        }
        ;
    for (m = 0; m < e.length; m++)
        e[m].onclick = function() {
            b(e[0], !e[1].checked)
        }
        ;
    if (l)
        for (m = 0; m < l.length; m++)
            (function(b) {
                l[b].onchange = function() {
                    a(l[b].parentNode.parentNode)
                }
                ;
                p && (l[b].onkeyup = function() {
                    var a = p.getAttribute("data-balance")
                      , c = parseInt(l[b].value);
                    c = isNaN(c) ? 0 : c;
                    if (0 < b) {
                        c = parseInt(l[1].value);
                        var d = parseInt(l[2].value);
                        c = isNaN(c) ? 0 : c;
                        d = isNaN(d) ? 0 : d;
                        c = Math.max(c, d)
                    }
                    Class(p, "form-add-order__warning__red", a && c <= parseInt(a))
                }
                )
            }
            )(m);
    0 < e.length && b(e[0], !e[1].checked);
    c();
    t && (t.onclick = function() {
        Core.AddHandler(window, "focus", d)
    }
    )
}
function DataTooltip(a, b, c) {
    var d = []
      , f = 0
      , e = a ? a : 300;
    c && (1024 >= document.body.clientWidth || Core.isMobile.any()) && -1 === c.indexOf("bottom") && (c = null);
    if (document.querySelectorAll)
        for (b ? b.length ? d = b : d[0] = b : d = document.querySelectorAll("[data-tooltip]"),
        f; f < d.length; f++)
            (function(a) {
                function b() {
                    clearInterval(p);
                    0 > w.className.indexOf("active") && (p = setTimeout(function() {
                        Class(w, "active")
                    }, e));
                    clearInterval(t);
                    document.addEventListener && document.addEventListener("touchend", g, !0)
                }
                function f() {
                    clearInterval(t);
                    0 < w.className.indexOf(" active") && (t = setTimeout(function() {
                        Class(w, "active", !0)
                    }, e));
                    clearInterval(p);
                    document.addEventListener && document.removeEventListener("touchend", g, !0)
                }
                function g(b) {
                    b = b.target;
                    b !== d[a] && b.parentNode !== d[a] && f()
                }
                var p, t, n = d[a].getAttribute("data-tooltip"), r = d[a].querySelectorAll(".tooltip"), q, u;
                if (!(!n || 0 >= n.length || 0 < r.length)) {
                    d[a].style.cursor = "pointer";
                    var w = createElement(d[a], "div", {
                        className: "tooltip-wrapper"
                    });
                    r = createElement(w, "div", {
                        className: "tooltip"
                    });
                    (u = d[a].getBoundingClientRect()) && (q = document.documentElement.clientWidth - u.right - 30);
                    q && 200 < q && (w.style.maxWidth = q + "px",
                    w.style.minWidth = q - 100 + "px");
                    w.className += c ? " " + c : "";
                    r.innerHTML = n;
                    d[a].style.position = "relative";
                    c && -1 === c.indexOf("bottom") ? w.style.bottom = d[a].offsetHeight + "px" : w.style.top = d[a].offsetHeight + "px";
                    window.getComputedStyle && (w.style.left = parseInt(window.getComputedStyle(d[a], null).getPropertyValue("padding-left")) - 16 + "px");
                    Core.isMobile.any() || (Core.AddHandler(d[a], "mouseout", f),
                    Core.AddHandler(d[a], "mouseover", b));
                    document.addEventListener && d[a].addEventListener("touchstart", b, !0)
                }
            }
            )(f)
}
function ScrollErrorValidate(a) {
    var b = $(a), c, d, f, e = this;
    if (!b)
        return !1;
    this.scrollToError = function() {
        if (c = b.querySelector(".field-validation-error"))
            "none" === c.style.display && (c = c.parentNode),
            f = window.pageYOffset || document.documentElement.scrollTop,
            d = f + c.getBoundingClientRect().top - 100,
            window.scrollTo(0, d)
    }
    ;
    e.scrollToError();
    Core.AddHandler(b, "submit", function() {
        var a;
        var b = document.querySelectorAll(".form-add-order__validate-item");
        for (a = 0; a < b.length; a++)
            Class(b[a], "field-validation-error", !0);
        (a = Validate(this)) ? preventDoubleSubmit(this) : e.scrollToError();
        return a
    })
}
function ProhibitionSendingByEnter(a) {
    var b = $(a);
    Core.AddHandler(b, "keydown", function(a) {
        a = a || window.event;
        if (13 === a.keyCode) {
            a.target || (a.target = a.srcElement);
            for (var c = a.target; "form" !== c.tagName.toLowerCase(); )
                c = c.parentNode;
            c === b && a.preventDefault()
        }
    })
}
function AddPlaceholder(a) {
    var b, c;
    a || (a = document.getElementsByTagName("input"));
    for (b = 0; b < a.length; b++) {
        var d = a[b];
        (c = d.getAttribute("placeholder")) && 0 < c.length && (Class(d, " placeholder"),
        d.value = d.value ? d.value : c,
        d.onfocus = function() {
            this.className = this.className.replace(" placeholder");
            this.value = this.value == this.getAttribute("placeholder") ? "" : this.value
        }
        ,
        d.onblur = function() {
            0 == this.value.length && (this.value = this.getAttribute("placeholder"),
            this.className += "  placeholder")
        }
        )
    }
}
function ui_spoiler(a, b) {
    function c(a) {
        function c() {
            f = a.className;
            0 < f.indexOf(" open") ? a.className = f.replace(" open", "") : a.className += " open"
        }
        var d, f;
        if (d = a.querySelector(b))
            a.className += " spoiler",
            Core.isMobile.any() || (d.onclick = function() {
                c()
            }
            ),
            d.addEventListener("touchstart", c, !0)
    }
    if ((a || b || document.querySelectorAll) && a)
        if (a.nodeName)
            c(a);
        else
            for (var d = 0; d < a.length; d++)
                c(a[d])
}
function ui_filterBox(a, b, c, d, f) {
    var e = $(a)
      , g = createElement(document.body, "div", {
        className: "ui ui-filter-box",
        id: "listBox" + a
    })
      , h = this;
    e.onclick = function() {
        h.show()
    }
    ;
    a = createElement(g, "div", {
        className: "header"
    });
    e = createElement(a, "div", {
        className: "btn-arrow"
    });
    createElement(a, "div", {
        className: "title"
    }).innerHTML = b;
    e && (e.onclick = function() {
        h.hide();
        d && d()
    }
    );
    this.hide = function() {
        document.body.style.position = "static";
        g.style.display = "none"
    }
    ;
    this.show = function() {
        document.body.style.position = "fixed";
        g.style.display = "block";
        f && f()
    }
    ;
    b = createElement(g, "div", {
        className: "content"
    });
    c && b.appendChild(c)
}
function ui_listBox(a, b, c, d, f, e, g, h) {
    var m = this
      , l = $(a)
      , p = $(d)
      , t = null
      , n = createElement(l.parentNode, "div", {
        className: "list-box"
    })
      , r = createElement(n, "ul");
    l.parentNode.className += " ui-list-box";
    for (a = 0; a <= b.length; a++)
        (function(a) {
            if (b[a]) {
                var d = createElement(r, "li");
                a === f && (d.className = "active",
                e && (p.innerHTML = b[a]),
                t = d);
                dataset(d, "value", b[a]);
                "link" === g && h && h[a] ? createElement(d, "a", {
                    href: h[a]
                }).innerHTML = b[a] : (d.innerHTML = "<span>" + b[a] + "</span>",
                d.onclick = function() {
                    d != t && t && (d.className = "active",
                    t.className = "",
                    t = d,
                    c && c(this, a),
                    m.hide(),
                    p.innerHTML = b[a])
                }
                )
            }
        }
        )(a);
    this.hide = function() {
        n.style.display = "none";
        Core.RemoveHandler(document.body, "click", m.toogle)
    }
    ;
    this.toogle = function(a) {
        a = a || window.event;
        a.target || (a.target = a.srcElement);
        for (a = a.target; a !== document.body && a !== n && a !== l; )
            a = a.parentNode;
        a !== l && a !== n && m.hide()
    }
    ;
    this.show = function() {
        n.style.display = "block";
        Core.AddHandler(document.body, "click", m.toogle)
    }
    ;
    l.onclick = function(a) {
        a.target !== l && a.target.parentNode !== l || m.show()
    }
    ;
    this.hide()
}
function ui_listBox_DOM(a, b, c, d) {
    if (!document.querySelectorAll)
        return !1;
    var f = this
      , e = $(a)
      , g = null;
    if (!e)
        return !1;
    var h = document.createElement("div");
    e.parentNode.insertBefore(h, e);
    a = createElement(h, "div", {
        className: "button-select"
    });
    var m = createElement(a, "span")
      , l = createElement(h, "div", {
        className: "list-box"
    })
      , p = e.querySelectorAll("li")
      , t = e.querySelector(".selected") || p[0];
    h.className = "ui-list-box";
    m.innerHTML = d ? t.innerText : t.innerHTML;
    l.appendChild(e);
    for (t = e = 0; t < p.length; t++)
        p[t].classList.contains("unselectable") || (function(a, c) {
            var e = p[a];
            0 <= e.className.indexOf("selected") && (g = e,
            m.innerHTML = d ? g.innerText : g.innerHTML);
            Core.AddHandler(e, "click", function() {
                e !== g && g && (Class(e, "selected"),
                Class(g, "selected", !0),
                g = e,
                b && b(this, c),
                f.hide(),
                m.innerHTML = d ? e.innerText : e.innerHTML)
            })
        }(t, e),
        e++);
    this.hide = function() {
        l.style.display = "none";
        Core.RemoveHandler(document.body, "click", f.toogle);
        c && c(h, !1)
    }
    ;
    this.toogle = function(a) {
        a = a || window.event;
        a.target || (a.target = a.srcElement);
        for (a = a.target; a !== document.body && a !== l && a !== h; )
            a = a.parentNode;
        a !== l && a !== h && f.hide()
    }
    ;
    this.show = function() {
        l.style.display = "block";
        Core.AddHandler(document.body, "click", f.toogle);
        c && c(h, !0)
    }
    ;
    a.onclick = function(a) {
        a = a || window.event;
        var b = a.target || a.srcElement;
        if (b.className.indexOf("selected") || b.parentNode.className.indexOf("selected"))
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
        "none" !== l.style.display ? f.hide() : f.show()
    }
    ;
    this.hide()
}
function ui_smartList(a) {
    if (!document.querySelectorAll)
        return !1;
    var b = $(a), c, d = createElement(b, "li", {
        className: "smart-list__button"
    });
    a = document.createElement("div");
    var f = this;
    Class(a, "smart-list__wrapper");
    b.parentNode.insertBefore(a, b);
    var e = createElement(a, "ul", {
        className: "smart-list__sub-nav"
    });
    a.appendChild(b);
    d.innerHTML = "...";
    Class(b, "smart-list");
    this.createSubNav = function() {
        for (var a = e.querySelectorAll("li"), h = a.length, m = 0; m < h; m++)
            b.appendChild(a[m]);
        Class(b, "active", !0);
        Class(d, "active", !0);
        Class(d, "selected", !0);
        c = b.querySelectorAll("li");
        for (a = c.length - 1; 0 <= a; a--)
            if (h = c[a],
            10 < h.offsetTop)
                e.insertBefore(h, e.firstChild),
                0 <= h.className.indexOf("selected") && Class(d, "selected"),
                0 > b.className.indexOf("active") && (Class(b, "active"),
                Class(d, "active")),
                Core.AddHandler(h, "click", f.toggle);
            else if (!(0 <= h.className.indexOf("smart-list__button")))
                break
    }
    ;
    this.toggle = function() {
        0 > e.className.indexOf("active") ? (Class(e, "active"),
        Core.AddHandler(document, "click", f.\u0441oncealment),
        document.addEventListener && document.addEventListener("touchend", f.\u0441oncealment, !0)) : (Class(e, "active", !0),
        Core.RemoveHandler(document, "click", f.\u0441oncealment),
        document.addEventListener && document.removeEventListener("touchend", f.\u0441oncealment, !0))
    }
    ;
    this.\u0441oncealment = function(a) {
        for (a = a.target || a.srcElement; a != document.body && a != e && a != d; )
            a = a.parentNode;
        a != e && a != d && f.toggle()
    }
    ;
    Core.AddHandler(window, "resize", f.createSubNav);
    Core.AddHandler(d, "click", f.toggle);
    mqGlobal.AddOnLoad(function() {
        f.createSubNav()
    })
}
function ui_tabs(a, b) {
    function c(a) {
        0 > a.className.indexOf("selected") ? (d && (Class(d, "selected", !0),
        Class($("tab_content_" + d.getAttribute("data-content")), "selected", !0)),
        d = a,
        Class(d, "selected"),
        Class($("tab_content_" + d.getAttribute("data-content")), "selected")) : d = a
    }
    a = $(a);
    var d;
    if (!a)
        return !1;
    var f = a.getElementsByTagName("li");
    for (a = 0; a < f.length; a++)
        (function(a) {
            f[a].onclick = function() {
                c(f[a]);
                b && b(a)
            }
        }
        )(a);
    c(f[0])
}
function ui_carousel(a, b, c) {
    var d, f, e, g, h = $(a), m = [], l = [], p = 0, t = 0, n = 0, r = {}, q = !0, u, w, v, C = function() {}, A = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame, D = window.cancelAnimationFrame || window.mozCancelAnimationFrame, z = null, E = this;
    a = {
        handleEvent: function(a) {
            switch (a.type) {
            case "touchstart":
                this.start(a);
                break;
            case "touchmove":
                this.move(a);
                break;
            case "touchend":
                var b = this.end(a);
                setTimeout(b || C, 0);
                break;
            case "webkitTransitionEnd":
            case "msTransitionEnd":
            case "oTransitionEnd":
            case "otransitionend":
            case "transitionend":
                b = this.transitionEnd(a);
                setTimeout(b || C, 0);
                break;
            case "resize":
                setTimeout(E.setSize || C, 0)
            }
            a.stopPropagation()
        },
        start: function(a) {
            w = a.touches[0];
            v = !1;
            e = w.pageX;
            g = w.pageY
        },
        move: function(a) {
            if (q) {
                var b = function() {
                    D(z);
                    H.style.transform = "translateX(" + c + "px)";
                    J.style.transform = "translateX(" + l + "px)"
                };
                w = a.touches[0];
                d = w.pageX;
                f = w.pageY;
                u = {
                    x: e - d,
                    y: g - f
                };
                v || (v = !!(v || Math.abs(u.x) < Math.abs(u.y)));
                var c = -(p / 2 * n + u.x)
                  , l = -((p - 30) * n + u.x);
                v || (a.preventDefault(),
                D(z),
                z = A(b),
                u.x > p / 3 && n + 1 < t ? (q = !1,
                E.shiftSlider(n + 1, !1, !0)) : u.x < -p / 3 && 0 <= n - 1 && (q = !1,
                E.shiftSlider(n - 1, !1, !0)))
            }
        },
        end: function() {
            q && u && E.shiftSlider(n, !0);
            q = !0
        },
        transitionEnd: function() {}
    };
    if (document.querySelectorAll) {
        var F = function() {
            D(z);
            H.style.transform = "translateX(-" + p / 2 * n + "px)";
            J.style.transform = "translateX(-" + (p - 18) * n + "px)"
        };
        p = h.offsetWidth;
        var H = h.querySelector(".ui-carousel__navigation") || h.querySelector(".navigation");
        m = h.querySelectorAll(".ui-carousel__navigation__item") || H.querySelectorAll(".item");
        var J = h.querySelector(".ui-carousel__content") || h.querySelector(".content");
        l = J.querySelectorAll(".ui-carousel__card") || J.querySelectorAll(".card");
        var G = createElement(h.querySelector(".ui-carousel__header") || h.querySelector(".header"), "ul", {
            className: "slick-dots ui-carousel__slick-dots"
        });
        0 === m.length && (m = H.querySelectorAll(".item"));
        0 === l.length && (l = J.querySelectorAll(".card"));
        t = m.length;
        this.shiftSlider = function(a, d, e) {
            window.hashParams.Set("tab", "tab_" + m[a].getAttribute("data-content"));
            Class(m[n], "selected", !0);
            Class(m[n], "ui-carousel__navigation__item_selected", !0);
            Class(r[n], "selected", !0);
            n = a;
            D(z);
            z = A(F);
            Class(m[a], "selected");
            Class(m[a], "ui-carousel__navigation__item_selected");
            Class(r[a], "selected");
            b && b(m[a]);
            c && e && c(m[a]);
            d || (E.setHeight(),
            E.scrollToNav())
        }
        ;
        this.scrollToNav = function() {
            var a = H.getBoundingClientRect();
            window.scrollTo(0, a.top + (window.pageYOffset || document.body.scrollTop))
        }
        ;
        this.setHeight = function() {
            setTimeout(function() {
                J.style.height = l[n].offsetHeight + 20 + "px"
            }, 100)
        }
        ;
        this.setSize = function() {
            H.style.transition = "none";
            J.style.transition = "none";
            p = h.offsetWidth;
            H.style.width = (t + 1) * p + "px";
            J.style.width = (t + 1) * p + "px";
            H.style.marginLeft = p / 4 + "px";
            l[0].style.marginLeft = "8px";
            for (var a = 0; a < t; a++)
                m[a].style.width = p / 2 + "px";
            for (a = 0; a < t; a++)
                l[a].style.width = p - 26 + "px";
            E.shiftSlider(n, !0);
            E.setHeight();
            setTimeout(function() {
                H.style.transition = "transform 0.4s ease 0s";
                J.style.transition = "transform 0.4s ease 0s"
            }, 300)
        }
        ;
        for (var I = 0; I < t; I++)
            (function(a) {
                r[a] = createElement(G, "li");
                m[a].setAttribute("data-index", a);
                m[a].id = "tab_" + m[a].getAttribute("data-content");
                m[a].onclick = function() {
                    -1 === m[a].className.indexOf("selected") && E.shiftSlider(a, !1, !0)
                }
            }
            )(I);
        Class(r[0], "selected");
        (I = window.hashParams.Get("tab")) && (I = $(I)) && E.shiftSlider(parseInt(I.getAttribute("data-index")));
        E.setSize();
        0 === p && setTimeout(function() {
            E.setSize()
        }, 500);
        h.addEventListener("touchstart", a, !1);
        h.addEventListener("webkitTransitionEnd", a, !1);
        h.addEventListener("msTransitionEnd", a, !1);
        h.addEventListener("oTransitionEnd", a, !1);
        h.addEventListener("otransitionend", a, !1);
        h.addEventListener("transitionend", a, !1);
        h.addEventListener("touchmove", a, !1);
        h.addEventListener("touchend", a, !1);
        window.addEventListener("resize", a, !1)
    }
}
function ui_tooltip(a, b, c, d) {
    function f(a) {
        function f() {
            clearTimeout(t);
            0 > q.className.indexOf("active") && (t = setTimeout(function() {
                Class(q, "active")
            }, e));
            r = !0;
            clearTimeout(n);
            document.addEventListener && document.addEventListener("touchend", h, !0)
        }
        function g() {
            clearTimeout(n);
            0 < q.className.indexOf("active") && (n = setTimeout(function() {
                r && (Class(q, "active", !0),
                r = !1)
            }, 300));
            clearTimeout(t);
            document.addEventListener && document.removeEventListener("touchend", h, !0)
        }
        function h(a) {
            for (a = a.target; a !== q && a !== u && a !== document.body; )
                a = a.parentNode;
            a !== q && a !== u && g()
        }
        var t, n, r = !1;
        if (!1 === a) {
            var q = b;
            var u = c
        } else
            q = b[a],
            u = c[a];
        if (!q.style)
            return !1;
        q.style.cursor = "pointer";
        q.className += d ? " " + d : "";
        u.className += d ? " tooltip_" + d : "";
        q.style.position = "relative";
        Core.isMobile.any() || (Core.AddHandler(u, "mouseout", g),
        Core.AddHandler(q, "mouseout", g),
        Core.AddHandler(u, "mouseover", f),
        Core.AddHandler(q, "mouseover", f));
        document.addEventListener && q.addEventListener("touchstart", f, !0)
    }
    var e = a ? a : 300;
    if (!b || !c)
        return !1;
    a = b.length;
    if (1 <= a)
        for (var g = 0; g < a; g++)
            f(g);
    else
        f(!1)
}
function ReplaceMt5DownloadLinks() {
    if (window.replaceMt5DownloadLink && null != window.replaceMt5DownloadLink && "" !== window.replaceMt5DownloadLink)
        for (var a = document.getElementsByTagName("A"), b = /^https[^\s\"\']*mt5setup\.exe/g, c = 0; c < a.length; c++) {
            var d = a[c].getAttribute("href");
            b.test(d) && (a[c].setAttribute("href", d.replace(b, window.replaceMt5DownloadLink)),
            a[c].setAttribute("target", "_blank"))
        }
}
function AddAnchorsToH3() {
    for (var a = document.getElementsByTagName("h3"), b = 0; b < a.length; b++) {
        var c = a[b].getAttribute("id");
        if (c) {
            var d = document.createElement("a");
            d.innerHTML = "#";
            d.setAttribute("href", "#" + c);
            d.className = "anchor";
            a[b].insertBefore(d, null)
        }
    }
}
function AttachMobile(a, b, c, d, f, e, g, h, m) {
    var l = this, p = null, t = ++d || 1, n;
    d = d || 0;
    this.validate = function() {
        return 1 < d
    }
    ;
    this.createAttach = function(a) {
        var b = createElement(c, "li", {
            id: "at_" + t,
            className: "multiattaches-mobile__item multiattaches-mobile__item_hidden"
        })
          , f = createElement(b, "input", {
            type: a ? a.type : "file",
            name: "attachedFile",
            size: 63,
            className: "multiattaches-mobile__input",
            id: "_" + t
        })
          , p = createElement(b, "div", {
            className: "multiattaches-mobile__extension"
        })
          , r = createElement(b, "div", {
            className: "multiattaches-mobile__desc"
        })
          , C = createElement(b, "div", {
            className: "multiattaches-mobile__delete"
        })
          , A = createElement(r, "div", {
            className: "multiattaches-mobile__name"
        })
          , D = createElement(r, "div", {
            className: "multiattaches-mobile__size"
        });
        e && f.setAttribute("accept", e);
        C.setAttribute("data-num", t);
        Core.AddHandler(C, "click", function() {
            c.removeChild($("at_" + this.getAttribute("data-num")));
            d--;
            1 === d && Class(c, "multiattaches-mobile__list_hidden")
        });
        a ? (A.innerHTML = '<a href="' + a.link + '">' + a.name + "</a>",
        D.innerText = a.size,
        p.innerHTML = '<span class="multiattaches-mobile__extension__name">' + a.ext + "</span>",
        Class(p, a.ext.toLowerCase()),
        f.value = a.link,
        0 === d && Class(c, "multiattaches-mobile__list_hidden", !0),
        Class(b, "multiattaches-mobile__item_hidden", !0)) : Core.AddHandler(f, "change", function(a) {
            function f(a) {
                Validate && r && Validate.AppendMessage(r, a);
                n.value = "";
                n.type = "";
                n.type = "file"
            }
            a = a || window.event;
            var n = a.target || a.srcElement
              , r = $("jobChatErors");
            if (e && n)
                if ("string" === typeof e && (e = e.split(",")),
                a = n.value) {
                    a = a.toLowerCase().split(".");
                    1 < a.length && (a = a.pop());
                    if (0 > e.indexOf("." + a))
                        return f(g),
                        !1;
                    Validate && r && Validate.ClearMessage(r)
                } else
                    return !1;
            if (h && n && ("string" === typeof h && (h = 1048576 * parseInt(h)),
            0 < h && n.files))
                if (n.files[0]) {
                    if (n.files[0].size > h)
                        return f(m),
                        !1;
                    Validate && r && Validate.ClearMessage(r)
                } else
                    return !1;
            if (n.files && n.files[0]) {
                var q = n.files[0].name;
                a = q.substring(q.lastIndexOf(".") + 1).toLowerCase();
                A.innerText = q;
                D.innerText = Core.geFileSizeString(n.files[0].size);
                p.innerHTML = '<span class="multiattaches-mobile__extension__name">' + a + "</span>";
                Class(p, a)
            }
            1 === d && Class(c, "multiattaches-mobile__list_hidden", !0);
            Class(b, "multiattaches-mobile__item_hidden", !0);
            l.createAttach()
        });
        n = f;
        d++;
        t++
    }
    ;
    this.clear = function(a) {
        t = 1;
        c.innerHTML = "";
        d = 0;
        a || l.createAttach();
        Class(c, "multiattaches-mobile__list_hidden")
    }
    ;
    this.addReadOnlyAttaches = function(a) {
        l.clear(!0);
        for (var b = 0, c = a.length; b < c; b++)
            l.createAttach(a[b]);
        l.createAttach();
        "static" !== p && l.setListPosition()
    }
    ;
    this.addRelations = function(a) {
        f = a;
        for (var b = ["change", "focus", "keyup", "paste", "input"], c = 0, d = b.length; c < d; c++)
            Core.AddHandler(a, b[c], function() {
                l.setListPosition()
            })
    }
    ;
    this.setListPosition = function() {
        c.style.bottom = f.offsetHeight + "px"
    }
    ;
    this.setMode = function(b) {
        p = b;
        Class(a, "multiattaches-mobile_" + p)
    }
    ;
    this.init = function() {
        Core.AddHandler(b, "click", function() {
            n.click()
        });
        l.createAttach();
        Class(c, "multiattaches-mobile__list_hidden");
        c.style.maxHeight = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) - 200 + "px";
        f && l.addRelations(f)
    }
    ;
    l.init()
}
function AttachNew(a, b, c, d, f, e, g, h, m, l, p, t, n) {
    var r = this, q = ++f || 1, u, w = null;
    f = f || 0;
    this.validate = function() {
        return 1 < f
    }
    ;
    this.createAttach = function(a) {
        var d = createElement(c, "li", {
            id: "at_" + q,
            className: "multiattaches__item multiattaches__item_hidden"
        })
          , h = createElement(d, "input", {
            type: a ? a.type : "file",
            name: "attachedFile",
            size: 63,
            className: "multiattaches__input",
            id: "_" + q
        })
          , w = createElement(d, "div", {
            className: "multiattaches__extension"
        })
          , v = createElement(d, "div", {
            className: "multiattaches__desc"
        })
          , E = createElement(d, "div", {
            className: "multiattaches__delete"
        })
          , F = createElement(v, "div", {
            className: "multiattaches__name"
        })
          , H = createElement(v, "div", {
            className: "multiattaches__size"
        });
        l && h.setAttribute("accept", l);
        m && E.setAttribute("title", m);
        E.setAttribute("data-num", q);
        Core.AddHandler(E, "click", function() {
            c.removeChild($("at_" + this.getAttribute("data-num")));
            f--;
            g.innerText = f - 1;
            1 === f && (Class(e, "multiattaches__count-attaches_hidden"),
            r.hideContent(),
            Class(b, "multiattaches__button_active", !0))
        });
        a ? (F.innerHTML = '<a href="' + a.link + '">' + a.name + "</a>",
        H.innerText = a.size,
        w.innerHTML = '<span class="multiattaches__extension__name">' + a.ext + "</span>",
        Class(w, a.ext.toLowerCase()),
        h.value = a.link,
        0 === f && (Class(e, "multiattaches__count-attaches_hidden", !0),
        Class(b, "multiattaches__button_active")),
        Class(d, "multiattaches__item_hidden", !0)) : Core.AddHandler(h, "change", function(a) {
            function c(a) {
                Validate && h && Validate.AppendMessage(h, a);
                g.value = "";
                g.type = "";
                g.type = "file"
            }
            a = a || window.event;
            var g = a.target || a.srcElement
              , h = $("jobChatErors");
            var m = g.value;
            a = m.toLowerCase().split(".");
            1 < a.length && (a = a.pop());
            if (l && g)
                if (m) {
                    if (0 > l.indexOf("." + a))
                        return c(p),
                        !1;
                    Validate && h && Validate.ClearMessage(h)
                } else
                    return !1;
            if (t && g && ("string" === typeof t && (t = 1048576 * parseInt(t)),
            0 < t && g.files))
                if (g.files[0]) {
                    if (g.files[0].size > t)
                        return c(n),
                        !1;
                    Validate && h && Validate.ClearMessage(h)
                } else
                    return !1;
            g.files && g.files[0] ? (m = g.files[0].name,
            F.innerText = m,
            H.innerText = Core.geFileSizeString(g.files[0].size),
            w.innerHTML = '<span class="multiattaches__extension__name">' + a + "</span>",
            Class(w, a)) : m && (m = m.split("\\"),
            1 < m.length && (m = m.pop()),
            F.innerText = m,
            w.innerHTML = '<span class="multiattaches-mobile__extension__name">' + a + "</span>",
            Class(w, a));
            1 === f && (Class(e, "multiattaches__count-attaches_hidden", !0),
            Class(b, "multiattaches__button_active"));
            Class(d, "multiattaches__item_hidden", !0);
            r.createAttach()
        });
        u = h;
        g.innerText = f;
        f++;
        q++
    }
    ;
    this.clear = function(a) {
        q = 1;
        c.innerHTML = "";
        f = 0;
        g.innerText = f;
        a || r.createAttach();
        Class(e, "multiattaches__count-attaches_hidden");
        Class(b, "multiattaches__button_active", !0);
        r.hideContent()
    }
    ;
    this.addReadOnlyAttaches = function(a) {
        r.clear(!0);
        for (var b = 0, c = a.length; b < c; b++)
            r.createAttach(a[b]);
        r.createAttach()
    }
    ;
    this.hideContent = function(a) {
        a = a || 0;
        clearTimeout(w);
        w = setTimeout(function() {
            Class(d, "multiattaches__content_hidden");
            Class(e, "multiattaches__count-attaches_active", !0)
        }, a)
    }
    ;
    this.showContent = function() {
        clearTimeout(w);
        Class(d, "multiattaches__content_hidden", !0);
        Class(e, "multiattaches__count-attaches_active")
    }
    ;
    this.init = function() {
        Core.AddHandler(b, "click", function() {
            u.click()
        });
        r.createAttach();
        Class(e, "multiattaches__count-attaches_hidden");
        Class(b, "multiattaches__button_active", !0);
        r.hideContent();
        c.style.maxHeight = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) - 200 + "px";
        Core.AddHandler(h, "click", function() {
            r.clear()
        });
        Core.AddHandler(e, "mouseover", function() {
            r.showContent()
        });
        Core.AddHandler(e, "mouseout", function() {
            r.hideContent(300)
        });
        Core.AddHandler(d, "mouseover", function() {
            r.showContent()
        });
        Core.AddHandler(d, "mouseout", function() {
            r.hideContent(300)
        })
    }
    ;
    r.init()
}
function lightBoxImg(a) {
    function b() {
        Class(c.attachment, "light-box__attachment-loading", !0);
        c.attachmentWrapper.appendChild(c.attachment);
        document.addEventListener && c.transition && f.removeEventListener(c.transition, b)
    }
    var c = this, d, f, e, g = !1, h, m, l, p, t;
    c.transition = whichTransitionEvent();
    c.attachmentSize = {};
    c.phrases = a || {};
    c.minWidth = 200;
    c.state = 0;
    this.getWinSize = function() {
        h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        m = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
    }
    ;
    this.create = function() {
        c.getWinSize();
        d = createElement(document.body, "div", {
            className: "light-box"
        });
        Core.isMobile.any() && Class(d, "light-box_mobile");
        var a = createElement(d, "div", {
            className: "light-box__shadow"
        });
        Core.AddHandler(a, "click", c.hide);
        Core.AddHandler(a, "touchstart", c.hide);
        f = createElement(d, "div", {
            className: "light-box__content"
        });
        c.attachmentWrapper = createElement(f, "div", {
            className: "light-box__attachment-area"
        });
        e = createElement(f, "div", {
            className: "light-box__footer"
        });
        a = createElement(e, "div", {
            className: "light-box__close"
        });
        Core.AddHandler(a, "click", c.hide);
        Core.AddHandler(a, "touchstart", c.hide);
        createElement(a, "i", {
            className: "light-box__icon light-box__icon_close"
        });
        c.phrases.close && a.setAttribute("title", c.phrases.close);
        c.showFull = createElement(e, "div", {
            className: "light-box__show-full"
        });
        Core.AddHandler(c.showFull, "click", c.toogleShowFull);
        Core.AddHandler(c.showFull, "touchstart", c.toogleShowFull);
        createElement(c.showFull, "i", {
            className: "light-box__icon light-box__icon_show-full"
        });
        c.phrases.showOriginal && c.showFull.setAttribute("title", c.phrases.showOriginal);
        c.save = createElement(e, "a", {
            className: "light-box__save",
            target: "_blank"
        });
        createElement(c.save, "i", {
            className: "light-box__icon light-box__icon_save"
        });
        c.phrases.save && c.save.setAttribute("title", c.phrases.save);
        c.sibling && (p = createElement(f, "div", {
            className: "light-box__prev"
        }),
        createElement(p, "i", {
            className: "light-box__icon light-box__icon_prev"
        }),
        Core.AddHandler(p, "click", c.prev),
        Core.AddHandler(p, "touchstart", c.prev),
        c.phrases.previous && p.setAttribute("title", c.phrases.previous),
        l = createElement(f, "div", {
            className: "light-box__next"
        }),
        createElement(l, "i", {
            className: "light-box__icon light-box__icon_next"
        }),
        Core.AddHandler(l, "click", c.next),
        Core.AddHandler(l, "touchstart", c.next),
        c.phrases.next && l.setAttribute("title", c.phrases.next));
        t = window.pageYOffset || document.documentElement.scrollTop;
        Class(document.body, "light-box-showed");
        document.body.style.top = "-" + t + "px";
        f.style.transition = "none";
        f.style.width = "300px";
        f.style.height = "300px";
        f.style.marginTop = "-150px";
        f.style.marginLeft = "-150px";
        f.style.transition = "all 0.3s ease-out";
        Core.AddHandler(window, "resize", c.setPosition);
        Core.AddHandler(document, "keydown", c.keyPressHandler)
    }
    ;
    this.toogleShowFull = function(a) {
        function b() {
            c.sibling && (c.disabledButton(p, 0 === c.active),
            c.disabledButton(l, c.active === c.count - 1));
            c.phrases.showOriginal && c.showFull.setAttribute("title", c.phrases.showOriginal);
            Class(d, "light-box_full-size", !0);
            Class(c.showFull.firstChild, "light-box__icon_show-full-active", !0);
            document.addEventListener && c.transition && document.removeEventListener(c.transition, b)
        }
        if (a = a || window.event)
            a.preventDefault ? a.preventDefault() : a.returnValue = !1,
            a.cancelBubble = !0,
            a.stopPropagation && a.stopPropagation();
        if (g)
            c.setPosition(),
            document.addEventListener && c.transition ? document.addEventListener(c.transition, b) : b();
        else {
            Class(d, "light-box_full-size");
            Class(c.showFull.firstChild, "light-box__icon_show-full-active");
            c.phrases.collapse && c.showFull.setAttribute("title", c.phrases.collapse);
            c.sibling && (c.disabledButton(p, !0),
            c.disabledButton(l, !0));
            a = m <= c.attachmentSize.width + 40 ? m - 40 : c.attachmentSize.width + 20;
            var e = h <= c.attachmentSize.height + 80 ? h - 80 : c.attachmentSize.height;
            f.style.width = a + "px";
            f.style.marginLeft = "-" + a / 2 + "px";
            f.style.height = e + "px";
            f.style.marginTop = "-" + (e / 2 + 20) + "px"
        }
        g = !g
    }
    ;
    this.disabledButton = function(a, b) {
        Class(a, "light-box__disabled", !b)
    }
    ;
    this.attachmentOnError = function() {
        Core.RemoveHandler(c.attachment, "error", c.attachmentOnError);
        Class(c.attachmentWrapper, "light-box__on-error");
        c.attachmentWrapper.innerHTML = "";
        var a = createElement(c.attachmentWrapper, "div", {
            className: "light-box__error-message"
        });
        a.innerHTML = (c.phrases.errorLoading ? c.phrases.errorLoading : "Unable to download attachment") + "<br>";
        var b = c.phrases.linkToImg ? c.phrases.linkToImg : "Link to the image";
        createElement(a, "a", {
            href: c.src,
            title: b,
            target: "_blank",
            innerText: b
        })
    }
    ;
    this.attachmentOnload = function() {
        document.addEventListener && c.transition && f.removeEventListener(c.transition, b);
        var a = c.attachmentSize.width
          , d = c.attachmentSize.height;
        c.attachmentSize.width = c.attachment.offsetWidth;
        c.attachmentSize.height = c.attachment.offsetHeight;
        c.disabledButton(c.showFull, !(c.attachmentSize.width > m || c.attachmentSize.height > h));
        c.oldAttach && (Core.RemoveHandler(c.oldAttach, "load", c.attachmentOnload),
        Core.RemoveHandler(c.oldAttach, "error", c.attachmentOnError),
        c.oldAttach.parentNode.removeChild(c.oldAttach),
        c.oldAttach = null);
        document.addEventListener && c.transition ? (a !== c.attachmentSize.width && d !== c.attachmentSize.height ? f.addEventListener(c.transition, b) : b(),
        c.setPosition()) : (c.setPosition(),
        b())
    }
    ;
    this.showAttachment = function(a) {
        c.src = a;
        c.oldAttach = c.attachment;
        c.disabledButton(c.showFull, !0);
        c.oldAttach && (Core.RemoveHandler(c.oldAttach, "load", c.attachmentOnload),
        Core.RemoveHandler(c.oldAttach, "error", c.attachmentOnError),
        c.oldAttach.parentNode === document.body && (c.oldAttach.parentNode.removeChild(c.oldAttach),
        c.oldAttach = null),
        c.attachment = null);
        c.attachment = new Image;
        Core.AddHandler(c.attachment, "load", c.attachmentOnload);
        Core.AddHandler(c.attachment, "error", c.attachmentOnError);
        c.attachment.setAttribute("src", c.src + (Core.isIE ? "?" + Math.random() : ""));
        Class(c.attachment, "light-box__attachment-loading");
        document.body.appendChild(c.attachment);
        c.sibling && (c.disabledButton(p, 0 === c.active),
        c.disabledButton(l, c.active === c.count - 1));
        c.save.setAttribute("href", c.src)
    }
    ;
    this.setPosition = function() {
        c.getWinSize();
        var a = c.attachmentSize.width
          , b = c.attachmentSize.height
          , d = a > b;
        if (!a || !b)
            return !1;
        if (d) {
            var e = a / b;
            a > m - 40 && (a = m - 40,
            b = a / e);
            b > h - 80 && (e = b / a,
            b = h - 80,
            a = b / e)
        } else
            e = b / a,
            b > h - 80 && (b = h - 80,
            a = b / e),
            a > m - 40 && (e = a / b,
            a = m - 40,
            b = a / e);
        a < c.minWidth && (a = c.minWidth,
        b = d ? a / e : a * e);
        f.style.width = a + "px";
        f.style.marginLeft = "-" + a / 2 + "px";
        f.style.height = b + "px";
        f.style.marginTop = "-" + (b / 2 + 20) + "px"
    }
    ;
    this.next = function(a) {
        if (a = a || window.event)
            a.preventDefault ? a.preventDefault() : a.returnValue = !1,
            a.cancelBubble = !0,
            a.stopPropagation && a.stopPropagation();
        c.active++;
        c.showAttachment(c.sibling[c.active])
    }
    ;
    this.prev = function(a) {
        if (a = a || window.event)
            a.preventDefault ? a.preventDefault() : a.returnValue = !1,
            a.cancelBubble = !0,
            a.stopPropagation && a.stopPropagation();
        c.active--;
        c.showAttachment(c.sibling[c.active])
    }
    ;
    this.hide = function(a) {
        c.state = 0;
        if (a = a || window.event)
            a.preventDefault ? a.preventDefault() : a.returnValue = !1,
            a.cancelBubble = !0,
            a.stopPropagation && a.stopPropagation();
        document.body.removeChild(d);
        c.attachment && c.attachment.parentNode.removeChild(c.attachment);
        Class(document.body, "light-box-showed", !0);
        window.scrollTo(0, t);
        Core.RemoveHandler(window, "resize", c.setPosition);
        Core.RemoveHandler(document, "keydown", c.keyPressHandler);
        Core.AddHandler(c.attachment, "load", c.attachmentOnload);
        c.attachment = null;
        document.body.style.top = ""
    }
    ;
    this.show = function(a, b) {
        c.state = 1;
        c.sibling = b;
        c.count = c.sibling ? c.sibling.length || 1 : 1;
        1 >= c.count ? c.sibling = null : c.active = c.sibling.indexOf(a);
        c.create();
        c.showAttachment(a)
    }
    ;
    this.getState = function() {
        return c.state
    }
    ;
    c.keyPressHandler = function(a) {
        a = a || window.event;
        var b = a.keyCode || a.which;
        27 === b && c.hide(a);
        39 === b && c.active !== c.count - 1 && c.next(a);
        37 === b && 0 !== c.active && c.prev(a)
    }
}
function lightBoxVideo(a) {
    function b(a) {
        Class(e.playBigBtn.firstChild, "light-box__player-icons_stop-big", a);
        Class(e.playBtn.firstChild, "light-box__player-icons_stop", !a);
        Class(e.attachmentWrapper, "light-box__hide-cursor", !a);
        Class(e.panel, "light-box__controls_active", a)
    }
    function c(a) {
        Class(e.waiting, "light-box__disabled", !a)
    }
    function d(a) {
        a = Math.floor(a);
        var b = parseInt(a / 60, 10)
          , c = a % 60;
        return b + ":" + (10 > a ? "0" + c : c)
    }
    function f() {
        var a = e.attachment.duration;
        if (0 < a)
            for (var b = 0; b < e.attachment.buffered.length; b++)
                if (e.attachment.buffered.start(e.attachment.buffered.length - 1 - b) <= e.attachment.currentTime) {
                    e.timeLineBuffered.style.width = e.attachment.buffered.end(e.attachment.buffered.length - 1 - b) / a * 100 + "%";
                    break
                }
    }
    var e = this;
    e.isMobile = Core.isMobile.any();
    lightBoxImg.apply(this, arguments);
    e.minWidth = e.isMobile ? 300 : 340;
    e.fullScreen = whichFullScreen();
    this.show = function(a, b, c) {
        e.state = 1;
        e.count = 1;
        e.active = 0;
        e.create();
        e.showAttachment(a, b, c);
        e.disabledButton(e.showFull, !0);
        e.disabledButton(e.save, !0)
    }
    ;
    this.attachmentOnError = function() {
        e.attachmentSize = {
            width: 300,
            height: 300
        };
        e.setPosition();
        Core.RemoveHandler(e.source, "error", e.attachmentOnError);
        Class(e.attachmentWrapper, "light-box__on-error");
        e.attachmentWrapper.innerHTML = "";
        var a = createElement(e.attachmentWrapper, "div", {
            className: "light-box__error-message"
        });
        a.innerHTML = (e.phrases.errorLoading ? e.phrases.errorLoading : "Unable to download attachment") + "<br>";
        var b = e.phrases.linkToVideo ? e.phrases.linkToVideo : "Link to the video";
        createElement(a, "a", {
            href: e.src,
            title: b,
            target: "_blank",
            innerText: b
        })
    }
    ;
    this.showAttachment = function(a, b, c) {
        e.src = a;
        e.img = createElement(document.body, "img", {
            className: "light-box__attachment-loading"
        });
        Core.AddHandler(e.img, "load", function() {
            e.attachmentSize = {
                width: e.img.offsetWidth,
                height: e.img.offsetHeight
            };
            e.setPosition();
            e.attachmentWrapper.appendChild(e.img);
            Class(e.img, "light-box__attachment-loading", !0);
            e.loadingSpinner = createElement(e.attachmentWrapper, "div", {
                className: "light-box__status"
            });
            createElement(e.loadingSpinner, "i", {
                className: "light-box__icon-spinner"
            })
        });
        e.img.setAttribute("src", b);
        e.attachment = createElement(document.body, "video", {
            poster: b
        });
        e.source = createElement(e.attachment, "source", {
            type: c + ';codecs="avc1.42E01E, mp4a.40.2"',
            src: a
        });
        a = createElement(e.attachment, "p", {
            innerText: (e.phrases.notSupport ? e.phrases.notSupport : "Your browser doesn't support HTML5 video.") + " "
        });
        b = e.phrases.linkToVideo ? e.phrases.linkToVideo : "Link to the video";
        createElement(a, "a", {
            href: e.src,
            title: b,
            target: "_blank",
            innerText: b
        });
        Core.AddHandler(e.attachment, "canplay", e.attachmentOnload);
        Class(e.attachment, "light-box__attachment-loading");
        Core.AddHandler(e.source, "error", e.attachmentOnError)
    }
    ;
    this.togglePlay = function(a) {
        e.disabledButton(e.playBigBtn, !a);
        e.attachment.paused || e.attachment.ended ? e.attachment.play() : e.attachment.pause()
    }
    ;
    this.toogleFullScreen = function(a) {
        if (a)
            document[e.fullScreen[1]]();
        else
            e.attachmentWrapper[e.fullScreen[0]]()
    }
    ;
    this.setVolume = function(a) {
        e.soundLineRange.value = a;
        e.soundLineValue.style.width = a + "%";
        Class(e.soundBtn.firstChild, "light-box__player-icons_mute", 0 !== parseInt(a))
    }
    ;
    this.setTimeLine = function(a) {
        e.attachment.paused || (e.timeLineRange.value = a);
        e.timeLineValue.style.width = a + "%"
    }
    ;
    this.createControls = function() {
        e.panel = createElement(e.attachmentWrapper, "div", {
            className: "light-box__controls" + (e.fullScreen ? "" : " light-box__controls_no-full-screen") + (e.isMobile ? " light-box__controls_mobile" : "")
        });
        var a = createElement(e.panel, "div", {
            className: "light-box__time"
        })
          , h = createElement(e.panel, "div", {
            className: "light-box__time-line"
        })
          , m = createElement(e.panel, "div", {
            className: "light-box__sound-line"
        })
          , l = !0
          , p = !1
          , t = createElement(e.panel, "a", {
            className: "light-box__download",
            href: e.src,
            target: "_blank"
        });
        e.playBtn = createElement(e.panel, "div", {
            className: "light-box__play"
        });
        e.playBigBtn = createElement(e.attachmentWrapper, "div", {
            className: "light-box__status light-box__status_animated light-box__disabled"
        });
        e.soundBtn = createElement(e.panel, "div", {
            className: "light-box__sound"
        });
        e.waiting = createElement(e.attachmentWrapper, "div", {
            className: "light-box__status light-box__disabled"
        });
        createElement(e.waiting, "i", {
            className: "light-box__icon-spinner"
        });
        if (e.fullScreen) {
            var n = createElement(e.panel, "div", {
                className: "light-box__full-screen"
            });
            var r = createElement(n, "i", {
                className: "light-box__player-icons light-box__player-icons_full-screen"
            });
            Core.AddHandler(n, "click", function() {
                e.toogleFullScreen(p);
                Class(r, "light-box__player-icons_collapsed", p);
                p = !p
            })
        }
        document.addEventListener ? e.playBigBtn.addEventListener("animationend", function() {
            e.disabledButton(e.playBigBtn, !0)
        }) : e.disabledButton(e.playBigBtn, !0);
        createElement(e.playBtn, "i", {
            className: "light-box__player-icons light-box__player-icons_play"
        });
        createElement(e.soundBtn, "i", {
            className: "light-box__player-icons light-box__player-icons_sound"
        });
        createElement(t, "i", {
            className: "light-box__player-icons light-box__player-icons_download"
        });
        createElement(e.playBigBtn, "i", {
            className: "light-box__player-icons light-box__player-icons_play-big"
        });
        e.currentTime = createElement(a, "span", {
            className: "light-box__time__current",
            innerText: "0:00"
        });
        createElement(a, "span", {
            className: "light-box__time__full",
            innerText: " / " + d(e.attachment.duration)
        });
        createElement(m, "div", {
            className: "light-box__sound-line__bg"
        });
        e.soundLineValue = createElement(m, "div", {
            className: "light-box__sound-line__value"
        });
        e.soundLineRange = createElement(m, "input", {
            className: "light-box__time-line__range"
        });
        setAttributes(e.soundLineRange, {
            type: "range",
            min: 0,
            max: 100,
            value: 100,
            step: 1
        });
        Core.AddHandler(e.soundLineRange, "mousemove", function() {
            e.attachment.volume = e.soundLineRange.value / 100
        });
        Core.AddHandler(e.soundLineRange, "change", function() {
            e.attachment.volume = e.soundLineRange.value / 100
        });
        createElement(h, "div", {
            className: "light-box__time-line__bg"
        });
        e.timeLineBuffered = createElement(h, "div", {
            className: "light-box__time-line__buffered"
        });
        e.timeLineValue = createElement(h, "div", {
            className: "light-box__time-line__value"
        });
        e.timeLineRange = createElement(h, "input", {
            className: "light-box__time-line__range"
        });
        setAttributes(e.timeLineRange, {
            type: "range",
            min: 0,
            max: 100,
            value: 0
        });
        f();
        var q = !1;
        Core.AddHandler(e.timeLineRange, "mousemove", function() {
            q && (e.attachment.currentTime = e.timeLineRange.value / 100 * e.attachment.duration)
        });
        Core.AddHandler(e.timeLineRange, "mousedown", function() {
            q = !0
        });
        Core.AddHandler(e.timeLineRange, "mouseup", function() {
            q = !1
        });
        Core.AddHandler(e.timeLineRange, "change", function() {
            e.attachment.currentTime = e.timeLineRange.value / 100 * e.attachment.duration
        });
        Core.AddHandler(e.attachment, "click", function(a) {
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
            a.cancelBubble = !0;
            a.stopPropagation && a.stopPropagation();
            e.togglePlay(!0)
        });
        Core.AddHandler(e.playBtn, "click", function(a) {
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
            a.cancelBubble = !0;
            a.stopPropagation && a.stopPropagation();
            e.togglePlay(!1)
        });
        Core.AddHandler(e.soundBtn, "click", function(a) {
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
            a.cancelBubble = !0;
            a.stopPropagation && a.stopPropagation();
            e.attachment.muted ? (e.attachment.muted = !1,
            e.attachment.volume = 1) : (e.attachment.muted = !0,
            e.attachment.volume = 0)
        });
        Core.AddHandler(e.attachment, "touchend", function(a) {
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
            a.cancelBubble = !0;
            a.stopPropagation && a.stopPropagation();
            e.togglePlay(!0)
        });
        Core.AddHandler(e.playBtn, "touchend", function(a) {
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
            a.cancelBubble = !0;
            a.stopPropagation && a.stopPropagation();
            e.togglePlay(!1)
        });
        Core.AddHandler(e.soundBtn, "touchend", function(a) {
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
            a.cancelBubble = !0;
            a.stopPropagation && a.stopPropagation();
            e.attachment.muted ? (e.attachment.muted = !1,
            e.attachment.volume = 1) : (e.attachment.muted = !0,
            e.attachment.volume = 0)
        });
        Core.AddHandler(e.attachment, "play", function() {
            b(!0);
            l = !1
        });
        Core.AddHandler(e.attachment, "ended", function() {
            b(!1);
            e.timeLineRange.value = e.attachment.currentTime / e.attachment.duration * 100;
            l = !0
        });
        Core.AddHandler(e.attachment, "pause", function() {
            if (!e.attachment)
                return !1;
            b(!1);
            e.timeLineRange.value = e.attachment.currentTime / e.attachment.duration * 100;
            l = !0
        });
        Core.AddHandler(e.attachment, "timeupdate", function() {
            c(!0);
            e.attachment && (e.currentTime.innerText = d(e.attachment.currentTime),
            e.setTimeLine(e.attachment.currentTime / e.attachment.duration * 100))
        });
        Core.AddHandler(e.attachment, "volumechange", function() {
            e.setVolume(100 * e.attachment.volume)
        });
        Core.AddHandler(e.attachment, "progress", function() {
            e.attachment && f()
        });
        Core.AddHandler(e.attachment, "waiting", function() {
            c(!1)
        });
        Class(e.panel, "light-box__controls_active");
        if (!e.isMobile) {
            var u = !0;
            Core.AddHandler(e.attachmentWrapper, "mousemove", function(a) {
                clearTimeout(u);
                Class(e.panel, "light-box__controls_active");
                Class(e.attachmentWrapper, "light-box__hide-cursor", !0);
                u = setTimeout(function() {
                    if (!l) {
                        for (var b = a.target; b && b.parentNode && b !== e.panel && b !== e.attachmentWrapper; )
                            b = b.parentNode;
                        Class(e.panel, "light-box__controls_active", b !== e.panel);
                        Class(e.attachmentWrapper, "light-box__hide-cursor", b === e.panel)
                    }
                }, 1E3)
            });
            Core.AddHandler(e.attachmentWrapper, "mouseout", function() {
                clearTimeout(u);
                l || (Class(e.panel, "light-box__controls_active", !0),
                Class(e.attachmentWrapper, "light-box__hide-cursor"))
            })
        }
    }
    ;
    this.attachmentOnload = function() {
        e.attachmentWrapper.innerHTML = "";
        Class(e.attachment, "light-box__attachment-loading", !0);
        e.createControls();
        Core.RemoveHandler(e.attachment, "canplay", e.attachmentOnload);
        e.attachmentSize = {
            width: e.attachment.videoWidth,
            height: e.attachment.videoHeight
        };
        e.setPosition();
        e.img && e.img.parentNode && e.img.parentNode.removeChild(e.img);
        e.attachmentWrapper.appendChild(e.attachment)
    }
    ;
    e.keyPressHandler = function(a) {
        a = a || window.event;
        var b = a.keyCode || a.which;
        27 === b && e.hide(a);
        32 === b && e.togglePlay()
    }
}
function checkImgForLightbox(a, b, c) {
    if (!a)
        return !1;
    (function(a, b, c) {
        function d(a) {
            function d(a, b) {
                if (!b)
                    return !1;
                b = b.split(",");
                for (var c = 0, d = b.length; c < d; c++)
                    if (0 <= a.className.split(" ").indexOf(b[c]))
                        return !0;
                return !1
            }
            function f(a, b) {
                for (; a && a.parentNode && !d(a, b); )
                    a = a.parentNode;
                return a
            }
            a = a || window.event;
            var g = a.target || a.srcElement;
            if ("img" === g.tagName.toLowerCase()) {
                var l = g.parentNode;
                if ("a" === l.tagName.toLowerCase() && (0 <= l.className.indexOf("lightbox__link") || "_blank" === l.getAttribute("target")) && 0 > l.parentNode.className.indexOf("ff__c")) {
                    a.preventDefault ? a.preventDefault() : a.returnValue = !1;
                    a.cancelBubble = !0;
                    a.stopPropagation && a.stopPropagation();
                    var h = f(l, b);
                    var m = [];
                    if (document.querySelectorAll) {
                        h = h.querySelectorAll("a");
                        for (var p = 0, C = h.length; p < C; p++)
                            if (h[p].firstChild) {
                                var A = h[p].firstChild.tagName;
                                A && "img" === A.toLowerCase() && m.push(h[p].getAttribute("href"))
                            }
                    }
                    e.show(l.getAttribute("href"), arrayDeleteDuplicates(m))
                }
            }
            if ("a" === g.tagName.toLowerCase() && (l = g.getAttribute("href")) && /.(png|jpeg|jpg|gif)$/i.test(l)) {
                a.preventDefault ? a.preventDefault() : a.returnValue = !1;
                a.cancelBubble = !0;
                a.stopPropagation && a.stopPropagation();
                m = [];
                if ((a = f(g, c)) && document.querySelectorAll)
                    for (h = a.parentNode.querySelectorAll("a"),
                    p = 0,
                    C = h.length; p < C; p++)
                        a = h[p].getAttribute("href"),
                        /.(png|jpeg|jpg|gif)$/i.test(a) && m.push(a);
                e.show(l, arrayDeleteDuplicates(m))
            }
        }
        var e = new lightBoxImg(window.lightBoxPhrases || null), f, l;
        Core.AddHandler(a, "click", d);
        Core.AddHandler(a, "touchstart", function(a) {
            a = a || window.event;
            if (a = a.touches[0])
                f = a.clientX,
                l = a.clientY
        }, {
            passive: !0
        });
        Core.AddHandler(a, "touchend", function(a) {
            a = a || window.event;
            var b = a.changedTouches[0];
            if (b) {
                var c = Math.abs(b.clientY - l);
                5 > Math.abs(b.clientX - f) && 5 > c && d(a)
            }
            l = f = 0
        })
    }
    )(a, b, c)
}
function ScrollContentAnimated() {
    function a(a) {
        if (!a)
            return !1;
        for (; a.parentNode && a !== v && (0 === a.offsetHeight || a.parentNode !== v); )
            a = a.parentNode;
        e(g(-a.offsetTop + C - a.offsetHeight))
    }
    function b() {
        var a = q.getBoundingClientRect()
          , b = a.left + a.width / 2
          , c = 0;
        a = a.bottom - 20;
        for (var d = document.elementFromPoint(b, a); d === v && 20 > c; )
            a -= 20,
            c++,
            d = document.elementFromPoint(b, a);
        if (d === v || d === q)
            d = !1;
        return d
    }
    function c() {
        var a = C
          , b = A;
        A = v.offsetHeight;
        C = F();
        q.style.height = C + "px";
        E = A - C;
        var c = parseInt(C / A * C);
        c > C ? (H = !0,
        Class(w, "scroll-container-animated__disabled")) : (H = !1,
        Class(w, "scroll-container-animated__disabled", !0));
        w.style.height = c + "px";
        z = (C - C / A * C) / E;
        return a !== C || A !== b
    }
    function d() {
        c() && a(J)
    }
    function f(a, b) {
        r ? a.style[r] = "msTransform" === r ? "translateY(" + b + "px)" : "translate3d(0," + b + "px,0)" : a.style.top = b + "px"
    }
    function e(a) {
        D = a;
        f(v, a);
        f(w, -a * z);
        J = b()
    }
    function g(a) {
        0 < a ? a = 0 : a <= -E && (a = -E);
        return a
    }
    function h(a) {
        var b = a;
        if (G) {
            var d = A;
            0 < b && D <= -E + C ? (G(1),
            c()) : 0 > b && 0 <= D + C && (G(-1),
            c(),
            e(D - (A - d)))
        }
        a = g(D - a);
        e(a)
    }
    function m() {
        function a(a) {
            a = a || window.event;
            if (H)
                return !1;
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
            var b = a.deltaY || a.detail || a.wheelDelta;
            100 > Math.abs(b) && (b = 100 * (0 < b ? 1 : -1));
            a.deltaY || a.detail || !a.wheelDelta || (b = -b);
            h(b)
        }
        "onwheel"in document ? Core.AddHandler(q, "wheel", a) : "onmousewheel"in document ? Core.AddHandler(q, "mousewheel", a) : Core.AddHandler(q, "MozMousePixelScroll", a)
    }
    function l(a, b) {
        a = a || window.event;
        if ((a.target || a.srcElement) !== w) {
            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
            a.stopPropagation && a.stopPropagation();
            var c = C - 20;
            a = b(a) < -D * z ? -1 : 1;
            20 > c && (c = 20);
            h(c * a)
        }
    }
    function p() {
        function a(a) {
            if (!c)
                return !1;
            a = a || window.event;
            a = a.pageY || a.clientY + document.body.scrollTop;
            var b = (a - d) / z;
            d = a;
            h(b)
        }
        function b() {
            if (!c)
                return !1;
            c = !1;
            Core.RemoveHandler(document, "mousemove", a);
            Core.RemoveHandler(document, "mouseup", b)
        }
        var c = !1
          , d = 0;
        w.ondragstart = function() {
            return !1
        }
        ;
        Core.AddHandler(w, "mousedown", function(e) {
            e = e || window.event;
            if (H)
                return !1;
            e.preventDefault ? e.preventDefault() : e.returnValue = !1;
            e.stopPropagation && e.stopPropagation();
            var f = e;
            !f.which && f.button && f.button & 1 && (f.which = 1);
            f = 1 !== f.which ? !1 : (f = f.target || f.srcElement) && f === w ? !0 : !1;
            f && (c = !0,
            d = e.pageY || e.clientY + document.body.scrollTop,
            Core.AddHandler(document, "mousemove", a),
            Core.AddHandler(document, "mouseup", b));
            return !1
        });
        Core.AddHandler(u, "click", function(a) {
            if (H)
                return !1;
            l(a, function(a) {
                return a.offsetY
            })
        })
    }
    function t() {
        function a() {
            var b = 25 * e;
            if (1 < b || -1 > b)
                e *= .95,
                h(b),
                m = f(a)
        }
        var b = !1, c = 0, d = 0, e = 0, f = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame, g = window.cancelAnimationFrame || window.mozCancelAnimationFrame, m = null, n;
        Core.AddHandler(q, "touchstart", function(a) {
            if (1 == a.touches.length) {
                if (H)
                    return b = !1;
                b = !0;
                d = c = a.changedTouches[0].pageY;
                n = Date.now();
                g(m)
            } else
                b = !1
        });
        Core.AddHandler(q, "touchmove", function(a) {
            if (1 == a.touches.length && b) {
                var d = a.changedTouches[0].pageY
                  , e = c - d;
                a.preventDefault();
                h(e);
                c = d
            }
        });
        Core.AddHandler(q, "touchend", function(c) {
            b && (c.preventDefault(),
            b = !1,
            e = (d - c.changedTouches[0].pageY) / (Date.now() - n),
            f && .5 < Math.abs(e) && (m = f(a)))
        });
        Core.AddHandler(u, "touchend", function(a) {
            if (H)
                return !1;
            l(a, function(a) {
                return a.changedTouches[0].pageY
            })
        });
        var p = !1
          , t = 0;
        Core.AddHandler(w, "touchstart", function(a) {
            if (1 == a.touches.length) {
                if (H)
                    return p = !1;
                a.preventDefault();
                p = !0;
                t = a.changedTouches[0].pageY
            } else
                p = !1
        });
        Core.AddHandler(w, "touchmove", function(a) {
            if (1 == a.touches.length && p) {
                var b = a.changedTouches[0].pageY
                  , c = -(t - b) / z;
                a.preventDefault();
                h(c);
                t = b
            }
        });
        Core.AddHandler(w, "touchend", function() {
            p = !1
        })
    }
    function n() {
        d()
    }
    var r = whichTransform(), q, u, w, v, C = 0, A = 0, D = 0, z = 0, E = 0, F, H = !1, J, G;
    return {
        init: function(a, d, e) {
            u = document.createElement("div");
            q = document.createElement("div");
            w = document.createElement("div");
            v = a;
            Class(v, "scroll-container-animated__content");
            Class(q, "scroll-container-animated");
            q.style.height = 0;
            Class(u, "scroll-container-animated__bar");
            Class(w, "scroll-container-animated__slider");
            v.parentNode.appendChild(q);
            q.appendChild(v);
            u.appendChild(w);
            q.appendChild(u);
            F = d || function() {
                return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
            }
            ;
            G = e;
            c();
            m();
            p();
            t();
            Core.AddHandler(window, "resize", n);
            J = b()
        },
        updateHeight: d,
        focusTo: a
    }
}
function ScrollContent() {
    function a() {
        if (!l.preloading || l.checkPreloadingStart)
            return !1;
        if (0 === l.currentScroll) {
            var a = l.scrollHeight;
            l.checkPreloadingStart = !0;
            l.preloading(-1, function() {
                l.stopPreloading = !0;
                l.slider && l.slider.stop();
                c();
                e(l.scrollHeight - a)
            })
        } else
            l.currentScroll + l.contentHeight === l.scrollHeight && (l.checkPreloadingStart = !0,
            l.preloading(1, function() {
                l.slider && l.slider.stop();
                c()
            }));
        l.checkPreloadingStart = !1
    }
    function b() {
        c();
        a();
        l.callback && l.callback()
    }
    function c() {
        l.currentScroll = l.content.scrollTop;
        l.scrollHeight = l.content.scrollHeight;
        l.contentHeight = l.content.offsetHeight;
        Class(l.scrollSlider, l.prefix + "__slider_hidden", l.scrollHeight !== l.contentHeight);
        l.scrollSlider.style.height = d(l.contentHeight, l.scrollHeight) + "px";
        var a = l.scrollSlider
          , b = parseInt(l.contentHeight / l.scrollHeight * l.currentScroll);
        p ? a.style[p] = "msTransform" === p ? "translateY(" + b + "px)" : "translate3d(0," + b + "px,0)" : a.style.top = b + "px"
    }
    function d(a, b) {
        this.hash || (this.hash = {});
        this.hash["h" + a + "x" + b] || (this.hash["h" + a + "x" + b] = parseInt(a / b * a));
        return this.hash["h" + a + "x" + b]
    }
    function f(a, d) {
        a ? (c(),
        Core.AddHandler(l.content, "scroll", b, {
            passive: !0
        }),
        d && d()) : Core.RemoveHandler(l.content, "scroll", b)
    }
    function e(a, b) {
        if (!a)
            return !1;
        f(!1);
        if ("object" === typeof a) {
            if (!a.element)
                return !1;
            l.content.scrollTop = a.element.offsetTop + (a.start ? 0 : a.element.offsetHeight)
        } else if ("string" === typeof a)
            switch (a) {
            case "top":
                l.content.scrollTopp = 0;
                break;
            case "bottom":
                l.content.scrollTop = l.scrollHeight;
                break;
            default:
                l.content.scrollTop = 0
            }
        else
            l.content.scrollTop = a;
        f(!0, b)
    }
    function g(a, b) {
        a = a || window.event;
        (a.target || a.srcElement) !== l.scrollSlider && (a = b(a),
        20 > a && (a = 20),
        l.content.scrollTop = l.scrollHeight * a / l.contentHeight)
    }
    function h() {
        function a(a) {
            if (!c)
                return !1;
            a = a || window.event;
            l.content.scrollTop = e + l.scrollHeight / l.contentHeight * ((a.pageY || a.clientY + document.body.scrollTop) - d)
        }
        function b() {
            if (!c)
                return !1;
            c = !1;
            Core.RemoveHandler(document, "mousemove", a);
            Core.RemoveHandler(document, "mouseup", b)
        }
        var c = !1
          , d = 0
          , e = 0;
        l.scrollSlider.ondragstart = function() {
            return !1
        }
        ;
        Core.AddHandler(l.scrollSlider, "mousedown", function(f) {
            f = f || window.event;
            if (l.scrollDisabled)
                return !1;
            f.preventDefault ? f.preventDefault() : f.returnValue = !1;
            f.stopPropagation && f.stopPropagation();
            var g = f;
            !g.which && g.button && g.button & 1 && (g.which = 1);
            g = 1 !== g.which ? !1 : (g = g.target || g.srcElement) && g === l.scrollSlider ? !0 : !1;
            g && (c = !0,
            d = f.pageY || f.clientY + document.body.scrollTop,
            e = l.currentScroll,
            Core.AddHandler(document, "mousemove", a),
            Core.AddHandler(document, "mouseup", b));
            return !1
        });
        Core.AddHandler(l.scrollContainer, "mousedown", function(a) {
            if (l.scrollDisabled || c)
                return !1;
            g(a, function(a) {
                return a.offsetY
            })
        });
        return {
            stop: b
        }
    }
    function m() {
        l.content.style.marginRight = "-" + (l.content.offsetWidth - l.content.clientWidth) + "px";
        c()
    }
    var l = this
      , p = whichTransform();
    this.prefix = "scroll-container";
    this.scrollHeight = this.scrollSlider = this.scrollContainer = this.container = null;
    this.contentHeight = 0;
    this.content = this.currentScroll = null;
    this.checkPreloadingStart = !1;
    this.preloading = null;
    this.scrollDisabled = !1;
    this.callback = null;
    this.slider = void 0;
    return {
        init: function(a) {
            if (!a)
                return !1;
            l.container = document.createElement("div");
            Class(l.container, l.prefix);
            l.content = a;
            Class(l.content, l.prefix + "__content");
            l.content.setAttribute("tabindex", -1);
            l.scrollContainer = document.createElement("div");
            l.scrollSlider = document.createElement("div");
            Class(l.scrollContainer, l.prefix + "__bar");
            Class(l.scrollSlider, l.prefix + "__slider");
            l.scrollContainer.appendChild(l.scrollSlider);
            l.container.appendChild(l.scrollContainer);
            l.content.parentNode.appendChild(l.container);
            l.container.appendChild(l.content);
            l.content.style.marginRight = "-" + (l.content.offsetWidth - l.content.clientWidth) + "px";
            window.mqGlobal && window.mqGlobal.AddOnLoad(function() {
                m()
            });
            f(!0);
            l.slider = h();
            Core.AddHandler(window, "resize", m)
        },
        setPreloading: function(a) {
            l.preloading = a
        },
        update: m,
        focusTo: e,
        updateCoord: c,
        checkScroll: f,
        getScrollPosition: function() {
            return l.currentScroll
        },
        getContentHeight: function() {
            return l.contentHeight
        },
        getScrollHeight: function() {
            return l.scrollHeight
        },
        setCallback: function(a) {
            l.callback = a
        }
    }
}
function FloatVerticalPanel(a, b) {
    function c() {
        Mql5Cookie.httpStorage(b, "1", {
            expires: new Date(2050,1,1),
            Domain: "." + Mql5Cookie.domain,
            Path: "/"
        });
        d.parentNode.removeChild(d)
    }
    if ("1" === Mql5Cookie.httpStorage(b))
        return !1;
    var d = createElement(document.body, "div", {
        className: "float-vertical-panel",
        innerHTML: a,
        id: "floatVerticalPanel"
    });
    d.innerHTML = a;
    a = createElement(d, "span", {
        className: "float-vertical-panel__cross"
    });
    Core.AddHandler(a, "click", c);
    Core.AddHandler(a, "touchend", c)
}
function CountableField(a, b) {
    function c() {
        var c = a.value
          , f = c.length;
        return b ? (f > b && (Class(a, "input-validation-error"),
        setTimeout(function() {
            Class(a, "input-validation-error", 300)
        }, 300)),
        c = c.substring(0, b),
        f = c.length,
        a.value = c,
        f + "/" + b) : f
    }
    if (!a)
        return !1;
    (function() {
        var d = createElement(a.parentNode, "div", {
            className: "countable-field"
        })
          , f = createElement(d, "div", {
            className: "countable-field__counter",
            innerText: "0/" + b
        });
        a.parentNode.insertBefore(d, a);
        d.appendChild(a);
        Core.AddHandler(a, "keyup", function() {
            f.innerText = c()
        });
        Core.AddHandler(a, "change", function() {
            f.innerText = c()
        });
        Core.AddHandler(a, "input", function() {
            f.innerText = c()
        })
    }
    )()
}
function shareBlock() {
    var a = $("shareBlock")
      , b = $("formShare");
    Core.AddHandler(a, "click", function(c) {
        c = c || window.event;
        for (var d = c.target || c.srcElement; d.parentNode && "A" !== d.nodeName && d !== a; )
            d = d.parentNode;
        if ("A" === d.nodeName) {
            var f = d.getAttribute("data-social");
            (d = d.getAttribute("data-track")) && window.fpush(d);
            "Mql5.community" === f && (c.preventDefault ? c.preventDefault() : c.returnValue = !1,
            Dialog.show(b.title, b, 500, 160, Dialog.hide, null, null, " ui no-bottom-margin", !1, "fixed"))
        }
    });
    b && Core.AddHandler(b, "submit", function(b) {
        Ajax.form(this, {
            onready: function(b) {
                b = JSON.parse(b);
                if (!b.Type || "REDIRECT" !== b.Type) {
                    if (b.Type && "SUCCESS" === b.Type) {
                        var c = createElement(null, "DIV", {
                            className: "share-mql5-com__res"
                        });
                        c.innerHTML = b.Message;
                        Dialog.show(b.Title || b.Message || "", c, 500, 100, Dialog.hide, null, null, " ui no-bottom-margin", !1, "fixed");
                        c = a.querySelector(".soc-icons_mql5");
                        c = getAncessor(c, "soc-share-block__item");
                        c.parentNode.removeChild(c)
                    }
                    b.Type && "ERROR" === b.Type && (c = createElement(null, "DIV", {
                        className: "share-mql5-com__res"
                    }),
                    c.innerHTML = b.Message,
                    Dialog.show(b.Message, c, 500, 100, Dialog.hide, null, null, " ui no-bottom-margin", !1, "fixed"))
                }
            },
            onerror: function() {},
            ismobile: !1
        });
        b = b || window.event;
        b.preventDefault ? b.preventDefault() : b.returnValue = !1
    })
}
function Resizable(a, b) {
    function c(l) {
        if (!e)
            return !1;
        l = l || window.event;
        l = l.pageY || (l.changedTouches ? l.changedTouches[0].pageY : null) || l.clientY + document.body.scrollTop;
        Core.RemoveHandler(document, "mouseup", c);
        Core.RemoveHandler(document, "mousemove", d);
        Core.RemoveHandler(document, "touchend", c);
        Core.RemoveHandler(document, "touchmove", d);
        f.style.transform ? f.style.transform = "none" : f.style.top = "auto";
        g -= m - l;
        a.style.height = g + "px";
        b && b(g);
        e = !1;
        Class(h, "resizable-wrapper_active", !0)
    }
    function d(a) {
        if (!e)
            return !1;
        a = a || window.event;
        a = a.pageY || (a.changedTouches ? a.changedTouches[0].pageY : null) || a.clientY + document.body.scrollTop;
        f.style.transform ? f.style.transform = "translateY(" + -(m - a) + "px)" : f.style.top = g - (m - a) + "px"
    }
    var f = createElement(a, "div", {
        className: "resizable-line"
    })
      , e = !1
      , g = a.offsetHeight
      , h = createElement(a, "div", {
        className: "resizable-wrapper"
    })
      , m = 0;
    Class(a, "resizable");
    Core.AddHandler(f, "mousedown", function(a) {
        !a.which && a.button && a.button & 1 && (a.which = 1);
        if (1 !== a.which)
            return !1;
        e = !0;
        a = a || window.event;
        m = a.pageY || a.clientY + document.body.scrollTop;
        Core.AddHandler(document, "mouseup", c);
        Core.AddHandler(document, "mousemove", d);
        Class(h, "resizable-wrapper_active");
        a.preventDefault ? a.preventDefault() : a.returnValue = !1;
        a.stopPropagation && a.stopPropagation();
        return !1
    });
    Core.AddHandler(f, "dragstart", function() {
        return !1
    });
    Core.AddHandler(f, "touchstart", function(a) {
        e = !0;
        m = a.changedTouches[0].pageY;
        Core.AddHandler(document, "touchend", c);
        Core.AddHandler(document, "touchmove", d);
        Class(h, "resizable-wrapper_active");
        a.preventDefault ? a.preventDefault() : a.returnValue = !1;
        a.stopPropagation && a.stopPropagation();
        return !1
    })
}
function Tooltip(a) {
    if (!a)
        return !1;
    var b = a.getAttribute("data-tooltip");
    if (!b)
        return !1;
    var c = this;
    this.setPosition = function() {
        var b = a.getBoundingClientRect()
          , f = getWindowSize();
        b.bottom < f.height ? (Class(c.tooltip, "tooltip-wrapper_bottom"),
        c.tooltip.style.top = b.bottom + "px",
        c.tooltip.style.bottom = "auto") : (Class(c.tooltip, "tooltip-wrapper_bottom", !0),
        c.tooltip.style.top = "auto",
        c.tooltip.style.bottom = f.height - b.top + "px");
        f.width - b.right > c.textWidth - 16 || .7 < (f.width - b.right) / c.textWidth ? (c.tooltip.style.left = b.left + "px",
        c.tooltip.style.maxWidth = f.width - b.right - 8 + "px") : f.width - b.left > c.textWidth - 16 ? c.tooltip.style.right = b.right + 8 + "px" : (c.tooltip.style.left = "8px",
        c.tooltip.style.maxWidth = f.width - 16 + "px");
        c.corner.style.left = b.left - c.tooltip.getBoundingClientRect().left + 2 + "px"
    }
    ;
    this.show = function() {
        Class(c.tooltip, "tooltip-wrapper_active");
        c.setPosition();
        Core.AddHandler(window, "scroll", c.setPosition, {
            passive: !0
        });
        Core.AddHandler(document.body, "click", c.checkContent);
        Core.AddHandler(document.body, "touchstart", c.checkContent);
        Core.AddHandler(window, "resize", c.setPosition)
    }
    ;
    this.checkContent = function(b) {
        b = b || window.event;
        for (b = b.target || b.srcElement; b.parentNode && b !== a && b !== c.tooltip; )
            b = b.parentNode;
        b !== a && b !== c.tooltip && c.hide()
    }
    ;
    this.hide = function() {
        Core.RemoveHandler(window, "scroll", c.setPosition);
        Core.RemoveHandler(document.body, "click", c.checkContent);
        Core.RemoveHandler(window, "resize", c.setPosition);
        Core.RemoveHandler(document.body, "touchstart", c.checkContent);
        Class(c.tooltip, "tooltip-wrapper_active", !0)
    }
    ;
    this.create = function() {
        c.tooltip = createElement(a, "span", {
            className: "tooltip-wrapper tooltip-wrapper_corner"
        });
        c.tooltip.style.position = "fixed";
        var d = createElement(c.tooltip, "span", {
            className: "tooltip",
            innerText: b
        });
        c.corner = createElement(d, "span", {
            className: "corner"
        });
        a.style.position = "relative";
        d = createElement(document.body, "div", {
            innerText: b
        });
        d.style.position = "fixed";
        d.style.left = "-9999px";
        var f = d.clientWidth;
        document.body.removeChild(d);
        c.textWidth = f;
        Core.AddHandler(a, "click", c.show);
        Core.AddHandler(a, "touchstart", c.show)
    }
    ;
    c.create()
}
function TrackScrollPosition(a, b, c) {
    this.create = function() {
        function d() {
            m = getWindowScroll().top + h;
            l && p ? (Core.RemoveHandler(window, "scroll", d),
            Core.RemoveHandler(window, "resize", f)) : (!l && m - h / 2 >= g / 2 && (l = !0,
            c || window.fpush("MQL5+" + a + "+Half")),
            !p && m >= g && (p = !0,
            window.fpush("MQL5+" + a + "+End")))
        }
        function f() {
            e = $("footer").offsetHeight;
            g = getDocumentBodySize().height - e;
            h = getWindowSize().height;
            d()
        }
        b || window.fpush("MQL5+" + a + "+Open");
        var e = $("footer").offsetHeight
          , g = getDocumentBodySize().height - e
          , h = getWindowSize().height
          , m = getWindowScroll().top + h
          , l = !1
          , p = !1;
        Core.AddHandler(window, "scroll", d, {
            passive: !0
        });
        Core.AddHandler(window, "resize", f, {
            passive: !0
        });
        d()
    }
    ;
    this.create()
}
function highlightAttenDeposit() {
    var a = document.getElementById("payBlockTable")
      , b = document.getElementById("accProblemsWrapper");
    a && b && (a.addEventListener("mouseover", function() {
        b.className += " acc-problems-wrapper_active"
    }),
    a.addEventListener("mouseleave", function() {
        b.className = "acc-problems-wrapper"
    }))
}
;(function() {
    function a(a) {
        for (a = a.previousSibling; a && 1 != a.nodeType; )
            a = a.previousSibling;
        return a
    }
    function b(a) {
        function b(a, b, d) {
            for (var e = 0, f = 0; f < d; ++f) {
                var g = c.charAt(f)
                  , h = a[f];
                g && (g = parseInt(g));
                e += g * h
            }
            if (0 == e)
                return !1;
            e %= 11;
            9 < e && (e %= 10);
            return e == b
        }
        var c = a.value;
        return 10 == c.length ? b([2, 4, 10, 3, 5, 9, 4, 6, 8], parseInt(c.charAt(c.length - 1)), 9) : 12 == c.length ? b([7, 2, 4, 10, 3, 5, 9, 4, 6, 8], parseInt(c.charAt(c.length - 2)), 10) && b([3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8], parseInt(c.charAt(c.length - 1)), 11) ? !0 : !1 : !1
    }
    function c(b, c) {
        var d = a(b);
        if (g)
            return g.style.display = "block",
            !0;
        d && "field-validation-error" == d.className && "SPAN" == d.nodeName ? d.innerHTML = c : (d = document.createElement("span"),
        d.innerHTML = c,
        d.className = "field-validation-error",
        b.parentNode.insertBefore(d, b),
        b.parentNode.insertBefore(document.createTextNode("\n"), d))
    }
    function d(b) {
        (b = a(b)) && "field-validation-error" == b.className && b.parentNode.removeChild(b)
    }
    function f(a, b, d) {
        MQTE.SetValidationClass(b.id, !0);
        if (a)
            if ("string" == typeof d)
                c(a, d);
            else {
                b = 0;
                for (var e = d.length; b < e; ++b)
                    c(a, d[b])
            }
    }
    var e = 0, g;
    window.preventDoubleSubmit = function(a) {
        void 0 !== document.getElementById("Login") && (window.mediaToken || (window.mediaToken = new MediaToken),
        window.mediaToken.Set());
        a && (a.className += " disabled",
        a.onsubmit = function() {
            return !1
        }
        )
    }
    ;
    window.Validate = function(a) {
        document.getElementById("validate_message") && (g = document.getElementById("validate_message"),
        g.style.display = "none");
        if (!window.V)
            return window.console ? console.warn("Задействована форма с валидацией, но нет ни одного правила") : alert("Задействована форма с валидацией, но нет ни одного правила"),
            !1;
        var c = !0, l, h = {};
        ++e;
        for (var t = 0, n = V.length; t < n; ++t) {
            var r = !0;
            if (!a)
                return !1;
            var q = V[t];
            if ((l = a.elements[q[0]]) && (l.id === q[0] || 16 === q[1]) && (!l.style || "none" != l.style.display || window.MQRTE && MQRTE.Editor(q[0]) || window.SmallRTE)) {
                var u = document.getElementById("validate_" + q[0]);
                if ("text" == l.type || "TEXTAREA" == l.nodeName)
                    l.value = l.value.replace(/(^[ \r\n\t]+|[ \r\n\t]+$)/g, "");
                switch (q[1]) {
                case 1:
                    r = !MQTE.IsEmpty(l.name);
                    break;
                case 2:
                    if (null !== q[3]) {
                        var w = parseInt(q[3]);
                        var v;
                        q[4] && (v = q[4]);
                        r = l.value.length >= w && (!v || l.value.length <= v)
                    }
                    break;
                case 3:
                    w = void 0;
                    r = (w = l.value) && /^[0-9]+(\.|,)?[0-9]*$/.test(w) ? !0 : !1;
                    break;
                case 4:
                    w = void 0;
                    r = (w = l.value) && (!/^\w+([-+.'']{1,2}\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/i.test(w) || 255 <= w.length) ? !1 : !0;
                    break;
                case 5:
                    r = l.value;
                    w = a.elements[q[3]].value;
                    q[4] && (r = r.toLowerCase(),
                    w = w.toLowerCase());
                    r = r == w;
                    r || (h[q[3]] && (w = document.getElementById("validate_" + q[3]),
                    d(w),
                    h[q[3]] = !1),
                    f(null, a.elements[q[3]], null));
                    break;
                case 6:
                    r = (new RegExp(q[3],q[4])).test(l.value);
                    break;
                case 7:
                    r = b(l);
                    break;
                case 8:
                    r = l.value ? /^((http|https):\/\/)?([a-z0-9\-]+\.)+[a-z]+(:\d+)?(\/[\w-_ .\/%]*)?(\/)?\??([\w=%&_ :+;\-\.\#/\(\)]+)?$/i.test(l.value) : !0;
                    break;
                case 9:
                    a: {
                        r = l;
                        i = 0;
                        for (sz = r.length; i < sz; i++)
                            if (r[i].checked) {
                                r = !0;
                                break a
                            }
                        r = !1
                    }
                    break;
                case 10:
                    try {
                        r = q[3](l)
                    } catch (F) {
                        window.console && console.error(F.message)
                    }
                    break;
                case 12:
                    r = l.value ? /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/i.test(l.value) : !0;
                    break;
                case 13:
                    l.value ? (r = !1,
                    window.XMLHttpRequest ? r = new XMLHttpRequest : window.ActiveXObject && (r = new ActiveXObject("Microsoft.XMLHTTP")),
                    r ? (r.open("get", "/api/website/status?r=" + Math.floor(1E4 * Math.random() + 1), !1),
                    r.timeout && (r.timeout = 2E3),
                    r.send(),
                    r = 4 == r.readyState && 200 == r.status && 0 <= r.responseText.indexOf("STATUS_OK")) : r = !0) : r = !0;
                    break;
                case 14:
                    r = l;
                    if (r = parseInt(r.value, 10) == r.value) {
                        r = parseInt(l.value);
                        w = parseInt(q[3]);
                        var C = parseInt(q[4]);
                        r = r <= C && r >= w
                    }
                    break;
                case 16:
                    if (null !== q[3] && (w = parseInt(q[3]),
                    C = l.length && 0 !== l.length ? l : [l],
                    window.FileReader))
                        for (var A = 0; A < C.length; A++) {
                            var D = C[A];
                            if (D.files && D.files.length)
                                for (var z = 0; z < D.files.length; z++)
                                    D.files[z].size > w && (q[2] && (q[2] = q[2].replace("{filename}", D.files[z].name).replace("{filesize}", bytesToSize(D.files[z].size))),
                                    r = !1)
                        }
                    break;
                case 17:
                    w = MQRTE.GetContent(l.name),
                    r = document.createElement("div"),
                    r.innerHTML = w,
                    (w = r.querySelector(".fquote")) && w.parentNode.removeChild(w),
                    r = r.innerText,
                    r = r.replace(/[ \n\r]/g, ""),
                    r = 0 < r.length
                }
                r || l.iteration == e ? !1 !== h[q[0]] && (h[q[0]] = !0) : (f(u, l, q[2]),
                c = !1,
                h[q[0]] = !1,
                l.iteration = e)
            }
        }
        if (c)
            window.form_is_valid && form_is_valid(a);
        else {
            for (var E in h)
                h[E] && (u = document.getElementById("validate_" + E),
                q = a.elements[E],
                d(u),
                MQTE.SetValidationClass(q.id, !1));
            window.form_is_not_valid && form_is_not_valid(a)
        }
        return c
    }
    ;
    window.Validate.AppendMessage = c;
    window.Validate.ClearMessage = d;
    window.Validate.SetValid = function(a, b) {
        var c, e;
        var g = 0;
        for (c = a.elements.length; g < c; ++g) {
            var h = a.elements[g].id || a.elements[g].name;
            if (h = document.getElementById("validate_" + h))
                (e = b[a.elements[g].name]) ? f(h, a.elements[g], e) : (e = a.elements[g],
                d(h),
                MQTE.SetValidationClass(e.id, !1))
        }
    }
}
)();
var lastCommand = null;
function ViewSearchResults(a, b) {
    if (1 > SEARCH_PARAMS.keyword.length && 1 > SEARCH_PARAMS.author.length)
        SEARCH_PARAMS.container.innerHTML = '<span class="search_none_found">' + SEARCH_DICTIONARY.keyword_length + "</span>",
        ClearPaginators();
    else {
        var c = SEARCH_PARAMS.url + "?callback=RenderSearchResults&hl=" + SEARCH_PARAMS.lng + SEARCH_PARAMS.module_params + ("&keyword=" + (encodeURIComponent(SEARCH_PARAMS.keyword) || "")) + "&from=" + a + "&count=" + b + (SEARCH_PARAMS.author ? "&author=" + SEARCH_PARAMS.author : "") + (0 < SEARCH_PARAMS.method ? "&method=" + SEARCH_PARAMS.method : "") + (0 < SEARCH_PARAMS.dt_from ? "&dt_from=" + SEARCH_PARAMS.dt_from : "");
        c += "&t=" + Math.random();
        var d = document.createElement("script");
        d.type = "text/javascript";
        d.src = c;
        ProcessSearchScript(d, a, b)
    }
}
function ProcessSearchScript(a, b, c, d) {
    0 == SEARCH_PARAMS.timeout_id ? (SEARCH_PARAMS.results_loaded = !1,
    a.onerror = CheckIfScriptLoaded,
    SEARCH_PARAMS.container.innerHTML = "",
    setTimeout(function() {
        SEARCH_PARAMS.container.appendChild(a)
    }, 20),
    SEARCH_PARAMS.next = b + c,
    SEARCH_PARAMS.current = b,
    SEARCH_PARAMS.prev = b >= c ? b - c : 0,
    SEARCH_PARAMS.isFirst = 0 == b,
    clearTimeout(SEARCH_PARAMS.timeout_id),
    SEARCH_PARAMS.timeout_id = setTimeout(CheckIfScriptLoaded, 15E3),
    lastCommand = null) : lastCommand = [a, b, c]
}
function CheckLastCommand() {
    lastCommand && ProcessSearchScript(lastCommand[0], lastCommand[1], lastCommand[2])
}
window.RenderSearchResults = function(a) {
    clearTimeout(SEARCH_PARAMS.timeout_id);
    SEARCH_PARAMS.timeout_id = 0;
    SEARCH_PARAMS.results_loaded = !0;
    var b = a.results
      , c = SEARCH_PARAMS.current + 1;
    SEARCH_PARAMS.isLast = SEARCH_PARAMS.from + SEARCH_PARAMS.count <= a.total;
    SEARCH_PARAMS.container.innerHTML = "";
    0 == b.length && (SEARCH_PARAMS.container.innerHTML = '<span class="search_none_found">' + SEARCH_DICTIONARY.found_none + "</span>");
    for (var d = 0; d < b.length && d < SEARCH_PARAMS.count; d++) {
        var f = b[d];
        if (f) {
            !f.module || !f.module.endsWith("profiles") || f.avatar60x60 && f.avatar60x60.length || (f.avatar60x60 = "/avatar/avatar_na2.png");
            var e = f.info.author_login && "[deleted]" != f.info.author_login && "" != f.info.author_login ? " | <a href='https://www." + SEARCH_PARAMS.target_site + "/" + SEARCH_PARAMS.lng + "/users/" + f.info.author_login.toLowerCase() + "' title='" + f.info.author_login + "'>" + f.info.author_name + "</a>" : ""
              , g = createElement(SEARCH_PARAMS.container, "div", {
                className: "search_result"
            })
              , h = createElement(g, "span", {
                className: "title"
            });
            createElement(h, "a", {
                href: f.info.url,
                innerHTML: encodeHtml(f.info.title),
                target: "_blank"
            });
            h = GetDateTime(f.date);
            var m = createElement(g, "div", {
                className: "text"
            });
            if (!SEARCH_PARAMS.is_mobile) {
                if (f.info.avatar60x60 && f.info.avatar60x60.length) {
                    var l = createElement(m, "span", {
                        className: "search_result_logo"
                    });
                    var p = createElement(l, "a", {
                        className: "frame",
                        href: f.info.url
                    });
                    var t = createElement(p, "span", {
                        className: "bg"
                    });
                    p = 0 == f.info.avatar60x60.indexOf("/") ? "https://c.mql5.com" + f.info.avatar60x60 : f.info.avatar60x60;
                    createElement(t, "img", {
                        src: p,
                        width: 60,
                        height: 60,
                        alt: encodeHtml(f.info.title),
                        title: encodeHtml(f.info.title)
                    });
                    l.style.cssFloat = "left"
                }
                f.info.avatar110x140 && f.info.avatar110x140.length && (l = createElement(m, "span", {
                    className: "search_result_logo"
                }),
                p = 0 == f.info.avatar110x140.indexOf("/") ? "https://c.mql5.com" + f.info.avatar110x140 : f.info.avatar110x140,
                createElement(l, "img", {
                    src: p,
                    width: 60,
                    height: 8400 / 110 | 0,
                    alt: encodeHtml(f.info.title),
                    title: encodeHtml(f.info.title)
                }),
                l.style.cssFloat = "left")
            }
            SEARCH_PARAMS.is_mobile && f.text && (f.text = f.text.substring(0, 120) + "...");
            m.innerHTML += f.text ? encodeHtml(f.text) : "";
            l = 0 <= b[d].module.indexOf("metatrader4") ? "module_metatrader4_" + b[d].module.split(".")[3] : 0 <= b[d].module.indexOf("metatrader5") ? "module_metatrader5_" + b[d].module.split(".")[3] : "module_" + b[d].module.split(".")[3];
            e = createElement(g, "span", {
                className: "user_info",
                innerHTML: (SEARCH_DICTIONARY[l] || SEARCH_DICTIONARY.module_empty) + " | " + h + e
            });
            f.info.review_rating && (e.innerHTML += " | ",
            e.innerHTML += '<div class="rating small" style="margin-top:5px;margin-bottom:-1px;"><div class="v' + f.info.review_rating + '0" style="margin:0;"></div></div>');
            f.info.category && (e.innerHTML += " | ",
            e.innerHTML += f.info.category);
            f.info.platform && f.info.platform != f.info.category && (e.innerHTML += " | ",
            e.innerHTML += f.info.platform);
            f.info.files_info && (h = eval(f.info.files_info)) && h.length && 0 < h.length && (e.innerHTML += " | ",
            l = createElement(e, "span", {
                className: "attachments searchAttachments",
                id: "attaches_container_" + f.id
            }),
            RenderAttaches(l, l, h, f.id));
            f.info.price && (e.innerHTML += " | ",
            e.innerHTML += "0.00" != f.info.price ? f.info.price + " " + SEARCH_DICTIONARY.of_credits : SEARCH_DICTIONARY.price_free);
            f.info.user_company && (e.innerHTML += " | ",
            f.info.user_company_position && (e.innerHTML += encodeHtml(f.info.user_company_position) + " " + SEARCH_DICTIONARY.at + " "),
            e.innerHTML += encodeHtml(f.info.user_company));
            f.info.user_country && (e.innerHTML += " | " + encodeHtml(f.info.user_country));
            if (d == b.length - 1 || d == SEARCH_PARAMS.count - 1)
                g.className += " last";
            c++
        }
    }
    1 == SEARCH_PARAMS.scrollBottom && (window.scrollTo(0, 100500),
    SEARCH_PARAMS.scrollBottom = null);
    RenderSearchPaginatorEx(b.length, a.total, $("paginator_block_top"), !1);
    RenderSearchPaginatorEx(b.length, a.total, $("paginator_block_bottom"), !0);
    CheckLastCommand()
}
;
function RenderAttaches(a, b, c, d) {
    createElement(b, "a", {
        id: "attaches_link_" + d,
        href: "#",
        className: "attachesLink poppedUp",
        onclick: function() {
            ToggleAttaches(d);
            return !1
        },
        innerHTML: SEARCH_DICTIONARY.files + " (" + c.length + ")<small>&#x25BC;</small>"
    });
    a = createElement(a, "div", {
        id: "attaches_" + d
    });
    a.style.display = "none";
    for (b = 0; b < c.length; b++) {
        var f = c[b]
          , e = createElement(a, "div", {
            className: "attachItem"
        })
          , g = createElement(e, "a", {
            href: f.url,
            innerHTML: f.name,
            className: f.ext
        });
        createElement(g, "i", {
            className: "attach-icon attach-icon_" + f.ext
        });
        createElement(e, "span", {
            className: "attachSize",
            innerHTML: "(" + f.size_KB + ")"
        })
    }
}
function ToggleAttaches(a) {
    var b = $("attaches_" + a);
    if (b) {
        var c = "none" == b.style.display
          , d = $("attaches_link_" + a);
        d && (a = $("attaches_container_" + a)) && (a.className = c ? "attachments searchAttachments opened" : "attachments searchAttachments",
        d.className = c ? "attachesLink" : "poppedUp attachesLink",
        b.style.display = c ? "block" : "none")
    }
}
function CheckIfScriptLoaded() {
    SEARCH_PARAMS.results_loaded || (SEARCH_PARAMS.container.innerHTML = '<span class="search_none_found">' + SEARCH_DICTIONARY.error_loading + "</span>");
    SEARCH_PARAMS.timeout_id = 0;
    CheckLastCommand()
}
function ClearPaginators() {
    $("paginator_block_bottom").innerHTML = "";
    $("paginator_block_top").innerHTML = ""
}
function RenderSearchPaginatorEx(a, b, c, d) {
    var f = SEARCH_PARAMS.is_mobile ? 1 : 7
      , e = SEARCH_PARAMS.is_mobile ? 1 : 7;
    b = Math.ceil(b / SEARCH_PARAMS.count);
    a = SEARCH_PARAMS.current / SEARCH_PARAMS.count + 1;
    if (c && (c.innerHTML = "",
    c.style.display = "none",
    !(1 >= b))) {
        c.style.display = "";
        f = Math.max(1, a - f);
        e = Math.min(b, a + e);
        1 == b - e && (e = b);
        if (1 < f && (createElement(c, "a", {
            innerHTML: "1",
            className: "search_paginator_page",
            href: "javascript:GoToSearchPage(1," + d + ")"
        }),
        2 < f)) {
            var g = Math.ceil((f - 2) / 2) + 1;
            createElement(c, "a", {
                innerHTML: "...",
                className: "search_paginator_page dots",
                href: "javascript:GoToSearchPage(" + g + "," + d + ")"
            })
        }
        for (; f <= e; f++)
            createElement(c, "a", {
                innerHTML: f,
                className: "search_paginator_page" + (a == f ? " current" : ""),
                href: "javascript:GoToSearchPage(" + f + "," + d + ")"
            });
        e < b && (g = Math.ceil((b - e) / 2) + e,
        createElement(c, "a", {
            innerHTML: "...",
            className: "search_paginator_page dots",
            href: "javascript:GoToSearchPage(" + g + "," + d + ")"
        }),
        e < b - 1 && createElement(c, "a", {
            innerHTML: b,
            className: "search_paginator_page" + (a == b ? " current" : ""),
            href: "javascript:GoToSearchPage(" + b + "," + d + ")"
        }))
    }
}
function RenderSearchPaginator(a, b, c, d) {
    if (c) {
        a = SEARCH_PARAMS.is_mobile ? 2 : 7;
        c.innerHTML = "";
        b = Math.ceil(b / SEARCH_PARAMS.count);
        var f = SEARCH_PARAMS.current / SEARCH_PARAMS.count + 1
          , e = f > b - a | f < a ? SEARCH_PARAMS.is_mobile ? 3 : 6 : SEARCH_PARAMS.is_mobile ? 2 : 4;
        if (1 >= b)
            c.style.display = "none";
        else {
            c.style.display = "block";
            var g = createElement(c, "a", {
                innerHTML: "1",
                className: "search_paginator_page",
                href: "javascript:GoToSearchPage(1," + d + ")"
            });
            SEARCH_PARAMS.isFirst && (g.className += " current");
            f >= e && (hidden_page_num = Math.round((f - e + 1) / 2),
            createElement(c, "a", {
                innerHTML: "...",
                className: "search_paginator_page dots",
                href: "javascript:GoToSearchPage(" + hidden_page_num + "," + d + ")"
            }));
            for (var h = f - e; h <= f + e && h <= b; h++)
                2 > h || h > b || (g = createElement(c, "a", {
                    innerHTML: h,
                    className: "search_paginator_page",
                    href: "javascript:GoToSearchPage(" + h + "," + d + ")"
                }),
                h == f && (g.className += " current"));
            f <= b - a && (hidden_page_num = Math.round((b + f + e) / 2),
            createElement(c, "a", {
                innerHTML: "...",
                className: "search_paginator_page dots",
                href: "javascript:GoToSearchPage(" + hidden_page_num + "," + d + ")"
            }),
            createElement(c, "a", {
                innerHTML: b,
                className: "search_paginator_page",
                href: "javascript:GoToSearchPage(" + b + "," + d + ")"
            }))
        }
    }
}
function GoToSearchPage(a, b) {
    elementFrom = (a - 1) * SEARCH_PARAMS.count;
    window.hashParams.Set("page", a);
    ViewSearchResults(elementFrom, SEARCH_PARAMS.count);
    SEARCH_PARAMS.scrollBottom = b
}
function Suggestions(a, b, c, d) {
    function f(a) {
        setTimeout(function() {
            l()
        }, 10)
    }
    function e(a) {
        a = a || window.event;
        27 == a.keyCode && (r.focus(),
        r.select())
    }
    function g(a) {
        a = a || window.event;
        return 27 == a.keyCode ? (l(),
        !1) : !0
    }
    function h() {
        var a = document.createElement("script");
        a.type = "text/javascript";
        a.src = t + "?keyword=" + encodeURIComponent(w) + "&hl=" + n + "&callback=window.SuggestionControllers[" + u + "].RenderSuggestions";
        r.parentNode.appendChild(a)
    }
    function m(a) {
        a = a || window.event;
        if (38 == a.keyCode || 40 == a.keyCode || 39 == a.keyCode) {
            var b = a.keyCode;
            if (v)
                for (a = v.getElementsByTagName("tr"),
                40 == b && (v.selectedSuggestIndex = v.selectedSuggestIndex || 0 == v.selectedSuggestIndex ? v.selectedSuggestIndex + 1 : 0,
                v.selectedSuggestIndex >= a.length && (v.selectedSuggestIndex = 0)),
                38 == b && (v.selectedSuggestIndex = v.selectedSuggestIndex ? v.selectedSuggestIndex - 1 : a.length - 1,
                0 > v.selectedSuggestIndex && (v.selectedSuggestIndex = a.length - 1)),
                b = 0; b < a.length; b++)
                    Class(a[b], "selected", b != v.selectedSuggestIndex),
                    b == v.selectedSuggestIndex && (r.value = a[b].firstChild.suggestValue);
            else
                40 == b && 0 < w.length && h()
        } else
            13 != a.keyCode && w != r.value && (w = r.value,
            l(),
            w && h())
    }
    function l(a) {
        a && (w = a,
        r.value = a);
        v && v.parentNode && v.parentNode.removeChild(v);
        v = null
    }
    function p(a, b) {
        b = b ? b.fulltrim() : null;
        var c = createElement(null, "tr")
          , d = a.indexOf(b)
          , e = 0 <= d ? a.substring(d, b.length) : ""
          , f = 0 <= d ? a.substring(0, d) : "";
        b = 0 <= d ? a.substring(d + b.length) : a;
        e = createElement(c, "td", {
            innerHTML: f + '<span class="highlighted">' + e + "</span>" + b,
            suggestValue: a
        });
        e.addEventListener ? e.addEventListener("mousedown", function() {
            l(a);
            q && q()
        }, !1) : e.attachEvent("onmousedown", function() {
            l(a);
            q && q()
        });
        return c
    }
    var t = b
      , n = c
      , r = $(a);
    if (r && !r.isSuggest) {
        var q = d;
        window.SuggestionControllers || (window.SuggestionControllers = []);
        var u = window.SuggestionControllers.length;
        window.SuggestionControllers[window.SuggestionControllers.length] = this;
        r.autocomplete = "off";
        var w = r.value, v;
        r.isSuggest = !0;
        r.addEventListener ? (r.addEventListener("keypress", g, !1),
        window.addEventListener("keypress", e, !1),
        r.addEventListener("keyup", m, !1),
        r.addEventListener("blur", f, !1)) : (r.attachEvent("onkeypress", g),
        window.attachEvent("onkeypress", e),
        r.attachEvent("onkeyup", m),
        r.attachEvent("onblur", f));
        this.HideSuggests = function() {
            l()
        }
        ;
        this.RenderSuggestions = function(a) {
            l();
            if (a[1] && a[1].length) {
                v = createElement(null, "table", {
                    className: "suggestionsTable",
                    cellPadding: 0,
                    cellSpacing: 0
                });
                for (var b = 0; b < a[1].length; b++)
                    v.appendChild(p(a[1][b][0], w));
                a = getElementSize(r.parentNode);
                v.style.width = a.width + 2 + "px";
                b = getOffsetSum(r.parentNode);
                v.style.top = b.top + a.height + 1 + "px";
                v.style.left = b.left + "px";
                document.body.appendChild(v)
            }
        }
    }
}
;function chatWidget(a, b, c, d, f, e, g, h, m, l, p, t) {
    function n(a, b) {
        a && 0 !== a.length || (a = d.errorOccuredMessage);
        "_" !== a && ua && (ua.style.display = "block",
        ua.innerHTML = "<span>" + a + "</span>",
        a = -1 < a.indexOf("<a") ? 1E4 : 3E3,
        403 != b && setTimeout(function() {
            ua.style.display = "none"
        }, a),
        ya && (ya = !1))
    }
    function r() {
        pa && (R = window.localStorage.setItem("chat", JSON.stringify(R)))
    }
    function q() {
        0 == da && E();
        A();
        Class(N, "active", !0);
        return D()
    }
    function u(a) {
        q();
        Y && Y.Close();
        window.ChatsData.CreatePrivateChat(a, function(a) {
            var b = window.ChatsData.GetChatLocal(a);
            b ? v(b) : window.ChatsData.GetChat(a, v)
        })
    }
    function w(a) {
        q();
        Y && Y.Close();
        var b = window.ChatsData.GetChatLocal(a);
        b ? v(b) : window.ChatsData.GetChat(a, v)
    }
    function v(a) {
        a && (a.length && (a = a[0]),
        Y = new ja(a,p),
        a.user_info || 3 === a.type && a.author_id !== O || window.ChatsData.GetChatUsers(a.id, I),
        "customScrollWrapper" === M.parentNode.className && (M.parentNode.style.visibility = "hidden"),
        C(a.id),
        R = R || {},
        R.latestChat = a.id,
        r())
    }
    function C(a) {
        A();
        Class(N, "active", !1);
        var b = $("chatLineLite_" + a);
        Class(b, "active", !1);
        Da = a
    }
    function A() {
        var a = M.getElementsByClassName("active");
        a && 0 < a.length && Class(a[0], "active", !0)
    }
    function D() {
        if (Y)
            Y.Close();
        else
            return !1;
        Y && Y.GetChatId() && window.siteUpdates.RegisterRenderCallback(13, 1, void 0, {
            chid: void 0
        });
        Y = null;
        M.parentNode.style.visibility = "visible";
        Da = null;
        R = R || {};
        R.latestChat = void 0;
        r();
        return !0
    }
    function z(a, b) {
        Y && Y.resizeScrollContainer();
        R = R || {};
        R.width = a;
        R.height = b;
        r()
    }
    function E() {
        window.ChatsData.WsSupport && (M && (M.innerHTML = ""),
        da = 1,
        window.ChatsData.GetChats(ma, 200, G))
    }
    function F(a) {
        if (Ca)
            N || (Ia = 48,
            ea = 54,
            N = $("chatContainer"),
            xa = new wa,
            M = createElement(N, "DIV", {
                className: "chatWidgetContent"
            }),
            ua = createElement(N, "DIV", {
                className: "chatWidgetError"
            }));
        else {
            if (!N) {
                var b = $("notifyMessages");
                if (b && !(0 <= b.className.indexOf("static"))) {
                    N = createElement(document.body, "div", {
                        className: "chatWidget"
                    });
                    N.style.width = R.width + "px";
                    N.style.height = R.height + "px";
                    if (b = $("cover"))
                        if (b = getElementsByClass("head", b)[0])
                            b = getOffsetSum(b),
                            5 < b.top && (N.style.top = b.top + 41 + "px");
                    M = createElement(N, "div", {
                        className: "chatWidgetContent"
                    });
                    b = createElement(N, "div", {
                        className: "chatWidgetBottom"
                    });
                    var c = createElement(b, "div", {
                        className: "chatWidgetError"
                    });
                    ua = createElement(c, "div");
                    createElement(b, "a", {
                        className: "bottomButton expandIco",
                        href: "/" + U + "/users/" + B + "/messages",
                        target: "_blank",
                        title: d.expandWindow
                    });
                    makeResizable(N, 240, 240, z)
                }
            }
            SearchBoxContainer.Hide(void 0, !0);
            Z = $(Z.id);
            N.style.display = "block"
        }
        ia = !0;
        window.ChatsData.WsSupport ? 0 != da || a || E() : N.innerHTML = '<div class="chatWidgetContent wsError" style="height:auto;">Your browser is not supported</div>';
        Ca || Class(Z, "link-notify_opened", !1)
    }
    function H() {
        q();
        N && (N.style.display = "none",
        ia = !1,
        Z && (Z = $(Z.id),
        Class(Z, "link-notify_opened", !0)))
    }
    function J(a, b) {
        pa && (a = va.getItem("chatWidget_lastNotified") || "0",
        a = parseInt(a),
        a > sa && (sa = a));
        a = sa;
        if ((!0 === window.isWindowActive || window.isLastActiveWindow()) && fa && a < b && Pa) {
            try {
                fa.play()
            } catch (Ma) {}
            pa && va.setItem("chatWidget_lastNotified", b + "");
            sa = b
        }
    }
    function G(a, b) {
        if (a && a.length) {
            var c = !t || L || Da || !Ca ? null : a[0];
            if (b)
                for (; M.firstChild; )
                    M.removeChild(M.firstChild);
            for (b = 0; b < a.length; b++)
                if (N) {
                    for (var d = ha(a[b]), e = $(d.id); e && e.parentNode; )
                        e.parentNode.removeChild(e),
                        e = $(d.id);
                    if (d)
                        a: {
                            e = d;
                            var f = M
                              , g = e.dataset.lastUpdate
                              , l = getElementsByClass("chatLineLite", f);
                            if (l && l.length)
                                for (var h = 0; h < l.length; h++) {
                                    var m = l[h];
                                    if (0 < window.ChatsData.CompareHex(g, m.dataset.lastUpdate)) {
                                        m.parentNode.insertBefore(e, m);
                                        break a
                                    }
                                }
                            f.appendChild(e)
                        }
                    a[b].user_info ? I([a[b].user_info]) : 3 !== a[b].type ? window.ChatsData.GetChatUsers(a[b].id, I) : a[b].avatarIsPresent && !a[b].avatar_url && ((e = za[a[b].id]) ? T(a[b].id, e) : window.ChatsData.GetChatAvatar(a[b].id, T));
                    ma < d.id && (ma = d.id)
                }
            Da && C(Da);
            c && (L = !0,
            (a = $("chatIntro")) && a.offsetWidth && (R.latestChat ? w(R.latestChat) : v(c)))
        }
    }
    function I(a) {
        if (a && a.length) {
            a = a[0];
            var b = a.chat_id
              , c = window.ChatsData.GetChatLocal(b);
            if (c && 1 === c.type) {
                if (!c.avatar_url || a.avatar && c.avatar_url !== a.avatar) {
                    var d = $("chat_avatar_" + b);
                    d && a.avatar && (d.src = a.avatar,
                    d.style.display = "inline",
                    c.avatar_url = a.avatar)
                }
                (c = $("chat_title_" + b)) && a.name && (c.innerText = a.name || a.login);
                (c = $("chatCommentsOtherUserLogin_" + b)) && a.login && (c.value = a.login);
                (c = $("chatCommentsOtherUserName_" + b)) && a.name && (c.value = a.name || a.login);
                (c = $("chatContentWrapper_" + b)) && a.login && (c.dataset.userLogin = a.login)
            }
            Y && Y.chat && Y.chat.id === b && Y.UpdateStatusBar()
        }
    }
    function T(a, b) {
        a && b && (za[a] = b,
        a = $("chat_avatar_" + a)) && (a.src = b,
        a.style.display = "inline")
    }
    function ha(a) {
        var b = createElement(null, "DIV", {
            className: "chatLineLite",
            id: "chatLineLite_" + a.id
        });
        b.dataset.lastUpdate = a.lastUpdateHex;
        var c = ca(a);
        c && b.appendChild(c);
        createElement(b, "INPUT", {
            id: "chatCommentsOtherUserLogin_" + a.id,
            type: "hidden",
            value: a.login
        });
        createElement(b, "INPUT", {
            id: "chatCommentsOtherUserName_" + a.id,
            type: "hidden",
            value: a.name
        });
        a.unread_count && createElement(b, "SPAN", {
            className: "chatMessagesCount unread",
            innerHTML: a.unread_count
        });
        window.ChatsData.IsChatUnread(a) && Class(b, "unread");
        c = createElement(b, "DIV", {
            className: "user"
        });
        c.innerText = ExtractLogoLetters(a.name);
        c = createElement(c, "IMG", {
            width: Ia,
            height: Ia,
            title: a.name,
            id: "chat_avatar_" + a.id
        });
        a.avatar_url ? c.src = a.avatar_url : 1 === a.type && a.user_info && a.user_info.length && a.user_info[0].avatar ? (a.avatar_url = a.user_info[0].avatar,
        c.src = a.avatar_url) : c.style.display = "none";
        c = createElement(b, "A", 1 === a.type && a.user_info && a.user_info.lengt ? {
            className: "chatContentWrapper",
            href: "/" + U + "/users/" + B + "/messages?user=" + a.user_info[0].login.toLowerCase(),
            target: "_blank",
            id: "chatContentWrapper_" + a.id
        } : {
            className: "chatContentWrapper",
            id: "chatContentWrapper_" + a.id
        });
        c.onclick = function() {
            w(a.id);
            return !1
        }
        ;
        createElement(c, "DIV", {
            className: "chatTitle",
            id: "chat_title_" + a.id,
            innerText: a.name
        });
        a.text && a.text.length > qa && (a.text = a.text.substring(0, qa));
        createElement(c, "DIV", {
            className: "chatTextWrapper",
            innerText: a.text || " ",
            id: "chat_text_" + a.id
        });
        c = createElement(c, "DIV", {
            className: "chatDate updatableDateContainer"
        });
        createElement(c, "INPUT", {
            type: "hidden",
            value: a.date
        });
        createElement(c, "SPAN", {
            innerHTML: a.date_text
        });
        return b
    }
    function ca(a) {
        return Ba && 1 === a.type ? createElement(null, "span", {
            className: "deleteChat",
            title: d.deleteChat + "*",
            onclick: function() {
                confirm(d.deleteChatConfirm) && Ajax.post("https://www.mql5.com/api/chat/delete", {
                    chatId: "0x" + a.id,
                    userId1: O,
                    userId2: a.user_info && a.user_info.length ? "0x" + a.user_info[0].idHex : null
                }, !1, {
                    onready: function() {
                        Da === a.id && D();
                        window.ChatsData.GetAllChatsRaw()
                    },
                    onerror: function() {
                        alert("Error occured");
                        window.ChatsData.GetAllChatsRaw()
                    }
                })
            }
        }) : null
    }
    function X(a) {
        a = parseInt(a);
        Z = $(Z.id);
        la = $(la.id);
        var b = getElementsByClass("notify-icon_messages", Z)
          , c = getElementsByClass("notify-icon_messages", la);
        if (b && b.length) {
            var d = getElementsByClass("notify-icon_count", b[0])
              , e = getElementsByClass("notify-icon_count", c[0]);
            d && d.length && b[0].removeChild(d[0]);
            e && e.length && c[0].removeChild(e[0]);
            0 < a && (createElement(b[0], "i", {
                className: "notify-icon_count",
                innerText: a
            }),
            createElement(c[0], "i", {
                className: "notify-icon_count",
                innerText: a
            }))
        }
    }
    function ja(a, b) {
        function c() {
            if (P && P.onscroll)
                P.onscroll()
        }
        function e() {
            L.innerHTML = "";
            createElement(L, "span", {
                className: "chatWidgetCommentsListClose ico",
                onclick: function() {
                    q(!0)
                },
                title: d.closeChat
            });
            3 !== a.type || C || (window.ChatsData.GetChatLocal(fa) ? createElement(L, "a", {
                className: "chatWidgetCommentsListSubscribe unsubscribe",
                onclick: function() {
                    window.ChatsData.Subscribe(fa, O, !1, Y.UpdateStatusBar);
                    window.ChatsData.RemoveChat(fa)
                },
                innerText: d.unsubscribe
            }) : createElement(L, "a", {
                className: "chatWidgetCommentsListSubscribe subscribe",
                onclick: function() {
                    window.ChatsData.Subscribe(fa, O, !0, w)
                },
                innerText: d.subscribe
            }));
            !T && 1 === a.type && a.user_info && a.user_info.length ? createElement(L, "a", {
                className: "chatWidgetCommentsListUserLink",
                href: "/" + U + "/users/" + a.user_info[0].login.toLowerCase(),
                target: "_blank",
                innerText: E
            }) : 2 === a.type && a.countMembers ? (C || createElement(L, "a", {
                className: "chatWidgetCommentsListSubscribe unsubscribe",
                onclick: function() {
                    window.ChatsData.Subscribe(fa, O, !1, Y.UpdateStatusBar);
                    window.ChatsData.RemoveChat(fa);
                    da = 0;
                    q(!0)
                },
                innerText: d.unsubscribe
            }),
            createElement(L, "b", {
                className: "chatWidgetCommentsListUserLink",
                innerText: E + ", " + d.ofUsers.replace("{0}", a.countMembers)
            })) : 3 === a.type && a.countMembers ? createElement(L, "b", {
                className: "chatWidgetCommentsListUserLink",
                innerText: E + ", " + d.ofSubscribers.replace("{0}", a.countMembers)
            }) : createElement(L, "b", {
                className: "chatWidgetCommentsListUserLink",
                innerText: E
            })
        }
        function f(a, b) {
            for (var c = 0; c < a.length; c++)
                if (!La || 0 > window.ChatsData.CompareHex(a[c].id, La))
                    La = a[c].id;
            console.log("loaded messages: " + a.length);
            b ? a.reverse() : sa = !0;
            g(P, {
                comments: a,
                chat_id: fa,
                chat_title: E
            });
            P._loading = !1;
            Class(P, "loading", !0);
            window.ChatsData.GetChat(fa, G);
            !sa && P.scrollHeight <= P.clientHeight && window.ChatsData.GetMessages(fa, La, f)
        }
        function g(a, b, c) {
            if (a = "string" == typeof b ? window.JSON ? JSON.parse(b) : eval("(" + b + ")") : b) {
                A || (H = F = a.answers_count,
                E = a.chat_title,
                fa = a.chat_id,
                e(),
                A = !0);
                a = a.comments || [];
                X(window.ChatsData.GetUnreadCount());
                b = h(a);
                m(a);
                if (H === F || c)
                    P.scrollTop = P.scrollHeight;
                H -= b
            }
        }
        function h(a) {
            if (!a)
                return n(),
                0;
            if ((a = "string" == typeof a ? window.JSON ? JSON.parse(a) : eval("(" + a + ")") : a) && 0 < a.length && a[0].hasOwnProperty("errorMessage"))
                return n(a[0].errorMessage),
                0;
            for (var b = (new Date).setHours(0, 0, 0, 0) / 1E3, c = 0, d = 0; d < a.length; d++)
                if (!(fa && a[d].parent_id !== fa || 0 <= Ka.indexOf(a[d].id) || 0 < a[d].record_id)) {
                    Ka.push(a[d].id);
                    var e = P.scrollTop
                      , f = P.scrollHeight;
                    p(a[d], P, b);
                    P.scrollTop = e + (P.scrollHeight - f);
                    c++
                }
            return c
        }
        function m(b) {
            if (b && b.length) {
                for (var c = $("unreadMessageIndicator"), e = null, f = 0; f < b.length; f++) {
                    var g = b[f];
                    g.author_id !== O && (0 >= window.ChatsData.CompareHex(g.id, a.lastAccessHex) || (e ? 0 > window.ChatsData.CompareHex(g.id, e.id) && (e = g) : e = g))
                }
                e && (b = $("chatWidgetComment_" + a.id + "_" + e.id)) && (c && c.parentNode.removeChild(c),
                c = createElement(null, "div", {
                    className: "chatWidgetCommentLine unreadMessage",
                    id: "unreadMessageIndicator"
                }),
                createElement(c, "div", {
                    className: "chatWidgetCommentWrapper",
                    innerHTML: '<div class="chatWidgetCommentText">' + d.unreadMessages + "</div>"
                }),
                b.parentNode.insertBefore(c, b))
            }
        }
        function p(b, c, e) {
            if (b.is_system)
                a: {
                    var f = "";
                    if (b.serviceChange)
                        switch (b.serviceChange) {
                        case ia.UserAdd:
                            f = b.serviceUserId === O ? d.systemYouInvited.replace("{0}", E) : d.systemUserInvited.replace("{0}", b.author_name).replace("{1}", b.serviceUserName);
                            break;
                        case ia.UserRemove:
                            f = b.author_id === b.serviceUserId ? d.systemChatUserLeft.replace("{0}", b.serviceUserName) : 3 === a.type ? d.systemChannelUserRemoved.replace("{0}", b.author_name).replace("{1}", b.serviceUserName) : d.systemChatUserRemoved.replace("{0}", b.author_name).replace("{1}", b.serviceUserName);
                            break;
                        case ia.IsPublic:
                            f = b.serviceChangeValue ? d.systemChannelPublic.replace("{0}", b.author_name) : d.systemChannelPrivate.replace("{0}", b.author_name);
                            break;
                        case ia.IsLimited:
                            f = b.serviceChangeValue ? d.systemChatLimited.replace("{0}", b.author_name) : d.systemChatUnlimited.replace("{0}", b.author_name);
                            break;
                        case ia.IsClosed:
                            f = 3 === a.type ? b.serviceChangeValue ? d.systemChannelClosed : d.systemChannelReopened : b.serviceChangeValue ? d.systemChatClosed.replace("{0}", b.author_name) : d.systemChatReopened.replace("{0}", b.author_name);
                            break;
                        case ia.Name:
                            f = 3 === a.type ? d.systemChannelRename.replace("{0}", '"' + b.serviceChatName + '"') : d.systemChatRename.replace("{0}", b.author_name).replace("{1}", '"' + b.serviceChatName + '"');
                            break;
                        case ia.ChatCreate:
                            f = 3 === a.type ? d.systemChannelCreate.replace("{0}", '"' + b.serviceChatName + '"') : d.systemChatCreate.replace("{0}", b.author_name).replace("{1}", '"' + b.serviceChatName + '"');
                            break;
                        default:
                            e = null;
                            break a
                        }
                    var g = createElement(null, "div", {
                        className: "chatWidgetCommentLine systemComment",
                        id: "chatWidgetComment_" + b.parent_id + "_" + b.id
                    })
                      , h = createElement(g, "div", {
                        className: "chatWidgetCommentWrapper"
                    });
                    createElement(h, "div", {
                        className: "chatWidgetCommentText",
                        innerText: f,
                        title: b.date < e ? GetDateTimeLocal(b.date) : DateToStringForTodayLocal(b.date)
                    });
                    g.dataset.seq = b.seq;
                    g.dataset.id = b.id;
                    e = g
                }
            else
                f = createElement(null, "div", {
                    className: "chatWidgetCommentLine" + (b.author_id === O ? " byOwner" : ""),
                    id: "chatWidgetComment_" + b.parent_id + "_" + b.id
                }),
                f.dataset.seq = b.seq,
                f.dataset.id = b.id,
                g = createElement(f, "div", {
                    className: "chatWidgetCommentWrapper"
                }),
                2 === a.type && b.author_id !== O && (createElement(g, "div", {
                    className: "chatWidgetCommentAuthor",
                    innerText: b.author_name
                }),
                Class(g, "has-author")),
                g = createElement(g, "div", {
                    className: "chatWidgetCommentText",
                    innerText: b.content
                }),
                g.innerHTML = urlsToLinks(g.innerHTML, l),
                h = createElement(g, "div", {
                    className: "chatWidgetCommentDate updatableDateContainer"
                }),
                createElement(h, "INPUT", {
                    type: "hidden",
                    value: b.date
                }),
                createElement(h, "SPAN", {
                    innerText: b.date < e ? GetDateTimeLocal(b.date) : DateToStringForTodayLocal(b.date),
                    title: GetDateTimeLocal(b.date)
                }),
                r(b, g, h),
                e = f;
            if (e)
                if ((f = getElementsByClass("chatWidgetCommentLine", c)) && f.length) {
                    g = void 0;
                    h = 0;
                    var Q = b.id;
                    if (Q)
                        for (var m = 0; m < f.length; m++) {
                            var W = f[m].dataset.id;
                            W && 0 < window.ChatsData.CompareHex(W, Q) && (!g || 0 > window.ChatsData.CompareHex(W, h)) && (g = f[m],
                            h = W)
                        }
                    g ? c.insertBefore(e, g) : c.appendChild(e);
                    b.attachments && b.attachments.length || 80 < (e.clientHeight || e.offsetHeight) && Class(e, "fullWidth")
                } else
                    c.appendChild(e)
        }
        function r(a, b, c) {
            if (a.attachments && 0 != a.attachments.length) {
                var d = getElementsByClass("attach", b);
                d && d.length ? (d = d[0],
                c = getElementsByClass("attachBlock", d),
                c.length && (c = c[0])) : (d = createElement(null, "DIV", {
                    className: "attach"
                }),
                c ? b.insertBefore(d, c) : b.appendChild(d),
                d.style.clear = "both",
                c = createElement(d, "DIV", {
                    className: "attachBlock",
                    id: "cb_" + a.id
                }));
                for (var e = 0; e < a.attachments.length; e++) {
                    var f = a.attachments[e];
                    var g = createElement(c, "DIV", {
                        className: "attachItem"
                    });
                    var h = getFileExtension(f.name, !0);
                    h && 0 <= Oa.indexOf(h) || (h = "unk");
                    var l = f.name + " (" + f.size_text + ")";
                    h = createElement(null, "A", {
                        className: h,
                        target: "_blank",
                        href: f.url,
                        innerText: f.name,
                        title: l
                    });
                    if (!f.preview_url || 1 !== f.type && 2 !== f.type)
                        g.appendChild(h);
                    else {
                        var Q = createElement(g, "DIV", {
                            className: "attachPreview"
                        });
                        l = createElement(Q, "A", {
                            target: "_blank",
                            href: f.url,
                            title: l
                        });
                        f.mimeType.startsWith("image") ? (createElement(l, "IMG", {
                            src: f.url
                        }),
                        t(f, l, Math.min(512, (P.clientWidth || P.offsetWidth || 560) - 48)),
                        g = function(a) {
                            a.preventDefault();
                            Aa.show(f.url);
                            return !1
                        }
                        ,
                        d.className = "attach image",
                        Core.AddHandler(l, "click", g),
                        Core.AddHandler(h, "click", g),
                        b.parentNode && Class(b.parentNode, "attach-wrapper")) : f.mimeType.startsWith("video") ? (t(f, l),
                        g = function(a) {
                            a = a || window.event;
                            a.preventDefault ? a.preventDefault() : a.returnValue = !1;
                            ra.show(f.url, f.preview_url, f.mimeType)
                        }
                        ,
                        d.className = "attach video",
                        Core.AddHandler(l, "click", g),
                        Core.AddHandler(h, "click", g),
                        createElement(d, "span", {
                            className: "size-badge",
                            innerText: f.size_text
                        }),
                        b.parentNode && Class(b.parentNode, "attach-wrapper")) : g.appendChild(h)
                    }
                }
            }
        }
        function t(a, b, c) {
            c = c || 512;
            b.style.backgroundImage = "url('" + a.preview_url + "')";
            var d = a.width
              , e = a.height;
            a.width > c ? a.height > a.width ? (e = c,
            d /= a.height / c) : (d = c,
            e /= a.width / c) : a.height > c && (e = c,
            d /= a.height / c);
            b.style.width = d + "px";
            b.style.height = e + "px";
            b.style.backgroundSize = d + "px auto"
        }
        function pa() {
            if (!sa) {
                var a = La;
                A || addCustomScroll(P, "#4A76B8", "#AEC2DD", 4, 4, null);
                P._loading = !0;
                window.ChatsData.GetMessages(fa, a, f)
            }
        }
        function u(a) {
            R.innerHTML = "";
            ca = createElement(R, "form", {
                method: "POST",
                action: ("/" + U + "/users/" + B + "/message/new").toLowerCase(),
                enctype: "multipart/form-data",
                onsubmit: function() {
                    va();
                    return !0
                }
            });
            createElement(ca, "input", {
                type: "hidden",
                name: "type",
                value: "widget"
            });
            createElement(ca, "input", {
                type: "hidden",
                name: "userTo",
                value: D
            });
            createElement(ca, "input", {
                type: "hidden",
                name: "__signature",
                id: "chatWidgetNewMessageSignature",
                value: oa
            });
            createElement(ca, "input", {
                type: "hidden",
                name: "formType",
                value: "ajax"
            });
            ja = createElement(ca, "input", {
                type: "hidden",
                name: "content",
                value: ""
            });
            createElement(ca, "button", {
                type: "button",
                className: "ico",
                onclick: va,
                title: d.sendMessageTitle
            });
            ha = createElement(ca, "div", {
                className: "inputWrapper"
            });
            ma = createElement(ha, "div", {
                className: "fileWrapper ico"
            });
            v();
            Z = createElement(ha, "textarea", {
                rows: 1
            });
            a && (Z.value = a);
            Z.onkeydown = function(a) {
                a = a || window.event;
                return 13 === a.keyCode && a.ctrlKey ? (va(),
                !1) : !0
            }
            ;
            Z.onkeyup = function(a) {
                z(this);
                c()
            }
            ;
            Z.focus();
            la = 0
        }
        function v() {
            return createElement(ma, "INPUT", {
                title: d.attachFileTitle + ": " + Ha.join(", "),
                type: "file",
                name: "attachedFile",
                onchange: function() {
                    window.ChatsData.AddFile(fa, this.files[0]);
                    c()
                },
                accept: "." + Ha.join(",."),
                onclick: function() {
                    return 4 > la
                }
            })
        }
        function va() {
            if (!ya) {
                var a = Z.value;
                if (a && a.length && !(1E5 < a.length)) {
                    ja.value = Z.value;
                    ya = !0;
                    window.ChatsData.AddText(fa, ja.value);
                    ja.value = "";
                    Z.value = "";
                    z(Z);
                    if ((a = getElementsByClass("readonlyAttachWrapper", ca)) && a.length)
                        for (var b = a.length - 1; 0 <= b; ) {
                            var d = a[b];
                            d.parentNode.removeChild(d);
                            --b
                        }
                    la = 0;
                    c()
                }
            }
        }
        function z(a) {
            if (window.getComputedStyle) {
                var b = a.clientHeight
                  , c = parseInt(window.getComputedStyle(P.parentNode, null).getPropertyValue("bottom") || 0);
                a.style.height = ea + "px";
                a.style.height = Math.min(a.scrollHeight, 160) + "px";
                P.parentNode.style.bottom = c - b + a.clientHeight + "px"
            }
        }
        var A = !1;
        this.chat = a;
        var C = O === a.author_id
          , fa = a.id || 0;
        this.GetChatId = function() {
            return fa
        }
        ;
        var D = a.login;
        this.GetUserToLogin = function() {
            return D
        }
        ;
        var E = a.name
          , Ka = []
          , F = 0
          , H = 0
          , La = void 0
          , sa = !1
          , I = $("chatWidgetComments");
        I && I.parentNode.removeChild(I);
        I = createElement(N, "div", {
            className: "chatWidgetComments",
            id: "chatWidgetComments"
        });
        var M = createElement(I, "div", {
            className: "chatWidgetCommentsStatus",
            id: "chatWidgetCommentsStatus"
        })
          , L = createElement(M, "div")
          , P = createElement(I, "div", {
            className: "chatWidgetCommentsList",
            id: "chatWidgetCommentsList"
        })
          , R = createElement(I, "div", {
            className: "chatWidgetCommentsEditor",
            id: "chatWidgetCommentsEditor"
        })
          , T = 18799 === a.author_id;
        T || 3 === a.type && !C || b ? Class(I, "systemChat") : u();
        window.ChatsData && (window.ChatsData.OnMessageAdd2 = function(a) {
            for (var b = 0, c = !1, d = 0; d < a.length; d++) {
                var e = a[d];
                e.author_id !== O && e.date > b && (b = e.date);
                e.author_id === O && (c = !0)
            }
            c = P.scrollTop === P.scrollHeight || c;
            h(a);
            window.siteUpdates.GetUpdates();
            ya = !1;
            c && (P.scrollTop = P.scrollHeight);
            0 < b && J(null, b)
        }
        );
        this.resizeScrollContainer = c;
        e();
        pa();
        (function() {
            var a = P.onscroll;
            P.onscroll = function() {
                if (!(1 > H)) {
                    var b = 50 > P.scrollTop;
                    b && !P._loading && pa();
                    a && a(arguments)
                }
            }
        }
        )();
        var ha, Z, ca, ja, ma, la;
        this.UpdatesCallback = function(a) {
            if (!a)
                return !1;
            var b = !1;
            a: {
                if (a.a && a.a.length && 0 != a.a.length)
                    for (var c = 0; c < a.a.length; c++)
                        if ("comments" === a.a[c].key) {
                            a = a.a[c].value;
                            break a
                        }
                a = []
            }
            if (a && a.length) {
                for (c = 0; c < a.length; c++)
                    a[c].parent_id == fa && (b = a[c].author_login != B);
                h(a);
                P.scrollTop = P.scrollHeight
            }
            return b
        }
        ;
        this.Close = function() {
            I && I.parentNode && I.parentNode.removeChild(I)
        }
        ;
        this.UpdateStatusBar = function() {
            e()
        }
        ;
        var ia = {
            UserAdd: 1,
            UserRemove: 2,
            IsPublic: 3,
            IsLimited: 4,
            IsClosed: 5,
            Name: 6,
            ChatCreate: 7
        }
    }
    function wa(a) {
        function b(a) {
            var b = createElement(null, "DIV", {
                className: "chatLineLite tiny",
                id: "chatLineLite_" + a.id
            });
            createElement(b, "INPUT", {
                id: "chatCommentsOtherUserLogin_" + a.id,
                type: "hidden",
                value: a.login
            });
            createElement(b, "INPUT", {
                id: "chatCommentsOtherUserName_" + a.id,
                type: "hidden",
                value: a.name
            });
            var c = createElement(b, "DIV", {
                className: "user"
            });
            c.innerText = ExtractLogoLetters(a.name);
            c = createElement(c, "IMG", {
                width: 25,
                height: 25,
                title: a.name,
                id: "chat_avatar_" + a.id
            });
            a.avatar ? c.src = a.avatar : c.style.display = "none";
            c = createElement(b, "A", 3 === a.type ? {
                className: "chatContentWrapper",
                id: "chatContentWrapper_" + a.id
            } : {
                className: "chatContentWrapper",
                href: "/" + U + "/users/" + B + "/messages?user=" + a.login.toLowerCase(),
                target: "_blank",
                id: "chatContentWrapper_" + a.id
            });
            c.onclick = function() {
                3 === a.type ? w(a.id) : u(a.id);
                h.value = "";
                Class(g, "active", !0);
                return e = !1
            }
            ;
            createElement(c, "DIV", {
                className: "chatTitle",
                id: "chat_title_" + a.id,
                innerText: a.name
            });
            return b
        }
        var c = null
          , e = !1
          , f = function() {
            if (!e)
                return !1;
            Class(g, "active", !0);
            h.value = "";
            e = !1;
            return !0
        }
          , g = createElement(a ? a : N, "div", {
            className: "chatWidgetSearch"
        });
        a = createElement(g, "div", {
            className: "search-form"
        });
        var h = createElement(a, "INPUT", {
            type: "text",
            className: "input",
            placeholder: d.searchUserPlaceholder
        });
        createElement(a, "a", {
            className: "close ico"
        }).onclick = f;
        a = createElement(createElement(g, "div", {
            className: "tabs"
        }), "ul");
        var l = createElement(createElement(a, "li", {
            className: "selected"
        }), "a", {
            innerText: d.users
        })
          , m = createElement(l, "span", {
            className: "badge"
        })
          , n = createElement(createElement(a, "li"), "a", {
            innerText: d.channels
        })
          , p = createElement(n, "span", {
            className: "badge"
        })
          , Aa = [l.parentNode, n.parentNode];
        a = createElement(g, "div", {
            className: "chatWidgetContent"
        });
        var q = createElement(a, "div", {
            className: "list selected"
        })
          , r = createElement(a, "div", {
            className: "list"
        })
          , ra = [q, r]
          , t = function(a, b) {
            for (var c = 0; c < Aa.length; c++)
                Aa[c].className = "";
            for (c = 0; c < ra.length; c++)
                ra[c].className = "list";
            a.parentNode.className = "selected";
            b.className = "list selected"
        };
        l.parentNode.onclick = function() {
            t(l, q)
        }
        ;
        n.parentNode.onclick = function() {
            t(n, r)
        }
        ;
        var pa = function(a, c) {
            if (!a.firstChild && c && c.length)
                for (var d = 0; d < c.length; d++)
                    if (!c[d].login || c[d].login !== B) {
                        var e = b(c[d]);
                        a.appendChild(e)
                    }
        }
          , v = function(a, c) {
            if (c && c.length)
                for (; a.firstChild; )
                    a.removeChild(a.firstChild);
            if (c && c.length)
                for (var d = 0; d < c.length; d++)
                    if (!c[d].login || c[d].login !== B) {
                        c[d].chat_id = c[d].idHex;
                        var e = b(c[d]);
                        a.appendChild(e)
                    }
        }
          , va = function(a) {
            clearTimeout(c);
            if (a && a.length) {
                for (var b = 0; b < ra.length; b++)
                    for (; ra[b].firstChild; )
                        ra[b].removeChild(ra[b].firstChild);
                window.ChatsData.UsersSearch(a, 100, function(a) {
                    v(q, a)
                });
                window.ChatsData.ChannelSearch(a, 100, function(a) {
                    v(r, a)
                });
                pa(q, window.ChatsData.UsersSearchLocal(a, 100), m);
                pa(r, window.ChatsData.ChannelSearchLocal(a, 100), p)
            }
        };
        h.onkeyup = function() {
            !this.value || !this.value.length || 2 > this.value.length ? f() : (Class(g, "active"),
            e = !0,
            clearTimeout(c),
            c = setTimeout(va, 300, this.value))
        }
        ;
        this.Open = function() {
            Class(g, "active");
            e = !0;
            h.focus()
        }
        ;
        this.Close = f
    }
    var O = a
      , B = b
      , U = c
      , Ba = !1;
    this.SetModerator = function() {
        Ba = !0
    }
    ;
    var da = 0, ma = 0, oa = g, N, M, xa, ia = !1, Z = document.getElementById("notifyMessages"), la = document.getElementById("notifyMessagesMobile"), Y = null, Oa = "gif jpeg jpg mq4 mq5 ex4 ex5 png rar txt zip mqh mt5".split(" "), Ha = "gif png jpg jpeg zip txt log mqh ex5 mq5 mq4 ex4 mt5 set tpl".split(" "), Pa = f ? !0 : !1, ua, qa = m || 128, Ca = !1, ea = 18, ya = !1, L = !1, Da = null, R = {}, za = {}, Ia = 25, Aa = new lightBoxImg, ra = new lightBoxVideo, pa = "undefined" !== typeof Storage && Core.isLocalStorageSupported() && window.JSON && window.JSON.stringify ? !0 : !1, va = {};
    if (pa)
        try {
            va = localStorage
        } catch (Ka) {
            va = sessionStorage
        }
    if (pa)
        try {
            R = JSON.parse(window.localStorage.getItem("chat") || "{}")
        } catch (Ka) {
            R = window.localStorage.setItem("chat", "{}"),
            R = {}
        }
    R = R || {};
    R.width = R.width || 345;
    R.height = R.height || Math.max(getWindowSize().height / 2 - (getWindowSize().height / 2 % 54 - 30) | 0, 354);
    (function() {
        var a = $("notifyMessages");
        a && 0 > a.className.indexOf("static") && (a.onclick = function() {
            ia ? H() : F();
            return !1
        }
        )
    }
    )();
    (function() {
        HotKeys.Add(27, function() {
            1 === Aa.getState() || 1 === ra.getState() || q() || xa && xa.Close() || Ca || H()
        });
        HotKeys.Add(113, function() {
            ia ? H() : F()
        })
    }
    )();
    window.ChatsData && (window.ChatsData.OnChatsUpdate = G);
    window.ChatsData && (window.ChatsData.OnMessageAdd = function(a) {
        for (var b = 0, c = 0; c < a.length; c++) {
            var d = a[c];
            d && (d.author_id !== O && d.date > b && (b = d.date),
            window.ChatsData.GetChat(d.parent_id, G))
        }
        0 < b && J(null, b)
    }
    );
    var fa = null;
    try {
        window.Audio && (fa = new window.Audio("https://c.mql5.com/i/message/new_message_click2.mp3"))
    } catch (Ka) {}
    var sa = 0;
    this.SetUnreadChatsCount = function(a) {
        X(a)
    }
    ;
    this.OpenChatWith = function(a, b, c) {
        !ia && a && setTimeout(function() {
            F(!0);
            N.scrollIntoView(!1)
        }, 50);
        setTimeout(function() {
            u(b)
        }, 52)
    }
    ;
    this.InitStaticChat = function() {
        Ca = !0;
        F(!1)
    }
    ;
    this.ResetCommentsList = function() {
        Y = new ja(Y.chat,p)
    }
    ;
    h && pa && J(null, h)
}
function urlsToLinks(a, b) {
    return a && a.replace && 0 !== a.length ? a.replace(/(^|[\s\n]|<[A-Za-z]*\/?>)((?:https?|ftp):\/\/[\-A-Z0-9+\u0026\u2019@#\/%?=()~_|!:,.;]*[\-A-Z0-9+\u0026@#\/%=~()_|])/gi, function(a, d, f) {
        if (b && b.length) {
            a = extractHostName(f);
            for (var c = 0; c < b.length; c++) {
                var g = a.indexOf(b[c]);
                if (0 <= g && g === a.length - b[c].length)
                    return [d, '<a href="', f, '" target="_blank" title="', f, '">', f, "</a>"].join("")
            }
        }
        return [d, '<a href="https://www.mql5.com/go?link=', encodeURIComponent(f), '" target="_blank" title="', f, '">', f, "</a>"].join("")
    }) : a
}
function extractHostName(a) {
    a = -1 < a.indexOf("://") ? a.split("/")[2] : a.split("/")[0];
    return a = a.split(":")[0].split("?")[0]
}
function ExtractLogoLetters(a) {
    if (!a || !a.length)
        return "";
    for (var b = [], c = 0; c < a.length; c++) {
        var d = a.charAt(c);
        if ("A" <= d && "Z" >= d && (b.push(d),
        2 === b.length))
            break
    }
    return 2 === b.length ? b.join("") : a.charAt(0)
}
function makeResizable(a, b, c, d) {
    function f(d) {
        d.preventDefault && d.preventDefault();
        0 < d.clientX && (g = Math.max(b, a.offsetLeft + a.offsetWidth - d.clientX),
        a.style.width = g + "px");
        d.clientY - document.body.offsetTop < window.innerHeight && (h = Math.max(c, d.clientY - a.offsetTop),
        a.style.height = h + "px")
    }
    function e(a) {
        a.preventDefault && a.preventDefault();
        window.removeEventListener("mousemove", f, !1);
        window.removeEventListener("mouseup", e, !1);
        d && 0 !== g && 0 !== h && d(g, h);
        window.setCursor("auto")
    }
    var g = 0
      , h = 0;
    (function() {
        for (var b = createElement(a, "DIV", {
            className: "resizeGrip"
        }), c = 0; 6 > c; c++)
            createElement(b, "DIV");
        return b
    }
    )().addEventListener("mousedown", function(a) {
        a.preventDefault && a.preventDefault();
        window.addEventListener("mousemove", f, !1);
        window.addEventListener("mouseup", e, !1);
        window.setCursor("ne-resize")
    }, !1)
}
;function ChatWs(a, b, c, d, f, e) {
    function g() {
        var a = ma;
        ma *= (3 + Math.random()) / 2;
        return Math.min(a, 3E4)
    }
    function h() {
        Ba || (Ba = !0,
        console.log("connecting"),
        oa = void 0,
        U = new WebSocket("wss://" + da),
        U.binaryType = "arraybuffer",
        U.onopen = function() {
            D(L.UserAuthorize, [{
                Uint8: 4
            }, {
                Uint8: 4
            }, {
                Uint32: 1
            }, I(O)]);
            Ba = !1;
            console.log("connected");
            B && (B(),
            B = null)
        }
        ,
        U.onerror = function() {
            Ba = !1;
            U.close()
        }
        ,
        U.onclose = function(a) {
            m(a);
            console.log("disconnected")
        }
        ,
        U.onmessage = A)
    }
    function m(a) {
        var b = U;
        setTimeout(function() {
            h();
            b.removeAllListeners && b.removeAllListeners();
            try {
                b.close()
            } catch (pa) {}
        }, g())
    }
    function l(a, b) {
        if (a && b) {
            var c = new FileReader
              , d = b.slice(0, b.size);
            c.onloadend = function(c) {
                function d(c) {
                    function d(c) {
                        c = String.fromCharCode.apply(null, new Uint8Array(c));
                        D(L.MessageAdd, [a, {
                            Uint8: 2
                        }, c, g, {
                            Uint32: b.size
                        }, {
                            String16: b.type
                        }, {
                            String16: b.name
                        }]);
                        qa = b
                    }
                    var g = String.fromCharCode.apply(null, new Uint8Array(c));
                    c = e.digest("SHA-256", f);
                    c.then ? c.then(d) : "undefined" != typeof c.oncomplete && (c.oncomplete = function(a) {
                        d(a.target.result)
                    }
                    )
                }
                if (c.target.readyState !== FileReader.DONE)
                    console.log("read file error"),
                    qa = null;
                else {
                    var e = null;
                    window.crypto && (e = window.crypto.subtle);
                    !e && window.msCrypto && (e = window.msCrypto.subtle);
                    if (e) {
                        c = c.target.result;
                        var f = new Uint8Array(c.byteLength + Ca.byteLength);
                        f.set(new Uint8Array(c));
                        f.set(Ca, c.byteLength);
                        c = e.digest("SHA-256", c);
                        c.then ? c.then(d) : "undefined" != typeof c.oncomplete && (c.oncomplete = function(a) {
                            d(a.target.result)
                        }
                        )
                    }
                }
            }
            ;
            c.readAsArrayBuffer(d)
        }
    }
    function p(a) {
        var b = a.getReader32();
        a = b.getBinary(8);
        var c = b.getBinary(8)
          , d = b.getBinary(8)
          , e = b.getString16()
          , f = b.getUint8()
          , g = b.getUint8()
          , h = b.getUint8()
          , l = b.getUint8()
          , m = b.getBinary(8);
        b.getBinary(8);
        var n = b.getString16();
        b.getString16();
        b.getString16();
        b.getString16();
        b.getInt32();
        var p = b.getInt32()
          , q = b.getInt32()
          , Aa = b.getInt32()
          , r = b.getInt32()
          , t = b.getUint8();
        b.getBinary(32);
        var u = b.getBinary(8)
          , v = b.getBinary(8);
        b.getInt16();
        b.getInt8();
        f === Ia.Private && 0 != X(oa, d) && (n = e);
        b = ha(m).getTime() / 1E3 | 0;
        e = G(a);
        c = G(c);
        return {
            id: e,
            author_id: H(d),
            login: null,
            name: n,
            date: b,
            lastUpdateBin: m,
            lastAccessBin: v,
            lastSeenBin: u,
            lastUpdateHex: G(m),
            lastAccessHex: G(v),
            lastSeenHex: G(u),
            date_text: GetDateTimeLocal(b),
            avatar_url: t ? "https://" + da + "/avatar/0x" + T(a) + "?" + c : null,
            type: f,
            avatarIsPresent: t,
            is_public: g,
            is_limited: h,
            is_closed: l,
            countMembers: p + q + Aa + r
        }
    }
    function t(a, b) {
        for (b = []; !a.isEnded(); ) {
            var c = a.getReader32()
              , d = p(c);
            d.is_verified = !0;
            b[b.length] = d;
            var e = "";
            if (!c.isEnded()) {
                c = c.getReader32();
                c.getBinary(8);
                c.getBinary(8);
                c.getString16();
                c.getUint32();
                var f = c.getUint8();
                c.getBinary(8);
                f == R.Text ? (c = c.getReader32(),
                0 < c.Size() ? (e = c.getBinary(c.getUint32()),
                c = c.getReader32(),
                e = wa(e, c),
                e = e.replace(/(?:\r\n|\r|\n)/g, " ")) : e = "[EMPTY TEXT]") : f == R.File && (c = c.getReader32(),
                0 < c.Size() ? (c.getBinary(32),
                c.getInt32(),
                c.getString16(),
                c = c.getString16(),
                e += c) : e = "[EMPTY FILE]");
                d.text = e
            }
        }
        return b
    }
    function n(a) {
        for (var b = []; !a.isEnded(); ) {
            var c = p(a);
            b[b.length] = c
        }
        return b
    }
    function r(a) {
        for (var b = a.getBinary(8), c = []; !a.isEnded(); ) {
            var d = {}
              , e = a.getReader32()
              , f = e.getReader32()
              , g = f.getBinary(8);
            d.idHex = G(g);
            d.id = H(g);
            d.name = f.getString16();
            d.login = f.getString16();
            d.avatar = f.getString16();
            d.is_deleted = f.getUint8();
            d.chat_id = G(b);
            e.getUint16();
            0 !== X(oa, g) && (c[c.length] = d)
        }
        return c
    }
    function q(a) {
        var b = a.getReader32();
        a = b.getBinary(8);
        var c = b.getBinary(8);
        b.getBinary(8);
        b.getString16();
        var d = b.getUint8();
        b.getUint8();
        b.getUint8();
        b.getUint8();
        var e = b.getBinary(8);
        b.getBinary(8);
        var f = b.getString16();
        b.getString16();
        b.getString16();
        b.getString16();
        b.getInt32();
        b.getInt32();
        b.getInt32();
        b.getInt32();
        var g = b.getUint8();
        b.getBinary(8);
        b = ha(e).getTime() / 1E3 | 0;
        var h = G(a);
        c = G(c);
        return [{
            id: h,
            login: null,
            name: f,
            date: b,
            lastUpdateBin: e,
            lastAccessBin: null,
            date_text: GetDateTimeLocal(b),
            avatar_url: g ? "https://" + da + "/avatar/0x" + T(a) + "?" + c : null,
            tupe: d,
            avatarIsPresent: g
        }]
    }
    function u(a, b) {
        a.getUint8();
        b = a.getBinary(8);
        a.getBinary(8);
        a.getInt16();
        to_download = [];
        for (var c = []; !a.isEnded(); ) {
            var d = a.getReader32()
              , e = d.getBinary(8)
              , f = d.getBinary(8)
              , g = d.getString16()
              , h = d.getUint32()
              , l = d.getUint8()
              , m = d.getBinary(8)
              , n = ha(e).getTime() / 1E3 | 0;
            (d = v(e, {
                id: G(e),
                parent_id: G(b),
                author_name: g,
                author_id: H(f),
                date: n,
                content: "",
                seq: h,
                record_id: m,
                type: l
            }, d)) && 3 !== l && (c[c.length] = d)
        }
        return c
    }
    function w(a) {
        var b = a.getBinary(8);
        a = a.getReader32();
        var c = a.getBinary(8)
          , d = a.getBinary(8)
          , e = a.getString16()
          , f = a.getUint32()
          , g = a.getUint8();
        a.getBinary(8);
        var h = ha(c).getTime() / 1E3 | 0;
        return [v(c, {
            id: G(c),
            parent_id: G(b),
            author_name: e,
            author_id: H(d),
            date: h,
            content: "",
            seq: f,
            type: g
        }, a)]
    }
    function v(a, b, c) {
        switch (b.type) {
        case R.Text:
            var d = c.getReader32();
            0 < d.Size() && (a = d.getBinary(d.getUint32()),
            c = d.getReader32(),
            b.content = wa(a, c));
            break;
        case R.Service:
            d = c.getReader32();
            if (0 < d.Size())
                switch (b.serviceChange = d.getUint8(),
                b.is_system = !0,
                b.serviceChange) {
                case za.UserAdd:
                case za.UserRemove:
                    b.serviceUserId = H(d.getBinary(8));
                    b.serviceUserName = d.getString16();
                    break;
                case za.IsPublic:
                case za.IsLimited:
                case za.IsClosed:
                    b.serviceChangeValue = d.getUint8();
                    break;
                case za.Name:
                case za.ChatCreate:
                    b.serviceChatName = d.getString16()
                }
            break;
        case R.File:
            d = c.getReader32();
            if (0 < d.Size()) {
                c = d.getBinary(32);
                var e = d.getInt32()
                  , f = d.getString16()
                  , g = d.getString16()
                  , h = void 0
                  , l = void 0
                  , m = 128
                  , n = 128;
                d.isEnded() || (d.getUint16(),
                l = d.getInt8(),
                m = d.getInt32(),
                n = d.getInt32(),
                h = d.getBlob(d.Size(), "image/png"));
                d = "https://" + da + "/attach/0x";
                a = G(a.split("").reverse().join(""));
                b.attachments = [{
                    name: g,
                    url: d + (16 != a.length ? void 0 : a),
                    size_text: Core.geFileSizeString(e),
                    preview_url: h,
                    type: l,
                    mimeType: f,
                    width: m,
                    height: n,
                    hash: c
                }]
            }
            break;
        default:
            return null
        }
        return b
    }
    function C(a) {
        for (var b = []; !a.isEnded(); ) {
            var c = {}
              , d = a.getReader32()
              , e = d.getBinary(8);
            c.idHex = G(e);
            c.id = H(e);
            c.name = d.getString16();
            c.login = d.getString16();
            c.avatar = d.getString16();
            c.is_deleted = d.getUint8();
            b[b.length] = c
        }
        return b
    }
    function A(a) {
        if (N) {
            a = new Uint8Array(a.data);
            var b = new Uint8Array(N.byteLength + a.byteLength);
            b.set(N, 0);
            b.set(a, N.byteLength);
            N = b
        } else
            N = new Uint8Array(a.data);
        var c = new DataView(N.buffer);
        if (!(13 > c.byteLength)) {
            a = c.getUint8(0);
            b = (new z(c,1,8)).getBinary(8);
            var d = c.getUint32(9, !0);
            if (!(c.byteLength < d + 13)) {
                c = new z(c,13);
                N = N.byteLength > d + 13 ? N.slice(d + 13) : null;
                try {
                    if (a === L.EventsGet)
                        var e = c.getInt8();
                    else if (a !== L.NotifyEvent)
                        if (oa)
                            if (a == L.NotifyChatUpdate) {
                                var f = q(c);
                                window.ChatsData.OnChatsUpdate && window.ChatsData.OnChatsUpdate(f)
                            } else if (a == L.NotifyLastSeen) {
                                var g = c.getBinary(8);
                                c.getBinary(8)
                            } else if (a == L.NotifyMessageAdd)
                                f = w(c),
                                window.ChatsData.OnMessageAdd && window.ChatsData.OnMessageAdd(f),
                                window.ChatsData.OnMessageAdd2 && window.ChatsData.OnMessageAdd2(f);
                            else {
                                e = c.getInt8();
                                var h = ja(e);
                                0 < e || console.log("error: " + h + " [" + e + "]");
                                if (0 < e)
                                    switch (a) {
                                    case L.ChatsGet:
                                        var l = t(c, e);
                                        l.sort(function(a, b) {
                                            return X(b.lastUpdateBin, a.lastUpdateBin)
                                        });
                                        if (0 === X(b, "\x00\x00\x00\x00ÿÿÿÿ")) {
                                            ea.VerifyChats(l);
                                            break
                                        }
                                        ea.Add(l);
                                        console.log("from net: " + l.length);
                                        void 0 != ya && (l = ea.Get(ya),
                                        console.log("from storage: " + l.length));
                                        M && M(l);
                                        break;
                                    case L.ChatUsers:
                                        var m = r(c);
                                        ea.AddUsersInfo(m);
                                        xa && xa(m);
                                        break;
                                    case L.MessagesGet:
                                        var p = u(c, e);
                                        Z && Z(p, 2 === e);
                                        break;
                                    case L.ChatGet:
                                        l = t(c, e);
                                        ea.Add(l, !0);
                                        Y && Y(l);
                                        break;
                                    case L.ChatNew:
                                        g = c.getBinary(8);
                                        la && la(G(g));
                                        break;
                                    case L.UsersSearch:
                                        m = C(c, e);
                                        Oa && Oa(m);
                                        break;
                                    case L.MessageAdd:
                                        var Aa = c.getBinary(8);
                                        if (qa && "\x00\x00\x00\x00\x00\x00\x00\x00" == Aa) {
                                            var v = new FileReader;
                                            v.onloadend = function(a) {
                                                a = a.target.result;
                                                for (var b = 0; b < a.byteLength; ) {
                                                    var c = Math.min(131072, a.byteLength - b);
                                                    D(L.Upload, [{
                                                        Uint32: b
                                                    }], c);
                                                    if (1E3 < c)
                                                        for (var d = 0, e = 1; 10 >= e; e++) {
                                                            var f = parseInt(e * c / 10, 10);
                                                            d = a.slice(b + d, b + f);
                                                            U.send(d);
                                                            d = f
                                                        }
                                                    else
                                                        U.send(a.slice(b));
                                                    b += c
                                                }
                                                qa = null
                                            }
                                            ;
                                            v.readAsArrayBuffer(qa)
                                        } else
                                            qa && (qa = null,
                                            console && console.log && console.log("file skipped by hash"));
                                        break;
                                    case L.ChatAvatarDownload:
                                        var A = c.getBlob(d - 1, "image/png");
                                        ia && ia(G(b), A);
                                        break;
                                    case L.ChatsSearch:
                                        l = n(c);
                                        Ha && Ha(l);
                                        break;
                                    case L.ChatAccessSet:
                                        var B = G(c.getBinary(8));
                                        G(c.getBinary(8));
                                        var Ga = c.getUint8();
                                        c.getUint16();
                                        Ga && 0 === X(b, "\x00\x00\x00\x00îîîî") ? (ea.RemoveChat(B),
                                        ua && ua(B)) : Pa && Pa(B)
                                    }
                            }
                        else
                            a == L.UserAuthorize && (e = c.getInt8(),
                            h = ja(e),
                            0 < e ? (oa = c.getBinary(8),
                            c.getString16(),
                            D(L.NotificationsEnable, [{
                                Uint8: 1
                            }]),
                            Ajax.post("https://" + da + "/auth", {
                                token: O
                            }, !1, null, !1, !0),
                            ma = 2E3) : console.log("auth failed: " + h))
                } catch (ba) {
                    h = "",
                    void 0 === ba ? h = "undefined error" : "object" === typeof ba ? "stack"in ba && ba.stack ? ("message"in ba && ba.message && (h += ba.message + "\r\n"),
                    h += ba.stack) : "message"in ba && ba.message ? (h = ba.message,
                    "filename"in ba && ba.filename && (h += ba.filename),
                    "lineno"in ba && ba.lineno && (h += ":" + ba.lineno),
                    "columnNumber"in ba && ba.columnNumber && (h += ":" + ba.columnNumber)) : h = "type"in ba && ba.type ? ba.type : ba : h = ba,
                    console.log(h)
                }
            }
        }
    }
    function D(a, b, c, d) {
        function e(a) {
            if (void 0 === a)
                return 0;
            if ("string" === typeof a)
                return a.length;
            if (Array.isArray(a)) {
                for (var b = 0, c = 0; c < a.length; ++c)
                    b += e(a[c]);
                return b
            }
            if ("String"in a)
                return E(a.String).length;
            if ("String8"in a)
                return E(a.String8).length + 1;
            if ("String16"in a)
                return E(a.String16).length + 2;
            if ("String32"in a)
                return E(a.String32).length + 4;
            if ("Uint8"in a || "Int8"in a)
                return 1;
            if ("Uint16"in a || "Int16"in a)
                return 2;
            if ("Uint32"in a || "Int32"in a)
                return 4
        }
        function f(a, b, c) {
            if ("string" === typeof c)
                for (var d = 0; d < c.length; d++)
                    a.setUint8(b++, c.charCodeAt(d));
            else if (Array.isArray(c))
                for (d = 0; d < c.length; ++d)
                    b = f(a, b, c[d]);
            else if ("String"in c || "String8"in c || "String16"in c || "String32"in c)
                for (("String8"in c) ? (c = E(c.String8),
                a.setUint8(b++, c.length)) : ("String16"in c) ? (c = E(c.String16),
                a.setUint16(b, c.length, !0),
                b += 2) : ("String32"in c) ? (c = E(c.String32),
                a.setUint32(b, c.length, !0),
                b += 4) : c = E(c.String),
                d = 0; d < c.length; d++)
                    a.setUint8(b++, c.charCodeAt(d));
            else
                "Uint8"in c ? a.setUint8(b++, c.Uint8) : "Int8"in c ? a.setInt8(b++, c.Int8) : "Uint16"in c ? (a.setUint16(b, c.Uint16, !0),
                b += 2) : "Int16"in c ? (a.setInt16(b, c.Int16, !0),
                b += 2) : "Uint32"in c ? (a.setUint32(b, c.Uint32, !0),
                b += 4) : "Int32"in c && (a.setInt32(b, c.Int32, !0),
                b += 4);
            return b
        }
        if (U.readyState !== U.OPEN)
            return setTimeout(function() {
                D(a, b, c, d)
            }, 100),
            !1;
        var g = 13 + (b ? e(b) : 0)
          , h = new ArrayBuffer(g)
          , l = new DataView(h);
        l.setUint8(0, a);
        d ? f(l, 1, d) : (l.setUint32(1, 287454020, !1),
        l.setUint32(5, 1432778632, !1));
        l.setUint32(9, g + (c ? c : 0) - 13, !0);
        f(l, 13, b);
        try {
            return U.send(h),
            !0
        } catch (Fa) {
            return console.log("Send message, error: " + Fa.message),
            U && 1 === U.readyState || (console.log("Socked closed: try reconnect"),
            m()),
            !1
        }
    }
    function z(a, b, c) {
        this.dataview = a;
        this.pos = b;
        this.pos_end = null != c ? b + c : a.byteLength
    }
    function E(a) {
        for (var b = [], c = 0, d = 0; d < a.length; d++) {
            var e = a.charCodeAt(d);
            128 > e ? b[c++] = String.fromCharCode(e) : (2048 > e ? b[c++] = String.fromCharCode(e >> 6 | 192) : (55296 === (e & 64512) && d + 1 < a.length && 56320 == (a.charCodeAt(d + 1) & 64512) ? (e = 65536 + ((e & 1023) << 10) + (a.charCodeAt(++d) & 1023),
            b[c++] = String.fromCharCode(e >> 18 | 240),
            b[c++] = String.fromCharCode(e >> 12 & 63 | 128)) : b[c++] = String.fromCharCode(e >> 12 | 224),
            b[c++] = String.fromCharCode(e >> 6 & 63 | 128)),
            b[c++] = String.fromCharCode(e & 63 | 128))
        }
        return b.join("")
    }
    function F(a) {
        for (var b, c, d = [], e = 0, f = 0; e < a.length; ) {
            var g = a.charCodeAt(e++);
            if (128 > g)
                d[f++] = String.fromCharCode(g);
            else if (191 < g && 224 > g)
                c = a.charCodeAt(e++),
                d[f++] = String.fromCharCode((g & 31) << 6 | c & 63);
            else if (239 < g && 365 > g) {
                c = a.charCodeAt(e++);
                b = a.charCodeAt(e++);
                var h = a.charCodeAt(e++);
                b = ((g & 7) << 18 | (c & 63) << 12 | (b & 63) << 6 | h & 63) - 65536;
                d[f++] = String.fromCharCode(55296 + (b >> 10));
                d[f++] = String.fromCharCode(56320 + (b & 1023))
            } else
                c = a.charCodeAt(e++),
                b = a.charCodeAt(e++),
                d[f++] = String.fromCharCode((g & 15) << 12 | (c & 63) << 6 | b & 63)
        }
        return d.join("")
    }
    function H(a) {
        for (var b = 0, c = a.length; 0 < c; c--)
            b = 256 * b + a.charCodeAt(c - 1);
        return b
    }
    function J(a) {
        for (var b = "", c = 0; 8 > c; c++)
            b += String.fromCharCode(a % 256),
            a = a / 256 | 0;
        return b
    }
    function G(a) {
        for (var b = "", c = 0; c < a.length; c++) {
            var d = a.charCodeAt(c) & 15
              , e = a.charCodeAt(c) >> 4 & 15;
            b += 10 > e ? String.fromCharCode(e + 48) : String.fromCharCode(e + 55);
            b += 10 > d ? String.fromCharCode(d + 48) : String.fromCharCode(d + 55)
        }
        return b
    }
    function I(a) {
        if (a.length % 2)
            return "";
        for (var b = "", c = 0; c + 1 < a.length; c += 2) {
            var d = ca(a.charCodeAt(c + 0))
              , e = ca(a.charCodeAt(c + 1));
            b += String.fromCharCode(16 * d + e)
        }
        return b
    }
    function T(a) {
        for (var b = "", c = a.length - 1; 0 <= c; c--) {
            var d = a.charCodeAt(c) & 15
              , e = a.charCodeAt(c) >> 4 & 15;
            b += 10 > e ? String.fromCharCode(e + 48) : String.fromCharCode(e + 55);
            b += 10 > d ? String.fromCharCode(d + 48) : String.fromCharCode(d + 55)
        }
        return b
    }
    function ha(a) {
        for (var b = 0, c = 0; c < a.length; c++)
            b = 256 * b + a.charCodeAt(a.length - c - 1);
        return new Date(b / 1E4 - 116444736E5)
    }
    function ca(a) {
        return 65 <= a && 70 >= a ? a - 55 : 97 <= a && 102 >= a ? a - 87 : 48 <= a && 57 >= a ? a - 48 : 0
    }
    function X(a, b) {
        for (var c = Math.min(a.length, b.length); 0 < c; c--) {
            if (a.charCodeAt(c - 1) < b.charCodeAt(c - 1))
                return -1;
            if (a.charCodeAt(c - 1) > b.charCodeAt(c - 1))
                return 1
        }
        return a.length < b.length ? -1 : a.length > b.length ? 1 : 0
    }
    function ja(a) {
        for (var b in Da)
            if (Da[b] == a)
                return b;
        return "[UNKNOWN_RESULT]"
    }
    function wa(a, b) {
        if (b)
            for (; !b.isEnded(); ) {
                b.getUint8();
                b.getVarLen();
                b.getVarLen();
                var c = b.getVarLen();
                b.getBinary(c)
            }
        return F(a)
    }
    var O = a, B = c, U = void 0, Ba = !1, da = "msg1.mql5.com", ma = 2E3, oa, N;
    H(f);
    b && (da = b);
    var M = void 0
      , xa = void 0
      , ia = void 0
      , Z = void 0
      , la = void 0
      , Y = void 0
      , Oa = void 0
      , Ha = void 0
      , Pa = void 0
      , ua = void 0
      , qa = void 0;
    a = !1;
    var Ca = new Uint8Array([139, 22, 96, 224, 115, 20, 103, 83, 95, 225, 254, 248, 9, 43, 105, 26, 39, 200, 253, 236, 146, 105, 43, 153, 4, 74, 142, 140, 44, 204, 239, 233, 137, 90, 166, 229, 193, 243, 253, 164, 31, 83, 99, 67, 154, 127, 243, 184, 19, 54, 180, 200, 198, 59, 213, 81, 215, 22, 74, 96, 224, 79, 170, 166])
      , ea = new function(a, b) {
        function c() {
            q.removeItem("mql5_chats_tok");
            q.removeItem("mql5_chats");
            q.setItem("mql5_chats", JSON.stringify(m));
            q.setItem("mql5_chats_tok", n + ";" + p);
            window.ChatsData && window.ChatsData.GetChats(e(), 100)
        }
        function d() {
            var a = {}, b;
            for (b in m)
                if (m.hasOwnProperty(b)) {
                    var c = b;
                    var d = void 0;
                    var e = m[b];
                    if (null == e || "object" != typeof e)
                        d = e;
                    else {
                        var f = e.constructor();
                        e.lastUpdateHex = G(e.lastUpdateBin || "\x00\x00\x00\x00\x00\x00\x00\x00");
                        e.lastAccessHex = G(e.lastAccessBin || "\x00\x00\x00\x00\x00\x00\x00\x00");
                        for (d in e)
                            "lastAccessBin" !== d && "lastUpdateBin" !== d && ("text" === d ? f[d] = e[d] : e.hasOwnProperty(d) && (f[d] = e[d]));
                        d = f
                    }
                    a[c] = d
                }
            q && q.supported && (a = JSON.stringify(a),
            q.setItem("mql5_chats", t.encode(a)),
            r = !1)
        }
        function e() {
            var a = "\x00\x00\x00\x00\x00\x00\x00\x00";
            if (!m)
                return a;
            for (var b in m)
                if (m.hasOwnProperty(b)) {
                    var c = m[b];
                    !0 !== c.notTrusted && 0 < X(c.lastUpdateBin, a) && (a = c.lastUpdateBin)
                }
            return a
        }
        function f(a) {
            a || (a = "\x00\x00\x00\x00\x00\x00\x00\x00");
            var b = [], c;
            for (c in m)
                if (m.hasOwnProperty(c)) {
                    var d = m[c];
                    d.notTrusted || 0 <= X(d.lastUpdateBin, a) && (b[b.length] = d)
                }
            b.sort(function(a, b) {
                return X(b.lastUpdateBin, a.lastUpdateBin)
            });
            return b
        }
        function g() {
            var a = 0, b;
            for (b in m)
                if (m.hasOwnProperty(b)) {
                    var c = m[b];
                    c.notTrusted || window.ChatsData.IsChatUnread(c) && a++
                }
            return a
        }
        function h(a, b) {
            if (a.length) {
                for (var c = 0; c < a; c++)
                    if (h(a[c], b))
                        return !0;
                return !1
            }
            c = a.login.toLowerCase();
            a = a.name.toLowerCase();
            for (var d = 0; d < b.length; d++)
                if (c.startsWith(b[d]) || a.startsWith(b[d]))
                    return !0;
            return !1
        }
        function l(a) {
            if (a && a.length) {
                for (var b = !1, c = {}, e = 0; e < a.length; e++)
                    c[a[e].id] = a[e];
                for (var f in m)
                    m.hasOwnProperty(f) && !c[f] && (delete m[f],
                    b = !0);
                c = [];
                for (e = 0; e < a.length; e++)
                    (f = m[a[e].id]) ? c.push(f) : (c.push(a[e]),
                    b = !0);
                b && (c.sort(function(a, b) {
                    return X(b.lastUpdateBin, a.lastUpdateBin)
                }),
                window.ChatsData.OnChatsVerified(c),
                d())
            }
        }
        var m = {}
          , n = a
          , p = b
          , q = window.globalStorage(window.globalStorageDomain, window.globalStoragePage)
          , r = !1;
        window.setInterval(function() {
            r && d()
        }, 5E3);
        var t = {
            _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
            encode: function(a) {
                var b = ""
                  , c = 0;
                for (a = t._utf8_encode(a); c < a.length; ) {
                    var d = a.charCodeAt(c++);
                    var e = a.charCodeAt(c++);
                    var f = a.charCodeAt(c++);
                    var g = d >> 2;
                    d = (d & 3) << 4 | e >> 4;
                    var h = (e & 15) << 2 | f >> 6;
                    var l = f & 63;
                    isNaN(e) ? h = l = 64 : isNaN(f) && (l = 64);
                    b = b + this._keyStr.charAt(g) + this._keyStr.charAt(d) + this._keyStr.charAt(h) + this._keyStr.charAt(l)
                }
                return b
            },
            decode: function(a) {
                var b = ""
                  , c = 0;
                for (a = a.replace(/[^A-Za-z0-9+/=]/g, ""); c < a.length; ) {
                    var d = this._keyStr.indexOf(a.charAt(c++));
                    var e = this._keyStr.indexOf(a.charAt(c++));
                    var f = this._keyStr.indexOf(a.charAt(c++));
                    var g = this._keyStr.indexOf(a.charAt(c++));
                    d = d << 2 | e >> 4;
                    e = (e & 15) << 4 | f >> 2;
                    var h = (f & 3) << 6 | g;
                    b += String.fromCharCode(d);
                    64 !== f && (b += String.fromCharCode(e));
                    64 !== g && (b += String.fromCharCode(h))
                }
                return b = t._utf8_decode(b)
            },
            _utf8_encode: function(a) {
                a = a.replace(/rn/g, "n");
                for (var b = "", c = 0; c < a.length; c++) {
                    var d = a.charCodeAt(c);
                    128 > d ? b += String.fromCharCode(d) : (127 < d && 2048 > d ? b += String.fromCharCode(d >> 6 | 192) : (b += String.fromCharCode(d >> 12 | 224),
                    b += String.fromCharCode(d >> 6 & 63 | 128)),
                    b += String.fromCharCode(d & 63 | 128))
                }
                return b
            },
            _utf8_decode: function(a) {
                for (var b = "", c = 0, d, e, f; c < a.length; )
                    d = a.charCodeAt(c),
                    128 > d ? (b += String.fromCharCode(d),
                    c++) : 191 < d && 224 > d ? (e = a.charCodeAt(c + 1),
                    b += String.fromCharCode((d & 31) << 6 | e & 63),
                    c += 2) : (e = a.charCodeAt(c + 1),
                    f = a.charCodeAt(c + 2),
                    b += String.fromCharCode((d & 15) << 12 | (e & 63) << 6 | f & 63),
                    c += 3);
                return b
            }
        };
        (function() {
            q.supported && window.JSON && q.getItem("mql5_chats_tok", function(a, b) {
                "mql5_chats_tok" === a && (b && b.length ? (a = b.split(";"),
                2 !== a.length ? c() : (b = a[1],
                a[0] !== n || b != p ? c() : q.getItem("mql5_chats", function(a, b) {
                    if ("mql5_chats" === a && b && b.length) {
                        a = void 0;
                        try {
                            b = t.decode(b),
                            a = window.JSON.parse(b)
                        } catch (Xa) {}
                        if (a) {
                            b = (new Date).getTime() / 1E3;
                            for (var c in a)
                                if (a.hasOwnProperty(c) && (a[c].lastUpdateBin = I(a[c].lastUpdateHex),
                                a[c].lastAccessBin = a[c].lastAccessHex ? I(a[c].lastAccessHex) : "\x00\x00\x00\x00\x00\x00\x00\x00",
                                a[c].user_info && a[c].user_info.length))
                                    for (var d = 0; d < a[c].user_info.length; d++)
                                        if (3600 < b - a[c].user_info[d].checkDate) {
                                            a[c].user_info = void 0;
                                            break
                                        }
                            m = a;
                            window.ChatsData && (window.ChatsData.GetChats(e(), 100),
                            window.ChatsData.GetAllChatsRaw())
                        }
                    }
                }))) : c())
            })
        }
        )();
        this.Get = function(a) {
            return f(a)
        }
        ;
        this.GetLastChatUpdateBin = function() {
            return e()
        }
        ;
        this.Add = function(a, b) {
            if (a && a.length) {
                for (var c = 0; c < a.length; c++) {
                    var d = m[a[c].id];
                    !d && b && (a[c].notTrusted = !0);
                    d && d.user_info && a[c].lastUpdateHex === d.lastUpdateHex && (a[c].user_info = d.user_info);
                    m[a[c].id] = a[c]
                }
                r = !0
            }
            window.MessagesWidget && window.MessagesWidget.SetUnreadChatsCount(g())
        }
        ;
        this.GetUnreadCount = function() {
            return g()
        }
        ;
        this.AddUsersInfo = function(a) {
            if (a && a.length) {
                for (var b = (new Date).getTime() / 1E3, c = 0, d = 0; d < a.length; d++) {
                    var e = a[d];
                    e.checkDate = b;
                    var f = m[e.chat_id];
                    f && (f.user_info || (f.user_info = []),
                    f.user_info.push(e),
                    c++)
                }
                0 < c && (r = !0)
            }
        }
        ;
        this.UsersSearch = function(a, b) {
            var c = [];
            if ((a = a.toLocaleLowerCase()) && a.length) {
                a.split(" ");
                for (var d in m)
                    if (m.hasOwnProperty(d)) {
                        var e = m[d];
                        if (1 === e.type && e.user_info && h(e.user_info, a) && (c[c.length] = e.user_info,
                        c.length >= b))
                            break
                    }
                b = c
            } else
                b = [];
            return b
        }
        ;
        this.ChannelSearch = function(a, b) {
            var c = [];
            if ((a = a.toLocaleLowerCase()) && a.length) {
                a = a.split(" ");
                for (var d in m)
                    if (m.hasOwnProperty(d)) {
                        var e = m[d], f;
                        if (f = 3 === e.type)
                            b: {
                                f = a;
                                for (var g = e.name.toLowerCase(), h = 0; h < f.length; h++)
                                    if (g.startsWith(f[h])) {
                                        f = !0;
                                        break b
                                    }
                                f = !1
                            }
                        if (f && (c[c.length] = e,
                        c.length >= b))
                            break
                    }
                b = c
            } else
                b = [];
            return b
        }
        ;
        this.ChatExists = function(a) {
            a: {
                for (var b in m)
                    if (m.hasOwnProperty(b) && m[b].id === a) {
                        a = !0;
                        break a
                    }
                a = !1
            }
            return a
        }
        ;
        this.RemoveChat = function(a) {
            delete m[a];
            d()
        }
        ;
        this.VerifyChats = function(a) {
            return l(a)
        }
        ;
        this.GetTrustedChatById = function(a) {
            return (a = m[a]) && !a.notTrusted ? a : null
        }
    }
    (d,e);
    this.GetStorage = function() {
        return ea
    }
    ;
    var ya = void 0;
    window.WebSocket && (a = !0);
    this.Close = function() {
        U.close()
    }
    ;
    if (this.WsSupport = a) {
        mqGlobal.AddOnReady(function() {
            h()
        });
        mqGlobal.AddOnLoad(function() {
            if (document.getElementById("chatContainer")) {
                var a = document.getElementById("left-panel")
                  , b = document.getElementById("footer");
                if (a && b) {
                    var c = a.getBoundingClientRect();
                    b = b.getBoundingClientRect();
                    a.style.minHeight = c.height + (window.innerHeight - b.bottom) + "px"
                }
            }
        });
        this.IsChatUnread = function(a) {
            return 0 > X(a.lastAccessBin || "\x00\x00\x00\x00\x00\x00\x00\x00", a.lastUpdateBin || "\x00\x00\x00\x00\x00\x00\x00\x00")
        }
        ;
        this.CreatePrivateChat = function(a, b) {
            a && (a = J(a),
            D(L.ChatNew, [{
                Uint8: Ia.Private
            }, a]),
            la = b)
        }
        ;
        this.GetChat = function(a, b, c) {
            a && (a = I(a),
            D(L.ChatGet, [a, {
                Uint8: 1
            }]),
            Y = b)
        }
        ;
        this.AddText = function(a, b) {
            a = a ? I(a) : "\x00\x00\x00\x00\x00\x00\x00\x00";
            D(L.MessageAdd, [a, {
                Uint8: 1
            }, {
                String32: b
            }, {
                Uint32: 0
            }])
        }
        ;
        this.AddFile = function(a, b) {
            a = a ? I(a) : "\x00\x00\x00\x00\x00\x00\x00\x00";
            l(a, b)
        }
        ;
        this.GetMessages = function(a, b, c) {
            a = a ? I(a) : "\x00\x00\x00\x00\x00\x00\x00\x00";
            b = b ? I(b) : "\x00\x00\x00\x00\x00\x00\x00\x00";
            Z = c;
            c = 0;
            0 === X(b, "\x00\x00\x00\x00\x00\x00\x00\x00") && (c = 0);
            D(L.MessagesGet, [a, {
                Uint8: c
            }, b, {
                Uint32: 4096
            }])
        }
        ;
        this.GetChatUsers = function(a, b) {
            a = a ? I(a) : "\x00\x00\x00\x00\x00\x00\x00\x00";
            xa || (xa = b);
            D(L.ChatUsers, [a, "\x00\x00\x00\x00\x00\x00\x00\x00", {
                Uint32: 0
            }])
        }
        ;
        this.GetChatAvatar = function(a, b) {
            a = a ? I(a) : "\x00\x00\x00\x00\x00\x00\x00\x00";
            ia || (ia = b);
            D(L.ChatAvatarDownload, [a, {
                Uint32: 0
            }, {
                Uint32: 0
            }], null, a)
        }
        ;
        this.GetChats = function(a, b, c) {
            a = a ? I(a) : "\x00\x00\x00\x00\x00\x00\x00\x00";
            M || (M = c);
            c = a;
            var d = ea.GetLastChatUpdateBin();
            a = c;
            0 < X(d, c) ? (ya = c,
            c = d) : ya = void 0;
            d = 1;
            0 === X(c, "\x00\x00\x00\x00\x00\x00\x00\x00") && (d = 0);
            D(L.ChatsGet, [{
                Uint8: d
            }, c, {
                Uint8: 1
            }, {
                Uint32: b
            }]) || (M && M(ea.Get(a)),
            window.MessagesWidget && window.MessagesWidget.SetUnreadChatsCount(ea.GetUnreadCount()))
        }
        ;
        this.GetAllChatsRaw = function() {
            D(L.ChatsGet, [{
                Uint8: 0
            }, "\x00\x00\x00\x00\x00\x00\x00\x00", {
                Uint8: 1
            }, {
                Uint32: 100
            }], null, "\x00\x00\x00\x00ÿÿÿÿ")
        }
        ;
        this.UsersSearch = function(a, b, c) {
            c && (Oa = c);
            D(L.UsersSearch, [{
                String16: a
            }, {
                Uint32: b
            }])
        }
        ;
        this.UsersSearchLocal = function(a, b) {
            return ea.UsersSearch(a, b)
        }
        ;
        this.ChannelSearch = function(a, b, c) {
            c && (Ha = c);
            D(L.ChatsSearch, [{
                String16: a
            }, {
                Uint32: b
            }])
        }
        ;
        this.ChannelSearchLocal = function(a, b) {
            return ea.ChannelSearch(a, b)
        }
        ;
        this.ChatExists = function(a) {
            return ea.ChatExists(a)
        }
        ;
        this.RemoveChat = function(a) {
            return ea.RemoveChat(a)
        }
        ;
        this.GetUnreadCount = function() {
            return ea.GetUnreadCount()
        }
        ;
        this.GetChatLocal = function(a) {
            return ea.GetTrustedChatById(a)
        }
        ;
        this.AssignChatUsers = function(a, b) {
            return ea.AssignChatUsers(a, b)
        }
        ;
        this.Subscribe = function(a, b, c, d) {
            a && (a = I(a),
            b = J(b),
            d && (Pa = d),
            D(L.ChatAccessSet, [a, b, {
                Uint8: c ? 0 : 1
            }, {
                Uint16: 0
            }]))
        }
        ;
        this.DeleteChat = function(a, b, c) {
            a && b && (a = I(a),
            b = J(b),
            c && (ua = c),
            D(L.ChatAccessSet, [a, b, {
                Uint8: 1
            }, {
                Uint16: 0
            }], null, "\x00\x00\x00\x00îîîî"))
        }
        ;
        this.OnChatsVerified = function(a) {
            M && M(a, !0)
        }
        ;
        z.prototype.Size = function() {
            return this.pos_end - this.pos
        }
        ;
        z.prototype.advance = function(a) {
            if (this.pos + a > this.pos_end)
                throw "Buffer overflow";
            this.pos += a
        }
        ;
        z.prototype.isEnded = function() {
            return this.pos >= this.pos_end
        }
        ;
        z.prototype.getInt32 = function() {
            if (this.pos + 4 > this.pos_end)
                throw "Buffer overflow";
            var a = this.dataview.getInt32(this.pos, !0);
            this.pos += 4;
            return a
        }
        ;
        z.prototype.getUint32 = function() {
            if (this.pos + 4 > this.pos_end)
                throw "Buffer overflow";
            var a = this.dataview.getUint32(this.pos, !0);
            this.pos += 4;
            return a
        }
        ;
        z.prototype.getInt16 = function() {
            if (this.pos + 2 > this.pos_end)
                throw "Buffer overflow";
            var a = this.dataview.getInt16(this.pos, !0);
            this.pos += 2;
            return a
        }
        ;
        z.prototype.getUint16 = function() {
            if (this.pos + 2 > this.pos_end)
                throw "Buffer overflow";
            var a = this.dataview.getUint16(this.pos, !0);
            this.pos += 2;
            return a
        }
        ;
        z.prototype.getInt8 = function() {
            if (this.pos >= this.pos_end)
                throw "Buffer overflow";
            return this.dataview.getInt8(this.pos++)
        }
        ;
        z.prototype.getUint8 = function() {
            if (this.pos >= this.pos_end)
                throw "Buffer overflow";
            return this.dataview.getUint8(this.pos++)
        }
        ;
        z.prototype.getString8 = function() {
            var a = this.getUint8();
            a = this.getBinary(a);
            return F(a)
        }
        ;
        z.prototype.getString16 = function() {
            var a = this.getUint16();
            a = this.getBinary(a);
            return F(a)
        }
        ;
        z.prototype.getString32 = function() {
            var a = this.getUint32();
            a = this.getBinary(a);
            return F(a)
        }
        ;
        z.prototype.getBinary = function(a) {
            if (0 == a)
                return "";
            if (this.pos + a > this.pos_end)
                throw "Buffer overflow";
            var b = new Uint8Array(this.dataview.buffer,this.pos,a);
            b = String.fromCharCode.apply(null, b);
            this.pos += a;
            return b
        }
        ;
        z.prototype.getBlob = function(a, b) {
            if (0 == a)
                return null;
            if (this.pos + a > this.pos_end)
                throw "Buffer overflow";
            b = new Blob([new Uint8Array(this.dataview.buffer,this.pos,a)],{
                type: b
            });
            this.pos += a;
            return URL.createObjectURL(b)
        }
        ;
        z.prototype.getReader32 = function() {
            var a = this.getUint32();
            if (a > this.pos_end - this.pos)
                throw "Buffer overflow";
            var b = new z(this.dataview,this.pos,a);
            this.pos += a;
            return b
        }
        ;
        z.prototype.getReader16 = function() {
            var a = this.getUint16();
            if (a > this.pos_end - this.pos)
                throw "Buffer overflow";
            var b = new z(this.dataview,this.pos,a);
            this.pos += a;
            return b
        }
        ;
        z.prototype.getVarLen = function() {
            var a = this.getUint8();
            if (a & 128) {
                var b = this.getUint8();
                if (b & 128) {
                    var c = this.getUint8();
                    if (c & 128) {
                        var d = this.getUint8();
                        if (d & 128)
                            throw "Invalid variable-length integer";
                        return ((a & 127) << 21) + ((b & 127) << 14) + ((c & 127) << 7) + d
                    }
                    return ((a & 127) << 14) + ((b & 127) << 7) + c
                }
                return ((a & 127) << 7) + b
            }
            return a
        }
        ;
        this.CompareHex = function(a, b) {
            a = I(a);
            b = I(b);
            return X(a, b)
        }
        ;
        var L = {
            NotifyEvent: 44,
            ChatAccessSet: 51,
            ChatUpdate: 52,
            ChatNew: 53,
            MessageAdd: 55,
            ChatsGet: 56,
            MessagesGet: 57,
            UserAuthorize: 58,
            UserUnauthorize: 59,
            NotifyChatUpdate: 62,
            NotifyMessageAdd: 63,
            NotifyAccessSet: 64,
            SetEventMask: 67,
            NotifyLastSeen: 68,
            Upload: 72,
            Download: 73,
            UsersSearch: 75,
            UsersInfo: 76,
            ChatUsers: 77,
            NotificationsEnable: 78,
            BansGet: 81,
            BanSet: 82,
            ChatsSearch: 83,
            FriendsGet: 84,
            FriendSet: 85,
            EventsGet: 86,
            EventGet: 87,
            ChatAvatarSet: 88,
            ChatAvatarDownload: 89,
            PrivateMessagesGet: 90,
            InviteLinkFind: 91,
            ChatLastAccessSet: 92,
            ChatMute: 93,
            ChatGet: 94,
            MessageGet: 95
        }
          , Da = {
            Fail: -1,
            FailAccessDenied: -2,
            FailInvalidParams: -3,
            FailInvalidRequest: -4,
            FailInvalidVersion: -5,
            FailDisconnect: -6,
            FailOutOfMemory: -7,
            FailSqlError: -8,
            FailBanned: -9,
            FailNetwork: -10,
            FailDataTooBig: -11,
            FailNotFound: -12,
            FailDuplicate: -13,
            Success: 1,
            SuccessPartial: 2,
            SuccessSame: 3,
            SuccessDisconnect: 4
        }
          , R = {
            Text: 1,
            File: 2,
            Delete: 3,
            Service: 4
        }
          , za = {
            UserAdd: 1,
            UserRemove: 2,
            IsPublic: 3,
            IsLimited: 4,
            IsClosed: 5,
            Name: 6,
            ChatCreate: 7
        }
          , Ia = {
            Private: 1,
            Group: 2,
            Channel: 3
        }
    }
}
;function globalStorage(a, b) {
    var c = {}
      , d = null
      , f = !1
      , e = []
      , g = {}
      , h = 0;
    try {
        var m = window.postMessage && window.JSON && "localStorage"in window && null !== window.localStorage
    } catch (n) {
        m = !1
    }
    var l = function(b) {
        d && (g[b.request.id] = b,
        d.contentWindow.postMessage(JSON.stringify(b.request), a))
    }
      , p = function() {
        f = !0;
        if (e.length) {
            for (var a = 0, b = e.length; a < b; a++)
                l(e[a]);
            e = []
        }
    }
      , t = function(b) {
        b.origin === a && (b = JSON.parse(b.data),
        "undefined" != typeof g[b.id] && ("function" === typeof g[b.id].callback && g[b.id].callback(b.key, c.cleaner.execute(b.value)),
        delete g[b.id]))
    };
    c.getItem = function(a, b) {
        m && (a = {
            request: {
                id: ++h,
                type: "get",
                key: a
            },
            callback: b
        },
        f ? l(a) : e.push(a))
    }
    ;
    c.setItem = function(a, b) {
        m && (a = {
            request: {
                id: ++h,
                type: "set",
                key: a,
                value: b
            }
        },
        f ? l(a) : e.push(a))
    }
    ;
    c.removeItem = function(a) {
        m && (a = {
            request: {
                id: ++h,
                type: "unset",
                key: a
            }
        },
        f ? l(a) : e.push(a))
    }
    ;
    c.cleaner = {};
    c.cleaner.safeTags = "A B BLOCKQUOTE BR CAPTION CENTER DIV EM H1 H2 H3 H4 H5 H6 HR I IMG LI OL P PRE SMALL SPAN LABEL STRONG SUP TABLE THEAD TBODY TD TH TR UL U".split(" ");
    c.cleaner.safeAttrs = "class style width height href src title alt border target rel cellspacing cellpadding".split(" ");
    c.cleaner.execute = function(a) {
        var b = document.createElement("DIV");
        b.innerHTML = a;
        var c = function(a, b, d) {
            for (var e = 0; e < d.length; e++) {
                var f = d[e];
                if (f.nodeType != (window.Node ? Node.TEXT_NODE : 3) && -1 == a.safeTags.indexOf(f.tagName))
                    b.removeChild(f),
                    e--;
                else {
                    if (f.attributes) {
                        for (var g = [], h = 0; h < f.attributes.length; h++)
                            g[h] = f.attributes[h];
                        for (h = 0; h < g.length; h++) {
                            var l = g[h];
                            (-1 == a.safeAttrs.indexOf(l.name.toLowerCase()) || /^\s*javascript\:/i.test(l.value)) && f.removeAttribute(l.name)
                        }
                    }
                    f.childNodes && c(a, f, f.childNodes)
                }
            }
        };
        c(this, b, b.childNodes);
        return b.innerHTML
    }
    ;
    c.supported = function() {
        return m
    }
    ;
    m && (d = document.createElement("iframe"),
    d.style.cssText = "position:absolute;width:1px;height:1px;left:-9999px;top:-9999px;",
    document.body.appendChild(d),
    window.addEventListener ? (d.addEventListener("load", function() {
        p()
    }, !1),
    window.addEventListener("message", function(a) {
        t(a)
    }, !1)) : d.attachEvent && (d.attachEvent("onload", function() {
        p()
    }, !1),
    window.attachEvent("onmessage", function(a) {
        t(a)
    })),
    d.src = a + b);
    return c
}
;!function() {
    function a(a) {
        if (aa && aa.externalStorage)
            try {
                return aa.externalStorage.getItem(a) || null
            } catch (W) {
                return null
            }
        return sa ? localStorage.getItem(a) : null
    }
    function b(a, b) {
        if (aa && aa.externalStorage)
            try {
                return aa.externalStorage.setItem(a, b),
                !0
            } catch (Ea) {
                return !1
            }
        if (!sa)
            return !1;
        try {
            return localStorage.setItem(a, b),
            !0
        } catch (Ea) {
            return !1
        }
    }
    function c(a) {
        if (aa && aa.externalStorage)
            try {
                return aa.externalStorage.removeItem(a),
                !0
            } catch (W) {
                return !1
            }
        if (!sa)
            return !1;
        try {
            return localStorage.removeItem(a),
            !0
        } catch (W) {
            return !1
        }
    }
    function d(a, c, d) {
        if (void 0 === d) {
            var e = void 0;
            d = (void 0 === e && (e = new Date),
            e.setFullYear(e.getFullYear() + 20),
            e)
        }
        Ma[a] = c;
        b(a, c);
        try {
            document.cookie = a + "=" + c + "; path=/" + (d ? "; expires=" + d.toUTCString() : "") + "; Secure; SameSite=None"
        } catch (Sa) {}
    }
    function f(c, e) {
        var f = null;
        try {
            f = document.cookie.match(RegExp("(?:^|; )" + c.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"))
        } catch (fb) {}
        f = f ? decodeURIComponent(f[1]) : "";
        return f ? a(c) !== f && b(c, f) : (f = a(c) || "") ? d(c, f, e) : (f = Ma[c]) && d(c, f, e),
        Ma[c] !== f && (Ma[c] = f),
        f
    }
    function e(a) {
        !Fa && a && "null" !== a && (Fa = a,
        d("_fz_uniq", a))
    }
    function g(a) {
        Qa.push(a);
        1 >= Qa.length && setTimeout(h, 0)
    }
    function h() {
        Qa.length && Qa[0](function() {
            return Qa.splice(0, 1),
            Fa ? m() : h()
        })
    }
    function m() {
        Qa.forEach(function(a) {
            return a(function() {})
        });
        Qa.length = 0
    }
    function l(a, b) {
        return a ? b ? [a, b && p(b)].join(-1 === a.indexOf("?") ? "?" : "&") : a : ""
    }
    function p(a) {
        var b = [];
        if (a instanceof Array)
            for (var c = 0; c < a.length; ++c)
                b.push([a[c][0], encodeURIComponent(a[c][1])].join("="));
        else
            for (var d in a)
                if (a.hasOwnProperty(d))
                    if (a[d]instanceof Array)
                        for (c = 0; c < a[d].length; ++c)
                            null != a[d][c] && b.push([d, encodeURIComponent(a[d][c])].join("="));
                    else
                        null != a[d] && b.push([d, encodeURIComponent(a[d])].join("="));
        return Fa && (b.length ? b.splice(b.length - 1, 0, "fz_uniq=" + encodeURIComponent(Fa)) : b.push("fz_uniq=" + encodeURIComponent(Fa))),
        b.join("&")
    }
    function t(a, b, c) {
        if (a) {
            var d = document.createElement("script");
            d.async = !0;
            d.src = l(a, b || {});
            "function" == typeof c && (d.onload = c,
            d.onerror = c);
            (a = document.getElementsByTagName("script")[0]) && a.parentNode ? a.parentNode.insertBefore(d, a) : document.head.appendChild(d)
        }
    }
    function n(a) {
        if (Na.href = a,
        Na.href = Na.href,
        "" === Na.host) {
            var b = window.location.protocol + "//" + window.location.host;
            if ("/" === a.charAt(1))
                Na.href = b + a;
            else {
                var c = (("/" + Na.pathname).match(/.*\//) || [])[0];
                Na.href = b + c + a
            }
        }
        return Na
    }
    function r(a) {
        return !!a && (0 === a.indexOf("http://") || 0 === a.indexOf("https://")) && 0 !== a.indexOf(window.location.origin)
    }
    function q() {
        na = u();
        Za = w();
        Ua = !1
    }
    function u() {
        var a = window.document, b = window.ia_document, c = -(new Date).getTimezoneOffset(), d = a.documentURI || "", e;
        if (d) {
            var f, g, h = {}, l = n(d).search.slice(1).split("&");
            var m = 0;
            for (g = l.length; m < g; m++)
                (e = l[m]) && (1 >= (f = e.split("=")).length || (h[decodeURIComponent(f[0])] = decodeURIComponent(f[1])));
            e = h
        } else
            e = {};
        h = a.referrer || "";
        g = a.title || "";
        !h && b && b.referrer && (h = b.referrer);
        !g && b && b.title && (g = b.title);
        f = "";
        try {
            f = a.domain
        } catch (sb) {}
        a = "";
        if (e.utm_referrer)
            try {
                a = decodeURIComponent(decodeURIComponent(e.utm_referrer))
            } catch (sb) {}
        b = window.location.origin;
        m = h;
        h = n(h).host || "";
        var p, q;
        l = (l = window.screen) ? (p = l.width || 0,
        q = l.height || 0,
        p && q ? p + "x" + q : "") : "";
        p = 1.5 <= window.devicePixelRatio || !(!window.matchMedia || !window.matchMedia("(-webkit-min-device-pixel-ratio: 1.5),(min--moz-device-pixel-ratio: 1.5),(-o-min-device-pixel-ratio: 3/2),(min-resolution: 1.5dppx)").matches);
        return {
            tz_offset: c,
            origin: b,
            uri: d,
            domain: f,
            referrer: m,
            referrer_domain: h,
            title: g,
            screen_res: l,
            hdpi: p,
            utm_source: e.utm_source || "",
            utm_campaign: e.utm_campaign || "",
            utm_medium: e.utm_medium || "",
            utm_term: e.utm_term || "",
            utm_content: e.utm_content || "",
            utm_referrer: a,
            utm_affiliate: e.utm_affiliate || ""
        }
    }
    function w() {
        var a = f("_fz_fvdt");
        return a || (a = Math.floor(+new Date / 1E3).toString(),
        d("_fz_fvdt", a)),
        a
    }
    function v() {
        return {
            utm_source: na.utm_source || null,
            utm_campaign: na.utm_campaign || null,
            utm_medium: na.utm_medium || null,
            utm_term: na.utm_term || null,
            utm_content: na.utm_content || null,
            utm_referrer: na.utm_referrer || null,
            utm_affiliate: na.utm_affiliate || null
        }
    }
    function C() {
        var a = D();
        return !((new Date).getTime() > a + 18E5)
    }
    function A() {
        var a = D();
        if (a) {
            a = new Date(a);
            var b = new Date;
            return a.getUTCDate() === b.getUTCDate() && a.getUTCMonth() === b.getUTCMonth() && a.getUTCFullYear() === b.getUTCFullYear()
        }
        return !1
    }
    function D() {
        var b = a("_fz_tr") || f("_fz_tr");
        return b && parseInt(b, 10) || 0
    }
    function z(a, b, c, d) {
        return a.addEventListener ? (a.addEventListener(b, c, d),
        function() {
            a.removeEventListener(b, c, d)
        }
        ) : a.attachEvent ? (a.attachEvent("on" + b, c),
        function() {
            a.detachEvent && a.detachEvent("on" + b, c)
        }
        ) : function() {}
    }
    function E() {
        if (!Ra) {
            Ra = [];
            for (var a = 0; 6 > a; a++) {
                Ra[a] = [];
                for (var b = 0; 8 > b; b++)
                    Ra[a][b] = 0
            }
            (a = document && document.body) && z(a, "mousemove", function(a) {
                var b = Math.floor(a.clientX / Math.floor(window.innerWidth / 8));
                a = Math.floor(a.clientY / Math.floor(window.innerHeight / 6));
                b = Math.min(Math.max(b, 0), 7);
                isNaN(a = Math.min(Math.max(a, 0), 5)) || isNaN(b) || Ra[a][b]++
            }, !0)
        }
    }
    function F() {
        if (!ka) {
            ka = {};
            for (var a = function() {
                var a = bb[b];
                ka[a] = 0;
                z(document, a, function(b) {
                    b.type === a && ka[a]++
                }, !0)
            }, b = 0; b < bb.length; b++)
                a()
        }
    }
    function H(a) {
        for (var b = 0, c = 0, d = a.length; c < d; c++)
            b += a[c];
        return b / a.length
    }
    function J(a) {
        var b = H(a);
        return H(a.map(function(a) {
            return Math.pow(a - b, 2)
        }))
    }
    function G(a, b) {
        return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))
    }
    function I() {
        function a(a) {
            return a.touches ? (a = a.touches.item(0)) ? {
                x: a.clientX,
                y: a.clientY
            } : {
                x: 0,
                y: 0
            } : {
                x: a.clientX,
                y: a.clientY
            }
        }
        function b(b) {
            var f = a(b)
              , Q = b.timeStamp;
            if (n) {
                var q = a(n);
                if (0 == q.x - f.x && 0 == q.y - f.y || Q === n.timeStamp)
                    return;
                Q - n.timeStamp > h && c()
            }
            q = p * l;
            m[q] = Q;
            m[q + 1] = f.x;
            m[q + 2] = f.y;
            n = b;
            e++;
            p++;
            b.touches && d++;
            g && clearTimeout(g);
            g = window.setTimeout(c, h)
        }
        function c() {
            var a, b = 0, c = a = null;
            if (0 != m.length % l)
                throw Error("Segemnt structure error");
            var g = m.length / l;
            if (!(0 >= g)) {
                for (var h = [], Q = l; Q < m.length; Q += l) {
                    var q = m[Q - l]
                      , W = m[Q - l + 1]
                      , r = m[Q - l + 2]
                      , t = m[Q]
                      , Ea = m[Q + 1] - W
                      , u = m[Q + 2] - r
                      , v = G(Ea, u);
                    if (b += v,
                    null == f && (a = null == a ? Ea : (a + Ea) / 2,
                    c = null == c ? u : (c + u) / 2),
                    Q !== l)
                        Ea = m[Q - 2 * l],
                        W = G(W - m[Q - 2 * l + 1], r - m[Q - 2 * l + 2]) / (q - Ea),
                        h.push((v / (t - q) - W) / (t - q))
                }
                if (null == f && null != a && null != c && (S.dz = G(m[1] - a, m[2] - c)),
                null != f && 0 < f.length && .25 > d / e)
                    a = f.length - l,
                    100 < G(m[1] - f[a + 1], m[2] - f[a + 2]) && S.jz++;
                2 <= g && (a = m.length - l,
                b /= G(m[a + 1] - m[1], m[a + 2] - m[2]),
                S.op = null == S.op ? b : (b + S.op) / 2);
                1 < h.length && (h = J(h.slice(0, h.length - 1)),
                S.va = null == S.va ? h : (h + S.va) / 2)
            }
            f = m;
            m = [];
            n = null;
            S.sg++;
            p = 0
        }
        var d, e, f, g;
        if (!S) {
            S = {
                sg: 0,
                jz: 0,
                dz: null,
                op: null,
                va: null
            };
            var h = 500;
            var l = 3;
            var m = [];
            var n = f = null;
            var p = d = e = 0;
            z(window, "mousemove", b);
            z(window, "touchmove", b)
        }
    }
    function T() {
        var a = document.createElement("canvas")
          , b = null;
        try {
            b = a.getContext("webgl") || a.getContext("experimental-webgl")
        } catch (Ea) {}
        return b
    }
    function ha() {
        try {
            var a = T();
            if (!a)
                return "";
            var b = a.getExtension("WEBGL_debug_renderer_info");
            return b ? a.getParameter(b.UNMASKED_VENDOR_WEBGL) + "~" + a.getParameter(b.UNMASKED_RENDERER_WEBGL) : ""
        } catch (Ea) {
            return ""
        }
    }
    function ca() {
        if (document.currentScript)
            return document.currentScript;
        var a = document.getElementsByTagName("script");
        try {
            throw Error();
        } catch (fb) {
            var b = /(?:at [^\(]*\((.*):.+:.+\)|@(.*):.+:.+)/gi.exec(fb.stack) || [];
            b = b[1] || b[2];
            for (var c in a)
                if (b && a[c].src === b || "interactive" == a[c].readyState)
                    return a[c];
            return null
        }
    }
    function X(a) {
        return JSON.stringify(a, function(a, b) {
            return !0 === b ? 1 : !1 === b ? 0 : null != b ? b : void 0
        })
    }
    function ja(a, c) {
        void 0 === c && (c = !1);
        var e;
        var f = new Date;
        var g = Math.floor(f.getTime() / 1E3)
          , h = -1
          , l = 0;
        if (Ga)
            if (C()) {
                if (!(e = ba)) {
                    e = na.referrer_domain;
                    var m = na.domain;
                    e = "" === e || m === e
                }
                e ? A() || (l = 40) : l = 30
            } else
                l = 20;
        else
            l = 10;
        if (0 !== l) {
            var n = 9;
            void 0 === n && (n = 4);
            var p;
            e = "";
            n = n || 4;
            for (p = 0; p < n; p++)
                e += "" + (10 * Math.random() >>> 0);
            Ga = "" + g + e;
            e = g
        } else
            e = Ga && "string" == typeof Ga ? (n = Ga.length) && 9 >= n ? 0 : (p = Ga.substring(0, n - 9),
            parseInt(p, 10)) : 0;
        f = (0 < e && (h = Math.floor(g - e)),
        d("_fz_ssn", Ga, new Date(f.getTime() + 18E5)),
        ba = !0,
        {
            sr: l,
            dr: h
        });
        a.ssn = Ga;
        a.ssn_dr = f.dr;
        a.ssn_sr = f.sr;
        kb || eb || (a.ssn_sr |= 128,
        kb = !0,
        b("_fz_erru", "1"));
        0 !== f.sr && (a.ssn_start = 1);
        a.fv_date = Za;
        a.ref || (a.ref = na.uri || null);
        a.back_ref = na.referrer || null;
        a.title = na.title || null;
        a.scr_res = na.screen_res || null;
        a.hdpi = na.hdpi ? 1 : null;
        h = 99999 * Math.random() + 1 >>> 0;
        h = "" + (new Date).getTime() + h;
        a.ac = h;
        a.sv = "1661";
        a.dr = Ta ? 1 : null;
        if (c) {
            if (!(c = Va)) {
                var q = [];
                if (c = navigator.plugins)
                    for (h = 0,
                    f = c.length; h < f; h++)
                        q.push(c[h].name);
                c = navigator.vendor;
                h = screen.colorDepth;
                f = navigator.deviceMemory || -1;
                g = navigator.language;
                l = [screen.width, screen.height];
                n = [screen.availWidth, screen.availHeight];
                p = !(!window.locationbar || !window.locationbar.visible);
                e = !!navigator.clipboard;
                m = "ActiveXObject"in window;
                var Q = navigator.cpuClass
                  , r = navigator.platform;
                q = q && q.length ? q : null;
                var W = document.createElement("canvas");
                W.getContext && W.getContext("2d") ? (W = T(),
                W = !!window.WebGLRenderingContext && !!W) : W = !1;
                W = W ? ha() : null;
                var t = (new Date).getTimezoneOffset()
                  , u = navigator.userAgent
                  , v = navigator.hardwareConcurrency
                  , w = document.createElement("div");
                w.innerHTML = "&nbsp;";
                w.className = "adsbox";
                try {
                    document.body.appendChild(w);
                    var z = document.querySelector(".adsbox");
                    var B = !z || 0 === z.offsetHeight;
                    document.body.removeChild(w)
                } catch (tb) {
                    B = !1
                }
                c = {
                    vn: c,
                    cd: h,
                    dm: f,
                    ln: g,
                    rn: l,
                    ar: n,
                    ss: fa,
                    lb: p,
                    cb: e,
                    ls: sa,
                    db: Ka,
                    ax: m,
                    cp: Q,
                    pm: r,
                    rp: q,
                    wv: W,
                    to: t,
                    ua: u,
                    hc: v,
                    ab: B,
                    ts: "ontouchstart"in window || window.DocumentTouch && document instanceof window.DocumentTouch || 0 < navigator.maxTouchPoints || 0 < window.navigator.msMaxTouchPoints,
                    ps: navigator.productSub,
                    od: La,
                    dr: Ta,
                    bb: Xa,
                    bo: $a,
                    bl: db,
                    bs: mb
                }
            }
            z = c;
            Va || (Va = z);
            B = Wa;
            z = Wa({}, z);
            c = (new Date).getTime() - nb;
            h = ka ? {
                kd: ka.keydown,
                ku: ka.keyup,
                md: ka.mousedown,
                mm: ka.mousemove,
                mu: ka.mouseup,
                fc: ka.focus,
                br: ka.blur,
                ts: ka.touchstart,
                tm: ka.touchmove,
                te: ka.touchend,
                sc: ka.scroll,
                sw: ka.swipe,
                ge: ka.gesture,
                gy: ka.gyroscope,
                dm: ka.devicemotion,
                "do": ka.deviceorientation
            } : null;
            if (Ra) {
                f = Ra;
                g = [];
                l = !0;
                for (n = 0; n < f.length; n++)
                    for (p = 0; p < f[n].length; p++)
                        g.push(f[n][p]),
                        l && 0 < f[n][p] && (l = !1);
                f = l ? void 0 : g
            } else
                f = null;
            g = S ? S.sg : null;
            l = S ? S.jz : null;
            n = S ? S.dz && Math.floor(S.dz) : null;
            p = S ? S.op && parseFloat(S.op.toFixed(4)) : null;
            e = S ? S.va && parseFloat(S.va.toFixed(4)) : null;
            S ? (Q = ka,
            m = 50,
            r = Q.touchmove,
            q = Q.focus,
            W = r + Q.mousemove,
            Ta ? Q = 100 : (0 === r && 0 < W ? (m += W === S.sg ? 10 : -5,
            m += 0 < S.jz && .9 < S.jz / (S.sg - 1) ? 10 : -5,
            W > S.sg && null != S.dz && null != S.op && null != S.va && (m += 100 > S.dz ? 20 : -5,
            m += .03 > Math.abs(1 - S.op) ? 20 : -10,
            m += 0 === Math.round(1E3 * S.va) ? 20 : -10)) : (t = navigator.userAgent.toLowerCase(),
            !(0 <= t.indexOf("iphone") || t.indexOf("ipad")) && 0 < r && r > W / 3 && (m += 10 > Q.devicemotion || 10 > Q.deviceorientation ? 10 : -5)),
            Q = (m += 0 === W && 0 < q || 0 < W && .35 > W / q ? 20 : -5,
            m += Xa ? 5 : 0,
            m += $a ? 5 : 0,
            Math.min(100, Math.max(m + (db ? 5 : 0), 0)))),
            m = Q) : m = null;
            B = B(z, {
                dt: c,
                at: h,
                ms: f,
                sg: g,
                jc: l,
                dz: n,
                op: p,
                va: e,
                bd: m
            });
            a.an = X(B)
        }
        for (B = 0; B < cb.length; B++)
            if (c = cb[B])
                z = c[0],
                c = c[1],
                (h = a[z]) && (a[z] = h.substr(0, c));
        return a
    }
    function wa() {
        var a = null;
        try {
            if (window.XMLHttpRequest)
                a = new XMLHttpRequest;
            else {
                var b = window.ActiveXObject;
                if (b)
                    try {
                        a = new b("Msxml2.XMLHTTP")
                    } catch (Ea) {
                        a = new b("Microsoft.XMLHTTP")
                    }
            }
        } catch (Ea) {
            a = null
        }
        return a
    }
    function O(a, b) {
        if (a) {
            var c = wa();
            c && (c.open("get", a, !0),
            c.withCredentials = !0,
            c.onreadystatechange = function() {
                switch (c.readyState) {
                case 4:
                    b && b(c),
                    c.abort()
                }
            }
            ,
            c.send())
        }
    }
    function B(a, b) {
        var c = new Image(1,1);
        "function" == typeof b && (c.onload = b);
        c.src = a
    }
    function U(a, c, f) {
        Fa || !wa() ? B(l(a, c), f) : g(function(b) {
            var d = l(a, c);
            try {
                O(d, function(a) {
                    (a = a.getResponseHeader("X-Fz-Uniq")) && e(a);
                    b();
                    f && f()
                })
            } catch (hb) {
                B(d, function() {
                    b();
                    f && f()
                })
            }
        });
        var h = (new Date).getTime();
        b("_fz_tr", h.toString()) || d("_fz_tr", h.toString(), new Date(h + 18E5))
    }
    function Ba(a, b) {
        Fa ? t(a, b) : g(function(c) {
            t(a, b, c)
        })
    }
    function da(a, b, c, d, e, f, g) {
        if (a || c) {
            if (b || (b = "Visit"),
            "Visit" === b) {
                if (Ua)
                    return;
                Ua = !0
            }
            var h = v();
            h.event = b.split(" ").join("+");
            h.value = f || null;
            h.unit = g || null;
            h.ref = e || null;
            h.id = a || null;
            h.zid = c || null;
            U(ta + "tr", ja(h, !0), d)
        }
    }
    function ma(a, b) {
        for (var c = [], d = 0; d < a.length; d++) {
            var e = a[d];
            b(e, d, a) && c.push(e)
        }
        return c
    }
    function oa(a) {
        return !!a
    }
    function N ( a ) {
        'end'
        function b() {
            c && (clearInterval(c),
            c = null);
            d || (a(),
            d = !0)
        }
        var c, d = !1;
        if (z(document, "DOMContentLoaded", b, !1),
        z(window, "load", b, !1),
        window.attachEvent) {
            var e = document.createElement("div")
              , f = !1;
            try {
                f = null === window.frameElement
            } catch (hb) {}
            e.doScroll && f && "external"in window && (c = window.setInterval(function() {
                try {
                    e && e.doScroll && e.doScroll(),
                    b()
                } catch (hb) {}
            }, 30))
        }
        "complete" === document.readyState && b()
    }
    function M() {
        null != this._id && (U(ta + "wh", ja({
            id: this._id,
            a: this._advert_id,
            uid: this._uid
        })),
        this._width && (this.width = this._width + "",
        this.style.width = this._width + "px"),
        this._height && (this.height = this._height + ""))
    }
    function xa(a) {
        for (var b = 0; b < P.length; b++) {
            var c = P[b];
            if (a.source === c.contentWindow) {
                var e = JSON.parse(a.data);
                if (e) {
                    if (0 <= e.height && (P[b].height = e.height),
                    e.setCookie)
                        try {
                            document.cookie = e.setCookie
                        } catch (ob) {}
                    if (e.getCookie) {
                        var g = window.JSON.stringify({
                            getCookie: f(e.getCookie)
                        });
                        c && c.contentWindow && c.contentWindow.postMessage(g, ta)
                    }
                    e.click && (c = c && c.parentNode) && (c = c.getAttribute("id")) && (g = Ja[c]) && (g = g.data) && window.open(l(ta + "go", ja({
                        link: g.link,
                        a: g.a,
                        s: g.s,
                        v: g.v,
                        host: ta,
                        id: c,
                        uid: g.uid
                    })), "_blank");
                    if (e.hide) {
                        e = P[b];
                        c = null != e._id ? e._hi : null;
                        if (!c)
                            break;
                        g = Math.floor(Date.now() / 1E3);
                        e.height = "0";
                        d(c, g + "")
                    }
                }
            }
        }
    }
    function ia() {
        window.parent && window.parent.postMessage && window.parent.postMessage('{"height":' + document.body.offsetHeight + "}", "*")
    }
    function Z() {
        z(window, "resize", ia, !1);
        N(ia)
    }
    function la(a, b) {
        var c, d, e;
        if (b) {
            if (!(c = b.img)) {
                (c = b.img = new Image).onload = M;
                c.src = ta + "si/" + b.name;
                c.height && (c.height = b.h);
                b.w && (c.width = b.w,
                c.style.width = b.w + "px");
                var f = c;
                f._id = b.zone;
                f._advert_id = b.a;
                f._uid = b.uid;
                f._width = b.w || null;
                f._height = b.h || null
            }
            a && ((d = document.createElement("a")).target = "_blank",
            d.href = l(ta + "go", ja({
                link: b.link,
                a: b.a,
                s: b.s,
                v: b.v,
                host: ta,
                id: b.zone,
                uid: b.uid
            })),
            (e = c.style).overflow = "hidden",
            e.verticalAlign = "top",
            e.height = "auto",
            e.border = "0",
            d.appendChild(c),
            a.appendChild(d),
            b.tl && U(b.tl, {}))
        }
    }
    function Y(a) {
        if (a && "string" == typeof a) {
            var b;
            (b = Ja[a]) || (b = (Ja[a] = {},
            Ja[a]));
            a = b
        } else
            a = null;
        return a
    }
    function Oa() {
        qa()
    }
    function Ha(a) {
        if (!a || "string" != typeof a)
            return null;
        da(void 0, void 0, a);
        var b = Y(a);
        return b ? (b.readyToShow = !0,
        b.data ? (ua(a),
        b) : (qa(),
        b)) : b
    }
    function Pa(a) {
        var b, c, d, f;
        if (a && a.b)
            for ((d = a.fz_uniq) && e(d),
            d = 0,
            f = (a = a.b).length; d < f; d++)
                (b = a[d]) && (c = Ja[b.zone]) && (c.data = b,
                c.dataLoading = !1,
                c.readyToShow ? ua(b.zone) : !(248 & b.format) && la(null, b))
    }
    function ua(a) {
        if (a) {
            var b = Ja[a];
            b = b && b.data || {};
            a = document.getElementById(a);
            if (b && a)
                if (248 & b.format) {
                    var c, d;
                    if (a && b && !(b.hi && (c = f(b.hi)) && (d = parseInt(c, 10) || 0,
                    Math.floor((new Date).getTime() / 1E3) < d + 604800))) {
                        d = c = document.createElement("iframe");
                        P.length || z(window, "message", xa, !1);
                        P.push(d);
                        c.width = (0 < b.w ? b.w : "100%") + "";
                        c.height = (b.h || 0) + "";
                        128 & b.format && (c.height = "100%");
                        64 & b.format && (c.height = "71px");
                        var e = {
                            link: b.link,
                            a: b.a,
                            s: b.s,
                            v: b.v + "",
                            w: c.width,
                            h: c.height,
                            r: window.location + "",
                            host: ta,
                            id: b.zone,
                            uid: b.uid,
                            custom: a.getAttribute("data") || ""
                        };
                        c.frameBorder = "0";
                        c.setAttribute("style", "vertical-align:top;max-width:100% !important");
                        c.scrolling = "no";
                        c.onload = M;
                        if (c._id = b.zone,
                        c._hi = b.hi,
                        c._advert_id = b.a,
                        c._uid = b.uid,
                        b.ext_urls)
                            for (d = 0; d < b.ext_urls.length; d++)
                                e["link_" + d] = b.ext_urls[d].link,
                                e["s_" + d] = b.ext_urls[d].s;
                        c.src = 32 & b.format ? b.iframe || "" : l(ta + "sh/" + b.name + "/", e);
                        c.style.display = "block";
                        a.appendChild(c);
                        b.tl && U(b.tl, {})
                    }
                } else
                    248 & b.format || la(a, b)
        }
    }
    function qa() {
        if (!Ta || lb) {
            var a, b, c, d = [], e = [], f = [], g = [];
            for (b in Ja)
                Ja.hasOwnProperty(b) && ((a = Ja[b]).data || a.dataLoading || (a.dataLoading = !0,
                d.push(b),
                (c = document.getElementById(b)) && (e.push(encodeURIComponent(c.getAttribute("data-utm-source") || "")),
                f.push(encodeURIComponent(c.getAttribute("data-utm-campaign") || "")),
                g.push(encodeURIComponent(c.getAttribute("data-server") || "")))));
            d.length && (a = {
                tz_offset: na.tz_offset,
                ids: d.join(","),
                utm_source: ma(e, oa).join(",") || null,
                utm_campaign: ma(f, oa).join(",") || null,
                server: ma(g, oa).join(",") || null
            },
            Ba(ta + "rq", ja(a)))
        }
    }
    function Ca(a, b, c, d) {
        if (a && b && c) {
            c = {
                id: c.id,
                name: c.name,
                brn: c.brand,
                cat: c.category,
                "var": c.variant,
                qnt: c.quantity,
                prc: c.price,
                val: c.value,
                ls_name: c.list_name,
                ls_pos: c.list_position,
                crt: c.cart_id,
                crt_items: c.cart_items,
                cur: c.currency,
                trn: c.transaction_id,
                afl: c.affiliation,
                dtl: c.detail,
                ch_stp: c.checkout_step,
                ch_opt: c.checkout_option,
                tx_val: c.tax_value,
                tx_name: c.tax_name,
                tx_type: c.tax_type,
                tx_inv: c.tax_invoice,
                ds_val: c.discount_value,
                ds_name: c.discount_name,
                ds_type: c.discount_type,
                ds_inv: c.discount_invoice,
                cp_val: c.coupon_value,
                cp_name: c.coupon_name,
                cp_type: c.coupon_type,
                cp_inv: c.coupon_invoice,
                sh_val: c.shipping_value,
                sh_name: c.shipping_name,
                sh_type: c.shipping_type,
                sh_inv: c.shipping_invoice,
                items: c.items && c.items.map(function(a) {
                    return {
                        id: a.id,
                        name: a.name,
                        brn: a.brand,
                        cat: a.category,
                        "var": a.variant,
                        qnt: a.quantity,
                        prc: a.price,
                        val: a.value,
                        ls_name: a.list_name,
                        ls_pos: a.list_position,
                        crt: a.cart_id,
                        crt_items: a.cart_items,
                        cur: a.currency,
                        trn: a.transaction_id,
                        afl: a.affiliation,
                        dtl: a.detail,
                        ch_stp: a.checkout_step,
                        ch_opt: a.checkout_option,
                        tx_val: a.tax_value,
                        tx_name: a.tax_name,
                        tx_type: a.tax_type,
                        tx_inv: a.tax_invoice,
                        ds_val: a.discount_value,
                        ds_name: a.discount_name,
                        ds_type: a.discount_type,
                        ds_inv: a.discount_invoice,
                        cp_val: a.coupon_value,
                        cp_name: a.coupon_name,
                        cp_type: a.coupon_type,
                        cp_inv: a.coupon_invoice,
                        sh_val: a.shipping_value,
                        sh_name: a.shipping_name,
                        sh_type: a.shipping_type,
                        sh_inv: a.shipping_invoice
                    }
                })
            };
            var e = wa();
            e && window.FormData && (e.open("GET", l(ta + "ec", ja({
                id: a,
                event: b,
                ec: JSON.stringify(c)
            }))),
            e.onreadystatechange = function() {
                d && 4 === e.readyState && d()
            }
            ,
            e.send())
        }
    }
    function ea(a, b, c) {
        function d() {
            clearTimeout(g);
            e || (c && c(),
            e = !0)
        }
        var e = !1;
        if (a && b) {
            var f = v();
            f.id = a;
            f.next_ref = b;
            U(ta + "tr", ja(f), d);
            var g = window.setTimeout(d, 500)
        }
    }
    function ya(a, b, c) {
        if (a) {
            var d = wa();
            if (d)
                try {
                    d.open("post", a, !0),
                    d.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"),
                    d.withCredentials = !0,
                    d.onreadystatechange = function() {
                        switch (d.readyState) {
                        case 4:
                            d.abort(),
                            c && c()
                        }
                    }
                    ,
                    d.send(p(b || {}))
                } catch (Sa) {}
        }
    }
    function L(a) {
        if (a) {
            var b = window.location
              , c = b.href;
            ya(b.protocol + "//" + b.host + "/go" + c.substr(c.lastIndexOf("?")));
            window.open(a)
        }
    }
    function Da() {
        z(window, "hashchange", function() {
            q();
            aa._websiteId && da(aa._websiteId)
        })
    }
    function R() {
        var a = document && document.body;
        a && z(a, "click", function(a) {
            var b = a.target || a.srcElement;
            if (aa._websiteId) {
                for (; null != b && 1 === b.nodeType && "a" !== b.tagName.toLowerCase(); )
                    b = b.parentNode;
                if (null != b && 1 === b.nodeType) {
                    var c = b.getAttribute("target")
                      , d = b.getAttribute("href");
                    if (d && r(d)) {
                        if (!c || "_self" === c || "_parent" === c || "_top" === c)
                            return a.preventDefault && a.preventDefault(),
                            ea(aa._websiteId, d, function() {
                                c && "_self" !== c ? "_top" !== c ? "_parent" !== c || (window.parent.location.href = d) : window.top.location.href = d : window.location.href = d
                            });
                        ea(aa._websiteId, d)
                    }
                }
            }
        })
    }
    function za() {
        a: {
            try {
                var a = !(!window.performance || "function" != typeof window.performance.getEntriesByType);
                break a
            } catch (Sa) {}
            a = !1
        }
        if (a) {
            var b = {
                err: Ya
            };
            (a = performance.getEntriesByType("navigation")[0]) && a.domComplete && (b.p_dns_s = a.domainLookupStart,
            b.p_dns_e = a.domainLookupEnd,
            b.p_tcp_s = a.connectStart,
            b.p_tcp_e = a.connectEnd,
            b.p_req = a.requestStart,
            b.p_res_s = a.responseStart,
            b.p_res_e = a.responseEnd,
            b.p_di = a.domInteractive,
            b.p_dcl_s = a.domContentLoadedEventStart,
            b.p_dcl_e = a.domContentLoadedEventEnd,
            b.p_dc = a.domComplete);
            var c = {};
            performance.getEntriesByType("resource").forEach(function(a) {
                var b = a.name.split(".");
                b = b[b.length - 1];
                for (var d in ib)
                    if (-1 !== ib[d].indexOf(b)) {
                        var e = d;
                        break
                    }
                e || (e = pb[a.initiatorType] || "other");
                c[e] || (c[e] = {
                    start: [],
                    end: []
                });
                c[e].start.push(a.startTime);
                c[e].end.push(a.responseEnd)
            }, {});
            Object.keys(c).forEach(function(a) {
                b["r_" + a + "_s"] = Math.min.apply(Math, c[a].start);
                b["r_" + a + "_e"] = Math.max.apply(Math, c[a].end)
            });
            performance.getEntriesByType("paint").forEach(function(a) {
                b["d_" + (qb.paint[a.name] || a.name)] = a.startTime
            });
            var d = {};
            return Object.keys(b).forEach(function(a) {
                var c = Math.round(b[a]);
                d[a] = isNaN(c) ? null : c
            }, {}),
            d
        }
    }
    function Ia(a) {
        var b = 0
          , c = 0
          , d = !1
          , e = setInterval(function() {
            if (ab && (b += 1),
            b > c && 0 == b % 15) {
                if (c = b,
                !C() || !A())
                    return void clearInterval(e);
                var f = {};
                if (!d) {
                    var g = za();
                    g && (f = g,
                    d = !0)
                }
                if (f.id = aa._websiteId,
                f.event = "Page+Active+Timer+" + b,
                f.pg_act = b,
                U(ta + "st", ja(f, !0)),
                !a)
                    return void clearInterval(e)
            }
            120 <= b && clearInterval(e)
        }, 1E3)
    }
    function Aa(a, b, c, d, e) {
        var f = aa._websiteId;
        "object" == typeof a && (a.websiteId && (f = a.websiteId),
        b = a.callback,
        c = a.referer,
        d = a.value,
        e = a.unit,
        a = a.name);
        f && da(f, a, void 0, b, c, d, e)
    }
    function ra(a, b) {
        a = a[(b = [].slice.call(b))[0]];
        "function" == typeof a ? a.apply(null, b.slice(1)) : "object" == typeof a && ra(a, b.slice(1))
    }
    function pa() {
        var a = document && document.body;
        a && z(a, "click", function(a) {
            if (aa._websiteId) {
                var b;
                for (a = a.target || a.srcElement; null != a && 1 === a.nodeType && (b = a.tagName.toLowerCase()) && "a" !== b && "button" !== b && ("input" !== b || "submit" !== a.getAttribute("type")); )
                    a = a.parentNode;
                if (null != a && 1 === a.nodeType) {
                    var c = a.getAttribute("data-fz-event") || a.getAttribute("data-vars-fz") || a.getAttribute("data-finteza-event");
                    if (c) {
                        b = a.getAttribute("data-fz-value") || void 0;
                        var d = a.getAttribute("data-fz-unit") || void 0
                          , e = a;
                        a.removeAttribute("data-finteza-event");
                        a.removeAttribute("data-vars-fz-event");
                        a.removeAttribute("data-fz-event");
                        da(aa._websiteId, c, void 0, function() {
                            e.setAttribute("data-vars-fz", c)
                        }, void 0, b, d)
                    }
                }
            }
        }, !0)
    }
    var va = "https://" + ((0 < window.location.hostname.indexOf(".src") || 0 < window.location.hostname.indexOf(".dev") ? "b.mql5." : "content.mql5.") + "com") + "/";
    try {
        var fa = !!window.sessionStorage
    } catch (Q) {
        fa = !1
    }
    try {
        var sa = !!window.localStorage
    } catch (Q) {
        sa = !1
    }
    try {
        var Ka = !!window.indexedDB
    } catch (Q) {
        Ka = !1
    }
    try {
        var La = !!window.openDatabase
    } catch (Q) {
        La = !1
    }
    var Ma = {}, Fa = function() {
        var a = f("_fz_uniq");
        a && isNaN(a) && (delete Ma._fz_uniq,
        c("_fz_uniq"),
        d("_fz_uniq", "", new Date(1E3)),
        a = null);
        return a
    }(), Qa = [], Na = document.createElement("a"), na = u(), Za = w(), Ua = !1, Ga = f("_fz_ssn"), ba = !1, Wa = function() {
        return (Wa = Object.assign || function(a) {
            for (var b, c = 1, d = arguments.length; c < d; c++)
                for (var e in b = arguments[c])
                    ({}).hasOwnProperty.call(b, e) && (a[e] = b[e]);
            return a
        }
        ).apply(this, arguments)
    }, Ra, ka, Ta = !!(/HeadlessChrome/.test(navigator.userAgent) || !0 === navigator.webdriver || document.documentElement.getAttribute("webdriver") || window.callPhantom || window._phantom || navigator.languages && !navigator.languages.length), Xa = function() {
        var a, b = navigator.userAgent.toLowerCase(), c = navigator.productSub;
        if (("Chrome" === (a = 0 <= b.indexOf("firefox") ? "Firefox" : 0 <= b.indexOf("opera") || 0 <= b.indexOf("opr") ? "Opera" : 0 <= b.indexOf("chrome") ? "Chrome" : 0 <= b.indexOf("safari") ? "Safari" : 0 <= b.indexOf("trident") ? "Internet Explorer" : "Other") || "Safari" === a || "Opera" === a) && "20030107" !== c)
            return !0;
        b = ("" + eval).length;
        if (37 === b && "Safari" !== a && "Firefox" !== a && "Other" !== a || 39 === b && "Internet Explorer" !== a && "Other" !== a || 33 === b && "Chrome" !== a && "Opera" !== a && "Other" !== a)
            return !0;
        try {
            throw "a";
        } catch (Sa) {
            try {
                Sa.toSource();
                var d = !0
            } catch (ob) {
                d = !1
            }
        }
        return d && "Firefox" !== a && "Other" !== a
    }(), db = function() {
        if ("undefined" != typeof navigator.languages)
            try {
                if (navigator.languages[0].substr(0, 2) !== navigator.language.substr(0, 2))
                    return !0
            } catch (Q) {
                return !0
            }
        return !1
    }(), mb = Math.min(screen.width, screen.height) < Math.min(screen.availWidth, screen.availHeight) || Math.max(screen.width, screen.height) < Math.max(screen.availWidth, screen.availHeight), $a = function() {
        var a, b = navigator.userAgent.toLowerCase(), c = navigator.platform.toLowerCase(), d = navigator.oscpu;
        return (a = 0 <= b.indexOf("windows phone") ? "Windows Phone" : 0 <= b.indexOf("win") ? "Windows" : 0 <= b.indexOf("android") ? "Android" : 0 <= b.indexOf("linux") || 0 <= b.indexOf("cros") ? "Linux" : 0 <= b.indexOf("iphone") || 0 <= b.indexOf("ipad") ? "iOS" : 0 <= b.indexOf("mac") ? "Mac" : "Other",
        ("ontouchstart"in window || 0 < navigator.maxTouchPoints || 0 < navigator.msMaxTouchPoints) && "Windows Phone" !== a && "Android" !== a && "iOS" !== a && "Other" !== a) || void 0 !== d && (0 <= (d = d.toLowerCase()).indexOf("win") && "Windows" !== a && "Windows Phone" !== a || 0 <= d.indexOf("linux") && "Linux" !== a && "Android" !== a || 0 <= d.indexOf("mac") && "Mac" !== a && "iOS" !== a || (-1 === d.indexOf("win") && -1 === d.indexOf("linux") && -1 === d.indexOf("mac")) != ("Other" === a)) ? !0 : 0 <= c.indexOf("win") && "Windows" !== a && "Windows Phone" !== a || (0 <= c.indexOf("linux") || 0 <= c.indexOf("android") || 0 <= c.indexOf("pike")) && "Linux" !== a && "Android" !== a || (0 <= c.indexOf("mac") || 0 <= c.indexOf("ipad") || 0 <= c.indexOf("ipod") || 0 <= c.indexOf("iphone")) && "Mac" !== a && "iOS" !== a || (0 > c.indexOf("win") && 0 > c.indexOf("linux") && 0 > c.indexOf("mac") && 0 > c.indexOf("iphone") && 0 > c.indexOf("ipad")) !== ("Other" === a) || "undefined" == typeof navigator.plugins && "Windows" !== a && "Windows Phone" !== a
    }(), S, bb = "keydown keyup mousedown mousemove mouseup focus blur touchstart touchmove touchend scroll swipe gesture gyroscope devicemotion deviceorientation".split(" "), Va = null, nb = (new Date).getTime(), eb = !1, ta = function() {
        var a = ca();
        a = a ? a.getAttribute("src") : "";
        return (a ? "https://" + n(a).host + "/" : "") === va && (eb = !0),
        va
    }(), cb = [["id", 64], ["event", 512], ["title", 256], ["ref", 512], ["back_ref", 512]], kb = !!a("_fz_erru"), lb = !1, Ja = {}, P = [], aa = function(a) {
        return a.sv = "1661",
        a.act = a.act || [],
        a.q = a.q || [],
        a._websiteId = null,
        a.loaded = !0,
        a.inited = !0,
        a.register = Y,
        a.finish = Oa,
        a.show = Ha,
        a.lead = da,
        a.leave = ea,
        a.ecommerce = Ca,
        a.response = Pa,
        a.openlink = L,
        a.routePage = q,
        a.autoHeight = Z,
        a
    }(function() {
        if (window.fcoreobj)
            return window.fcoreobj;
        var a = window.FintezaCoreObject;
        if (a && (window.fcoreobj = window[a],
        window.FintezaCoreObject = null,
        window.fcoreobj))
            return window.fcoreobj;
        a = {};
        return window.fcoreobj = a,
        a
    }()), ab = !0, jb = ["hidden", "msHidden", "webkitHidden"], rb = ["visibilitychange", "msvisibilitychange", "webkitvisibilitychange"], ib = {
        style: ["css"],
        font: ["eot", "woff", "woff2", "ttf"],
        img: ["jpeg", "jpg", "gif", "png", "svg"]
    }, pb = {
        xmlhttprequest: "ajax",
        fetch: "ajax",
        link: "style",
        img: "img",
        script: "script",
        iframe: "iframe"
    }, qb = {
        paint: {
            "first-paint": "fp",
            "first-contentful-paint": "fcp"
        }
    }, Ya = 0, gb = {
        config: function(a) {
            void 0 === a && (a = {});
            if (aa.externalStorage !== a.externalStorage) {
                aa.externalStorage = a.externalStorage;
                for (var c in Ma)
                    b(c, Ma[c])
            }
        },
        register: {
            website: function(a, b, c, d, e, f, g, h) {
                "object" == typeof a && (b = !1 !== a.sendVisit,
                c = a.trackHash,
                d = a.trackLinks,
                e = !1 !== a.accurateBounceRate,
                f = a.timeOnPage,
                g = a.trackMouseMove,
                h = a.trackActivity,
                a = a.id || a.websiteId);
                aa._websiteId = a;
                !1 !== b && da(a);
                !0 === c && Da();
                !0 === d && R();
                !1 === e && !0 !== f || Ia(!0 === f);
                !1 !== g && E();
                !1 !== h && F();
                I()
            },
            zone: function(a) {
                var b;
                if ("[object Array]" === {}.toString.call(a)) {
                    var c = 0;
                    for (b = a.length; c < b; c++)
                        Y(a[c])
                } else
                    Y(a);
                qa()
            }
        },
        route: {
            page: function(a) {
                q();
                !1 !== a && aa._websiteId && da(aa._websiteId)
            }
        },
        event: Aa,
        track: Aa,
        show: function(a) {
            Ha(a)
        },
        autoheight: function() {
            Z()
        },
        ecommerce: function(a) {
            var b = a.callback, c = a.event, d = ["callback", "event"], e = {}, f;
            for (f in a)
                ({}).hasOwnProperty.call(a, f) && 0 > d.indexOf(f) && (e[f] = a[f]);
            if (null != a && "function" == typeof Object.getOwnPropertySymbols) {
                var g = 0;
                for (f = Object.getOwnPropertySymbols(a); g < f.length; g++)
                    0 > d.indexOf(f[g]) && {}.propertyIsEnumerable.call(a, f[g]) && (e[f[g]] = a[f[g]])
            }
            aa._websiteId && Ca(aa._websiteId, c, e, b)
        },
        testMode: function(a) {
            lb = !0 === a
        }
    };
    (function(a, b) {
        window.onerror = function(b, c, d, e, f) {
            return Ya += 1,
            !!a && a(b, c, d, e, f)
        }
        ;
        window.onunhandledrejection = function(a) {
            return Ya += 1,
            !!b && b(a)
        }
    }
    )();
    (function() {
        var a, b = aa.act;
        for (a = b.shift(); void 0 !== a; a = b.shift())
            a && "function" == typeof a && a();
        aa.act.push = function(a) {
            a()
        }
    }
    )();
    (function() {
        for (var a = aa.q, b = a.shift(); void 0 !== b; b = a.shift())
            b && b.length && ra.call(null, gb, b);
        aa.q.push = function(a) {
            ra.call(null, gb, a)
        }
    }
    )();
    (function() {
        for (var a = function(a) {
            var b = jb[a];
            if ("undefined" == typeof document[b])
                return "continue";
            z(document, rb[a], function() {
                ab = !document[b]
            }, !1)
        }, b = 0; b < jb.length; b++)
            a(b)
    }
    )();
    N(function() {
        pa()
    })
}();
mqGlobal.AddOnReady(function() {
    HeadSideBarMenu();
    window.LangMenu = new LangMenu;
    window.UserLinkMenu = new UserLinkMenu
});
window.emersionTooltip = function() {
    function a(a) {
        var b = document.createElement("div");
        b.className = "emersion-tooltip";
        var c = document.createElement("div");
        c.className = "emersion-tooltip__content";
        b.appendChild(c);
        c.innerHTML = a;
        return b
    }
    function b(a, b) {
        a = a.getBoundingClientRect();
        b.style.top = a.bottom + 10 + "px";
        b.style.left = a.left + "px"
    }
    return {
        init: function(c, d) {
            function f() {
                b(c, h)
            }
            function e(a) {
                a = a || window.event;
                for (a = a.target || window.srcElement; a && a.parentNode && a !== c; )
                    a = a.parentNode;
                a !== c && g(h)
            }
            function g() {
                h.parentNode && (h.className = "emersion-tooltip",
                setTimeout(function() {
                    h.parentNode.removeChild(h)
                }, 300),
                window.removeEventListener("scroll", f),
                document.body.removeEventListener("click", e))
            }
            c = document.getElementById(c);
            if (!c)
                return !1;
            c.style.position = "relative";
            var h = a(d);
            c.appendChild(h);
            b(c, h);
            document.body.addEventListener("click", e);
            window.addEventListener("scroll", f, {
                passive: !0
            });
            setTimeout(function() {
                h.className += " emersion-tooltip_showed"
            }, 200);
            return {
                hide: g
            }
        }
    }
}();